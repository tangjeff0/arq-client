#!/bin/sh

echo "API_HOST='"$API_HOST"'" > .env
echo "API_URL='"$API_URL"'" >> .env
echo "API_VERSION='"$API_VERSION"'" >> .env
echo "CLIENT_BASE_URL='"$CLIENT_BASE_URL"'" >> .env
echo "ES_HOST='"$ES_HOST"'" >> .env
echo "NODE_ENV='"$NODE_ENV"'" >> .env
echo "ENV='"$ENV"'" >> .env
echo "PITCHBOOK_URL='"$PITCHBOOK_URL"'" >> .env
echo "SALESFORCE_ENABLED='"$SALESFORCE_ENABLED"'" >> .env
echo "SALESFORCE_URL='"$SALESFORCE_URL"'" >> .env
echo "SOURCE_ARQ_CODE='"$SOURCE_ARQ_CODE"'" >> .env
echo "SOURCE_ARQ_NAME='"$SOURCE_ARQ_NAME"'" >> .env
echo "ZENDESK_ARQ_SECTION='"$ZENDESK_ARQ_SECTION"'" >> .env
echo "ZENDESK_ENABLED='"$ZENDESK_ENABLED"'" >> .env
echo "ZENDESK_HOST='"$ZENDESK_HOST"'" >> .env
echo "ZENDESK_SEARCH_TERMS_ARTICLE='"$ZENDESK_SEARCH_TERMS_ARTICLE"'" >> .env
echo "TECH_TAGS_ARCHITECTURE_ID='"$TECH_TAGS_ARCHITECTURE_ID"'" >> .env
