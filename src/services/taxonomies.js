import client from 'apollo';
import { GET_ALL_TAXONOMIES } from 'queries/taxonomies';

export function getTaxonomyCSV() {
  return client.watchQuery({
    query: GET_ALL_TAXONOMIES,
    fetchPolicy: 'network-only',
  });
}

export default getTaxonomyCSV;
