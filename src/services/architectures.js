import client from 'apollo';
import { now } from 'utils/time';
import { ARCHITECTURE_DRAFT, ARCHITECTURE_SUBMITTED } from 'constants/architectureStates';
import {
  UPDATE_ARCHITECTURE,
  UPDATE_ARCHITECTURE_STATUS_DRAFT,
  UPDATE_ARCHITECTURE_STATUS_SUBMITTED,
} from 'queries/architectures';

// @note: user (non admin) facing route
export function patchArchitecture(id, status) {
  const timestamp = now();
  const variables = { id };
  let mutation;

  if (status === ARCHITECTURE_DRAFT) {
    variables.draftAt = timestamp;
    mutation = UPDATE_ARCHITECTURE_STATUS_DRAFT;
  } else if (status === ARCHITECTURE_SUBMITTED) {
    variables.submittedAt = timestamp;
    mutation = UPDATE_ARCHITECTURE_STATUS_SUBMITTED;
  }

  return client.mutate({ mutation, variables })
    .then(response => response.data);
}

export function updateArchitecture(documentQID, architecture) {
  const { id, title } = architecture;

  return client.mutate({
    mutation: UPDATE_ARCHITECTURE,
    variables: {
      id,
      title,
    },
  })
    .then(response => response.data);
}
