import client from 'elastic/connection';

export function searchTaxonomies(contains) {
  return client.search({
    index: 'taxonomies',
    body: {
      size: 90,
      query: {
        wildcard: {
          'term.raw': `*${contains}*`,
        },
      },
    },
  })
    .then(response => response.hits.hits);
}

export default searchTaxonomies;
