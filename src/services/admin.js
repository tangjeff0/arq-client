import client from 'apollo';
import { now } from 'utils/time';
import {
  GET_ADMIN_ARCHITECTURE,
  PUBLISH_ARCHITECTURE,
  ACCEPT_NEW_TERMS,
  UPDATE_ARCHITECTURE_STATUS_IN_REVIEW,
  UPDATE_ARCHITECTURE_STATUS_REJECTED,
} from 'queries/admin';

export function getAdminArchitecture(id) {
  return client.query({
    query: GET_ADMIN_ARCHITECTURE,
    fetchPolicy: 'network-only',
    variables: { id },
  })
    .then(response => response.data);
}

export function publishArchitecture(id) {
  return client.mutate({
    mutation: PUBLISH_ARCHITECTURE,
    variables: { object: { id } },
  })
    .then(response => response.data);
}

export function patchAdminArchitectureInReview(id) {
  const inReviewAt = now();

  return client.mutate({
    mutation: UPDATE_ARCHITECTURE_STATUS_IN_REVIEW,
    variables: {
      id,
      inReviewAt,
    },
  })
    .then(response => response.data);
}

export function patchAdminArchitectureRejected(id) {
  const rejectedAt = now();

  return client.mutate({
    mutation: UPDATE_ARCHITECTURE_STATUS_REJECTED,
    variables: {
      id,
      rejectedAt,
    },
  })
    .then(response => response.data);
}

export function submitApprovedTerms(linkTerms, insertTerms) {
  return client.mutate({
    mutation: ACCEPT_NEW_TERMS,
    variables: {
      linkTerms,
      insertTerms,
    },
  })
    .then(response => response.data);
}
