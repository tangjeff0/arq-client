import { isAdminLocalDev } from 'utils/user';

// local dev only
export function toggleUser(user) {
  localStorage.setItem('X-Hasura-Remote-User', user.email);
  localStorage.setItem('X-Hasura-Role', isAdminLocalDev(user.email) ? 'api-admin' : 'api-user');
}

export default toggleUser;
