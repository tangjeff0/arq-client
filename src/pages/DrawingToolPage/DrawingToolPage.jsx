import React from 'react';
import PropTypes from 'prop-types';
import { matchPropType } from 'proptypes';
import DrawingToolInterface from 'components/DrawingTool/DrawingToolInterface.jsx';

const DrawingToolPage = ({
  match,
  shared,
  published,
}) => (
  <DrawingToolInterface
    documentID={match.params.documentQID}
    architectureID={match.params.architectureQID}
    shared={shared}
    published={published}
  />
);

DrawingToolPage.propTypes = {
  match: matchPropType.isRequired,
  shared: PropTypes.bool,
  published: PropTypes.bool,
};

DrawingToolPage.defaultProps = {
  shared: false,
  published: false,
};

export default DrawingToolPage;
