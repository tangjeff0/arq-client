import React from 'react';
import ProblemSearch from 'components/ProblemSearch/ProblemSearch.jsx';

const ProblemSearchPage = () => <ProblemSearch />;

ProblemSearchPage.propTypes = {};

ProblemSearchPage.defaultProps = {};

export default ProblemSearchPage;
