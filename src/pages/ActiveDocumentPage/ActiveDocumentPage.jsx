import React from 'react';
import { matchPropType } from 'proptypes';
import DocumentInterface from 'components/Document/DocumentInterface.jsx';

const ActiveDocumentPage = ({ match }) => <DocumentInterface id={match.params.id} />;

ActiveDocumentPage.propTypes = {
  match: matchPropType.isRequired,
};

ActiveDocumentPage.defaultProps = {};

export default ActiveDocumentPage;
