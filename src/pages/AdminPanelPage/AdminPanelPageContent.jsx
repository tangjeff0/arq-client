import React from 'react';
import PropTyes from 'prop-types';
import { nicetime } from 'utils/time';
import { ARCHITECTURE_REJECTED } from 'constants/architectureStates.js';
import Button from 'components/Button/Button.jsx';
import Heading from 'components/Heading/Heading.jsx';
import Table from 'components/Table/Table.jsx';
import Loader from 'components/Loader/Loader.jsx';
import useAdminPanel from 'pages/AdminPanelPage/useAdminPanel.jsx';

const AdminPanelContent = ({ includeRejected }) => {
  const { loading, error, adminArchitectures } = useAdminPanel(includeRejected);

  if (loading) {
    return <Loader size={36} isCentered isHeight100 />;
  } if (error) {
    return null; // @todo: error state
  }
  return (
    <div className="AdminPanel-content">
      <Heading level={2}>
        {adminArchitectures.length > 0
          ? 'Architectures For Review'
          : 'No Architectures For Review'}
      </Heading>
      {adminArchitectures.length > 0 && (
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.Cell header>Submitted By</Table.Cell>
            <Table.Cell header>Architecture Name</Table.Cell>
            <Table.Cell header>New Terms</Table.Cell>
            <Table.Cell header>Date</Table.Cell>
            <Table.Cell header />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {adminArchitectures.map((architecture) => {
            const newTerms = architecture.allPackages.filter(p => !p.taxonomy);

            return (
              <Table.Row key={architecture.id}>
                <Table.Cell>{architecture.author.email}</Table.Cell>
                <Table.Cell>{architecture.title}</Table.Cell>
                <Table.Cell>{newTerms.length}</Table.Cell>
                <Table.Cell>
                  {nicetime(architecture.submittedAt)}
                </Table.Cell>
                <Table.Cell>
                  {architecture.status === ARCHITECTURE_REJECTED ? (
                    'Rejected'
                  ) : (
                    <Button
                      appearance="secondary"
                      asLink
                      linkTo={`/admin/architectures/${architecture.id}`}
                    >
                      Review
                    </Button>
                  )}
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
      )}
    </div>
  );
};

AdminPanelContent.propTypes = {
  includeRejected: PropTyes.bool.isRequired,
};

export default AdminPanelContent;
