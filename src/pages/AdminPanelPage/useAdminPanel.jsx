import { useQuery } from '@apollo/client';
import { get } from 'lodash';
import useAdminCheck from 'hooks/useAdminCheck.jsx';
import { GET_ADMIN_ARCHITECTURES } from 'queries/admin';
import {
  ARCHITECTURE_SUBMITTED,
  ARCHITECTURE_IN_REVIEW,
  ARCHITECTURE_REJECTED,
} from 'constants/architectureStates.js';
import { ARCHITECTURE_STATUSES_TYPENAME } from 'constants/gql';

const useAdminPanel = (includeRejected) => {
  useAdminCheck();

  const statuses = [
    ARCHITECTURE_SUBMITTED,
    ARCHITECTURE_IN_REVIEW,
  ];

  if (includeRejected) statuses.push(ARCHITECTURE_REJECTED);

  const { loading, error, data } = useQuery(GET_ADMIN_ARCHITECTURES, {
    fetchPolicy: 'network-only',
    variables: {
      statuses: `{${statuses.map(s => `${s}`)}}`,
    },
  });

  const adminArchitectures = get(data, ARCHITECTURE_STATUSES_TYPENAME);

  return {
    loading,
    error,
    adminArchitectures,
  };
};

export default useAdminPanel;
