import React, { useState } from 'react';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import AdminPanelPageContent from 'pages/AdminPanelPage/AdminPanelPageContent.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Checkbox from 'components/Checkbox/Checkbox.jsx';
import 'pages/AdminPanelPage/AdminPanelPage.scss';

const AdminPanelPage = () => {
  const [includeRejected, setIncludeRejected] = useState(false);
  const onChange = () => setIncludeRejected(!includeRejected);

  return (
    <div className="AdminPanel">
      <Dashboard>
        <Dashboard.Header />
        <Dashboard.Section fit>
          <Dashboard.Column padded size="sm" theme="dark">
            <Grid direction="column" justify="spaceBetween">
              <SidebarNavigationInterface active="admin" />
              <Dashboard.Footer>
                <WelcomeBackInterface />
              </Dashboard.Footer>
            </Grid>
          </Dashboard.Column>
          <Dashboard.Column fit theme="light">
            <Dashboard.Section>
              <Dashboard.Toolbar>
                <Toolbar>
                  <Toolbar.Group fill />
                  <Toolbar.Group padded>
                    <Toolbar.Item>
                      <Checkbox
                        id="rejectedRadio"
                        label="Include rejected architectures"
                        onChange={onChange}
                        checked={includeRejected}
                      />
                    </Toolbar.Item>
                  </Toolbar.Group>
                </Toolbar>
              </Dashboard.Toolbar>
            </Dashboard.Section>
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              <AdminPanelPageContent includeRejected={includeRejected} />
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

AdminPanelPage.propTypes = {};

AdminPanelPage.defaultProps = {};

export default AdminPanelPage;
