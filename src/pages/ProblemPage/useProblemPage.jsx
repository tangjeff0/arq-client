import { useQuery } from '@apollo/client';
import { GET_PROBLEM_DETAILS } from 'queries/problems';
import { PROBLEM_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useProblemPage = ({ id }) => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_PROBLEM_DETAILS, { variables: { id } });

  const problem = get(data, PROBLEM_QUERY_ROOT, {});

  return {
    problem,
    error,
    loading,
  };
};

export default useProblemPage;
