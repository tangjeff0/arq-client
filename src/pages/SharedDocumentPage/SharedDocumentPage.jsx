import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { matchPropType } from 'proptypes';
import useCollaborationInvitation from 'hooks/useCollaborationInvitation.jsx';
import useSharedDocumentPage from 'pages/SharedDocumentPage/useSharedDocumentPage.jsx';
import usePackageComments from 'hooks/usePackageComments.jsx';
import useCommentsThread from 'hooks/useCommentsThread.jsx';
import useUser from 'hooks/useUser.jsx';
import SharedDocument from 'components/SharedDocument/SharedDocument.jsx';
import EntityError from 'components/EntityError/EntityError.jsx';

const SharedDocumentPage = ({ match, publishedView }) => {
  const { id } = match.params;

  const { user } = useUser();

  const {
    data,
    ui,
    entities,
    getSharedDoc,
  } = useSharedDocumentPage({ id });

  const {
    addPackageComment,
    deletePackageComment,
  } = usePackageComments();

  const {
    activeComments,
    setDocActiveComments,
    unsetDocActiveComments,
  } = useCommentsThread();

  const { updateCollaborationInvitation } = useCollaborationInvitation();

  useEffect(() => {
    getSharedDoc(id);
  }, [getSharedDoc, id]);

  useEffect(() => {
    const invitation = data.invitations.find(i => i.invitee.email === user.email);

    if (
      !!invitation
      && !invitation.seenAt
    ) {
      updateCollaborationInvitation(invitation.id);
    }
  }, [data, updateCollaborationInvitation, user]);

  // TODO: handle graphql error to render not found
  const docUIError = ui.error;

  if (
    docUIError
    && (docUIError.status === 403 || docUIError.status === 404)
  ) {
    return (
      <EntityError
        error={docUIError}
        entityName="Document"
        canRequestAccess
      />
    );
  }
  return (
    <SharedDocument
      user={user}
      data={data}
      entities={entities}
      activeComments={activeComments}
      ui={ui}
      setSharedDocActiveComments={setDocActiveComments}
      unsetSharedDocActiveComments={unsetDocActiveComments}
      addSharedDocPackageComment={addPackageComment}
      deleteSharedDocPackageComment={deletePackageComment}
      publishedView={publishedView}
    />
  );
};

SharedDocumentPage.propTypes = {
  match: matchPropType.isRequired,
  publishedView: PropTypes.bool,
};

SharedDocumentPage.defaultProps = {
  publishedView: false,
};

export default SharedDocumentPage;
