import { useQuery, useMutation } from '@apollo/client';
import { GET_MY_DOCUMENTS, ARCHIVE_DOCUMENT } from 'queries/documents';
import { MY_DOCUMENTS_QUERY_ROOT } from 'constants/gql';
import useCreateDoc from 'hooks/useCreateDoc.jsx';
import { get } from 'lodash';

const useDocumentsLibrary = () => {
  const {
    loading,
    error,
    data,
    refetch,
  } = useQuery(GET_MY_DOCUMENTS, {
    fetchPolicy: 'cache-and-network',
  });

  const docs = get(data, MY_DOCUMENTS_QUERY_ROOT, []);

  const [archiveDocument] = useMutation(ARCHIVE_DOCUMENT);

  const { createDoc } = useCreateDoc();

  return {
    docs,
    error,
    loading,
    refetch,
    archiveDocument,
    createDoc,
  };
};

export default useDocumentsLibrary;
