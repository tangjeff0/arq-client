import React from 'react';
import { DEFAULT_DOC } from 'constants/doctypes.js';
import DocumentsLibrary from 'components/DocumentsLibrary/DocumentsLibrary.jsx';
import useDocumentsLibrary from 'pages/DocumentsLibraryPage/useDocumentsLibrary.jsx';
import useViewport from 'hooks/useViewport.jsx';

const DocumentsLibraryPage = () => {
  const {
    docs,
    error,
    loading,
    refetch,
    archiveDocument,
    createDoc,
  } = useDocumentsLibrary();

  const { viewport } = useViewport();

  return (
    <DocumentsLibrary
      active="home"
      doctype={DEFAULT_DOC}
      docs={docs}
      error={error}
      loading={loading}
      refetch={refetch}
      archiveDocument={archiveDocument}
      createDoc={createDoc}
      viewport={viewport}
    />
  );
};

DocumentsLibraryPage.propTypes = {};

DocumentsLibraryPage.defaultProps = {};

export default DocumentsLibraryPage;
