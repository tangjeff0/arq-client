import React from 'react';
import PropTypes from 'prop-types';
import { matchPropType } from 'proptypes';
import { ARCHITECTURE_TYPENAME } from 'constants/gql';
import ActiveGraphInterface from 'components/ActiveGraph/ActiveGraphInterface.jsx';

const ArchitectureGraphPage = ({
  shared,
  published,
  match,
}) => (
  <ActiveGraphInterface
    id={match.params.architectureQID}
    type={ARCHITECTURE_TYPENAME}
    shared={shared}
    published={published}
    documentQID={match.params.documentQID}
  />
);

ArchitectureGraphPage.propTypes = {
  shared: PropTypes.bool,
  published: PropTypes.bool,
  match: matchPropType.isRequired,
};

ArchitectureGraphPage.defaultProps = {
  shared: false,
  published: false,
};

export default ArchitectureGraphPage;
