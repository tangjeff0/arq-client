import React from 'react';
import ArchitecturesLibrary from 'components/ArchitecturesLibrary/ArchitecturesLibrary.jsx';
import useArchitecturesLibrary from 'pages/ArchitecturesLibraryPage/useArchitecturesLibrary.jsx';
import useViewport from 'hooks/useViewport.jsx';

const ArchitecturesLibraryPage = () => {
  const {
    loading,
    error,
    publishedArchitectures,
  } = useArchitecturesLibrary();

  const { viewport } = useViewport();

  return (
    <ArchitecturesLibrary
      loading={loading}
      error={error}
      publishedArchitectures={publishedArchitectures}
      viewport={viewport}
    />
  );
};

ArchitecturesLibraryPage.propTypes = {};

ArchitecturesLibraryPage.defaultProps = {};

export default ArchitecturesLibraryPage;
