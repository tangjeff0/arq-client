import { get } from 'lodash';
import { GET_PUBLISHED_ARCHITECTURES } from 'queries/architectures';
import { PUBLISHED_ARCHITECTURE_TYPENAME } from 'constants/gql';
import { useQuery } from '@apollo/client';

const useArchitecturesLibrary = () => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_PUBLISHED_ARCHITECTURES, {
    fetchPolicy: 'cache-and-network',
  });

  const publishedArchitectures = get(data, PUBLISHED_ARCHITECTURE_TYPENAME, []);

  return {
    loading,
    error,
    publishedArchitectures,
  };
};

export default useArchitecturesLibrary;
