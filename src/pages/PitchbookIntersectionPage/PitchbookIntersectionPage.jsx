import React from 'react';
import PitchbookIntersection from 'components/PitchbookIntersection/PitchbookIntersection.jsx';

const PitchbookIntersectionPage = () => <PitchbookIntersection />;

PitchbookIntersectionPage.propTypes = {};

PitchbookIntersectionPage.defaultProps = {};

export default PitchbookIntersectionPage;
