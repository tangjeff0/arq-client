import React from 'react';
import Button from 'components/Button/Button.jsx';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import Checkbox from 'components/Checkbox/Checkbox.jsx';
import ActionBox from 'components/ActionBox/ActionBox.jsx';
import Loader from 'components/Loader/Loader.jsx';
import useTaxonomiesPageDownload from 'pages/TaxonomiesPage/useTaxonomiesPageDownload.jsx';

const TaxonomiesPageDownload = () => {
  const {
    cancelGetTaxonomyCSV,
    csvLoading,
    getTaxonomyCSV,
    sortAlpha,
    setSortAlpha,
  } = useTaxonomiesPageDownload();

  return (
    <Dropdown>
      <Dropdown.Toggle>
        <Button unstyled>...</Button>
      </Dropdown.Toggle>
      <Dropdown.Menu position="right">
        <div className="TaxonomiesPage-action-content">
          <ActionBox>
            <ActionBox.Content>
              {csvLoading ? (
                <>
                  <p>Hang tight, your CSV will download shortly. You can use ArQ as normal.</p>
                  <Loader isCentered size={14} />
                </>
              ) : (
                <>
                  <p>Download a CSV of all Taxonomies. This may take a minute.</p>
                  <Checkbox
                    id="taxonomy-download-sort"
                    checked={sortAlpha}
                    onChange={() => setSortAlpha(!sortAlpha)}
                    label="Sort A-Z"
                  />
                </>
              )}
            </ActionBox.Content>
            {csvLoading ? (
              <Button
                onClick={cancelGetTaxonomyCSV}
                display="block"
              >
                Cancel Download
              </Button>
            ) : (
              <Button
                onClick={getTaxonomyCSV}
                display="block"
              >
                Download CSV
              </Button>
            )}
          </ActionBox>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default TaxonomiesPageDownload;
