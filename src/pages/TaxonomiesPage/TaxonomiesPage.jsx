import React from 'react';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Grid from 'components/Grid/Grid.jsx';
import TaxonomySearchInterface from 'components/TaxonomySearch/TaxonomySearchInterface.jsx';
import TaxonomiesPageDownload from 'pages/TaxonomiesPage/TaxonomiesPageDownload.jsx';
import TaxonomiesPageContent from 'pages/TaxonomiesPage/TaxonomiesPageContent.jsx';
import 'pages/TaxonomiesPage/TaxonomiesPage.scss';

const Taxonomies = () => (
  <div className="TaxonomiesPage">
    <Dashboard>
      <Dashboard.Header />
      <Dashboard.Section fit>
        <Dashboard.Column padded minWidth="sm" size="sm" theme="dark">
          <Grid direction="column" justify="spaceBetween">
            <SidebarNavigationInterface active="taxonomies" />
            <Dashboard.Footer>
              <WelcomeBackInterface />
            </Dashboard.Footer>
          </Grid>
        </Dashboard.Column>
        <Dashboard.Column fit>
          <Dashboard.Toolbar display="flex">
            <Toolbar.Group fill>
              <TaxonomySearchInterface />
            </Toolbar.Group>
            <Toolbar.Group alignRight padded>
              <Toolbar.Item>
                <TaxonomiesPageDownload />
              </Toolbar.Item>
            </Toolbar.Group>
          </Dashboard.Toolbar>
          <Dashboard.Content padding={['top', 'right', 'left']}>
            <TaxonomiesPageContent />
          </Dashboard.Content>
        </Dashboard.Column>
      </Dashboard.Section>
    </Dashboard>
  </div>
);

Taxonomies.propTypes = {};

Taxonomies.defaultProps = {};

export default Taxonomies;
