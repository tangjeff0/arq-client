import { useState, useRef } from 'react';
import { toCSV, sortCSVString } from 'utils/csv';
import { TAXONOMIES_QUERY_ROOT } from 'constants/gql';
import { csvLoadingVar } from 'apollo/cache';
import * as services from 'services/taxonomies';

const useTaxonomiesPageDownload = () => {
  const observable = useRef();
  const csvLoading = csvLoadingVar();

  const [sortAlpha, setSortAlpha] = useState(false);

  const cancelGetTaxonomyCSV = () => {
    csvLoadingVar(false);
    // https://github.com/apollographql/apollo-client/issues/4150#issuecomment-500127694
    // apollo doesn't provide cancellation; simulate with unsubscribe
    observable.current.unsubscribe();
  };

  const getTaxonomyCSV = () => {
    csvLoadingVar(true);

    const query = services.getTaxonomyCSV();

    observable.current = query.subscribe((response) => {
      getTaxonomyCSVSuccess(toCSV(response.data[TAXONOMIES_QUERY_ROOT]));
    });
  };

  const getTaxonomyCSVSuccess = (csvString) => {
    csvLoadingVar(false);
    observable.current.unsubscribe();

    const result = sortAlpha ? sortCSVString(csvString) : csvString;

    const url = window.URL.createObjectURL(new Blob([result]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'taxonomies.csv');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return {
    cancelGetTaxonomyCSV,
    csvLoading,
    getTaxonomyCSV,
    sortAlpha,
    setSortAlpha,
  };
};

export default useTaxonomiesPageDownload;
