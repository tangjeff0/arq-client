import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Button from 'components/Button/Button.jsx';
import { IconPlus } from 'components/Icons/Icons.jsx';
import TaxonomyCreate from 'components/TaxonomyCreate/TaxonomyCreate.jsx';

const AdminTaxonomyPageCoreToolbar = ({ onCreate }) => {
  const dropdown = useRef();

  const onApprove = (value) => {
    dropdown.current && dropdown.current.close();
    onCreate(value);
  };

  return (
    <Toolbar>
      <Toolbar.Group alignRight padded>
        <Toolbar.Item>
          <Dropdown ref={dropdown}>
            <Dropdown.Toggle>
              <Button unstyled icon display="flex">
                <IconPlus display="block" position="left" />
                {' '}
                Add Root
              </Button>
            </Dropdown.Toggle>
            <Dropdown.Menu position="right">
              <div className="AdminTaxonomyPage-core-create">
                <TaxonomyCreate onApprove={onApprove} />
              </div>
            </Dropdown.Menu>
          </Dropdown>
        </Toolbar.Item>
      </Toolbar.Group>
    </Toolbar>
  );
};

AdminTaxonomyPageCoreToolbar.propTypes = {
  onCreate: PropTypes.func.isRequired,
};

export default AdminTaxonomyPageCoreToolbar;
