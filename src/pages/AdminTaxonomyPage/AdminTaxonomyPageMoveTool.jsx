import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import Button from 'components/Button/Button.jsx';
import { IconMove, IconInsertBelow } from 'components/Icons/Icons.jsx';

const AdminTaxonomyPageMoveTool = ({
  item,
  moveTaxonomy,
  setMoveTaxonomy,
  onMoveTaxonomy,
}) => {
  const canMoveInto = moveTaxonomy && item.id !== moveTaxonomy.id;

  const handleClick = (e) => {
    e.stopPropagation();
    if (moveTaxonomy) {
      onMoveTaxonomy(item);
    } else {
      setMoveTaxonomy(item);
    }
  };

  return (
    <Button
      unstyled
      readOnly={moveTaxonomy && !canMoveInto}
      onClick={handleClick}
    >
      {moveTaxonomy ? (
        <IconInsertBelow opacity={canMoveInto ? 1 : 0.6} />
      ) : (
        <IconMove />
      )}
    </Button>
  );
};

AdminTaxonomyPageMoveTool.propTypes = {
  item: taxonomyPropType.isRequired,
  moveTaxonomy: taxonomyPropType,
  setMoveTaxonomy: PropTypes.func.isRequired,
  onMoveTaxonomy: PropTypes.func.isRequired,
};

AdminTaxonomyPageMoveTool.defaultProps = {
  moveTaxonomy: null,
};

export default AdminTaxonomyPageMoveTool;
