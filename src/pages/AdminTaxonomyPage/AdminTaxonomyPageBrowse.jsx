import React from 'react';
import { useHistory } from 'react-router-dom';
import Tabs from 'components/Tabs/Tabs.jsx';
import TabsInterface from 'components/Tabs/TabsInterface.jsx';
import AdminTaxonomyPageSource from 'pages/AdminTaxonomyPage/AdminTaxonomyPageSource.jsx';
import {
  SOURCE_ACM,
  SOURCE_IAS,
  SOURCE_IEEE,
  SOURCE_PLOS,
  sourceNames,
} from 'constants/sources';
import { IconMerge } from 'components/Icons/Icons.jsx';

const AdminTaxonomyPageBrowse = () => {
  const history = useHistory();

  const sources = [SOURCE_ACM, SOURCE_IAS, SOURCE_IEEE, SOURCE_PLOS];

  const browserTreeActions = {
    primary: [
      {
        title: 'Begin merge with this term',
        icon: <IconMerge />,
        handler: taxonomy => history.replace(`/admin/merge/${taxonomy.id}`),
      },
    ],
  };

  return (
    <div className="AdminTaxonomyPage-browser">
      <TabsInterface>
        {sources.map(source => (
          <Tabs.Pane key={source} label={sourceNames[source]}>
            <AdminTaxonomyPageSource
              sourceName={source}
              actions={browserTreeActions}
            />
          </Tabs.Pane>
        ))}
      </TabsInterface>
    </div>
  );
};

export default AdminTaxonomyPageBrowse;
