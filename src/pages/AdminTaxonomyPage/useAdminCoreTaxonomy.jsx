import { useCallback } from 'react';
import { useLazyQuery, useMutation } from '@apollo/client';
import { createAddTaxonomies } from 'utils/gql';
import useAdminCheck from 'hooks/useAdminCheck.jsx';
import {
  UPDATE_TAXONOMY_TERM,
  UPDATE_TAXONOMY_MOVE,
  INSERT_UNDER_CORE_TAXONOMY,
  DELETE_TAXONOMY_FROM_CORE,
  GET_TAXONOMY_TREE,
} from 'queries/taxonomies';
import { TAXONOMY_QUERY_ROOT } from 'constants/gql';
import { GET_SOURCE_TREE } from 'queries/sources';
import { SOURCE_ARQ } from 'constants/sources';
import { get } from 'lodash';


const useAdminCoreTaxonomy = () => {
  useAdminCheck();

  const refetchQueries = [{ query: GET_SOURCE_TREE, variables: { sourceName: SOURCE_ARQ } }];

  // create mutation
  const [createInCoreTree, { loading: createMutationLoading }] = useMutation(
    INSERT_UNDER_CORE_TAXONOMY,
    {
      refetchQueries,
    },
  );
  const createCoreTaxonomy = (tree) => {
    const objects = createAddTaxonomies(null, [tree]);
    createInCoreTree({
      variables: { objects },
      refetchQueries,
    });
  };

  // update taxonomy term
  const [updateTaxonomyTermMutation, { loading: updateTermLoading }] = useMutation(
    UPDATE_TAXONOMY_TERM,
    { refetchQueries },
  );
  const updateCoreTaxonomy = useCallback((taxonomy, term) => updateTaxonomyTermMutation({
    variables: {
      id: taxonomy.id,
      term,
    },
  }), [updateTaxonomyTermMutation]);

  // move taxonomy
  const [updateTaxonomyMoveMutation, { loading: updateMoveLoading }] = useMutation(
    UPDATE_TAXONOMY_MOVE,
    { refetchQueries },
  );
  const setCoreTaxonomyParent = useCallback((taxonomy, parentId) => {
    updateTaxonomyMoveMutation({
      variables: {
        id: taxonomy.id,
        parentId,
      },
    });
  }, [updateTaxonomyMoveMutation]);

  // delete taxonomy
  const [deleteTaxonomyMutation, { loading: deleteLoading }] = useMutation(
    DELETE_TAXONOMY_FROM_CORE,
    { refetchQueries },
  );
  const deleteCoreTaxonomy = useCallback((taxonomies) => {
    deleteTaxonomyMutation({
      variables: {
        coreIds: taxonomies,
      },
    });
  }, [deleteTaxonomyMutation]);

  const [
    getTaxonomyDetails,
    { loading: taxonomyTreeLoading, data: taxonomyTree },
  ] = useLazyQuery(GET_TAXONOMY_TREE);

  const getTaxonomyTree = useCallback(
    id => getTaxonomyDetails({ variables: { id } }),
    [getTaxonomyDetails],
  );

  const taxonomy = get(taxonomyTree, [TAXONOMY_QUERY_ROOT]);
  const deleteTaxonomies = taxonomy ? taxonomy.allChildren.map(item => item.id) : [];

  return {
    createCoreTaxonomy,
    deleteCoreTaxonomy,
    getTaxonomyTree,
    updateCoreTaxonomy,
    setCoreTaxonomyParent,
    deleteTaxonomies,
    loading: updateTermLoading || createMutationLoading || updateMoveLoading
      || deleteLoading || taxonomyTreeLoading,
  };
};

export default useAdminCoreTaxonomy;
