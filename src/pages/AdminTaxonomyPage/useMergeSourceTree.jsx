import { useState, useEffect, useCallback } from 'react';
import { get } from 'lodash';
import { useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { removeDescendents } from 'utils/tree';
import { GET_TAXONOMY_TREE } from 'queries/taxonomies';
import { TAXONOMY_QUERY_ROOT } from 'constants/gql';

const useMergeSourceTree = (id) => {
  const history = useHistory();

  const [sourceTree, setSourceTree] = useState(null);
  const [expanded, setExpanded] = useState(true);

  const { loading, error, data } = useQuery(GET_TAXONOMY_TREE, { variables: { id } });

  useEffect(() => {
    const taxonomy = get(data, TAXONOMY_QUERY_ROOT, null);

    if (taxonomy) {
      setExpanded(taxonomy.allChildren.length < 1000);
      setSourceTree(taxonomy);
    }
  }, [data]);

  // exclude from merge tree
  const onDelete = useCallback(({ id: removeID }) => {
    if (removeID === sourceTree.id) {
      history.push('/admin/merge/');
    } else {
      const tree = {
        ...sourceTree,
        allChildren: removeDescendents(sourceTree.allChildren, removeID),
      };

      setSourceTree(tree);
    }
  }, [history, sourceTree]);

  return {
    loading,
    error,
    sourceTree,
    onDelete,
    expanded,
  };
};

export default useMergeSourceTree;
