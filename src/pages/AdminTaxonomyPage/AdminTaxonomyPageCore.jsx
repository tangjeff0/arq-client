import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import Tree from 'components/Tree/Tree.jsx';
import AdminTaxonomyPageCoreToolbar from 'pages/AdminTaxonomyPage/AdminTaxonomyPageCoreToolbar.jsx';
import AdminTaxonomyPageDeleteTool from 'pages/AdminTaxonomyPage/AdminTaxonomyPageDeleteTool.jsx';
import AdminTaxonomyPageEditTool from 'pages/AdminTaxonomyPage/AdminTaxonomyPageEditTool.jsx';
import AdminTaxonomyPageMoveTool from 'pages/AdminTaxonomyPage/AdminTaxonomyPageMoveTool.jsx';
import useMergeState from 'pages/AdminTaxonomyPage/useMergeState.jsx';
import useAdminCoreTaxonomy from 'pages/AdminTaxonomyPage/useAdminCoreTaxonomy.jsx';

const AdminTaxonomyPageCore = ({ roots }) => {
  const [moveTaxonomy, setMoveTaxonomy] = useState(null);
  const [deleteTaxonomy, setDeleteTaxonomy] = useState(null);

  const { target, setTarget } = useMergeState();

  const {
    createCoreTaxonomy,
    deleteCoreTaxonomy,
    getTaxonomyTree,
    updateCoreTaxonomy,
    setCoreTaxonomyParent,
    deleteTaxonomies,
  } = useAdminCoreTaxonomy();

  const onSetMoveTaxonomy = (taxonomy) => {
    setTarget(null);
    setMoveTaxonomy(taxonomy);
  };

  const onSetDeleteTaxonomy = useCallback((taxonomy) => {
    setTarget(null);
    setDeleteTaxonomy(taxonomy);
    if (taxonomy) {
      getTaxonomyTree(taxonomy.id);
    }
  }, [setTarget, getTaxonomyTree]);

  const handleMoveTaxonomy = useCallback((targetTaxonomy) => {
    if (targetTaxonomy && (targetTaxonomy.id !== moveTaxonomy.id)) {
      setCoreTaxonomyParent(moveTaxonomy, targetTaxonomy.id);
    }
  }, [setCoreTaxonomyParent, moveTaxonomy]);

  const handleDeleteTaxonomy = useCallback((taxonomy) => {
    if (taxonomy) {
      deleteCoreTaxonomy(deleteTaxonomies);
    }
  }, [deleteCoreTaxonomy, deleteTaxonomies]);


  const handleTaxonomyClick = useCallback((taxonomy) => {
    if (target && taxonomy.id === target.id) {
      setTarget(null);
    } else {
      setTarget(taxonomy);
      setMoveTaxonomy(null);
    }
  }, [target, setTarget, setMoveTaxonomy]);

  const tools = [
    {
      key: 'moveup',
      renderTool: ({ item }) => { // eslint-disable-line
        if (deleteTaxonomy) return null;
        return (
          <AdminTaxonomyPageMoveTool
            item={item}
            moveTaxonomy={moveTaxonomy}
            setMoveTaxonomy={onSetMoveTaxonomy}
            onMoveTaxonomy={handleMoveTaxonomy}
          />
        );
      },
    },
    {
      key: 'edit',
      renderTool: ({ item }) => { // eslint-disable-line
        if (moveTaxonomy || deleteTaxonomy) return null;
        return (
          <AdminTaxonomyPageEditTool
            item={item}
            updateTaxonomyTerm={updateCoreTaxonomy}
          />
        );
      },
    },
    {
      key: 'delete',
      renderTool: ({ item }) => { // eslint-disable-line
        if (moveTaxonomy) return null;
        return (
          <AdminTaxonomyPageDeleteTool
            item={item}
            deleteTaxonomy={deleteTaxonomy}
            setDeleteTaxonomy={onSetDeleteTaxonomy}
            onDeleteTaxonomy={handleDeleteTaxonomy}
          />
        );
      },
    },
  ];

  return (
    <div className="AdminTaxonomyPage-core">
      <AdminTaxonomyPageCoreToolbar onCreate={createCoreTaxonomy} />
      <div className="AdminTaxonomyPage-core-tree">
        {roots.map(root => (
          <Tree
            key={root.id}
            item={root}
            allItems={root.allChildren}
            size="sm"
            selected={target}
            onSelect={handleTaxonomyClick}
            moveTaxonomy={moveTaxonomy}
            deleteTaxonomy={deleteTaxonomy}
            deleteTaxonomies={deleteTaxonomies}
            tools={tools}
          />
        ))}
      </div>
    </div>
  );
};

AdminTaxonomyPageCore.propTypes = {
  roots: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default AdminTaxonomyPageCore;
