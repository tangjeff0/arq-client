import { useCallback } from 'react';
import { useMutation } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { createInsertTaxonomies } from 'utils/gql';
import { taxonomiesSortByParent } from 'utils/taxonomies';
import useAdminCheck from 'hooks/useAdminCheck.jsx';
import { toast } from 'react-toastify';
import {
  MERGE_INTO_CORE_TAXONOMY,
  INSERT_UNDER_CORE_TAXONOMY,
} from 'queries/taxonomies';
import { GET_SOURCE_TREE } from 'queries/sources';
import { SOURCE_ARQ } from 'constants/sources';

const useMergeTaxonomies = () => {
  useAdminCheck();

  const history = useHistory();

  // success callback after successful insert/merge
  const onSuccess = (type) => {
    toast.success(`Taxonomy ${type} successful!`, { autoClose: 1500 });
    history.push('/admin/merge');
  };
  const refetchQueries = [{ query: GET_SOURCE_TREE, variables: { sourceName: SOURCE_ARQ } }];

  // merge mutation
  const [mergeIntoCoreTree, { loading: mergeMutationLoading }] = useMutation(
    MERGE_INTO_CORE_TAXONOMY,
    {
      onCompleted: () => onSuccess('merge'),
    },
  );
  const onMerge = useCallback((targetID, tree) => {
    const treeChildren = taxonomiesSortByParent(tree, 'merge');

    const objects = createInsertTaxonomies(targetID, treeChildren);
    mergeIntoCoreTree({
      variables: {
        id: tree.id,
        coreTaxonomy: targetID,
        objects,
      },
      refetchQueries,
    });
  }, [mergeIntoCoreTree, refetchQueries]);

  // insert mutation
  const [insertIntoCoreTree, { loading: insertMutationLoading }] = useMutation(
    INSERT_UNDER_CORE_TAXONOMY,
    {
      onCompleted: () => onSuccess('insert'),
    },
  );
  const onInsert = useCallback((targetID, tree) => {
    const treeChildren = taxonomiesSortByParent(tree, 'insert');
    const objects = createInsertTaxonomies(targetID, treeChildren);

    insertIntoCoreTree({
      variables: { objects },
      refetchQueries,
    });
  }, [insertIntoCoreTree, refetchQueries]);

  return {
    onMerge,
    onInsert,
    mutationLoading: insertMutationLoading || mergeMutationLoading,
  };
};

export default useMergeTaxonomies;
