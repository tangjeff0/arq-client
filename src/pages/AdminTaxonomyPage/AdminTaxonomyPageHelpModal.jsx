import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'components/Modal/Modal.jsx';

const AdminTaxonomyPageHelpModal = ({ infoModalOpen, setInfoModalOpen }) => (
  <Modal
    title="Merging and Inserting Taxonomy Trees"
    isOpened={infoModalOpen}
    closeModal={() => setInfoModalOpen(false)}
  >
    Merge will merge the selected source taxonomy with the selected
    target taxonomy. Any children included in the merge will be added as
    new children of the target. Any architecture links to the taxonomies
    in the source tree will be updated to link to the new target tree.
    <br />
    <br />
    Insert will add the source tree and any included children as
    new children of the target tree. Any architecture links to the taxonomies
    in the source tree will be updated to link to the new target tree.
  </Modal>
);

AdminTaxonomyPageHelpModal.propTypes = {
  infoModalOpen: PropTypes.bool.isRequired,
  setInfoModalOpen: PropTypes.func.isRequired,
};

export default AdminTaxonomyPageHelpModal;
