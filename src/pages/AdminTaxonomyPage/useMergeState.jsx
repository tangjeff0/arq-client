import { useQuery } from '@apollo/client';
import { GET_MERGE_STATE } from 'queries/admin';
import { mergeStateVar } from 'apollo/cache';

const useMergeState = () => {
  const { data: { mergeState } } = useQuery(GET_MERGE_STATE);

  const updateMergeState = newMergeState => mergeStateVar({
    ...mergeState,
    ...newMergeState,
  });

  const setTarget = target => updateMergeState({ target });

  const setSource = source => updateMergeState({ source });

  const resetMergeState = () => updateMergeState({
    target: null,
    source: null,
  });

  const { target, source } = mergeState;

  return {
    setTarget,
    target,
    setSource,
    source,
    resetMergeState,
  };
};

export default useMergeState;
