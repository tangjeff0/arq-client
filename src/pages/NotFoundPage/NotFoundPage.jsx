import React from 'react';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Alert from 'components/Alert/Alert.jsx';

const NotFoundPage = () => (
  <div className="NotFoundPage">
    <Dashboard>
      <Dashboard.Header />
      <Dashboard.Section fit>
        <Dashboard.Column padded size="sm" theme="dark">
          <Grid direction="column" justify="spaceBetween">
            <SidebarNavigationInterface />
            <Dashboard.Footer>
              <WelcomeBackInterface />
            </Dashboard.Footer>
          </Grid>
        </Dashboard.Column>
        <Dashboard.Column fit theme="light">
          <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
            <Alert
              heading="Uh Oh..."
              subheading="The page you're looking for can't be found."
            />
          </Dashboard.Content>
        </Dashboard.Column>
      </Dashboard.Section>
    </Dashboard>
  </div>
);

export default NotFoundPage;
