import React from 'react';
import { companyPropType } from 'proptypes';
import Panel from 'components/Panel/Panel.jsx';
import CardList from 'components/CardList/CardList.jsx';
import CapabilityCard from 'components/CapabilityCard/CapabilityCard.jsx';
import RelatedPanelInterface from 'components/RelatedPanel/RelatedPanelInterface.jsx';
import GraphPanelInterface from 'components/GraphPanel/GraphPanelInterface.jsx';
import {
  GET_CUSTOMER_ARCHITECTURES,
  GET_CUSTOMER_COMPANIES,
  GET_CUSTOMER_PROBLEMS,
  GET_CUSTOMER_TAXONOMIES,
} from 'queries/customers';
import { GET_GRAPH_CUSTOMER } from 'queries/graph';
import { CUSTOMER_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';
import Loader from 'components/Loader/Loader.jsx';
import { useQuery } from '@apollo/client';
import { generateSearchLink } from 'utils/links';

const CustomerPageContent = ({ customer }) => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_CUSTOMER_TAXONOMIES, {
    variables: { id: customer.id },
    fetchPolicy: 'cache-and-network',
  });
  const { capabilities } = get(data, CUSTOMER_QUERY_ROOT, []);

  const filter = 'agencies';
  const propName = 'name';
  const items = [customer];

  return (
    <div className="CustomerPage-content">
      <Panel>
        <Panel.Heading>
          <div className="CustomerPage-name">{customer.name}</div>
        </Panel.Heading>
        <Panel.Content>
          {loading && <Loader isCentered size={24} />}
          {!loading && error && <div>Error</div>}
          {!loading && capabilities && (
            <CardList
              title="Capabilities"
              items={capabilities}
              renderCard={capability => (
                <CapabilityCard
                  key={capability.id}
                  capability={capability}
                />
              )}
            />
          )}
        </Panel.Content>
      </Panel>
      <RelatedPanelInterface
        id={customer.id}
        query={GET_CUSTOMER_ARCHITECTURES}
        parentKey={CUSTOMER_QUERY_ROOT}
        childKey="architectures"
      />
      <RelatedPanelInterface
        id={customer.id}
        query={GET_CUSTOMER_COMPANIES}
        parentKey={CUSTOMER_QUERY_ROOT}
        childKey="companies"
      />
      <RelatedPanelInterface
        id={customer.id}
        query={GET_CUSTOMER_PROBLEMS}
        parentKey={CUSTOMER_QUERY_ROOT}
        childKey="problemSets"
        parentSearchLink={generateSearchLink(filter, 'problems', propName, items)}
      />
      <GraphPanelInterface
        id={customer.id}
        query={GET_GRAPH_CUSTOMER}
        type={CUSTOMER_QUERY_ROOT}
      />
    </div>
  );
};

CustomerPageContent.propTypes = {
  customer: companyPropType.isRequired,
};

CustomerPageContent.defaultProps = {};

export default CustomerPageContent;
