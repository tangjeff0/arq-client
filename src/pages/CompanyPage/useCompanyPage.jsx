import { useQuery } from '@apollo/client';
import { get } from 'lodash';
import { GET_COMPANY_DETAILS } from 'queries/companies';
import { COMPANY_QUERY_ROOT } from 'constants/gql';

const useCompanyPage = ({ id }) => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_COMPANY_DETAILS, { variables: { id } });

  const company = get(data, COMPANY_QUERY_ROOT, {});

  return {
    company,
    error,
    loading,
  };
};

export default useCompanyPage;
