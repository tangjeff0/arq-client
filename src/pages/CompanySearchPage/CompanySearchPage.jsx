import React from 'react';
import {
  SearchBox,
  Pagination,
  NoHits,
  ViewSwitcherHits,
  HitsStats,
  ResetFilters,
  SelectedFilters,
} from 'searchkit';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Alert from 'components/Alert/Alert.jsx';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import SearchKit from 'components/SearchKit/SearchKit.jsx';
import SearchkitManagerCompanies from 'components/SearchKit/SearchKitManagerCompanies.jsx';
import CompanyCard from 'components/CompanyCard/CompanyCard.jsx';
import CompanySearchFilters from 'pages/CompanySearchPage/CompanySearchPageFilters.jsx';
import useCompanySearch from 'hooks/useCompanySearch.jsx';
import 'pages/CompanySearchPage/CompanySearchPage.scss';

const CompanySearchPage = () => {
  const queryBuilder = query => ({
    bool: {
      should: [
        {
          match: {
            name: {
              query,
              fuzziness: 'AUTO',
            },
          },
        },
        {
          wildcard: {
            name: {
              value: `*${query.toLowerCase()}*`,
            },
          },
        },
        {
          nested: {
            path: 'capabilities',
            query: {
              match: {
                'capabilities.term': {
                  query,
                  fuzziness: 'AUTO',
                },
              },
            },
          },
        },
        {
          nested: {
            path: 'capabilities',
            query: {
              wildcard: {
                'capabilities.term': {
                  value: `*${query.toLowerCase()}*`,
                },
              },
            },
          },
        },
      ],
    },
  });

  const {
    error,
    loading,
    setTaxonomiesOperator,
    taxonomiesOperator,
    setTaxonomiesSort,
    taxonomiesSort,
    filter,
    setFilter,
    filterOpen,
    setFilterOpen,
    include,
  } = useCompanySearch();

  return (
    <div className="CompanySearchPage">
      <Dashboard>
        <Dashboard.Header />
        <Dashboard.Section fit>
          <Dashboard.Column padded size="sm" theme="dark">
            <Grid direction="column" justify="spaceBetween">
              <SidebarNavigationInterface active="companies" />
              <Dashboard.Footer>
                <WelcomeBackInterface />
              </Dashboard.Footer>
            </Grid>
          </Dashboard.Column>
          {error && (
            <Alert
              padded
              heading="Uh Oh..."
              subheading="There was an error. Check your network connection and try again."
            />
          )}
          {!error && loading && <Loader size={36} isFullscreen />}
          {!loading && !error && taxonomiesOperator && (
            <SearchKit searchkit={SearchkitManagerCompanies}>
              <>
                <Dashboard.Column fit>
                  <Dashboard.Toolbar>
                    <SearchBox
                      autofocus
                      placeholder="Search Company Name or Capabilities"
                      searchOnChange
                      queryBuilder={queryBuilder}
                    />
                  </Dashboard.Toolbar>
                  <div className="CompanySearchPage-results">
                    <SearchKit.ActionBar>
                      <HitsStats
                        translations={{
                          'hitstats.results_found': '{hitCount} companies found',
                        }}
                      />
                      <ResetFilters component={SearchKit.Reset} />
                    </SearchKit.ActionBar>
                    <SelectedFilters itemComponent={SearchKit.SelectedFilter} />
                    <ViewSwitcherHits
                      hitsPerPage={12}
                      highlightFields={['name']}
                      customHighlight={{ number_of_fragments: 0 }}
                      hitComponents={[
                        {
                          key: 'list',
                          title: 'List',
                          itemComponent: ({ result }) => ( // eslint-disable-line
                            <CompanyCard
                              item={result._source} // eslint-disable-line
                              highlight={result.highlight} // eslint-disable-line
                              searchResult
                            />
                          ),
                          defaultOption: true,
                        },
                      ]}
                      scrollTo="body"
                    />
                    <NoHits
                      translations={{
                        'NoHits.NoResultsFound': 'No Companies were found.',
                      }}
                      suggestionsField="name"
                      errorComponent={SearchKit.Error}
                    />
                    <Pagination showNumbers />
                  </div>
                </Dashboard.Column>
                <Dashboard.Column fit size="lg" theme="white">
                  <Dashboard.Content>
                    <CompanySearchFilters
                      taxonomiesOperator={taxonomiesOperator}
                      setTaxonomiesOperator={setTaxonomiesOperator}
                      taxonomiesSort={taxonomiesSort}
                      setTaxonomiesSort={setTaxonomiesSort}
                      filter={filter}
                      setFilter={setFilter}
                      filterOpen={filterOpen}
                      setFilterOpen={setFilterOpen}
                      include={include}
                    />
                  </Dashboard.Content>
                </Dashboard.Column>
              </>
            </SearchKit>
          )}
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

export default CompanySearchPage;
