import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import ArchitecturesLibraryPage from 'pages/ArchitecturesLibraryPage/ArchitecturesLibraryPage.jsx';
import ActiveDocumentPage from 'pages/ActiveDocumentPage/ActiveDocumentPage.jsx';
import AdminPanelPage from 'pages/AdminPanelPage/AdminPanelPage.jsx';
import AdminTaxonomiesPage from 'pages/AdminTaxonomiesPage/AdminTaxonomiesPage.jsx';
import CustomerPage from 'pages/CustomerPage/CustomerPage.jsx';
import CustomersPage from 'pages/CustomersPage/CustomersPage.jsx';
import CompanyPage from 'pages/CompanyPage/CompanyPage.jsx';
import CompanySearchPage from 'pages/CompanySearchPage/CompanySearchPage.jsx';
import DocumentsLibraryPage from 'pages/DocumentsLibraryPage/DocumentsLibraryPage.jsx';
import DrawingToolPage from 'pages/DrawingToolPage/DrawingToolPage.jsx';
import NotFoundPage from 'pages/NotFoundPage/NotFoundPage.jsx';
import AdminTaxonomyPage from 'pages/AdminTaxonomyPage/AdminTaxonomyPage.jsx';
import TaxonomiesPage from 'pages/TaxonomiesPage/TaxonomiesPage.jsx';
import TaxonomyPage from 'pages/TaxonomyPage/TaxonomyPage.jsx';
import PublishedArchitecturePage from 'pages/PublishedArchitecturePage/PublishedArchitecturePage.jsx';
import ArchitectureReviewPage from 'pages/ArchitectureReviewPage/ArchitectureReviewPage.jsx';
import ArchitectureGraphPage from 'pages/ArchitectureGraphPage/ArchitectureGraphPage.jsx';
import GraphPage from 'pages/GraphPage/GraphPage.jsx';
import PitchbookIntersectionPage from 'pages/PitchbookIntersectionPage/PitchbookIntersectionPage.jsx';
import ProblemPage from 'pages/ProblemPage/ProblemPage.jsx';
import ProblemSearchPage from 'pages/ProblemSearchPage/ProblemSearchPage.jsx';
import SharedDocumentPage from 'pages/SharedDocumentPage/SharedDocumentPage.jsx';
import SharedDocumentsLibraryPage from 'pages/SharedDocumentsLibraryPage/SharedDocumentsLibraryPage.jsx';
import App from 'components/App/App.jsx';

import {
  COMPANY_TYPENAME,
  CUSTOMER_TYPENAME,
  PROBLEM_TYPENAME,
  TAXONOMY_TYPENAME,
} from 'constants/gql';

const Root = () => (
  <App>
    <Switch>
      <Route exact path="/shared" component={SharedDocumentsLibraryPage} />
      <Route exact path="/shared/:id" component={SharedDocumentPage} />
      <Route exact path="/documents/:id" component={ActiveDocumentPage} />
      <Route
        exact
        path="/published_architectures"
        component={ArchitecturesLibraryPage}
      />
      <Route
        exact
        path="/published_architectures/:id"
        component={props => <PublishedArchitecturePage {...props} />}
      />
      <Redirect
        from="/tech-capability-tags"
        to={`/published_architectures/${TECH_TAGS_ARCHITECTURE_ID}`}
      />
      <Route
        exact
        path="/published_architectures/:architectureQID/drawing"
        component={props => <DrawingToolPage published {...props} />}
      />
      <Route
        exact
        path="/published_architectures/:architectureQID/graph"
        component={props => (
          <ArchitectureGraphPage published {...props} />
        )}
      />
      <Route
        exact
        path="/documents/:documentQID/architectures/:architectureQID/drawing"
        component={DrawingToolPage}
      />
      <Route
        exact
        path="/shared/:documentQID/architectures/:architectureQID/drawing"
        component={props => <DrawingToolPage shared {...props} />}
      />
      <Route
        exact
        path="/documents/:documentQID/architectures/:architectureQID/graph"
        component={ArchitectureGraphPage}
      />
      <Route
        exact
        path="/shared/:documentQID/architectures/:architectureQID/graph"
        component={props => (
          <ArchitectureGraphPage shared {...props} />
        )}
      />
      <Route exact path="/taxonomies/:id" component={TaxonomyPage} />
      <Route exact path="/taxonomies/:id/graph" component={props => <GraphPage type={TAXONOMY_TYPENAME} {...props} />} />
      <Route exact path="/taxonomies" component={TaxonomiesPage} />
      <Route exact path="/problems/:id" component={ProblemPage} />
      <Route exact path="/problems/:id/graph" component={props => <GraphPage type={PROBLEM_TYPENAME} {...props} />} />
      <Route exact path="/problems" component={ProblemSearchPage} />
      <Route exact path="/companies/:id" component={CompanyPage} />
      <Route exact path="/customers/:id" component={CustomerPage} />
      <Route exact path="/customers" component={CustomersPage} />
      <Route exact path="/customers/:id/graph" component={props => <GraphPage type={CUSTOMER_TYPENAME} {...props} />} />
      <Route exact path="/companies/:id/graph" component={props => <GraphPage type={COMPANY_TYPENAME} {...props} />} />
      <Route exact path="/companies" component={CompanySearchPage} />
      <Route exact path="/admin/architectures" component={AdminPanelPage} />
      <Route exact path="/admin/architectures/:id" component={ArchitectureReviewPage} />
      <Route exact strict path="/admin/merge" component={AdminTaxonomiesPage} />
      <Route exact strict path="/admin/merge/" component={AdminTaxonomyPage} />
      <Route exact path="/admin/merge/:id" component={AdminTaxonomyPage} />
      <Route exact path="/pitchbook" component={PitchbookIntersectionPage} />
      <Route exact path="/" component={DocumentsLibraryPage} />
      <Route component={NotFoundPage} />
    </Switch>
    <ToastContainer
      position="bottom-center"
      autoClose={false}
      closeOnClick
    />
  </App>
);

Root.propTypes = {};

Root.defaultProps = {};

export default Root;
