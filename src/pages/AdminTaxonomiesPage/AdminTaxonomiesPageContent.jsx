import React from 'react';
import PropTypes from 'prop-types';
import { sourceNames } from 'constants/sources';
import Button from 'components/Button/Button.jsx';
import Heading from 'components/Heading/Heading.jsx';
import Table from 'components/Table/Table.jsx';

const AdminTaxonomiesPageContent = ({ linkedTaxonomies }) => (
  <div className="AdminTaxonomiesPage-content">
    <Heading level={2}>
      {linkedTaxonomies.length > 0
        ? 'Externally linked Taxonomies'
        : 'No Externally linked Taxonomies For Review'}
    </Heading>
    {linkedTaxonomies.length > 0 && (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.Cell header>Term</Table.Cell>
          <Table.Cell header>Source</Table.Cell>
          <Table.Cell header>Published Architectures</Table.Cell>
          <Table.Cell header />
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {linkedTaxonomies.map(taxonomy => (
          <Table.Row key={taxonomy.id}>
            <Table.Cell>{taxonomy.term}</Table.Cell>
            <Table.Cell>{sourceNames[taxonomy.source.name]}</Table.Cell>
            <Table.Cell>
              {taxonomy.architectureCount}
            </Table.Cell>
            <Table.Cell alignRight>
              <Button
                appearance="secondary"
                asLink
                linkTo={`/admin/merge/${taxonomy.id}`}
              >
                {`Merge into ${SOURCE_ARQ_NAME}`}
              </Button>
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
    )}
  </div>
);

AdminTaxonomiesPageContent.propTypes = {
  linkedTaxonomies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

AdminTaxonomiesPageContent.defaultProps = {
};

export default AdminTaxonomiesPageContent;
