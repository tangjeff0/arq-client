import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

// eslint-disable-next-line import/prefer-default-export
export const GET_USERS = gql`
  query User {
    Users(order_by: {email: asc}) {
      ...UserFields
    }
  }
  ${fragments.userFields}
`;
