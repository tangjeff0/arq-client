import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

export default {
  documentFields: gql`
    fragment DocumentFields on arq_my_documents {
      id
      title
      createdAt
      updatedAt
      deletedAt
    }
  `,
  sharedDocumentFields: gql`
    fragment SharedDocumentFields on arq_documents {
      id
      title
      createdAt
      updatedAt
      deletedAt
    }
  `,
  architectureFields: gql`
    fragment ArchitectureFields on arq_architectures {
      id
      createdAt
      publishedAt
      submittedAt
      status
      title
    }
  `,
  customerFields: gql`
    fragment CustomerFields on arq_customers {
      id
      name
    }
  `,
  authenticatedUserFields: gql`
    fragment AuthenticatedUserFields on arq_authenticated_user_view {
      email
      admin
    }
  `,
  userFields: gql`
    fragment UserFields on arq_users {
      email
    }
  `,
  packageFields: gql`
    fragment PackageFields on arq_packages {
      id
      createdAt
      updatedAt
      deletedAt
      term
      description
      sequence
      show
      linkBroken
    }
  `,
  commentFields: gql`
    fragment CommentFields on arq_package_comments {
      id
      text
      deletedAt
      createdAt
      author {
        email
      }
    }
  `,
  problemFields: gql`
    fragment ProblemFields on arq_problem_sets {
      id
      customerNumber
      fiscalYear
      originationYear
      priority
      categoryOfNeed
      customerCategory
    }
  `,
  previewPackageFields: gql`
    fragment PreviewPackageFields on arq_packages {
      id
      term
      show
    }
  `,
  architectureCardFields: gql`
    fragment ArchitectureCardFields on arq_architectures {
      id
      title
      status
      isLatestArch
      packages {
        id
        term
        packages {
          id
          term
          packages {
            id
            term
          }
        }
      }
    }
  `,
  companyCardFields: gql`
    fragment CompanyCardFields on arq_organizations {
      id
      name
      pitchbookId
      portfolio
      capabilities {
        id
        term
      }
    }
  `,
  problemCardFields: gql`
    fragment ProblemCardFields on arq_problem_sets {
      id
      customerNumber
      fiscalYear
      originationYear
      priority
      categoryOfNeed
      customerCategory
      capabilities {
        id
        term
      }
      customer {
        id
        name
      }
    }
  `,
  taxonomyCardFields: gql`
    fragment TaxonomyCardFields on arq_taxonomies {
      id
      term
      source {
        name
      }
      ancestors {
        id
        term
        parent {
          id
        }
        source {
          name
        }
      }
      referencedBy {
        id
        architecture {
          id
          status
          isLatestArch
        }
      }
      problemSets {
        id
      }
      companies {
        id
      }
    }
  `,
  packageDrawingFields: gql`
    fragment PackageDrawingFields on arq_packages {
      id
      level0position
      level0titlePosition
      level1x
      level1y
      level1width
      level1height
      level1drawn
      level1hideChildren
      level1columns
      level1position
      level1titlePosition
      level2position
    }
  `,
  annotationFields: gql`
    fragment Annotation on arq_package_annotations {
      id
      x
      y
      width
      height
      rotation
      organization {
        id
        logo
      }
    }
  `,
};
