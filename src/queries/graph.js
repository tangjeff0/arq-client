import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

const fragments = {
  relatedArchitecture: gql`
    fragment RelatedArchitecture on arq_architectures {
      id
      status
      isLatestArch
    }
  `,
  relatedCustomer: gql`
    fragment RelatedCustomer on arq_customers {
      id
      name
      problemSets {
        id
      }
    }
  `,
  relatedPackage: gql`
    fragment RelatedPackage on arq_packages {
      id
      term
      linkBroken
      taxonomy {
        id
      }
      packages {
        id
        architecture {
          id
          status
          isLatestArch
        }
      }
      parent {
        id
        architecture {
          id
          status
          isLatestArch
        }
      }
      referencePackage {
        id
        architecture {
          id
          status
          isLatestArch
        }
      }
      architecture {
        id
        status
        isLatestArch
      }
    }
  `,
  relatedTaxonomy: gql`
    fragment RelatedTaxonomy on arq_taxonomies {
      id
      term
      source {
        name
      }
      parent {
        id
      }
      children {
        id
      }
      companies {
        id
      }
      problemSets {
        id
      }
      referencedBy {
        id
        architecture {
          id
          status
          isLatestArch
        }
      }
    }
  `,
  relatedProblem: gql`
    fragment RelatedProblem on arq_problem_sets {
      id
      fiscalYear
      originationYear
      customerNumber
    }
  `,
};

export const GET_GRAPH_ARCHITECTURE = gql`
  query ArchitectureTree($id: uuid!) {
    ArchitecturesByID(id: $id) {
      id
      status
      isLatestArch
      title
      packages(where: {parentId: {_is_null: true}}) {
        id
        term
        linkBroken
        packages {
          id
          architecture {
            ...RelatedArchitecture
          }
        }
        taxonomy {
          id
        }
        parent {
          id
          architecture {
            ...RelatedArchitecture
          }
        }
        referencePackage {
          id
          architecture {
            ...RelatedArchitecture
          }
        }
        architecture {
          ...RelatedArchitecture
        }
      }
    }
  }
  ${fragments.relatedArchitecture}
`;

export const GET_ARCHITECTURE_RELATED_NODES = gql`
  query ArchitectureRelatedNodes($id: uuid!) {
    ArchitecturesByID(id: $id) {
      id
      packages {
        id
        term
        linkBroken
        packages {
          id
          architecture {
            ...RelatedArchitecture
          }
        }
        taxonomy {
          id
        }
        parent {
          id
          architecture {
            ...RelatedArchitecture
          }
        }
        referencePackage {
          id
          architecture {
            ...RelatedArchitecture
          }
        }
        architecture {
          ...RelatedArchitecture
        }
      }
    }
  }
  ${fragments.relatedArchitecture}
`;

export const GET_PACKAGE_RELATED_NODES = gql`
  query PackageRelatedNodes($id: uuid!) {
    PackagesByID(id: $id) {
      id
      linkBroken
      taxonomy {
        ...RelatedTaxonomy
      }
      packages {
        ...RelatedPackage
      }
      parent {
        ...RelatedPackage
      }
      referencePackage {
        ...RelatedPackage
      }
      architecture {
        id
        title
        status
        isLatestArch
        packages {
          id
          architecture {
            ...RelatedArchitecture
          }
        }
      }
    }
  }
  ${fragments.relatedPackage}
  ${fragments.relatedTaxonomy}
  ${fragments.relatedArchitecture}
`;

export const GET_TAXONOMY_RELATED_NODES = gql`
  query TaxonomyRelatedNodes($id: uuid!) {
    TaxonomiesByID(id: $id) {
      id
      term
      parent {
        ...RelatedTaxonomy
      }
      children {
        ...RelatedTaxonomy
      }
      problemSets {
        ...RelatedProblem
        customer {
          id
          name
        }
        capabilities {
          id
        }
      }
      companies {
        id
        name
        capabilities {
          id
        }
      }
      referencedBy {
        ...RelatedPackage
      }
    }
  }
  ${fragments.relatedProblem}
  ${fragments.relatedTaxonomy}
  ${fragments.relatedPackage}
`;

export const GET_GRAPH_PROBLEM = gql`
  query GraphProblem($id: String!) { # imported from Salesforce; use String
    ProblemSetsByID(id: $id) {
      ...RelatedProblem
      capabilities {
        ...RelatedTaxonomy
      }
      customer {
        ...RelatedCustomer
      }
    }
  }
  ${fragments.relatedProblem}
  ${fragments.relatedTaxonomy}
  ${fragments.relatedCustomer}
`;

export const GET_PROBLEM_RELATED_NODES = gql`
  query ProblemRelatedNodes($id: String!) { # imported from Salesforce; use String
    ProblemSetsByID(id: $id) {
      id
      capabilities {
        ...RelatedTaxonomy
      }
      customer {
        ...RelatedCustomer
      }
    }
  }
  ${fragments.relatedTaxonomy}
  ${fragments.relatedCustomer}
`;

export const GET_GRAPH_COMPANY = gql`
  query GraphCompany($id: String!) { # imported from Salesforce; use String
    OrganizationsByID(id: $id) {
      id
      name
      capabilities {
        id
        ...RelatedTaxonomy
      }
    }
  }
  ${fragments.relatedTaxonomy}
`;

export const GET_COMPANY_RELATED_NODES = gql`
  query CompanyRelatedNodes($id: String!) { # imported from Salesforce; use String
    OrganizationsByID(id: $id) {
      id
      capabilities {
        ...RelatedTaxonomy
      }
    }
  }
  ${fragments.relatedTaxonomy}
`;

export const GET_GRAPH_CUSTOMER = gql`
  query GraphCustomer($id: uuid!) {
    CustomersByID(id: $id) {
      id
      name
      description
      problemSets {
        ...RelatedProblem
        customer {
          id
          name
        }
        capabilities {
          ...RelatedTaxonomy
        }
      }
    }
  }
  ${fragments.relatedProblem}
  ${fragments.relatedTaxonomy}
`;

export const GET_CUSTOMER_RELATED_NODES = gql`
  query CustomerRelatedNodes($id: uuid!) {
    CustomersByID(id: $id) {
      id
      name
      description
      problemSets {
        ...RelatedProblem
        customer {
          id
          name
        }
        capabilities {
          ...RelatedTaxonomy
        }
      }
    }
  }
  ${fragments.relatedProblem}
  ${fragments.relatedTaxonomy}
`;
