import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_DOC = gql`
  # no row level permissions set here
  query GetDocument($id: uuid!) {
    MyDocuments(where: {id: {_eq: $id}}) {
      ...DocumentFields
      author {
        ...UserFields
      }
      invitations {
        id
        seenAt
        invitee {
          ...UserFields
        }
      }
      blocks {
        body
        architecture {
          id
          status
          title
          createdAt
          updatedAt
          publishedAt
          deletedAt
          submittedAt
          inReviewAt
          rejectedAt
          latestArch {
            id
            publishedAt
          }
          allPackages {
            ...PackageFields
            comments(order_by: {createdAt: asc}) {
              ...CommentFields
            }
            parent {
              id
            }
            referencePackage {
              id
            }
            taxonomy {
              id
            }
          }
        }
      }
    }
  }
  ${fragments.documentFields}
  ${fragments.packageFields}
  ${fragments.commentFields}
  ${fragments.userFields}
`;


export const GET_MY_DOCUMENTS = gql`
  query MyDocuments {
    MyDocuments(order_by: {updatedAt: asc}) {
      id
      title
      createdAt
      updatedAt
      deletedAt
      author {
        ...UserFields
      }
      blocks {
        body
        architecture {
          id
          status
          title
          packages {
            ...PreviewPackageFields
            packages {
              ...PreviewPackageFields
              packages {
                ...PreviewPackageFields
              }
            }
          }
        }
      }
    }
  }
  ${fragments.previewPackageFields}
  ${fragments.userFields}
`;

export const GET_SHARED_DOC = gql`
  # no row level permissions set here
  query GetSharedDocument($id: uuid!) {
    SharedDocumentsByID(id: $id) {
      ...SharedDocumentFields
      author {
        ...UserFields
      }
      invitations {
        id
        seenAt
        invitee {
          ...UserFields
        }
      }
      blocks {
        body
        architecture {
          id
          status
          title
          createdAt
          updatedAt
          publishedAt
          deletedAt
          submittedAt
          inReviewAt
          rejectedAt
          latestArch {
            id
            publishedAt
          }
          allPackages {
            ...PackageFields
            comments(order_by: {createdAt: asc}) {
              ...CommentFields
            }
            parent {
              id
            }
            referencePackage {
              id
            }
            taxonomy {
              id
            }
          }
        }
      }
    }
  }
  ${fragments.sharedDocumentFields}
  ${fragments.packageFields}
  ${fragments.commentFields}
  ${fragments.userFields}
`;

export const SHARED_DOCUMENTS = gql`
  query SharedDocuments {
    SharedDocuments {
      id
      title
      createdAt
      updatedAt
      author {
        ...UserFields
      }
      invitation {
        status
      }
      deletedAt
      blocks {
        body
        architecture {
          id
          status
          title
          packages {
            ...PreviewPackageFields
            packages {
              ...PreviewPackageFields
              packages {
                ...PreviewPackageFields
              }
            }
          }
        }
      }
    }
  }
  ${fragments.previewPackageFields}
  ${fragments.userFields}
`;


export const SAVE_DOCUMENT = gql`
  mutation SaveDocument(
    $id: uuid!,
    $title: String!
  ) {
    UpdateMyDocuments(
      where: {id: {_eq: $id}}, _set: {title: $title}
    ) {
      returning {
        id
        title
        updatedAt
      }
    }
  }
`;


export const ARCHIVE_DOCUMENT = gql`
  mutation ArchiveDocument($id: uuid!, $deletedAt: timestamp) {
    UpdateMyDocuments(where: {id: {_eq: $id}}, _set: {deletedAt: $deletedAt}) {
      returning {
        id
      }
    }
  }
`;

export const CREATE_DOCUMENT = gql`
  mutation CreateDocument($id: uuid) {
    InsertMyDocumentsOne(object: {
      id: $id
    }) {
      id
      createdAt
      deletedAt
      title
      document_blocks
    }
  }
`;
