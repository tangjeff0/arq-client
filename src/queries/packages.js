import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

export const GET_PACKAGE_REFERENCE_PACKAGE_ANCESTORS = gql`
  query GetPackageReferencePackage($id: uuid!) {
    PackagesByID(id: $id) {
      id
      referencePackage {
        id
        ancestors {
          id
          term
          architecture {
            title
          }
          parent {
            id
          }
        }
      }
    }
  }
`;

export const LINK_PACKAGE_TAXONOMY = gql`
  mutation LinkPackageTaxonomy($id: uuid!, $term: String!, $taxonomyId: uuid!) {
    UpdateByIDPackages(pk_columns: {id: $id}, _set: {term: $term, linkBroken: false, taxonomyId: $taxonomyId}) {
      id
    }
  }
`;

export const UNLINK_PACKAGE_TAXONOMY = gql`
  mutation UnlinkPackageTaxonomy($id: uuid!, $term: String!) {
    UpdateByIDPackages(pk_columns: {id: $id}, _set: {term: $term, linkBroken: false, taxonomyId: null}) {
      id
    }
  }
`;

// hasura: unused — no architecture results in search
export const LINK_PACKAGE_REFERENCE_PACKAGE = gql`
  mutation LinkPackageReferencePackage(
    $packageQID: String!
    $referencePackageQID: String!
  ) {
    linkReferenceToPackage(
      packageQID: $packageQID
      referencePackageQID: $referencePackageQID
    ) { qid }
  }
`;

// hasura: unused — no architecture results in search
export const UNLINK_PACKAGE_REFERENCE_PACKAGE = gql`
  mutation UnlinkPackageReferencePackage($packageQID: String!) {
    unlinkReferenceToPackage(packageQID: $packageQID) { qid }
  }
`;

export const DELETE_DOC_PACKAGE = gql`
  mutation SoftDeleteDocPackage($id: uuid!) {
    DeletePackages(where: {id: {_eq: $id}}) {
      returning {
        id
      }
    }
  }
`;

export const MOVE_PACKAGE_UP = gql`
  mutation MovePackageUp($packageID: uuid!) {
    InsertMutatePackageOne(object: {
      action: "moveUp",
      id: $packageID,
    }){
      id
    }
  }
`;

export const MOVE_PACKAGE_DOWN = gql`
  mutation MovePackageDown($packageID: uuid!) {
    InsertMutatePackageOne(object: {
      action: "moveDown",
      id: $packageID,
    }){
      id
    }
  }
`;

export const INSERT_PACKAGE_BEFORE = gql`
  mutation InsertPackageBefore(
    $packageID: uuid!
    $targetID: uuid!
  ) {
    InsertMutatePackageOne(object: {
      action: "insertBefore",
      id: $packageID,
      referencePackageId:$targetID,
    }){
      id
    }
  }
`;

export const INSERT_PACKAGE_AFTER = gql`
  mutation InsertPackageAfter(
    $packageID: uuid!
    $targetID: uuid!
  ) {
    InsertMutatePackageOne(object: {
      action: "insertAfter",
      id: $packageID,
      referencePackageId:$targetID,
    }){
      id
    }
  }
`;

export const INDENT_PACKAGE = gql`
  mutation IndentPackage($packageID: uuid!) {
    InsertMutatePackageOne(object: {
      action: "indent",
      id: $packageID,
    }){
      id
    }
  }
`;

export const UNINDENT_PACKAGE = gql`
  mutation UnindentPackage($packageID: uuid!) {
    InsertMutatePackageOne(object: {
      action: "unindent",
      id: $packageID,
    }){
      id
    }
  }
`;

export const TOGGLE_PACKAGE_CHILDREN = gql`
  mutation TogglePackageChildren(
    $packageID: uuid!,
  ) {
    InsertMutatePackageOne(object: {
      id: $packageID,
      action: "toggleChildren"
    }) {
      id
    }
  }
`;

export const UPDATE_DOC_PACKAGE = gql`
  mutation UpdateDocArchitecturePackage(
    $id: uuid!, $term: String!,
    $linkBroken: Boolean!, $description: String!) {
    # {
    #   "id": "8d4ea9e7-7506-448b-ba1b-bdb68a6da40c",
    #   "term": "B",
    #   "linkBroken": false,
    #   "description": "update desc"
    # }
    UpdateByIDPackages(
      pk_columns: {id: $id},
      _set: {description: $description, term: $term, linkBroken: $linkBroken}) {
      id
      architecture {
        updatedAt
      }
      description
      updatedAt
      term
      linkBroken
    }
  }
`;

export const MERGE_PACKAGE = gql`
  mutation MergePackages($objects: [arq_merge_packages_mutation_view_insert_input!]!) {
    # {
    #   {
    #     "id": "85ca62d0-487c-4954-8b9d-06ce8054a4cc",
    #     "taxonomy_id": "7a6ac5f8-5147-4b81-9d92-96930e67ad4d",
    #     "parent_id": null
    #   }
    # }
    InsertMergePackages(objects: $objects) {
      returning {
        id
      }
    }
  }
`;

export const REPLACE_PACKAGE = gql`
  mutation ReplacePackages($objects: [arq_replace_packages_mutation_view_insert_input!]!) {
    # {
    #   {
    #     "id": "85ca62d0-487c-4954-8b9d-06ce8054a4cc",
    #     "taxonomyId": "7a6ac5f8-5147-4b81-9d92-96930e67ad4d",
    #     "term": "Biology"
    #   }
    # }
    InsertReplacePackages(objects: $objects) {
      returning {
        id
      }
    }
  }
`;

export const INSERT_CHILD_PACKAGE = gql`
  mutation InsertPackageChild(
    $id: uuid!
  ) {
    UpdateByIDPackages(
      pk_columns: {id: $id}
      _set: {deletedAt: null}
    ) {
      id
    }
  }
`;
