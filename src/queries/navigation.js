import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

export const GET_NAVIGATION_STATE = gql`
  query OnboardingModuleShown {
    onboardingModuleShown @client
    toggleUserShown @client
  }
`;

export const GET_CONTROLS_POPUP_STATE = gql`
  query ControlsPopupState {
    controlsPopup @client {
      completed
      panel
    }
  }
`;

export const GET_ARCHITECTURE_POPUP_STATE = gql`
  query ControlsPopupState {
    controlsPopup @client {
      completed
    }
    architecturePopup @client {
      completed
      panel
    }
  }
`;

export const SET_ARCHITECTURE_POPUP = gql`
  mutation SetArchitecturePopupPanel($architecturePopup: Popup) {
    setArchitecturePopupPanel(architecturePopup: $architecturePopup) @client
  }
`;

export const SET_CONTROLS_POPUP = gql`
  mutation SetControlsPopupPanel($controlsPopup: Popup) {
    setControlsPopupPanel(controlsPopup: $controlsPopup) @client
  }
`;
