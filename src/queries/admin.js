import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_ADMIN_ARCHITECTURES = gql`
  # https://github.com/hasura/graphql-engine/issues/3772
  query GetAdminArchitectures($statuses: _text) {
    arq_architectures_statuses(args: {statuses: $statuses}, order_by: {submittedAt: desc}) {
      ...ArchitectureFields
      author {
        ...UserFields
      }
      allPackages {
        id
        taxonomy {
          id
        }
      }
    }
  }
  ${fragments.userFields}
  ${fragments.architectureFields}
`;

export const GET_ADMIN_ARCHITECTURE = gql`
  query GetAdminArchitecture($id: uuid!) {
    ArchitecturesByID(id: $id) {
      ...ArchitectureFields
      author {
      ...UserFields
      }
      publisher {
        ...UserFields
      }
      allPackages {
        id
        createdAt
        updatedAt
        term
        description
        sequence
        show
        linkBroken
        parent {
          id
          architecture {
            id
            status
          }
        }
        referencePackage {
          id
          architecture {
            id
            status
          }
        }
        taxonomy {
          id
        }
      }
    }
  }
  ${fragments.userFields}
  ${fragments.architectureFields}
`;

// update_arq_architectures mutation for admin because arch might not be "MyArchitecture"
export const UPDATE_ARCHITECTURE_STATUS_IN_REVIEW = gql`
  mutation UpdateArchitectureStatusInReview(
    $id: uuid!
    $inReviewAt: timestamp
  ) {
    UpdateArchitectures(_set: {inReviewAt: $inReviewAt}, where: {id: {_eq: $id}}) {
      returning {
        id
        status
        inReviewAt
        updatedAt
      }
    }
  }
`;

// update_arq_architectures mutation for admin because arch might not be "MyArchitecture"
export const UPDATE_ARCHITECTURE_STATUS_REJECTED = gql`
  mutation UpdateArchitectureStatusRejected(
    $id: uuid!
    $rejectedAt: timestamp
  ) {
    UpdateArchitectures(_set: {rejectedAt: $rejectedAt}, where: {id: {_eq: $id}}) {
      returning {
        id
        status
        rejectedAt
        updatedAt
      }
    }
  }
`;

export const PUBLISH_ARCHITECTURE = gql`
  mutation PublishArchitecture($object: arq_publish_architectures_mutation_view_insert_input!) {
    # query variables:
    # {
    #   "object": {
    #       "id": "a3136803-3633-4445-bd44-b9e8049f50ea"
    #   }
    # }
    InsertPublishArchitecturesOne(object: $object) {
      id
    }
  }
`;

export const ACCEPT_NEW_TERMS = gql`
  mutation AcceptNewTerms(
    $insertTerms: [arq_accept_terms_mutation_view_insert_input!]!
    $linkTerms: [arq_replace_packages_mutation_view_insert_input!]!
  ) {
    # query variables:
    # {
    #   "object": {
    #         "id": "99186803-3633-4445-bd44-b9e8049f50ea",
    #         "taxonomy_id": "1a33d7ed-21fd-48ec-a824-609acdfd8c8b",
    #         "terms":"{Research and analysis methods}"
    #       }
    # }
    InsertAcceptTerms(objects: $insertTerms) {
      returning {
        term
      }
    }
    InsertReplacePackages(objects: $linkTerms) {
      returning {
        term
      }
    }
  }
`;

export const GET_MERGE_STATE = gql`
  query GetMergeState {
    mergeState @client {
      target {
        id
        term
      }
    }
  }
`;
