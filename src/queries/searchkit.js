import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

export const GET_PROBLEM_SEARCH_SETTINGS = gql`
  query ProblemSearchSettings {
    problemSearch @client {
      filterOperator
      filterSort {
        term
        key
        direction
      }
    }
  }
`;

export const GET_COMPANY_SEARCH_SETTINGS = gql`
  query CompanySearchSettings {
    companySearch @client {
      filterOperator
      filterSort {
        term
        key
        direction
      }
    }
  }
`;
