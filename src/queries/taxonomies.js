import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_TAXONOMY_DETAILS = gql`
  query TaxonomyDetails($id: uuid!) {
    TaxonomiesByID(id: $id) {
      id
      term
      source {
        name
      }
      ancestors {
        id
        term
        parentId
        source {
          name
        }
      }
    }
  }
`;

export const GET_TAXONOMY_PROBLEMS = gql`
  query TaxonomyProblems($id: uuid!) {
    TaxonomiesByID(id: $id) {
      id
      term
      problemSets {
        ...ProblemCardFields
      }
    }
  }
  ${fragments.problemCardFields}
`;

export const GET_TAXONOMY_PROBLEMS_NAMES = gql`
  query TaxonomyProblemsNames($id: uuid!) {
    TaxonomiesByID(id: $id) {
      id
      problemSets {
        ...ProblemFields
        customer {
          ...CustomerFields
        }
      }
    }
  }
  ${fragments.problemFields}
  ${fragments.customerFields}
`;

export const GET_TAXONOMY_COMPANIES = gql`
  query TaxonomyCompanies($id: uuid!) {
    TaxonomiesByID(id: $id) {
      id
      term
      companies {
        ...CompanyCardFields
      }
    }
  }
  ${fragments.companyCardFields}
`;

export const GET_TAXONOMY_ARCHITECTURES = gql`
  query TaxonomyArchitectures($id: uuid!) {
    TaxonomiesByID(id: $id) {
      id
      referencedBy {
        id
        linkBroken
        architecture {
          ...ArchitectureCardFields
        }
      }
    }
  }
  ${fragments.architectureCardFields}
`;

export const GET_TAXONOMY_TREE = gql`
  query TaxonomyAllChildren($id: uuid!) {
    TaxonomiesByID(id: $id) {
      id
      term
      parentId
      source {
        name
      }
      ancestors(order_by: {term: asc}) {
        id
        term
        parentId
        source {
          name
        }
      }
      allChildren(order_by: {term: asc}) {
        id
        term
        parentId
      }
    }
  }
`;

export const GET_ALL_TAXONOMIES = gql`
  query GetAllTaxonomies {
    Taxonomies {
      id
      term
      ancestors {
        id
        term
      }
    }
  }
`;

export const UPDATE_TAXONOMY_TERM = gql`
  mutation UpdateTaxonomyTerm($id: uuid!, $term: String!) {
    update_arq_taxonomies_by_pk(pk_columns: {id: $id}, _set: {term: $term}) {
      id
    }
  }
`;

export const DELETE_TAXONOMY_FROM_CORE = gql`
  mutation SoftDeleteTaxonomyFromCore($coreIds: [uuid!]) {
    UpdateTaxonomies(_set: {deletedAt: "now()"}, where: {id: { _in: $coreIds }}) {
      returning {
        id
      }
    }
  }
`;


export const UPDATE_TAXONOMY_MOVE = gql`
  mutation UpdateTaxonomyTerm($id: uuid!, $parentId: uuid!) {
    update_arq_taxonomies_by_pk(pk_columns: {id: $id}, _set: {parentId: $parentId}) {
      id
    }
  }
`;

export const MERGE_INTO_CORE_TAXONOMY = gql`
  mutation MergeIntoCoreTaxonomy($id: uuid!, $coreTaxonomy: uuid!, $objects: [arq_insert_core_taxonomies_mutation_view_insert_input!]!) {
    SetCoreTaxonomiesOne(object: {id: $id, reference_id: $coreTaxonomy}) {
      id
    }
    InsertCoreTaxonomies(objects: $objects) {
      __typename
      returning {
        id
      }
    }
  }
`;

export const INSERT_UNDER_CORE_TAXONOMY = gql`
  mutation InsertUnderCoreTaxonomy($objects: [arq_insert_core_taxonomies_mutation_view_insert_input!]!) {
    # query variables:
    # {
    #   "object": {
    #       "id": "a3136803-3633-4445-bd44-b9e8049f50ea"
    #       "parent_id": "b3136803-3633-4445-bd44-b9e8049f50ea"
    #       "reference_id": "c3136803-3633-4445-bd44-b9e8049f50ea"
    #       "term": "Biology"
    #   }
    # }
    InsertCoreTaxonomies(objects: $objects) {
      returning {
        id
      }
    }
  }
`;
