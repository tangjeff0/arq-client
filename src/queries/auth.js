import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

// eslint-disable-next-line import/prefer-default-export
export const GET_AUTHENTICATED_USER = gql`
  query GetAuthenticatedUser {
    arq_authenticated_user {
      ...AuthenticatedUserFields
    }
  }
  ${fragments.authenticatedUserFields}
`;
