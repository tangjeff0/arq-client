import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

// TODO: Use constants/architectureStates instead of string values
// eslint-disable-next-line
export const GET_SIDEBAR_ITEMS = gql`
  query SidebarItems {
    arq_my_collaboration_invitations_aggregate(where: {seenAt: {_is_null: true}}) {
      aggregate {
        count(distinct: true)
      }
    }
    arq_architectures_statuses_aggregate(args: {statuses: "{submitted}"}) {
      aggregate {
        count(distinct: true)
      }
    }
  }
`;
