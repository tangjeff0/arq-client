import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_PUBLISHED_ARCHITECTURES = gql`
  query PublishedArchitectures {
    arq_published_architectures {
      id
      createdAt
      updatedAt
      rejectedAt
      publishedAt
      status
      title
      author {
        ...UserFields
      }
      publisher {
        ...UserFields
      }
      packages(
        where: {sequence: {_lte: "1"}}
        order_by: {sequence: asc}
      ) {
        ...PreviewPackageFields
        packages(
          where: {sequence: {_lte: "1"}}
          order_by: {sequence: asc}
        ) {
          ...PreviewPackageFields
          packages(
            where: {sequence: {_lte: "1"}}
            order_by: {sequence: asc}
          ) {
            ...PreviewPackageFields
          }
        }
      }
    }
  }
  ${fragments.previewPackageFields}
  ${fragments.userFields}
`;

export const GET_PUBLISHED_ARCHITECTURE = gql`
  query GetPublishedArchitecture($id: uuid!) {
    ArchitecturesByID(id: $id) {
      id
      latestArch {
        id
        createdAt
        publishedAt
        updatedAt
        status
        title
        author {
          ...UserFields
        }
        publisher {
          ...UserFields
        }
        allPackages {
          ...PackageFields
          parent {
            id
            architecture {
              id
              status
            }
          }
          referencePackage {
            id
            architecture {
              id
              status
            }
          }
          taxonomy {
            id
          }
        }
      }
    }
  }
  ${fragments.packageFields}
  ${fragments.userFields}
`;

export const UPDATE_ARCHITECTURE_STATUS_SUBMITTED = gql`
  mutation UpdateArchitectureStatusSubmitted(
    $id: uuid!
    $submittedAt: timestamp
  ) {
    # to update status: set rejectedAt, inReviewAt, submittedAt, draftAt
    UpdateArchitectures(_set: {submittedAt: $submittedAt}, where: {id: {_eq: $id}}) {
      returning {
        id
        status
        submittedAt
        updatedAt
      }
    }
  }
`;

export const UPDATE_ARCHITECTURE_STATUS_DRAFT = gql`
  mutation UpdateArchitectureStatusDraft(
    $id: uuid!
    $draftAt: timestamp
  ) {
    # to update status: set rejectedAt, inReviewAt, submittedAt, draftAt
    UpdateArchitectures(_set: {draftAt: $draftAt}, where: {id: {_eq: $id}}) {
      returning {
        id
        status
        draftAt
        updatedAt
      }
    }
  }
`;

export const UPDATE_ARCHITECTURE = gql`
  # can you only update architectures that belong to you?
  mutation UpdateArchitecture($id: uuid!, $title: String) {
    UpdateArchitectures(_set: {title: $title}, where: {id: {_eq: $id}}) {
      returning {
        id
        updatedAt
        title
      }
    }
  }
`;

export const GET_ARCHITECTURES_BY_CAPABILITIES = gql`
  query GetPublishedArchitecturesByCapabilities($capabilities: [uuid!]!) {
    Architectures(
      where: {
        publishedAt: {_is_null: false},
        nextVersionId: {_is_null: true},
        allPackages: {taxonomy: {id: {_in: $capabilities}}}
      }
    ) {
      ...ArchitectureCardFields
    }
  }
  ${fragments.architectureCardFields}
`;
