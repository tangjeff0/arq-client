import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

export const UPDATE_DOC_HTML_BLOCK = gql`
  mutation UpdateDocHTMLBlock($id: uuid, $document_blocks: _block!) {
    #   {
    #   "document_blocks": "{(\\,87536803-3633-4445-bd44-b9e8049f50ea)}"
    # }
    #   {
    #   "document_blocks": "{(some-text\\,)}"
    # }
    #   {
    #   "document_blocks": "{(some-text\\,),(some-more-text\\,)}"
    # }
    UpdateMyDocuments(_set: {document_blocks: $document_blocks}, where: {id: {_eq: $id}}) {
      returning {
        document_blocks
        blocks {
          architecture_id
          body
        }
      }
    }
  }
`;

export const DELETE_DOC_ARCHITECTURE_BLOCK = gql`
  mutation DeleteArchitectures($id: uuid!) {
    DeleteArchitectures(where: {id: {_eq: $id}}) {
      affected_rows
      returning {
        id
      }
    }
  }
`;

export const APPEND_DOC_ARCHITECTURE_BLOCK = gql`
  mutation AppendDocArchitectureBlockOne(
    $id: uuid!
    $architectureID: uuid!
    $packageID: uuid!
  ) {
    AppendArchitectureBlocksOne(object: {
      id: $id
      architecture_id: $architectureID
    }) {
      id
    }
    InsertPackagesOne(object: {
      id: $packageID
      architectureId: $architectureID
      sequence: 0
      show: true
    }) {
      id
    }
  }
`;
