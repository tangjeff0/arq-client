import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

export const GET_SOURCES = gql`
  query Source {
    TaxonomySources {
      name
    }
  }
`;

export const GET_SOURCE_CHILDREN = gql`
  query SourceChildren($sourceName: String) {
    TaxonomySources(where: {name: {_eq: $sourceName}}) {
      name
      roots {
        id
        term
        source {
          name
        }
      }
    }
  }
`;

export const GET_SOURCE_TREE = gql`
  query SourceTree($sourceName: String) {
    TaxonomySources(where: {name: {_eq: $sourceName}}) {
      name
      roots(order_by: {term: asc}) {
        id
        term
        allChildren(order_by: {term: asc}) {
          id
          term
          parentId
        }
      }
    }
  }
`;

export const GET_SOURCE_TAB_SELECTED_INDEX = gql`
  query SourceTabSelectedIndex {
    selectedSourceIndex @client
  }
`;

export const GET_TAXONOMY_SOURCE_REFERENCED_TAXONOMIES = gql`
  query Sources($source_in: [String!]!) {
    TaxonomySources(where: {name: {_in: $source_in}}) {
      referencedTaxonomyRoots {
          id
          term
          source {
            name
          }
          coreTaxonomy {
            id
            term
          }
          referencedBy {
            architecture {
              isLatestArch
            }
          }
       }
    }
  }
`;
