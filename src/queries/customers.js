import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_CUSTOMER = gql`
  query Customer($id: uuid!) {
    CustomersByID(id: $id) {
      id
      name
      description
    }
  }
`;

export const GET_CUSTOMER_ARCHITECTURES = gql`
  query CustomerArchitectures($id: uuid!) {
    CustomersByID(id: $id) {
      id
      architectures {
        ...ArchitectureCardFields
      }
    }
  }
  ${fragments.architectureCardFields}
`;

export const GET_CUSTOMER_COMPANIES = gql`
  query CustomerCompanies($id: uuid!) {
    CustomersByID(id: $id) {
      id
      companies {
        ...CompanyCardFields
      }
    }
  }
  ${fragments.companyCardFields}
`;

export const GET_CUSTOMER_PROBLEMS = gql`
  query CustomerProblems($id: uuid!) {
    CustomersByID(id: $id) {
      id
      problemSets  {
        ...ProblemCardFields
      }
    }
  }
  ${fragments.problemCardFields}
`;

export const GET_CUSTOMER_TAXONOMIES = gql`
  # not all problemSets are being imported? ps.qid='c699d6de-43af-11ea-808d-0242c0a82003'
  query CustomerTaxonomies($id: uuid!) {
    CustomersByID(id: $id) {
      id
      capabilities {
        ...TaxonomyCardFields
      }
    }
  }
  ${fragments.taxonomyCardFields}
`;

export const GET_CUSTOMERS = gql`
  query Customer {
    Customers(order_by: {name: asc}) {
      id
      name
      description
    }
  }
`;
