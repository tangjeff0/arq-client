import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

export const GET_DOC_COLLABORATORS = gql`
  query GetDocCollaborators($id: uuid!) {
    Users(order_by: { email: asc }) {
      email
    }
    MyDocuments(where: {id: {_eq: $id}}) {
      id
      invitations {
        status
        invitee {
          email
        }
      }
    }
  }
`;

export const INVITE_COLLABORATORS = gql`
  mutation InviteCollaborators($objects: [arq_collaboration_invitations_insert_input!]!) {
    # query variables:
    # {
    #   "objects": [
    #       {
    #         "documentId": "061a2e30-56d9-4c4b-80bb-4947d7c4636e",
    #         "collaboratorEmail": "jim@iqt.org"
    #       },
    #       {
    #         "documentId": "061a2e30-56d9-4c4b-80bb-4947d7c4636e",
    #         "collaboratorEmail": "mike@iqt.org"
    #       }
    #     ]
    # }
    InsertCollaborationInvitations(
      objects: $objects) {
      returning {
        collaboratorEmail
        documentId
        id
      }
    }
  }
`;

export const UPDATE_COLLABORATION_INVITATION = gql`
  mutation UpdateCollaborationInvitation($id: uuid!, $seenAt: timestamp) {
    # query variables:
    # {
    #    "id": "ac25c140-c55c-11ea-a6c5-0242c0a82004",
    #    "seenAt": "now()"
    # }
    UpdateMyCollaborationInvitations(where: {id: {_eq: $id}}, _set: {seenAt: $seenAt}) {
      returning {
        status
      }
    }
  }
`;
