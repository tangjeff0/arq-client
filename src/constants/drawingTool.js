export const THEME_BRAND = 'BRAND';
export const THEME_ARQ = 'ARQ';
export const THEME_GREENERY = 'GREENERY';

export const POSITION_DEFAULT = 0;
export const POSITION_LEFT = 1;
export const POSITION_RIGHT = 2;

export const TITLE_POSITION_DEFAULT = 0;
export const TITLE_POSITION_LEFT = 1;
export const TITLE_POSITION_RIGHT = 2;

export const COLUMNS = 12;

export const ROW_HEIGHT = 60;
export const MAX_ROWS = 10;

export const WIDTH_DEFAULT = 3;
export const HEIGHT_DEFAULT = 3;

export const itemProps = {
  minW: 1,
  maxW: COLUMNS,
  minH: 1,
  maxH: MAX_ROWS,
};

export const themes = [
  {
    name: THEME_BRAND,
    swatches: [
      {
        name: 'THEME_COLOR_1', // primary blue
        fill: '#213964',
      },
      {
        name: 'THEME_COLOR_2', // accent teal
        fill: '#92CDCF',
      },
      {
        name: 'THEME_COLOR_3', // accent blue
        fill: '#445878',
      },
      {
        name: 'THEME_COLOR_4', // mid grey
        fill: '#A2B1B2',
      },
      {
        name: 'THEME_COLOR_5', // light grey
        fill: '#D1E5E6',
      },
      {
        name: 'ACCENT', // primary red
        fill: '#CC2228',
      },
      {
        name: 'DISABLED',
        fill: '#CAD2D3',
      },
    ],
  },
  {
    name: THEME_ARQ,
    swatches: [
      {
        name: 'THEME_COLOR_1',
        fill: '#5d5c6b',
      },
      {
        name: 'THEME_COLOR_2',
        fill: '#87999b',
      },
      {
        name: 'THEME_COLOR_3',
        fill: '#cad2d3',
      },
      {
        name: 'THEME_COLOR_4',
        fill: '#dae0e1',
      },
      {
        name: 'THEME_COLOR_5',
        fill: '#edf1f2',
      },
      {
        name: 'ACCENT',
        fill: '#6a71d7',
      },
      {
        name: 'DISABLED',
        fill: '#CAD2D3',
      },
    ],
  },
  {
    name: THEME_GREENERY,
    swatches: [
      {
        name: 'THEME_COLOR_1', // dark blue
        fill: '#2C5066',
      },
      {
        name: 'THEME_COLOR_2', // dark grey
        fill: '#708384',
      },
      {
        name: 'THEME_COLOR_3', // mid grey
        fill: '#A2B1B2',
      },
      {
        name: 'THEME_COLOR_4', // light grey
        fill: '#D1E5E6',
      },
      {
        name: 'THEME_COLOR_5', // super light grey
        fill: '#EAF3F3',
      },
      {
        name: 'ACCENT', // blue
        fill: '#457B9D',
      },
      {
        name: 'DISABLED',
        fill: '#CAD2D3',
      },
    ],
  },
];

export const swatchSize = 21;

export const swatchCount = themes[0].swatches.length;
