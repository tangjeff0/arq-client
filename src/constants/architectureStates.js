export const ARCHITECTURE_DRAFT = 'draft';
export const ARCHITECTURE_SUBMITTED = 'submitted';
export const ARCHITECTURE_IN_REVIEW = 'inreview';
export const ARCHITECTURE_REJECTED = 'rejected';
export const ARCHITECTURE_PUBLISHED = 'published';
