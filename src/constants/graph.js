import { getProblemShortname } from 'utils/problem';
import {
  ARCHITECTURE_TYPENAME,
  TAXONOMY_TYPENAME,
  PACKAGE_TYPENAME,
  PROBLEM_TYPENAME,
  COMPANY_TYPENAME,
  CUSTOMER_TYPENAME,
} from 'constants/gql';

export const FORCE = 'cose-bilkent';
export const CIRCLE = 'circle';
export const TREE = 'breadthfirst';
export const GRID = 'grid';

export const layouts = {
  Tree: TREE,
  Force: FORCE,
  Circle: CIRCLE,
  Grid: GRID,
};

export const nodeTypes = {
  [ARCHITECTURE_TYPENAME]: {
    displayLabel: 'Architecture',
    getContent: a => a.title,
    classes: 'rectangle color-0',
  },
  [PACKAGE_TYPENAME]: {
    displayLabel: 'Term',
    getContent: p => p.term,
    classes: 'roundrectangle color-0',
  },
  [TAXONOMY_TYPENAME]: {
    displayLabel: 'Capability',
    getContent: t => t.term,
    classes: 'ellipse color-2',
  },
  [PROBLEM_TYPENAME]: {
    displayLabel: 'Customer Problem',
    getContent: p => getProblemShortname(p),
    classes: 'barrel color-4',
  },
  [COMPANY_TYPENAME]: {
    displayLabel: 'Company',
    getContent: c => c.name,
    classes: 'diamond color-3',
  },
  [CUSTOMER_TYPENAME]: {
    displayLabel: 'Customer',
    getContent: c => c.name,
    classes: 'diamond color-3',
  },
};
