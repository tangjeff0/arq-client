// These could be changed to objects to simplify the creation of
// cache redirects in /apollo/index.js
// Something like this:
//
// const types = {
//   Package: {
//     typename: 'arq_packages',
//     queryRoot: 'PackagesByID',
//     listQueryRoot: 'Packages',
//     parentFields: ['packages', 'allPackages']
//   }
// }
// Note: upgrading to apollo v3 would change redirect behavior
export const ARCHITECTURE_QUERY_ROOT = 'ArchitecturesByID';
export const COMPANY_QUERY_ROOT = 'OrganizationsByID';
export const CUSTOMER_QUERY_ROOT = 'CustomersByID';
export const DOCUMENT_QUERY_ROOT = 'DocumentsByID';
export const SHARED_DOCUMENT_QUERY_ROOT = 'SharedDocumentsByID';
export const PACKAGE_QUERY_ROOT = 'PackagesByID';
export const PROBLEM_QUERY_ROOT = 'ProblemSetsByID';
export const TAXONOMY_QUERY_ROOT = 'TaxonomiesByID';

export const ARCHITECTURES_QUERY_ROOT = 'Architectures';
export const COMPANIES_QUERY_ROOT = 'Organizations';
export const CUSTOMERS_QUERY_ROOT = 'Customers';
export const MY_DOCUMENTS_QUERY_ROOT = 'MyDocuments';
export const SHARED_DOCUMENTS_QUERY_ROOT = 'SharedDocuments';
export const USERS_QUERY_ROOT = 'Users';
export const SOURCES_QUERY_ROOT = 'TaxonomySources';
export const ANNOTATIONS_QUERY_ROOT = 'AnnotationSpecs';
export const PROBLEMS_QUERY_ROOT = 'ProblemSets';
export const TAXONOMIES_QUERY_ROOT = 'Taxonomies';

export const ARCHITECTURE_TYPENAME = 'arq_architectures';
export const PUBLISHED_ARCHITECTURE_TYPENAME = 'arq_published_architectures';
export const TAXONOMY_TYPENAME = 'arq_taxonomies';
export const PACKAGE_TYPENAME = 'arq_packages';
export const PACKAGE_COMMENT_TYPENAME = 'arq_package_comments';
export const PROBLEM_TYPENAME = 'arq_problem_sets';
export const COMPANY_TYPENAME = 'arq_organizations';
export const CUSTOMER_TYPENAME = 'arq_customers';
export const PACKAGE_ANNOTATION_TYPENAME = 'arq_package_annotations';
export const ARCHITECTURE_STATUSES_TYPENAME = 'arq_architectures_statuses';
