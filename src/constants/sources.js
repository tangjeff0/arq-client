export const SOURCE_ACM = 'ACM';
export const SOURCE_IAS = 'IAS';
export const SOURCE_IEEE = 'IEEE';
export const SOURCE_PLOS = 'PLOS';
export const SOURCE_ARQ = SOURCE_ARQ_CODE;
export const SOURCE_MISSION = 'Mission';

export const sourceNames = {
  [SOURCE_ACM]: 'ACM',
  [SOURCE_IAS]: 'IaS',
  [SOURCE_IEEE]: 'IEEE',
  [SOURCE_PLOS]: 'PLoS',
  [SOURCE_ARQ]: SOURCE_ARQ_NAME,
  [SOURCE_MISSION]: 'Mission',
};
