import * as docServices from 'services/docs';
import * as packageServices from 'services/packages';
import * as architectureServices from 'services/architectures';
import * as blockServices from 'services/blocks';

import { composeDocFromState } from 'selectors/doc';
import { patchArchitectureStatusDraft } from 'actions/architecture';
import { ARCHITECTURE_REJECTED } from 'constants/architectureStates';
import { SEARCH_PREFIX } from 'constants/search';
import { MY_DOCUMENTS_QUERY_ROOT } from 'constants/gql';
import { createArchitectureBlock, createHTMLBlock } from 'utils/blocks';
import {
  createEmptyPackage,
  findParentNode,
  findPackageInTree,
  packageToggleChildren as packageToggleChildrenHelper,
} from 'utils/tree';
import { get, debounce } from 'lodash';

export const INIT_DOC = 'INIT_DOC';
export const GET_DOC = 'GET_DOC';
export const GET_NEW_DOC = 'GET_NEW_DOC';
export const GET_DOC_SUCCESS = 'GET_DOC_SUCCESS';
export const GET_DOC_ERROR = 'GET_DOC_ERROR';
export const RESET_DOC = 'RESET_DOC';
export const CREATE_DOC = 'CREATE_DOC';
export const CREATE_DOC_CREATING = 'CREATE_DOC_CREATING';
export const CREATE_DOC_CANCELLED = 'CREATE_DOC_CANCELLED';
export const CREATE_DOC_SUCCESS = 'CREATE_DOC_SUCCESS';
export const CREATE_DOC_ERROR = 'CREATE_DOC_ERROR';
export const SAVE_DOC = 'SAVE_DOC';
export const SAVE_DOC_SAVING = 'SAVE_DOC_SAVING';
export const SAVE_DOC_SUCCESS = 'SAVE_DOC_SUCCESS';
export const SAVE_DOC_ERROR = 'SAVE_DOC_ERROR';
export const SAVE_DOC_CANCELLED = 'SAVE_DOC_CANCELLED';
export const CANCEL_DOC_UPDATES = 'CANCEL_DOC_SAVING';

export const UPDATE_DOC_TITLE = 'UPDATE_DOC_TITLE';
export const UPDATE_DOC_TITLE_SUCCESS = 'UPDATE_DOC_TITLE_SUCCESS';
export const UPDATE_DOC_TITLE_ERROR = 'UPDATE_DOC_TITLE_ERROR';

export const INSERT_DOC_ARCHITECTURE_BLOCK = 'INSERT_DOC_ARCHITECTURE_BLOCK';
export const INSERT_DOC_ARCHITECTURE_BLOCK_SUCCESS = 'INSERT_DOC_ARCHITECTURE_BLOCK_SUCCESS';
export const INSERT_DOC_ARCHITECTURE_BLOCK_ERROR = 'INSERT_DOC_ARCHITECTURE_BLOCK_ERROR';

export const SET_DOC_ACTIVE_EDITOR_QID = 'SET_DOC_ACTIVE_EDITOR_QID';
export const UNSET_DOC_ACTIVE_EDITOR_QID = 'UNSET_DOC_ACTIVE_EDITOR_QID';
export const UPDATE_DOC_EDITOR = 'UPDATE_DOC_EDITOR';

export const SET_DOC_ACTIVE_ARCHITECTURE_QID = 'SET_DOC_ACTIVE_ARCHITECTURE_QID';
export const UNSET_DOC_ACTIVE_ARCHITECTURE_QID = 'UNSET_DOC_ACTIVE_ARCHITECTURE_QID';

export const UPDATE_DOC_ARCHITECTURE = 'UPDATE_DOC_ARCHITECTURE';
export const UPDATE_DOC_ARCHITECTURE_SUCCESS = 'UPDATE_DOC_ARCHITECTURE_SUCCESS';
export const UPDATE_DOC_ARCHITECTURE_ERROR = 'UPDATE_DOC_ARCHITECTURE_ERROR';

export const SET_DOC_ACTIVE_PACKAGE_QID = 'SET_DOC_ACTIVE_PACKAGE_QID';
export const UNSET_DOC_ACTIVE_PACKAGE_QID = 'UNSET_DOC_ACTIVE_PACKAGE_QID';

export const UPDATE_DOC_ARCHITECTURE_PACKAGE = 'UPDATE_DOC_ARCHITECTURE_PACKAGE';
export const UPDATE_DOC_ARCHITECTURE_PACKAGE_SUCCESS = 'UPDATE_DOC_ARCHITECTURE_PACKAGE_SUCCESS';
export const UPDATE_DOC_ARCHITECTURE_PACKAGE_BE_SUCCESS = 'UPDATE_DOC_ARCHITECTURE_PACKAGE_BE_SUCCESS';
export const UPDATE_DOC_ARCHITECTURE_PACKAGE_ERROR = 'UPDATE_DOC_ARCHITECTURE_PACKAGE_ERROR';

export const DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION = 'DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION';
export const DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_SUCCESS = 'DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_SUCCESS';
export const DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_ERROR = 'DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_ERROR';

export const LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY = 'LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY';
export const LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS = 'LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS';
export const LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR = 'LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR';

export const UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY = 'UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY';
export const UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS = 'UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS';
export const UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR = 'UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR';

export const REVERT_DOC_ARCHITECTURE_TAXONOMY = 'REVERT_DOC_ARCHITECTURE_TAXONOMY';
export const REVERT_DOC_ARCHITECTURE_TAXONOMY_SUCCESS = 'REVERT_DOC_ARCHITECTURE_TAXONOMY_SUCCESS';
export const REVERT_DOC_ARCHITECTURE_TAXONOMY_ERROR = 'REVERT_DOC_ARCHITECTURE_TAXONOMY_ERROR';

export const DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE = 'DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE';
export const DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE_SUCCESS = 'DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE_SUCCESS';
export const DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE_ERROR = 'DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE_ERROR';

export const DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE = 'DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE';
export const DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE_SUCCESS = 'DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE_SUCCESS';
export const DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE_ERROR = 'DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE_ERROR';

export const MODIFY_DOC_ARCHITECTURE_BLOCK_ERROR = 'MODIFY_DOC_ARCHITECTURE_BLOCK_ERROR';
export const MODIFY_DOC_ARCHITECTURE_BLOCK_SUCCESS = 'MODIFY_DOC_ARCHITECTURE_BLOCK_SUCCESS';

export const MOVE_DOC_ARCHITECTURE_PACKAGE_UP = 'MOVE_DOC_ARCHITECTURE_PACKAGE_UP';
export const MOVE_DOC_ARCHITECTURE_PACKAGE_UP_ERROR = 'MOVE_DOC_ARCHITECTURE_PACKAGE_UP_ERROR';
export const MOVE_DOC_ARCHITECTURE_PACKAGE_UP_SUCCESS = 'MOVE_DOC_ARCHITECTURE_PACKAGE_UP_SUCCESS';

export const MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN = 'MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN';
export const MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_ERROR = 'MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_ERROR';
export const MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_SUCCESS = 'MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_SUCCESS';

export const INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER = 'INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER';
export const INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_SUCCESS = 'INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_SUCCESS';
export const INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_ERROR = 'INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_ERROR';

export const INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE = 'INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE';
export const INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_SUCCESS = 'INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_SUCCESS';
export const INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_ERROR = 'INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_ERROR';

export const INDENT_DOC_ARCHITECTURE_PACKAGE = 'INDENT_DOC_ARCHITECTURE_PACKAGE';
export const INDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS = 'INDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS';
export const INDENT_DOC_ARCHITECTURE_PACKAGE_ERROR = 'INDENT_DOC_ARCHITECTURE_PACKAGE_ERROR';

export const UNINDENT_DOC_ARCHITECTURE_PACKAGE = 'UNINDENT_DOC_ARCHITECTURE_PACKAGE';
export const UNINDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS = 'UNINDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS';
export const UNINDENT_DOC_ARCHITECTURE_PACKAGE_ERROR = 'UNINDENT_DOC_ARCHITECTURE_PACKAGE_ERROR';

export const REMOVE_DOC_ARCHITECTURE_PACKAGE = 'REMOVE_DOC_ARCHITECTURE_PACKAGE';
export const REMOVE_DOC_ARCHITECTURE_PACKAGE_SUCCESS = 'REMOVE_DOC_ARCHITECTURE_PACKAGE_SUCCESS';
export const REMOVE_DOC_ARCHITECTURE_PACKAGE_ERROR = 'REMOVE_DOC_ARCHITECTURE_PACKAGE_ERROR';

export const REMOVE_DOC_ARCHITECTURE = 'REMOVE_DOC_ARCHITECTURE';
export const REMOVE_DOC_ARCHITECTURE_SUCCESS = 'REMOVE_DOC_ARCHITECTURE_SUCCESS';
export const REMOVE_DOC_ARCHITECTURE_ERROR = 'REMOVE_DOC_ARCHITECTURE_ERROR';

export const TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN = 'TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN';
export const TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_ERROR = 'TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_ERROR';
export const TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_BY_LEVEL = 'TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_BY_LEVEL';

export const LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE = 'LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE';
export const LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR = 'LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR';
export const LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS = 'LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS';

export const UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE = 'UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE';
export const UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR = 'UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR';
export const UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS = 'UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS';

export const UNDO_ARCHITECTURE_ACTION = 'UNDO_ARCHITECTURE_ACTION';
export const UNDO_ARCHITECTURE_ACTION_ERROR = 'UNDO_ARCHITECTURE_ACTION_ERROR';

export const UNDO_MERGE_PACKAGE = 'UNDO_MERGE_PACKAGE';
export const UNDO_INDENT_PACKAGE = 'UNDO_INDENT_PACKAGE';
export const UNDO_UNINDENT_PACKAGE = 'UNDO_UNINDENT_PACKAGE';
export const UNDO_MODIFY_PACKAGE = 'UNDO_MODIFY_PACKAGE';
export const UNDO_INSERT_PACKAGE = 'UNDO_INSERT_PACKAGE';
export const UNDO_DELETE_PACKAGE = 'UNDO_DELETE_PACKAGE';
export const UNDO_MOVE_PACKAGE_UP = 'UNDO_MOVE_PACKAGE_UP';
export const UNDO_MOVE_PACKAGE_DOWN = 'UNDO_MOVE_PACKAGE_DOWN';
export const UNDO_DELETE_PACKAGE_ONLY_CHILD = 'UNDO_DELETE_PACKAGE_ONLY_CHILD';
export const UNDO_DELETE_PACKAGE_AFTER = 'UNDO_DELETE_PACKAGE_AFTER';
export const UNDO_DELETE_PACKAGE_BEFORE = 'UNDO_DELETE_PACKAGE_BEFORE';
export const UNDO_LINK_TAXONOMY_TO_PACKAGE = 'UNDO_LINK_TAXONOMY_TO_PACKAGE';
export const UNDO_UNLINK_TAXONOMY_TO_PACKAGE = 'UNDO_UNLINK_TAXONOMY_TO_PACKAGE';
export const UNDO_LINK_REFERENCE_TO_PACKAGE = 'UNDO_LINK_REFERENCE_TO_PACKAGE';
export const UNDO_UNLINK_REFERENCE_TO_PACKAGE = 'UNDO_UNLINK_REFERENCE_TO_PACKAGE';

/** doc initializers, getters, setters, and persistent data actions */

export function initDoc() {
  return {
    type: INIT_DOC,
  };
}

export function getDoc(qid) {
  return dispatch => docServices.getDoc(qid)
    .then((data) => {
      const doc = get(data, [MY_DOCUMENTS_QUERY_ROOT, 0]);

      if (doc === null) {
        dispatch(getDocError({ status: 404 })); // temp fix — see DocumentInterface
        return false;
      }

      dispatch(getDocSuccess(doc));

      // Check all architectures for status rejected, flip to draft if possible
      doc.blocks.forEach((block) => {
        if (
          block.architecture
            && block.architecture.status === ARCHITECTURE_REJECTED
        ) {
          dispatch(patchArchitectureStatusDraft(block.architecture.id));
        }
      });

      return doc.author;
    })
    .catch((error) => {
      dispatch(getDocError(error));
    });
}

export function getNewDoc() {
  return {
    type: GET_NEW_DOC,
  };
}

export function getDocSuccess(doc) {
  return {
    type: GET_DOC_SUCCESS,
    doc,
  };
}

export function getDocError(error) {
  return {
    type: GET_DOC_ERROR,
    error,
  };
}

export function resetDoc() {
  return {
    type: RESET_DOC,
  };
}

export function saveDoc() {
  return (dispatch, getState) => {
    if (!getState().activeDoc.ui.cancelled) {
      dispatch(saveDocSaving());

      const doc = composeDocFromState(getState());

      docServices
        .saveDoc(doc)
        .then(() => {
          dispatch(saveDocSuccess(doc));
        })
        .catch((error) => {
          dispatch(saveDocError(error));
        });
    }
  };
}

export function saveDocSaving() {
  return {
    type: SAVE_DOC_SAVING,
  };
}

export function saveDocSuccess(doc) {
  return {
    type: SAVE_DOC_SUCCESS,
    doc,
  };
}

export function saveDocError(error) {
  return {
    type: SAVE_DOC_ERROR,
    error,
  };
}

export function saveDocCancelled() {
  return {
    type: SAVE_DOC_CANCELLED,
  };
}

/** top level doc actions */

const debouncedUpdateDocTitle = debounce((dispatch, doc) => {
  dispatch(saveDocSaving());

  docServices
    .saveDoc(doc)
    .then(() => {
      dispatch({
        type: UPDATE_DOC_TITLE_SUCCESS,
      });
    })
    .catch((error) => {
      dispatch({
        type: UPDATE_DOC_TITLE_ERROR,
        error,
      });
    });
}, 1000);

export function updateDocTitle(title) {
  return (dispatch, getState) => {
    dispatch({
      type: UPDATE_DOC_TITLE,
      title,
    });

    debouncedUpdateDocTitle(dispatch, {
      ...getState().activeDoc,
      title,
    });
  };
}

/** doc block actions */
export function insertDocArchitectureBlock() {
  return async (dispatch, getState) => {
    if (!getState().activeDoc.ui.cancelled) {
      dispatch(saveDocSaving());

      const doc = composeDocFromState(getState());

      const newArchitectureBlock = createArchitectureBlock();
      const newHTMLBlock = createHTMLBlock();

      dispatch({
        type: INSERT_DOC_ARCHITECTURE_BLOCK,
        architectureBlock: newArchitectureBlock,
        htmlBlock: newHTMLBlock,
      });

      await blockServices
        .appendArchitectureBlock(
          doc.id,
          newArchitectureBlock.architecture.id,
          newArchitectureBlock.architecture.packages[0].id,
        )
        .then(() => {
          dispatch({
            type: INSERT_DOC_ARCHITECTURE_BLOCK_SUCCESS,
          });
        })
        .catch((error) => {
          dispatch({
            type: INSERT_DOC_ARCHITECTURE_BLOCK_ERROR,
            err: error,
          });
        });
    }
  };
}

const debouncedModifyBlock = debounce((dispatch, docQID, blocks) => {
  dispatch(saveDocSaving());

  blockServices
    .modifyBlock(docQID, blocks)
    .then(() => {
      dispatch({
        type: MODIFY_DOC_ARCHITECTURE_BLOCK_SUCCESS,
      });
    })
    .catch(() => {
      dispatch({
        type: MODIFY_DOC_ARCHITECTURE_BLOCK_ERROR,
      });
    });
}, 1000);

export function updateDocHTMLBlock() {
  return (dispatch, getState) => {
    if (!getState().activeDoc.ui.cancelled) {
      const { id, blocks } = composeDocFromState(getState());

      debouncedModifyBlock(dispatch, id, blocks);
    }
  };
}

/** doc editor(s) actions */

export function setDocActiveEditorQID(qid) {
  return {
    type: SET_DOC_ACTIVE_EDITOR_QID,
    qid,
  };
}

export function unsetDocActiveEditorQID() {
  return {
    type: UNSET_DOC_ACTIVE_EDITOR_QID,
  };
}

export function updateDocEditor(qid, editorState) {
  return {
    type: UPDATE_DOC_EDITOR,
    qid,
    editorState,
  };
}

/** doc architecture(s) actions */

export function setDocActiveArchitectureQID(qid) {
  return {
    type: SET_DOC_ACTIVE_ARCHITECTURE_QID,
    qid,
  };
}

export function unsetDocActiveArchitectureQID() {
  return {
    type: UNSET_DOC_ACTIVE_ARCHITECTURE_QID,
  };
}

const debouncedUpdateArchitecture = debounce((dispatch, docQID, architecture) => {
  dispatch(saveDocSaving());

  architectureServices
    .updateArchitecture(docQID, architecture)
    .then(() => {
      dispatch({
        type: UPDATE_DOC_ARCHITECTURE_SUCCESS,
      });
    })
    .catch(() => {
      dispatch({
        type: UPDATE_DOC_ARCHITECTURE_ERROR,
      });
    });
}, 1000);

export function updateDocArchitecture(architecture) {
  return (dispatch, getState) => {
    if (!getState().activeDoc.ui.cancelled) {
      const doc = composeDocFromState(getState());

      dispatch({
        type: UPDATE_DOC_ARCHITECTURE,
        architecture,
      });

      debouncedUpdateArchitecture(dispatch, doc.id, architecture);
    }
  };
}

export function removeDocArchitecture(blockQID, architecture) {
  return (dispatch, getState) => {
    if (!getState().activeDoc.ui.cancelled) {
      dispatch(saveDocSaving());

      dispatch({
        type: REMOVE_DOC_ARCHITECTURE,
        blockQID,
      });

      blockServices
        .deleteArchitectureBlock(architecture.id)
        .then(() => {
          dispatch({
            type: REMOVE_DOC_ARCHITECTURE_SUCCESS,
          });
        })
        .catch(() => {
          dispatch({
            type: REMOVE_DOC_ARCHITECTURE_ERROR,
          });
        });
    }
  };
}

/** doc architecture packages stuff */

export function setDocActivePackageQID(qid) {
  return {
    type: SET_DOC_ACTIVE_PACKAGE_QID,
    qid,
  };
}

export function unsetDocActivePackageQID() {
  return {
    type: UNSET_DOC_ACTIVE_PACKAGE_QID,
  };
}

export function resetDocArchitecturePackage(packageQID) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const architectureQID = doc.ui.activeArchitectureQID;
    const { entities } = doc;
    const architecture = entities.architectures[architectureQID];
    const pkg = findPackageInTree(architecture, packageQID);
    const hasSearchPrefix = pkg.term.indexOf(SEARCH_PREFIX) === 0;

    const updatePkg = {
      ...pkg,
      term: pkg.term.substr(SEARCH_PREFIX.length),
    };

    const removePrefix = () => {
      dispatch(updateDocArchitecturePackage(updatePkg));
      dispatch(updateDocArchitecturePackageBE(updatePkg));
    };

    if (hasSearchPrefix) {
      if (!pkg.taxonomy) {
        removePrefix();
      } else {
        dispatch(unlinkDocArchitecturePackageTaxonomy(packageQID)).then(
          () => removePrefix(),
        );
      }
    } else if (pkg.taxonomy) {
      dispatch(unlinkDocArchitecturePackageTaxonomy(packageQID));
    }
  };
}

export function updateDocArchitecturePackage(pkg) {
  return (dispatch) => {
    dispatch({
      type: UPDATE_DOC_ARCHITECTURE_PACKAGE,
      pkg,
    });
  };
}

export function updateDocArchitecturePackageBE(pkg) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;

    dispatch(saveDocSaving());

    dispatch({
      type: UPDATE_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
      action: 'modifyPackage',
      pkg,
      activeArchitectureQID,
      activePackageQID,
    });

    packageServices
      .modifyPackage(pkg, activeArchitectureQID)
      .then(() => {
        dispatch({
          type: UPDATE_DOC_ARCHITECTURE_PACKAGE_BE_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          type: UPDATE_DOC_ARCHITECTURE_PACKAGE_ERROR,
          error,
        });
      });
  };
}

export function moveDocArchitecturePackageUp() {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      type: MOVE_DOC_ARCHITECTURE_PACKAGE_UP,
      packageQID: activePackageQID,
      architectureQID: activeArchitectureQID,
    });

    // Send out API request to update backend
    packageServices
      .movePackageUp(activePackageQID, activeArchitectureQID)
      .then(() => {
        dispatch({
          type: MOVE_DOC_ARCHITECTURE_PACKAGE_UP_SUCCESS,
          action: 'movePackageUp',
          activeArchitectureQID,
          activePackageQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: MOVE_DOC_ARCHITECTURE_PACKAGE_UP_ERROR,
          error: err,
        });
      });
  };
}

export function moveDocArchitecturePackageDown() {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;

    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      type: MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN,
      packageQID: activePackageQID,
      architectureQID: activeArchitectureQID,
    });

    packageServices
      .movePackageDown(activePackageQID, activeArchitectureQID)
      .then(() => {
        dispatch({
          type: MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_SUCCESS,
          action: 'movePackageDown',
          activePackageQID,
          activeArchitectureQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_ERROR,
          error: err,
        });
      });
  };
}

export function insertDocArchitecturePackageAfter() {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const newPackage = createEmptyPackage();

    // Trigger saving state
    dispatch(saveDocSaving());

    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;

    // Trigger update in redux state
    dispatch({
      type: INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER,
      pkg: newPackage,
    });

    packageServices
      .insertPackageAfter(newPackage, activePackageQID)
      .then(() => {
        dispatch({
          type: INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_SUCCESS,
          pkg: newPackage,
          action: 'insertPackageAfter',
          activeArchitectureQID,
          activePackageQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_ERROR,
          error: err,
        });
      });
  };
}

export function insertDocArchitecturePackageBefore() {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;

    const newPackage = createEmptyPackage();

    // Trigger saving state
    dispatch(saveDocSaving());

    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;

    // Trigger update in redux state
    dispatch({
      type: INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE,
      pkg: newPackage,
    });

    packageServices
      .insertPackageBefore(newPackage, activePackageQID)
      .then(() => {
        dispatch({
          type: INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_SUCCESS,
          pkg: newPackage,
          action: 'insertPackageBefore',
          activeArchitectureQID,
          activePackageQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_ERROR,
          error: err,
        });
      });
  };
}

export function indentDocArchitecturePackage() {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;

    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      type: INDENT_DOC_ARCHITECTURE_PACKAGE,
      packageQID: activePackageQID,
      architectureQID: activeArchitectureQID,
    });

    packageServices
      .indentPackage(activePackageQID)
      .then(() => {
        dispatch({
          type: INDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
          action: 'indentPackage',
          activeArchitectureQID,
          activePackageQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: INDENT_DOC_ARCHITECTURE_PACKAGE_ERROR,
          error: err,
        });
      });
  };
}

export function unindentDocArchitecturePackage() {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;

    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;

    const architectureEntity = doc.entities.architectures[activeArchitectureQID];

    const unableToUnindent = architectureEntity.packages.find(
      pkg => pkg.id === activePackageQID,
    );

    if (!unableToUnindent) {
      // Trigger saving state
      dispatch(saveDocSaving());

      // Trigger update in redux state
      dispatch({
        type: UNINDENT_DOC_ARCHITECTURE_PACKAGE,
        packageQID: activePackageQID,
        architectureQID: activeArchitectureQID,
      });

      packageServices
        .unindentPackage(activePackageQID)
        .then(() => {
          dispatch({
            type: UNINDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
            action: 'unindentPackage',
            activeArchitectureQID,
            activePackageQID,
          });
        })
        .catch((err) => {
          dispatch({
            type: UNINDENT_DOC_ARCHITECTURE_PACKAGE_ERROR,
            error: err,
          });
        });
    }
  };
}

export function packageToggleChildren(architectureQID, packageQID) {
  return (dispatch, getState) => {
    const { activeDoc } = getState();
    const { entities } = activeDoc;
    const architecture = entities.architectures[architectureQID];

    if (architecture) {
      architecture.packages = packageToggleChildrenHelper(
        architecture,
        packageQID,
      ).tree.packages;

      const pkg = findPackageInTree(architecture, packageQID);

      packageServices
        .modifyPackageChildren(pkg.id)
        .then(() => {
          dispatch({
            type: TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN,
            packages: architecture.packages,
            architectureQID,
          });
        })
        .catch((err) => {
          dispatch({
            type: TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_ERROR,
            error: err,
          });
        });
    }
  };
}

export function packageToggleChildrenByLevel(level) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const { activePackageQID } = doc.ui;
    const { activeArchitectureQID } = doc.ui;

    dispatch({
      type: TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_BY_LEVEL,
      architectureQID: activeArchitectureQID,
      packageQID: activePackageQID,
      level,
    });
  };
}

export function linkDocArchitecturePackageTaxonomy(taxonomy) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const { activePackageQID } = doc.ui;
    const { activeArchitectureQID } = doc.ui;

    // need to include old package in order to undo link with link to previous
    const { entities } = doc;
    const architecture = entities.architectures[activeArchitectureQID];
    const pkg = findPackageInTree(architecture, activePackageQID);

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      type: LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY,
      taxonomy,
    });

    const updatePackage = {
      id: activePackageQID,
      term: taxonomy.term,
      taxonomy,
      linkBroken: false,
    };

    packageServices
      .linkTaxonomyToPackage(
        pkg.id,
        taxonomy.term,
        taxonomy.id,
      )
      .then(() => {
        dispatch({
          type: LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS,
          action: 'linkTaxonomyToPackage',
          pkg: updatePackage,
          oldPackage: pkg,
          activeArchitectureQID,
          activePackageQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR,
          error: err,
        });
      });
  };
}

export function unlinkDocArchitecturePackageTaxonomy(pkgQID, architectureQID) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const activePackageQID = pkgQID || doc.ui.activePackageQID;
    const activeArchitectureQID = architectureQID || doc.ui.activeArchitectureQID;

    // need to include old package in order to undo unlink
    const { entities } = doc;
    const architecture = entities.architectures[activeArchitectureQID];
    const pkg = findPackageInTree(architecture, activePackageQID);

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      activePackageQID,
      activeArchitectureQID,
      type: UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY,
    });

    const updatePackage = {
      id: activePackageQID,
      taxonomy: null,
      linkBroken: false,
    };

    return packageServices
      .unlinkTaxonomyToPackage(
        pkg.id,
        pkg.term,
      )
      .then(() => {
        dispatch({
          type: UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS,
          action: 'unlinkTaxonomyToPackage',
          pkg: updatePackage,
          oldPackage: pkg,
          activeArchitectureQID,
          activePackageQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR,
          error: err,
        });
      });
  };
}

export function revertDocArchitectureTaxonomy(pkg) {
  return async (dispatch, getState) => {
    const { activeDoc } = getState();
    const { activeArchitectureQID } = activeDoc.ui;

    dispatch(saveDocSaving());

    dispatch({
      type: REVERT_DOC_ARCHITECTURE_TAXONOMY,
      pkg,
    });

    const updatePkg = {
      ...pkg,
      term: pkg.taxonomy.term,
      linkBroken: false,
    };

    await packageServices
      .modifyPackage(updatePkg, activeArchitectureQID)
      .then(() => {
        dispatch({
          type: REVERT_DOC_ARCHITECTURE_TAXONOMY_SUCCESS,
        });
      })
      .catch((err) => {
        dispatch({
          type: REVERT_DOC_ARCHITECTURE_TAXONOMY_ERROR,
          error: err,
        });
      });
  };
}

export function updateDocArchitecturePackageDescription(pkg, newDescription) {
  return (dispatch) => {
    pkg.description = newDescription;

    dispatch(saveDocSaving());

    dispatch({
      type: DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION,
      pkg,
    });

    packageServices
      .modifyPackage(pkg)
      .then(() => {
        dispatch({
          type: DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_SUCCESS,
          pkg,
        });
      })
      .catch((error) => {
        dispatch({
          type: DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_ERROR,
          error,
        });
      });
  };
}

export function docArchitecturePackageInsertTaxonomyTree(packageFromTaxonomyTree) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;

    const { entities, ui } = doc;
    const { activePackageQID, activeArchitectureQID } = ui;

    // set id for new package root to merge target id
    packageFromTaxonomyTree.id = activePackageQID;

    // set new package root children show prop based on existing children
    const architecture = entities.architectures[activeArchitectureQID];
    const { packages } = findPackageInTree(architecture, activePackageQID);
    if (packages.length) {
      const collapsed = !!packages.find(p => !p.show);
      packageFromTaxonomyTree.packages.forEach((p) => { p.show = !collapsed; });
    }

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      type: DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE,
      pkg: packageFromTaxonomyTree,
    });

    packageServices
      .mergePackage(packageFromTaxonomyTree)
      .then(() => {
        dispatch({
          type: DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE_SUCCESS,
        });
      })
      .catch((err) => {
        dispatch({
          type: DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE_ERROR,
          error: err,
        });
      });
  };
}

export function linkDocArchitecturePackageReference(reference) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const { activePackageQID } = doc.ui;
    const { activeArchitectureQID } = doc.ui;

    // need to include old package in order to undo link with link to previous
    const { entities } = doc;
    const architecture = entities.architectures[activeArchitectureQID];
    const pkg = findPackageInTree(architecture, activePackageQID);

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      type: LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE,
      reference,
    });

    const updatePackage = {
      qid: activePackageQID,
      term: reference.term,
      taxonomy: reference.taxonomy,
      linkBroken: false,
      referencePackage: reference,
    };

    packageServices
      .linkReferenceToPackage(updatePackage)
      .then(() => {
        dispatch({
          type: LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS,
          action: 'linkReferenceToPackage',
          pkg: updatePackage,
          oldPackage: pkg,
          activePackageQID,
          activeArchitectureQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR,
          error: err,
        });
      });
  };
}

export function unlinkDocArchitecturePackageReference(pkgQID) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;
    const { activePackageQID } = doc.ui;
    const { activeArchitectureQID } = doc.ui;

    // need to include old package in order to undo unlink
    const { entities } = doc;
    const architecture = entities.architectures[activeArchitectureQID];
    const pkg = findPackageInTree(architecture, activePackageQID);

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      pkgQID,
      type: UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE,
    });

    const updatePackage = {
      qid: pkgQID,
      referencePackage: null,
    };

    packageServices
      .unlinkReferenceToPackage(updatePackage)
      .then(() => {
        dispatch({
          type: UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS,
          action: 'unlinkReferenceToPackage',
          pkg: updatePackage,
          oldPackage: pkg,
          activeArchitectureQID,
          activePackageQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR,
          error: err,
        });
      });
  };
}

export function docArchitecturePackageInsertReferenceTree(pkg) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;

    const { activePackageQID } = doc.ui;
    pkg.id = activePackageQID;

    // Trigger saving state
    dispatch(saveDocSaving());

    // Trigger update in redux state
    dispatch({
      type: DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE,
      pkg,
    });

    packageServices
      .mergePackage(pkg)
      .then(() => {
        dispatch({
          type: DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE_SUCCESS,
        });
      })
      .catch((err) => {
        dispatch({
          type: DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE_ERROR,
          error: err,
        });
      });
  };
}

export function removeDocArchitecturePackage(pkgQID) {
  return (dispatch, getState) => {
    const doc = getState().activeDoc;

    const { activeArchitectureQID } = doc.ui;
    const { activePackageQID } = doc.ui;
    const activeArchitecture = doc.entities.architectures[activeArchitectureQID];

    // Cannot delete first package in the architecture
    if (activeArchitecture.packages[0].id === pkgQID) return;

    dispatch(saveDocSaving());

    dispatch({
      type: REMOVE_DOC_ARCHITECTURE_PACKAGE,
      pkgQID,
    });

    const parentNode = findParentNode(activeArchitecture, pkgQID);
    const removedPackage = findPackageInTree(activeArchitecture, pkgQID);
    const targetPkgSiblingOrder = parentNode.packages
      .map(p => p.id)
      .indexOf(pkgQID);

    let targetPackage;
    let action;

    if (parentNode.packages.length === 1) {
      targetPackage = parentNode;
      action = 'deletePackageOnlyChild';
    } else if (targetPkgSiblingOrder === 0) {
      targetPackage = parentNode.packages[targetPkgSiblingOrder + 1];
      action = 'deletePackageChildBefore';
    } else {
      targetPackage = parentNode.packages[targetPkgSiblingOrder - 1];
      action = 'deletePackageChildAfter';
    }

    packageServices
      .deletePackage(pkgQID)
      .then(() => {
        dispatch({
          type: REMOVE_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
          pkg: removedPackage,
          action,
          targetPackage,
          activePackageQID,
          activeArchitectureQID,
        });
      })
      .catch((err) => {
        dispatch({
          type: REMOVE_DOC_ARCHITECTURE_PACKAGE_ERROR,
          error: err,
        });
      });
  };
}

function undoActionPromiseWrapper(promise, currentUndoAction, dispatch) {
  promise
    .then(() => {
      dispatch({
        type: UNDO_ARCHITECTURE_ACTION,
        action: currentUndoAction,
      });
    })
    .catch((err) => {
      dispatch({
        type: UNDO_ARCHITECTURE_ACTION_ERROR,
        action: currentUndoAction,
        error: err,
      });
    });
}

// For all undo actions
export function undoArchitectureAction() {
  return (dispatch, getState) => {
    const { activeDoc } = getState();
    const { undoStack, ui } = activeDoc;
    const { activeArchitectureQID } = ui;

    // empty undo stack, don't do anything
    if (undoStack.length === 0) {
      return;
    }

    const currentUndoAction = undoStack[undoStack.length - 1];

    dispatch(saveDocSaving());

    switch (currentUndoAction.action) {
      case 'mergePackage':
        dispatch({
          type: UNDO_MERGE_PACKAGE,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.replacePackage(currentUndoAction.oldPackage),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'indentPackage':
        dispatch({
          type: UNDO_INDENT_PACKAGE,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.unindentPackage(currentUndoAction.activePackageQID),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'unindentPackage':
        dispatch({
          type: UNDO_UNINDENT_PACKAGE,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.indentPackage(currentUndoAction.activePackageQID),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'modifyPackage':
        dispatch({
          type: UNDO_MODIFY_PACKAGE,
          undo: true,
        });

        currentUndoAction.pkg.term = currentUndoAction.pkg.oldTerm;
        undoActionPromiseWrapper(
          packageServices.modifyPackage(currentUndoAction.pkg, activeArchitectureQID),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'movePackageUp':
        dispatch({
          type: UNDO_MOVE_PACKAGE_UP,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.movePackageDown(
            currentUndoAction.activePackageQID,
            activeArchitectureQID,
          ),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'movePackageDown':
        dispatch({
          type: UNDO_MOVE_PACKAGE_DOWN,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.movePackageUp(
            currentUndoAction.activePackageQID,
            activeArchitectureQID,
          ),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'insertPackageAfter':
      case 'insertPackageBefore':
        dispatch({
          type: UNDO_INSERT_PACKAGE,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.deletePackage(currentUndoAction.pkg.id),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'deletePackageOnlyChild':
        dispatch({
          type: UNDO_DELETE_PACKAGE_ONLY_CHILD,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.insertPackageChild(currentUndoAction.pkg.id),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'deletePackageChildBefore':
        dispatch({
          type: UNDO_DELETE_PACKAGE_BEFORE,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.insertPackageBefore(
            currentUndoAction.pkg,
            currentUndoAction.targetPackage.id,
            activeArchitectureQID,
          ),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'deletePackageChildAfter':
        dispatch({
          type: UNDO_DELETE_PACKAGE_AFTER,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.insertPackageAfter(
            currentUndoAction.pkg,
            currentUndoAction.targetPackage.id,
            activeArchitectureQID,
          ),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'linkTaxonomyToPackage':
        dispatch({
          type: UNDO_LINK_TAXONOMY_TO_PACKAGE,
          undo: true,
        });

        if (currentUndoAction.oldPackage.taxonomy) {
          // if previous state was a link to another package, need to link to that
          undoActionPromiseWrapper(
            packageServices.linkTaxonomyToPackage(
              currentUndoAction.oldPackage.id,
              currentUndoAction.oldPackage.term,
              currentUndoAction.oldPackage.taxonomy.id,
            ),
            currentUndoAction,
            dispatch,
          );
        } else {
          // previous state had no taxonomy, remove link altogether
          // note: this does not necessarily restore previous state of pkg
          // it's possible that the link action changed the term and old term
          // needs to be restored. This change can be made here for DB, but also needs
          // to be made in reducer.
          undoActionPromiseWrapper(
            packageServices.unlinkTaxonomyToPackage(
              currentUndoAction.oldPackage.id,
              currentUndoAction.pkg.term,
            ),
            currentUndoAction,
            dispatch,
          );
        }

        break;
      case 'unlinkTaxonomyToPackage':
        dispatch({
          type: UNDO_UNLINK_TAXONOMY_TO_PACKAGE,
          undo: true,
        });
        undoActionPromiseWrapper(
          packageServices.linkTaxonomyToPackage(
            currentUndoAction.oldPackage.id,
            currentUndoAction.oldPackage.term,
            currentUndoAction.oldPackage.taxonomy.id,
          ),
          currentUndoAction,
          dispatch,
        );
        break;
      case 'linkReferenceToPackage':
        dispatch({
          type: UNDO_LINK_REFERENCE_TO_PACKAGE,
          undo: true,
        });

        if (currentUndoAction.oldPackage.referencePackage) {
          // if previous state was a link to another package, need to link to that
          undoActionPromiseWrapper(
            packageServices.linkReferenceToPackage(currentUndoAction.oldPackage),
            currentUndoAction,
            dispatch,
          );
        } else {
          // previous state had no reference, remove link altogether
          undoActionPromiseWrapper(
            packageServices.unlinkReferenceToPackage(currentUndoAction.pkg),
            currentUndoAction,
            dispatch,
          );
        }

        break;
      case 'unlinkReferenceToPackage':
        dispatch({
          type: UNDO_UNLINK_REFERENCE_TO_PACKAGE,
          undo: true,
        });

        undoActionPromiseWrapper(
          packageServices.linkReferenceToPackage(currentUndoAction.oldPackage),
          currentUndoAction,
          dispatch,
        );
        break;
      default:
    }
  };
}
