import * as services from 'services/architectures';
import {
  ARCHITECTURE_SUBMITTED,
  ARCHITECTURE_DRAFT,
} from 'constants/architectureStates.js';

// @note submitting an architecture for review
export const PATCH_ARCHITECTURE_STATUS_SUBMITTED = 'PATCH_ADMIN_ARCHITECTURE_STATUS_SUBMITTED';
export const PATCH_ARCHITECTURE_STATUS_SUBMITTED_FETCHING = 'PATCH_ARCHITECTURE_STATUS_SUBMITTED_FETCHING';
export const PATCH_ARCHITECTURE_STATUS_SUBMITTED_SUCCESS = 'PATCH_ARCHITECTURE_STATUS_SUBMITTED_SUCCESS';
export const PATCH_ARCHITECTURE_STATUS_SUBMITTED_ERROR = 'PATCH_ARCHITECTURE_STATUS_SUBMITTED_ERROR';

export const GET_PUBLISHED_ARCHITECTURE_FETCHING = 'GET_PUBLISHED_ARCHITECTURE_FETCHING';
export const GET_PUBLISHED_ARCHITECTURE_SUCCESS = 'GET_PUBLISHED_ARCHITECTURE_SUCCESS';
export const GET_PUBLISHED_ARCHITECTURE_ERROR = 'GET_PUBLISHED_ARCHITECTURE_ERROR';

export function patchArchitectureStatusSubmitted(architectureQID) {
  return (dispatch) => {
    dispatch(patchArchitectureStatusSubmittedFetching());

    return services
      .patchArchitecture(architectureQID, ARCHITECTURE_SUBMITTED)
      .then(() => {
        dispatch(
          patchArchitectureStatusSubmittedSuccess(
            architectureQID,
            ARCHITECTURE_SUBMITTED,
          ),
        );
      })
      .catch((error) => {
        dispatch(patchArchitectureStatusSubmittedError(error));
      });
  };
}

function patchArchitectureStatusSubmittedFetching() {
  return {
    type: PATCH_ARCHITECTURE_STATUS_SUBMITTED_FETCHING,
  };
}

function patchArchitectureStatusSubmittedSuccess(qid, status) {
  return {
    type: PATCH_ARCHITECTURE_STATUS_SUBMITTED_SUCCESS,
    qid,
    status,
  };
}

function patchArchitectureStatusSubmittedError(error) {
  return {
    type: PATCH_ARCHITECTURE_STATUS_SUBMITTED_ERROR,
    error,
  };
}

// @note converting an architecture back to draft
// two valid cases:
//  1. immedietly recalling a submitted architecture
//  2. after receiving a rejected architecture
export const PATCH_ARCHITECTURE_STATUS_DRAFT = 'PATCH_ADMIN_ARCHITECTURE_STATUS_DRAFT';
export const PATCH_ARCHITECTURE_STATUS_DRAFT_FETCHING = 'PATCH_ARCHITECTURE_STATUS_DRAFT_FETCHING';
export const PATCH_ARCHITECTURE_STATUS_DRAFT_SUCCESS = 'PATCH_ARCHITECTURE_STATUS_DRAFT_SUCCESS';
export const PATCH_ARCHITECTURE_STATUS_DRAFT_ERROR = 'PATCH_ARCHITECTURE_STATUS_DRAFT_ERROR';

export function patchArchitectureStatusDraft(architectureQID) {
  return (dispatch) => {
    dispatch(patchArchitectureStatusDraftFetching());

    return services
      .patchArchitecture(architectureQID, ARCHITECTURE_DRAFT)
      .then(() => {
        dispatch(
          patchArchitectureStatusDraftSuccess(
            architectureQID,
            ARCHITECTURE_DRAFT,
          ),
        );
      })
      .catch((error) => {
        dispatch(patchArchitectureStatusDraftError(error));
      });
  };
}

function patchArchitectureStatusDraftFetching() {
  return {
    type: PATCH_ARCHITECTURE_STATUS_DRAFT_FETCHING,
  };
}

function patchArchitectureStatusDraftSuccess(qid, status) {
  return {
    type: PATCH_ARCHITECTURE_STATUS_DRAFT_SUCCESS,
    qid,
    status,
  };
}

function patchArchitectureStatusDraftError(error) {
  return {
    type: PATCH_ARCHITECTURE_STATUS_DRAFT_ERROR,
    error,
  };
}
