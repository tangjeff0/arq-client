import { get } from 'lodash';
import * as docServices from 'services/docs';

import { SHARED_DOCUMENT_QUERY_ROOT } from 'constants/gql';

export const GET_SHARED_DOC = 'GET_SHARED_DOC';
export const GET_SHARED_DOC_LOADING = 'GET_SHARED_DOC_LOADING';
export const GET_SHARED_DOC_SUCCESS = 'GET_SHARED_DOC_SUCCESS';
export const GET_SHARED_DOC_ERROR = 'GET_SHARED_DOC_ERROR';

export const TOGGLE_SHARED_DOC_ARCHITECTURE_PACKAGE_CHILDREN = 'TOGGLE_SHARED_DOC_ARCHITECTURE_PACKAGE_CHILDREN';

export function getSharedDoc(qid) {
  return (dispatch) => {
    dispatch(getSharedDocLoading());

    return docServices
      .getSharedDoc(qid)
      .then((data) => {
        const doc = get(data, SHARED_DOCUMENT_QUERY_ROOT);

        if (doc === null) {
          dispatch(getSharedDocError({ status: 404 })); // temp fix — see DocumentInterface
          return;
        }

        dispatch(getSharedDocSuccess(doc));
      })
      .catch((error) => {
        dispatch(getSharedDocError(error));
      });
  };
}

export function getSharedDocLoading() {
  return {
    type: GET_SHARED_DOC_LOADING,
  };
}

export function getSharedDocSuccess(sharedDoc) {
  return {
    type: GET_SHARED_DOC_SUCCESS,
    sharedDoc,
  };
}

export function getSharedDocError(error) {
  return {
    type: GET_SHARED_DOC_ERROR,
    error,
  };
}

export function packageToggleChildren(architectureQID, packageQID) {
  return {
    type: TOGGLE_SHARED_DOC_ARCHITECTURE_PACKAGE_CHILDREN,
    architectureQID,
    packageQID,
  };
}
