export const customers = [
  {
    id: '3a8f2d61-9783-4430-9152-9e7168c180dc',
    name: 'Band of Angels',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
  {
    id: '9c0f2b59-8f8c-4671-bd5f-1b0894f7bd09',
    name: 'Golden Seeds LLC',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
  {
    id: 'fde4b2b4-7c67-4212-a701-e610da4dffb9',
    name: 'North Coast Angel Fund',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
  {
    id: 'b1e1cf7e-b642-4d95-89bb-03f70f50f508',
    name: 'Tech Coast Angels',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
  {
    id: '6375b7fd-6025-4767-aa25-f43399bdf715',
    name: 'Hyde Park Angel Network',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
  {
    id: 'c9fab820-8c44-4d69-a885-d6f3afc369b8',
    name: 'Alliance of Angels',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
  {
    id: 'a9352fb4-9397-42e2-bf8d-476dfa3fbd28',
    name: 'Pasadena Angels',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
  {
    id: '8620a58d-d891-43eb-8da6-a0966d8c47b4',
    name: 'New York Angels Inc',
    createdAt: '0001-01-01T00:00:00Z',
    updatedAt: '0001-01-01T00:00:00Z',
    description: '',
  },
];

export const customer = customers[0];
