export const users = [
  {
    email: 'vrubin@example.com',
  },
  {
    email: 'fnightingale@example.com',
  },
  {
    email: 'jgoodall@example.com',
  },
  {
    email: 'efranklin@example.com',
  },
  {
    email: 'mcurie@example.com',
  },
];

export const user = users[0];
