export function architectureToClipboard(architecture) {
  const title = architecture.title || 'Untitled Architecture';

  let text = `${title}\n\n`;
  let html = `<h2>${title}</h2>`;
  (function sub(packages, level) {
    html = html.concat('<ul>');

    packages.forEach((pkg) => {
      const term = pkg.term.length > 0 ? pkg.term : 'Untitled';
      text = text.concat(`${'\t'.repeat(level)}- ${term}\n`);
      html = html.concat(`<li>${term}`);

      if (pkg.packages.length > 0) {
        sub(pkg.packages, level + 1);
      }

      html = html.concat('</li>');
    });

    html = html.concat('</ul>');
  }(architecture.packages, 0));

  return { text, html };
}

export default architectureToClipboard;
