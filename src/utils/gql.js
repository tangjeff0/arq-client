import { sortBy } from 'lodash';
import { v4 as uuid } from 'uuid';
import { flattenPackages, listToTree } from 'utils/tree';
import { ARCHITECTURE_PUBLISHED } from 'constants/architectureStates.js';
import { PACKAGE_TYPENAME } from 'constants/nodetypes';

/**
 * @name isPackage
 * @description Checks whether item is a Package
 * @param {Object} item The item
 * @return {Boolean} True if Package
 */
export function isPackage(item) {
  return item.__typename === PACKAGE_TYPENAME; // eslint-disable-line no-underscore-dangle
}

/**
 * @name getPackagesUniquePublishedArchitectures
 * @description Creates an array of unique linked architectures for a capability
 * @param {Object} capability The capability
 * @return {Array} The array of unique architectures
 */
export function getPackagesUniquePublishedArchitectures(packages) {
  const architecturesObj = {};
  packages.forEach((p) => {
    const { architecture: a } = p;
    if (!p.linkBroken && a.status === ARCHITECTURE_PUBLISHED && a.isLatestArch) {
      architecturesObj[a.id] = a;
    }
  });

  return Object.values(architecturesObj);
}

/**
 * @name getCapabilitiesUniquePublishedArchitectures
 * @description Creates an array of unique linked architectures for a list of capabilities
 * @param {Array} capabilities The list of capabilities
 * @return {Array} The array of unique architectures
 */
export function getCapabilitiesUniquePublishedArchitectures(capabilities) {
  const architecturesObj = {};
  capabilities.forEach((c) => {
    c.referencedBy.forEach((p) => {
      const { architecture: a } = p;
      if (!p.linkBroken && a.status === ARCHITECTURE_PUBLISHED && a.isLatestArch) {
        architecturesObj[a.id] = a;
      }
    });
  });
  return Object.values(architecturesObj);
}

/**
 * @name getCapabilitiesUniqueCompanies
 * @description Creates an array of unique companies for a list of capabilities
 * @param {Array} capabilities The list of capabilities
 * @return {Array} The array of unique companies
 */
export function getCapabilitiesUniqueCompanies(capabilities = []) {
  const companiesObj = {};
  capabilities.forEach((c) => {
    c.companies.forEach((o) => {
      companiesObj[o.id] = o;
    });
  });

  return Object.values(companiesObj);
}

/**
 * @name getCapabilitiesUniqueProblems
 * @description Creates an array of unique problems for a list of capabilities
 * @param {Array} capabilities The list of capabilities
 * @return {Array} The array of unique problems
 */
export function getCapabilitiesUniqueProblems(capabilities = []) {
  const problemsObj = {};
  capabilities.forEach((c) => {
    c.problemSets.forEach((o) => {
      problemsObj[o.id] = o;
    });
  });

  return Object.values(problemsObj);
}

/**
 * @name getCapabilitiesUniqueResource
 * @description Creates an array of unique companies for a list of capabilities
 * @param {Array} capabilities The list of capabilities
 * @return {Array} The array of unique companies
 */
export function getCapabilitiesUniqueResource(capabilities = [], resourceName) {
  const resourceObj = {};
  capabilities.forEach((c) => {
    c[resourceName].forEach((o) => {
      resourceObj[o.id] = o;
    });
  });

  return Object.values(resourceObj);
}

/**
 * @name constructPackageTree
 * @description Constructs tree using parent and order props
 * @param {Array} packages Linear array of packages
 * @return {Array} Tree of packages
 */
export function constructPackageTree(packages) {
  const packagesWithDefaults = packages.map(pkg => ({
    ...pkg,
    term: pkg.term || '',
    description: pkg.description || '',
  }));

  const roots = listToTree(sortBy(packagesWithDefaults, 'sequence', 'asc'), 'packages');

  return roots;
}

/**
 * @name createInsertTaxonomies
 * @description Creates an array of CoreTaxonomyUpdate objects from an array of taxonomies
 * @param {String} coreParentID id of the core taxonomy parent to insert under
 * @param {Array} taxonomies Array of taxonomies; can include nested children
 * @return {Array} Array of CoreTaxonomyUpdate objects
 */
export function createInsertTaxonomies(coreParentID, taxonomies) {
  const result = [];
  const map = {};

  taxonomies.forEach((taxonomy) => { map[taxonomy.id] = uuid(); });

  taxonomies.forEach((taxonomy) => {
    result.push({
      id: map[taxonomy.id],
      term: taxonomy.term,
      reference_id: taxonomy.id,
      parent_id: map[taxonomy.parentId] || coreParentID,
    });
  });

  return result;
}

/**
 * @name createAddTaxonomies
 * @description Creates an array of CoreTaxonomyUpdate objects from an array of taxonomies
 * @param {String} coreParentQID QID of the core taxonomy parent to insert under
 * @param {Array} taxonomies Array of taxonomies; can include nested children
 * @param {Array} acc Optional accummulator
 * @return {Array} Array of arq_insert_core_taxonomies_mutation_view_insert_input objects
 */
export function createAddTaxonomies(coreParentQID, taxonomies, acc = []) {
  taxonomies.forEach((child) => {
    const { id, term, children } = child;
    acc.push({
      id,
      term,
      reference_id: null,
      parent_id: coreParentQID,
    });
    if (children && children.length > 0) {
      createAddTaxonomies(id, children, acc);
    }
  });
  return acc;
}

/**
 * @name createMergePackageInputsFromPackageTree
 * @description Creates arq_merge_packages_mutation_view_insert_input array from pkg tree
 * @param {Object} pkg A package with descendents
 * @return {Array} arq_merge_packages_mutation_view_insert_input for GQL mutation
 */
export function createMergePackageInputsFromPackageTree(pkg) {
  return flattenPackages([pkg], true, true)
    .map(p => ({
      id: p.id,
      taxonomy_id: p.taxonomy.id,
      parent_id: p.parentQID,
      show: p.show,
    }));
}

/**
 * @name createReplacePackageInputsFromPackageTree
 * @description Creates arq_replace_packages_mutation_view_insert_input array from pkg tree
 * @param {Object} pkg A package with descendents
 * @return {Array} arq_replace_packages_mutation_view_insert_input for GQL mutation
 */
export function createReplacePackageInputsFromPackageTree(pkg) {
  return flattenPackages([pkg], true)
    .map(p => ({
      id: p.id,
      taxonomyId: p.taxonomy ? p.taxonomy.id : null,
      term: p.term,
    }));
}
