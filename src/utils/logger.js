const logger = (error) => {
  if (process.env.NODE_ENV === 'development') {
    console.log(error); // eslint-disable-line
  }
};

export default logger;
