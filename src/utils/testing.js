import React from 'react';
import checkPropTypes from 'check-prop-types'; // can possibly use PropTypes.checkPropTypes from 'prop-types' in future

export const setUp = (Component, props = {}) => shallow(<Component {...props} />);

export const setUpMount = (Component, props = {}) => mount(<Component {...props} />);

export const checkProps = (Component, expectedProps) => {
  // eslint-disable-next-line react/forbid-foreign-prop-types
  const propsErr = checkPropTypes(Component.propTypes, expectedProps, 'props', Component.name);
  return propsErr;
};
