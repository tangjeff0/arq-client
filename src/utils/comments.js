import { v4 as uuid } from 'uuid';
import { now } from 'utils/time';

export function createComment(author, text) {
  const timestamp = now();

  return {
    id: uuid(),
    text,
    author,
    createdAt: timestamp,
    updatedAt: null,
    deletedAt: null,
    resolvedAt: null,
    __typename: 'arq_package_comments',
  };
}

export default createComment;
