export function isOpera() {
  return (
    (!!window.opr && !!opr.addons) // eslint-disable-line no-undef
    || !!window.opera
    || navigator.userAgent.indexOf(' OPR/') >= 0
  );
}

// Firefox 1.0+
export function isFirefox() {
  return typeof InstallTrigger !== 'undefined';
}

// Safari 3.0+ "[object HTMLElementConstructor]"
export function isSafari() {
  return (
    /constructor/i.test(window.HTMLElement)
    || (function f(p) {
      return p.toString() === '[object SafariRemoteNotification]';
    }(
      !window.safari
        || (typeof safari !== 'undefined' && safari.pushNotification), // eslint-disable-line no-undef
    ))
  );
}

// Internet Explorer 6-11
export function isIE() {
  return /* @cc_on!@ */ false || !!document.documentMode;
}

// Edge 20+
export function isEdge() {
  return !isIE && !!window.StyleMedia;
}

// Chrome 1+
export function isChrome() {
  return !!window.chrome && !!window.chrome.webstore;
}

// Blink engine detection
export function isBlink() {
  return (isChrome || isOpera) && !!window.CSS;
}
