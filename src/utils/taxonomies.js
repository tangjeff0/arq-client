import { cloneDeep, some, slice } from 'lodash';
import { sourceNames } from 'constants/sources';

/**
 * @name taxonomyDelete
 * @description Deletes a taxonomy from the tree
 * @param {Object} originalTree The original taxonomy tree
 * @param {String} qid The qid of the taxonomy to be deleted
 * @return {Object} The updated taxonomy tree
 */
export function taxonomyDelete(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.children, (pkg, index) => {
      if (pkg.id === qid) {
        currentTree.children = [
          ...slice(currentTree.children, 0, index),
          ...slice(currentTree.children, index + 1, currentTree.children.length),
        ];

        return true;
      }
      if (pkg.children.length > 0) {
        return sub(currentTree.children[index]);
      }
      return false;
    });
  }(tree));
  return { tree };
}

/**
 * @name getTaxonomyRootAncestor
 * @description Gets root ancestor of taxonomy
 * @param {Object} taxonomy The original taxonomy tree
 * @return {Object} The root taxonomy
 */
export function getTaxonomyRootAncestor(ancestors) {
  return ancestors.find(a => !a.parentId);
}

/**
 * @name taxonomyAncestorsToBreadcrumbs
 * @description Generates a breadcrumbs string from an ancestors array
 * @param {Object} ancestors The taxonomy ancestors array
 * @return {String} The breadcrumbs string
 */
export function taxonomyAncestorsToBreadcrumbs(ancestors) {
  const root = getTaxonomyRootAncestor(ancestors);
  let result = `${sourceNames[root.source.name]} > `;

  (function sub(currentTaxonomy) {
    result += `${currentTaxonomy.term} > `;
    const child = ancestors.find(a => a.parentId === currentTaxonomy.id);
    if (child) sub(child);
  }(root));

  return result.slice(0, -3);
}

/**
 * @name taxonomiesSortByParent
 * @description To merge taxonomies to Core, parent must be inserted before the child.
 *       This function reconstructs the tree and then flattens it.
 * @param {Object} root The taxonomy to be merged into/inserted under a core taxonomy
 * @param {String} action The action to take [merge || insert]
 * @return {Array} Taxonomies ordered by parent then children
 */
export function taxonomiesSortByParent(root, action) {
  const rootId = root.id;
  const rootDescendants = root.allChildren;

  let taxonomyTree = [];
  if (action === 'insert') {
    taxonomyTree = [rootDescendants.find(el => el.id === rootId)];
    taxonomyTree[0].children = constructTree(rootDescendants, rootId);
  } else {
    taxonomyTree = constructTree(rootDescendants, rootId);
  }

  return [...flatten(taxonomyTree)];
}

/**
 * @name constructTree
 * @description Recursively generates a taxonomy tree
 * @param {Array} nodes  Taxonomy descendants
 * @param {String} parentId Taxonomy parent id
 * @return {Array} Taxonomies tree
 */
function constructTree(nodes, parentId) {
  return nodes
    .filter(node => node.parentId === parentId)
    .reduce(
      (tree, node) => [
        ...tree,
        {
          ...node,
          children: constructTree(nodes, node.id),
        },
      ],
      [],
    );
}

/**
 * @name flatten
 * @description Generator function to flatten a tree
 * @param {Array} tree Taxonomy descendants
 * @return {Array} Taxonomy descendants
 */
function* flatten(tree) {
  // eslint-disable-next-line no-restricted-syntax
  for (const i of tree) {
    yield { id: i.id, term: i.term, parentId: i.parentId };
    yield* flatten(i.children);
  }
}
