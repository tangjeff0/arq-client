import { size } from 'lodash';

export function getProblemShortname(problem, customer = problem.customer) {
  if (size(problem) && size(customer)) {
    const prefix = `${customer.name} ${problem.fiscalYear}`;
    return `${prefix} ${getProblemSuffix(problem)}`;
  }
  return null;
}

export function getProblemSuffix(problem) {
  return problem.customerNumber || `${problem.id.substring(0, 4)}...`;
}

export function getProblemPriorities(problem) {
  const priorities = [];
  if (problem.priority) {
    const priorityLower = problem.priority.toLowerCase();
    priorityLower.indexOf('low') > -1 && priorities.push('low');
    priorityLower.indexOf('medium') > -1 && priorities.push('medium');
    priorityLower.indexOf('high') > -1 && priorities.push('high');
  }
  return priorities;
}
