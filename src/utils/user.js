export function toUserName(email) {
  return email ? email.split('@', 1).join('') : 'friend';
}

export function isAdminLocalDev(email) {
  return (ADMIN_EMAILS || '').indexOf(email) > -1;
}

export default toUserName;
