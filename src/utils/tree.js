import { v4 as uuid } from 'uuid';
import {
  cloneDeep,
  some,
  slice,
  sortBy,
  get,
} from 'lodash';
import { now } from 'utils/time';

/**
 * @name initTree
 * @description Initializes an empty tree
 * @param {Object} pkg A package
 * @return {Object} A package tree
 */
export function initTree(pkg) {
  return {
    packages: [pkg],
  };
}

/**
 * @name createPackage
 * @description Creates a new package
 * @return {Object} A new package
 */
export function createPackage() {
  const id = uuid();
  const createdAt = now();
  const updatedAt = createdAt;

  return {
    id,
    term: '',
    oldTerm: '',
    description: '',
    taxonomy: null,
    linkBroken: false,
    show: true,
    packages: [],
    createdAt,
    updatedAt,
  };
}

/**
 * @name createEmptyPackage
 * @description Creates a new empty package
 * @return {Object} A new empty package
 */
export function createEmptyPackage() {
  return createPackage();
}

/**
 * @name flattenPackages
 * @description Takes the package tree and flattens it out based on DFS and
 *   whether children show or not
 * @param addParent Whether a parentQID should be added to each package
 * @param includeHidden Whether to include packages with show false
 * @param {Array} packages An array of packages
 * @return {Array} A flattened array of packages
 */
export function flattenPackages(packages, addParent = false, includeHidden = false) {
  return (function flatten(currentPackages, parentQID) {
    return currentPackages.reduce((accumulator, currentValue) => {
      if (addParent) currentValue.parentQID = parentQID;

      if (currentValue.show || includeHidden) {
        if (currentValue.packages.length > 0) {
          return accumulator
            .concat(currentValue)
            .concat(flatten(currentValue.packages, currentValue.id));
        }
        return accumulator.concat(currentValue);
      }
      return accumulator;
    }, []);
  }(packages));
}

/**
 * @name updatePackageTermAt
 * @description Updates a package term in a tree based on its qid
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package we want to update
 * @param {String} term The new term for the package
 * @return {Object} The updated tree
 */
export function updatePackageTermAt(originalTree, qid, term) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg, index) => {
      if (pkg.id === qid) {
        currentTree.packages[index].term = term;
        if (term === '') {
          currentTree.packages[index].taxonomy = null;
        }
      } else if (pkg.packages.length > 0) {
        return sub(currentTree.packages[index]);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name updatePackageDescriptionAt
 * @description Updates a package description in a tree based on its qid
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package we want to update
 * @param {String} description The new description url for the package
 * @return {Object} The updated tree
 */
export function updatePackageDescriptionAt(originalTree, qid, description) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg, index) => {
      if (pkg.id === qid) {
        currentTree.packages[index].description = description;
        return true;
      } if (pkg.packages.length > 0) {
        return sub(currentTree.packages[index]);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name updatePackageOldTermAt
 * @description Updates a package oldTerm (for undoing) in a tree based on its qid
 * @param {Object} originalTree The original package tree
 * @param {String} pkg the pkg we want to update oldTerm for
 * @return {Object} The updated tree
 */
export function updatePackageOldTermAt(originalTree, pkg) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (treePkg) => {
      if (treePkg.id === pkg.id) {
        treePkg.oldTerm = pkg.term;
      } else if (treePkg.packages.length > 0) {
        return sub(treePkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageInsertBefore
 * @description Inserts a new package as a "before sibling" to another package
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package that will get the before sibling
 * @param {Object} newPackage The new package that we want to add
 * @return {Object} The updated tree
 */
export function packageInsertBefore(originalTree, qid, newPackage) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg, index) => {
      if (pkg.id === qid) {
        currentTree.packages = [
          ...slice(currentTree.packages, 0, index),
          newPackage,
          ...slice(currentTree.packages, index, currentTree.packages.length),
        ];

        return true;
      } if (pkg.packages.length > 0) {
        return sub(currentTree.packages[index]);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageInsertChild
 * @description Inserts a new package as a child of parentPackageQID
 * @param {Object} originalTree The original package tree
 * @param {String} parentPackageQID The qid of the parent
 * @param {Object} newPackage The new package that we want to add under the parent
 * @param {String} key The key for descendents; default 'packages'
 * @return {Object} The updated tree
 */
export function packageInsertChild(
  originalTree,
  parentPackageQID,
  newPackage,
  key = 'packages',
) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree[key], (pkg, index) => {
      if (pkg.id === parentPackageQID) {
        pkg[key] = pkg[key].concat(newPackage);
        return true;
      } if (pkg[key].length > 0) {
        return sub(currentTree[key][index]);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageInsertAfter
 * @description Inserts a new package as an "after sibling" to another package
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package that will get the after sibling
 * @param {Object} newPackage The new package that we want to add
 * @return {Object} The updated package tree
 */
export function packageInsertAfter(originalTree, qid, newPackage) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg, index) => {
      if (pkg.id === qid) {
        currentTree.packages = [
          ...slice(currentTree.packages, 0, index + 1),
          newPackage,
          ...slice(currentTree.packages, index + 1, currentTree.packages.length),
        ];

        return true;
      }
      if (pkg.packages.length > 0) {
        return sub(currentTree.packages[index]);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageDelete
 * @description Deletes a package from the tree
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package to be deleted
 * @return {Object} The updated package tree
 */
export function packageDelete(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg, index) => {
      if (pkg.id === qid) {
        currentTree.packages = [
          ...slice(currentTree.packages, 0, index),
          ...slice(currentTree.packages, index + 1, currentTree.packages.length),
        ];

        return true;
      }
      if (pkg.packages.length > 0) {
        return sub(currentTree.packages[index]);
      }
      return false;
    });
  }(tree));

  if (tree.packages.length === 0) {
    const newPackage = createPackage();

    return {
      active: newPackage.id,
      ...initTree(newPackage),
    };
  }

  return { tree };
}

/**
 * @name packageIndent
 * @description Indents a package
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package to be indented
 * @return {Object} The updated package tree
 */
export function packageIndent(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg, index) => {
      if (pkg.id === qid && index !== 0) {
        currentTree.packages = [
          ...slice(currentTree.packages, 0, index),
          ...slice(currentTree.packages, index + 1, currentTree.packages.length),
        ];

        currentTree.packages[index - 1].packages = [
          ...currentTree.packages[index - 1].packages,
          pkg,
        ];

        return true;
      } if (pkg.packages.length > 0) {
        return sub(currentTree.packages[index]);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageUnindent
 * @description Unindents a package
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package to be unindented
 * @return {Object} The updated package tree
 */
export function packageUnindent(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  const depth = 0;
  (function sub(treeParent, treeParentPackageIndex, treeChild, currentDepth) {
    some(treeChild.packages, (pkg, index) => {
      if (pkg.id === qid && currentDepth !== 0) {
        treeChild.packages = [
          ...slice(treeChild.packages, 0, index),
          ...slice(treeChild.packages, index + 1, treeChild.packages.length),
        ];

        treeParent.packages = [
          ...slice(treeParent.packages, 0, treeParentPackageIndex + 1),
          pkg,
          ...slice(
            treeParent.packages,
            treeParentPackageIndex + 1,
            treeParent.packages.length,
          ),
        ];

        return true;
      } if (pkg.packages.length > 0) {
        return sub(treeChild, index, treeChild.packages[index], currentDepth + 1);
      }
      return false;
    });
  }(null, null, tree, depth));

  return { tree };
}

/**
 * @name packageMoveUp
 * @description Moves a package directly up in the tree if there
 * is a sibling above it
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package to move up
 * @return {Object} The updated package tree
 */
export function packageMoveUp(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    const currentPkgIndex = currentTree.packages.findIndex(pkg => pkg.id === qid);
    if (currentPkgIndex > 0) {
      const shiftPkg = currentTree.packages.splice(currentPkgIndex, 1)[0];

      currentTree.packages.splice(currentPkgIndex - 1, 0, shiftPkg);
    } else {
      currentTree.packages.forEach((pkg) => {
        sub(pkg);
      });
    }
  }(tree));

  return { tree };
}

/**
 * @name packageMoveDown
 * @description Moves a package directly down in the tree, regardless of whether
 *   the nearest package is a parent, child, or sibling
 * @param {Object} originalTree The original package tree
 * @param {String} qid The qid of the package to move down
 * @return {Object} The updated package tree
 */
export function packageMoveDown(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    const currentPkgIndex = currentTree.packages.findIndex(pkg => pkg.id === qid);
    if (currentPkgIndex > -1 && currentPkgIndex < currentTree.packages.length - 1) {
      const shiftPkg = currentTree.packages.splice(currentPkgIndex, 1)[0];

      currentTree.packages.splice(currentPkgIndex + 1, 0, shiftPkg);
    } else {
      currentTree.packages.forEach((pkg) => {
        sub(pkg);
      });
    }
  }(tree));

  return { tree };
}

/**
 * @name packageFindPrevious
 * @description Finds the previous package in the tree based on a DFS flattened
 *   tree
 * @param {Object} originalTree The original package tree
 * @param {String} qid The package qid
 * @return {Object} The updated package tree
 */
export function packageFindPrevious(originalTree, qid) {
  const flattenedPackages = flattenPackages(originalTree.packages);
  const flattenedPackagesQIDs = flattenedPackages.map(pkg => pkg.id);
  const currentPackageIndex = flattenedPackagesQIDs.indexOf(qid);
  const previousPackage = flattenedPackages[currentPackageIndex - 1];

  return previousPackage;
}

/**
 * @name packageFindNext
 * @description Finds the next package in the tree based on a DFS flattened
 *   tree
 * @param {Object} originalTree The original package tree
 * @param {String} qid The package qid
 * @return {Object} The updated package tree
 */
export function packageFindNext(originalTree, qid) {
  const flattenedPackages = flattenPackages(originalTree.packages);
  const flattenedPackagesQIDs = flattenedPackages.map(pkg => pkg.id);
  const currentPackageIndex = flattenedPackagesQIDs.indexOf(qid);
  const nextPackage = flattenedPackages[currentPackageIndex + 1];

  return nextPackage;
}

/**
 * @name packageToggleChildren
 * @description Toggles a package's children
 * @param {Object} originalTree The original package tree
 * @param {String} qid The package qid
 * @return {Object} The updated package tree
 */
export function packageToggleChildren(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree && currentTree.packages, (pkg) => {
      if (pkg.id === qid) {
        pkg.packages.forEach((child) => {
          child.show = !child.show;
        });

        return true;
      }

      if (pkg.packages.length > 0) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageToggleChildrenByLevel
 * @description Toggles a package's children by level
 * @param {Object} originalTree The original package tree
 * @param {String} qid The current package qid
 * @param {Number} level The toggle level
 * @return {Object} The updated package tree and new active package qid
 */
export function packageToggleChildrenByLevel(originalTree, qid, level) {
  const tree = cloneDeep(originalTree);
  let newActivePackageQID;
  (function sub(currentTree, currentLevel) {
    some(currentTree.packages, (pkg) => {
      if (currentLevel === level) {
        if (findPackageInTree(pkg, qid)) {
          newActivePackageQID = pkg.id;
        }

        pkg.packages.forEach((child) => {
          child.show = !child.show;
        });

        return true;
      }
      sub(pkg, currentLevel + 1);
      return false;
    });
  }(tree, 0));

  return { tree, activePackageQID: newActivePackageQID };
}

/**
 * @name findPackageInTree
 * @description Checks if a package exists in another package's children
 * @param {Object} parent The parent package in question
 * @param {String} qid The qid of the child package
 * @param {String} key The key for children in the tree; default 'packages'
 * @return {Object} The package found in children or nil
 */
export function findPackageInTree(parent, qid, key = 'packages') {
  let foundPackage;

  if (parent && parent.id === qid) {
    return parent;
  }
  if (parent) {
    (function sub(currentParent) {
      some(currentParent[key], (child) => {
        if (child.id === qid) {
          foundPackage = child;
          return true;
        } if (child[key].length > 0) {
          sub(child);
        }
        return false;
      });
    }(parent));
  }
  return foundPackage;
}

/**
 * @name findArchitectureOfPackage
 * @description Returns architecture of package
 * @param {Object} architectureEntities Object with architecture keys
 * @param {String} pkg target package
 * @return {Object} The architecture that has this pkg in it
 */
export function findArchitectureOfPackage(architectureEntities, pkg) {
  let parentArchitecture;

  Object.keys(architectureEntities).forEach((archQID) => {
    const currentArchitecture = architectureEntities[archQID];
    (function sub(parent) {
      some(parent.packages, (childPkg) => {
        if (childPkg.id === pkg.id) {
          parentArchitecture = currentArchitecture;
          return true;
        } if (childPkg.packages.length > 0) {
          sub(childPkg);
        }
        return false;
      });
    }(currentArchitecture));
  });

  return parentArchitecture;
}

/**
 * @name findParentNode
 * @description Checks if a package exists in another package's children
 * @param {Object} parent The parent package in question
 * @param {String} qid The qid of the child package
 * @return {Object} The package found in children or nil
 */
export function findParentNode(parent, qid, key = 'packages') {
  let parentNode;
  (function sub(currentParent) {
    some(currentParent[key], (child) => {
      if (child.id === qid) {
        parentNode = currentParent;
        return true;
      } if (child[key].length > 0) {
        sub(child);
      }
      return false;
    });
  }(parent));

  return parentNode;
}

/**
 * @name packageBreakTaxonomyLink
 * @description Sets a package's taxonomy link to broken
 * @param {Object} originalTree The original package tree
 * @param {String} qid The package qid
 * @return {Object} The updated package tree
 */
export function packageBreakTaxonomyLink(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg) => {
      if (pkg.id === qid) {
        pkg.linkBroken = true;
        return true;
      } if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageDeleteTaxonomyLink
 * @description Deletes a taxonomy from a package
 * @param {Object} originalTree The original package tree
 * @param {String} qid The package qid
 * @return {Object} The updated package tree
 */
export function packageDeleteTaxonomyLink(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg) => {
      if (pkg.id === qid) {
        pkg.taxonomy = null;
        pkg.linkBroken = false;
        return true;
      } if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageUpdateTaxonomyLink
 * @description Updates a package taxonomy link
 * @param {Object} originalTree The original package tree
 * @param {String} qid The package qid
 * @param {Object} taxonomy The new taxonomy
 * @return {Object} The updated package tree
 */
export function packageUpdateTaxonomyLink(originalTree, qid, taxonomy) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg) => {
      if (pkg.id === qid) {
        if (taxonomy) {
          pkg.term = taxonomy.term;
          pkg.taxonomy = taxonomy;
        } else {
          pkg.taxonomy = null;
        }

        pkg.linkBroken = false;
        return true;
      } if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageDeleteReferenceLink
 * @description Deletes a referencePackage from a package
 * @param {Object} originalTree The original package tree
 * @param {String} qid The package qid
 * @return {Object} The updated package tree
 */
export function packageDeleteReferenceLink(originalTree, qid) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg) => {
      if (pkg.id === qid) {
        pkg.referencePackage = null;
        return true;
      } if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name packageUpdateReferenceLink
 * @description Updates a package reference link
 * @param {Object} originalTree The original package tree
 * @param {String} qid The active package qid
 * @param {Object} package The new reference package
 * @return {Object} The updated package tree
 */
export function packageUpdateReferenceLink(originalTree, qid, referencePackage) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg) => {
      if (pkg.id === qid) {
        if (referencePackage) {
          pkg.term = referencePackage.term;
          pkg.referencePackage = referencePackage;
          if (referencePackage.taxonomy) {
            pkg.taxonomy = referencePackage.taxonomy;
            pkg.linkBroken = false;
          }
        } else {
          pkg.referencePackage = null;
        }

        return true;
      } if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

export function packageMergeTree(originalTree, treePkg) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg) => {
      if (pkg.id === treePkg.id) {
        pkg.packages = pkg.packages.concat(treePkg.packages);
        pkg.term = treePkg.term;
        pkg.referencePackage = treePkg.referencePackage;
        pkg.taxonomy = treePkg.taxonomy;
        pkg.linkBroken = false;

        return true;
      } if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

export function packageReplaceTree(originalTree, treePkg) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg) => {
      if (pkg.id === treePkg.id) {
        pkg.packages = treePkg.packages;
        pkg.term = treePkg.term;
        pkg.taxonomy = treePkg.taxonomy;
        pkg.referencePackage = treePkg.referencePackage;
        pkg.linkBroken = false;

        return true;
      } if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

// @todo: revisit these
export function buildNewTermTree(terms) {
  const rootPtr = {};
  let currentPtr = rootPtr;

  for (let i = 0; i < terms.length; i++) {
    currentPtr.term = terms[i];
    currentPtr.id = uuid();
    if (i < terms.length - 1) {
      currentPtr.children = [{}];
      ({ children: [currentPtr] } = currentPtr);
    }
  }

  return rootPtr;
}

export function removeTermFromTaxonomies(taxonomies, term) {
  for (let i = 0; i < taxonomies.length; i++) {
    (function sub(parent) {
      if (parent.children && parent.children.length > 0) {
        const foundIndex = parent.children.findIndex(
          child => child.id === term.packageID,
        );
        if (foundIndex > -1) {
          parent.children = parent.children
            .slice(0, foundIndex)
            .concat(
              parent.children.slice(foundIndex + 1, parent.children.length),
            );
        } else {
          parent.children.forEach((child) => {
            if (child.children.length > 0) {
              return sub(child);
            }
            return false;
          });
        }
      }
    }(taxonomies[i]));
  }
}

/**
 * @name orderPackageDescendents
 * @description Takes the package tree and adds order property to all descendent children
 * @param {Array} pkg A package tree
 * @param {Integer} offset A number by which to offset order for merging
 * @return {Array} The modified package tree
 */
export function orderPackageDescendents(originalTree) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    some(currentTree.packages, (pkg, index) => {
      pkg.sequence = index;
      if (pkg.packages) {
        sub(pkg);
      }
      return false;
    });
  }(tree));

  return { tree };
}

/**
 * @name sortPackagesByOrder
 * @description Takes the package tree and adds order property to all descendent children
 * @param {Array} pkg A package tree
 * @param {Integer} offset A number by which to offset order for merging
 * @return {Array} The modified package tree
 */
export function sortPackagesByOrder(originalTree) {
  const tree = cloneDeep(originalTree);
  (function sub(currentTree) {
    if (currentTree.packages && currentTree.packages.length > 0) {
      currentTree.packages = sortBy(currentTree.packages, 'sequence', 'asc');
      some(currentTree.packages, (pkg) => {
        if (pkg.packages) {
          sub(pkg);
        }
        return false;
      });
    }
  }(tree));

  return { tree };
}

/**
 * @name listToTree
 * @description Takes a list and creates a tree
 * @param {Array} list A list of taxonomies
 * @param {String} childKey The key for a list item's children: 'packages' or 'children'
 * @param {Boolean} expand Set expand value on resulting tree
 * @return {Array} The roots of the tree
 */
export function listToTree(origList, childKey = 'children', expand = true) {
  const list = origList.map(listItem => ({ ...listItem }));
  const map = {};
  const roots = [];
  let entity;
  let i;

  for (i = 0; i < list.length; i += 1) {
    map[list[i].id] = i; // initialize the map
    list[i][childKey] = []; // initialize the children
  }

  for (i = 0; i < list.length; i += 1) {
    entity = list[i];
    entity.expand = expand;

    const parentId = get(entity, 'parent.id') || get(entity, 'parentId');

    if (parentId && map[parentId] !== undefined) {
      list[map[parentId]][childKey].push(entity);
    } else {
      roots.push(entity);
    }
  }

  return roots;
}

/**
 * @name removeDescendents
 * @description Removes a node and its descendents from a tree array
 * @param {Array} list A list of taxonomies
 * @return {Array} The new tree array
 */
export function removeDescendents(list, id) {
  let filteredList = list.filter(item => item.id !== id && item.parentId !== id);
  list.forEach((item) => {
    if (item.parentId === id) {
      filteredList = removeDescendents(filteredList, item.id);
    }
  });
  return filteredList;
}

/**
 * @name updateTaxonomiesPropertyAt
 * @description Updates a given property in an array of taxonomy trees by id
 * @param {Object} taxonomies Array of taxonomies
 * @param {String} id The id of the package we want to update
 * @param {String} key The key
 * @param {String} value The new value
 */
export function updateTaxonomiesPropertyAt(taxonomies, id, key, value) {
  for (let i = 0; i < taxonomies.length; i++) {
    const tree = taxonomies[i];

    if (tree.id === id) {
      taxonomies[i][key] = value;
    } else {
      (function sub(currentTree) {
        some(currentTree.children, (tax, index) => {
          if (tax.id === id) {
            currentTree.children[index][key] = value;
          } else if (tax.children.length > 0) {
            return sub(currentTree.children[index]);
          }
          return false;
        });
      }(tree));
    }
  }
}
