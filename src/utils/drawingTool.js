// takes the architecture and packages by qid stored in the state, and the
// qid of the focused package, and generates the breadcrumb trail
export function generateDrawingBreadcrumbsTrail(architecture, packages, qid) {
  let breadcrumbs = [];
  (function sub(qidInner) {
    if (qidInner === architecture.id) {
      breadcrumbs = [
        {
          id: architecture.id,
          title: architecture.title,
        },
        ...breadcrumbs,
      ];
    } else {
      const pkg = packages[qidInner];
      breadcrumbs = [
        {
          id: pkg.id,
          title: pkg.term,
        },
        ...breadcrumbs,
      ];

      sub(pkg.parent ? pkg.parent.id : architecture.id);
    }
  }(qid));

  return breadcrumbs;
}

export function getArrowCoordsFromAnnotation(width, height, orientation) {
  let coords;
  switch (orientation) {
    case 0:
      coords = {
        x1: 0,
        y1: height / 2,
        x2: width,
        y2: height / 2,
      };
      break;
    case 45:
      coords = {
        x1: 0,
        y1: 0,
        x2: width,
        y2: height,
      };
      break;
    case 90:
      coords = {
        x1: width / 2,
        y1: 0,
        x2: width / 2,
        y2: height,
      };
      break;
    case 135:
      coords = {
        x1: width,
        y1: 0,
        x2: 0,
        y2: height,
      };
      break;
    case 180:
      coords = {
        x1: width,
        y1: height / 2,
        x2: 0,
        y2: height / 2,
      };
      break;
    case 225:
      coords = {
        x1: width,
        y1: height,
        x2: 0,
        y2: 0,
      };
      break;
    case 270:
      coords = {
        x1: width / 2,
        y1: height,
        x2: width / 2,
        y2: 0,
      };
      break;
    case 315:
      coords = {
        x1: 0,
        y1: height,
        x2: width,
        y2: 0,
      };
      break;
    default: // 0
      coords = {
        x1: 0,
        y1: height / 2,
        x2: width,
        y2: height / 2,
      };
      break;
  }
  return coords;
}
