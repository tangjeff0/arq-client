import { v4 as uuid } from 'uuid';
import { now } from 'utils/time';

export function createInvitation(collaborator) {
  const createdAt = now();

  return {
    qid: uuid(),
    createdAt,
    updatedAt: createdAt,
    invitee: collaborator,
  };
}

export default createInvitation;
