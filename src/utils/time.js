import moment from 'moment';

// ISO utils - deprecated
export function timeago(date, prefix) {
  const timethen = moment(date);
  const timenow = moment();

  return `${prefix} ${timethen.from(timenow)}`;
}

export function now() {
  return moment().toISOString();
}

export function nicetime(date) {
  return moment(date).format('MMM D, YYYY');
}

export function timeAfter(date1, date2) {
  if (date1 === undefined || date2 === undefined) {
    return false;
  }

  return moment(date1).isAfter(date2);
}
