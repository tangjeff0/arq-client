import { timeago } from 'utils/time';
import {
  GET_SHARED_DOC_LOADING,
  GET_SHARED_DOC_SUCCESS,
  GET_SHARED_DOC_ERROR,
} from 'actions/sharedDoc';

const initialState = {
  loading: false,
  error: null,
  status: '',
};

export default function ui(state = initialState, action) {
  switch (action.type) {
    case GET_SHARED_DOC_LOADING:
      return getSharedDocLoading(state, action);
    case GET_SHARED_DOC_SUCCESS:
      return getSharedDocSuccess(state, action);
    case GET_SHARED_DOC_ERROR:
      return getSharedDocError(state, action);
    default:
      return state;
  }
}

function getSharedDocLoading(state) {
  return {
    ...state,
    loading: true,
    status: 'Loading...',
  };
}

function getSharedDocSuccess(state, action) {
  const { updatedAt } = action.sharedDoc;

  return {
    ...state,
    loading: false,
    error: null,
    status: timeago(updatedAt, 'Updated'),
  };
}

function getSharedDocError(state, action) {
  return {
    ...state,
    loading: false,
    error: action.error,
    status: 'Error loading document...',
  };
}
