import { GET_SHARED_DOC_SUCCESS } from 'actions/sharedDoc';

const initialState = {
  id: null,
  title: null,
  author: null,
  createdAt: null,
  updatedAt: null,
  status: null,
  invitations: [],
};

export default function data(state = initialState, action) {
  switch (action.type) {
    case GET_SHARED_DOC_SUCCESS:
      return getSharedDocSuccess(state, action);
    default:
      return state;
  }
}

function getSharedDocSuccess(state, action) {
  const { sharedDoc } = action;
  const {
    id,
    author,
    title,
    createdAt,
    updatedAt,
    status,
    invitations,
  } = sharedDoc;

  return {
    ...state,
    id,
    author,
    title,
    createdAt,
    updatedAt,
    status,
    invitations,
  };
}
