import { v4 as uuid } from 'uuid';
import {
  createHTMLBlockEntity,
  createArchitectureBlockEntity,
  createEditorEntity,
  createArchitectureEntity,
} from 'utils/entities';
import { isArchitectureBlock } from 'utils/blocks';
import { packageToggleChildren } from 'utils/tree';
import {
  GET_SHARED_DOC_SUCCESS,
  TOGGLE_SHARED_DOC_ARCHITECTURE_PACKAGE_CHILDREN,
} from 'actions/sharedDoc';

const initialState = {
  collaborators: [],
  blocks: {},
  editors: {},
  architectures: {},
};

export default function entities(state = initialState, action) {
  switch (action.type) {
    case GET_SHARED_DOC_SUCCESS:
      return getSharedDocSuccess(state, action);
    case TOGGLE_SHARED_DOC_ARCHITECTURE_PACKAGE_CHILDREN:
      return toggleSharedDocPackageChildren(state, action);
    default:
      return state;
  }
}

function getSharedDocSuccess(state, action) {
  const { sharedDoc } = action;

  const entitiesStore = state;

  const blocks = sharedDoc.blocks.map((block) => {
    const mutableBlock = {
      ...block,
      id: uuid(),
    };

    if (!isArchitectureBlock(mutableBlock)) {
      const htmlBlockEntity = createHTMLBlockEntity(mutableBlock);
      const editorEntity = createEditorEntity(mutableBlock);

      entitiesStore.blocks[htmlBlockEntity.id] = htmlBlockEntity;
      entitiesStore.editors[editorEntity.id] = editorEntity;
    } else {
      const architectureBlockEntity = createArchitectureBlockEntity(mutableBlock);
      const architectureEntity = createArchitectureEntity(mutableBlock);

      entitiesStore.blocks[architectureBlockEntity.id] = architectureBlockEntity;
      entitiesStore.architectures[architectureEntity.id] = architectureEntity;
    }

    return mutableBlock;
  });

  return {
    ...entitiesStore,
    allBlocks: blocks.map(b => b.id),
    collaborators: sharedDoc.invitations.map(i => i.invitee),
  };
}

function toggleSharedDocPackageChildren(state, action) {
  const { architectureQID, packageQID } = action;
  const architectureEntity = state.architectures[architectureQID];

  const newPackages = packageToggleChildren(
    architectureEntity,
    packageQID,
  ).tree.packages;

  return {
    ...state,
    architectures: {
      ...state.architectures,
      [architectureQID]: {
        ...state.architectures[architectureQID],
        packages: newPackages,
      },
    },
  };
}
