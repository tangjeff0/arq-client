import {
  GET_ADMIN_ARCHITECTURE_FETCHING,
  GET_ADMIN_ARCHITECTURE_SUCCESS,
  GET_ADMIN_ARCHITECTURE_ERROR,
  RESET_ADMIN_ARCHITECTURE,
  PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_SUCCESS,
  PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_FETCHING,
  PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_SUCCESS,
  PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_ERROR,
  PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_FETCHING,
  PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_SUCCESS,
  PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_ERROR,
  HIGHLIGHT_PACKAGE,
  TOGGLE_ADMIN_ARCHITECTURE_PACKAGE_CHILDREN,
} from 'actions/adminArchitecture';
import { ARCHITECTURE_IN_REVIEW } from 'constants/architectureStates';
import { cloneDeep } from 'lodash';
import { packageToggleChildren } from 'utils/tree';
import { createArchitectureEntity } from 'utils/entities';

const initialState = {
  id: '',
  title: '',
  status: '',
  packages: [],
  createdAt: '',
  updatedAt: '',
  newTerms: [],
  ui: {
    loading: false,
    error: null,
  },
};

export default function adminArchitecture(state = initialState, action) {
  switch (action.type) {
    case GET_ADMIN_ARCHITECTURE_FETCHING:
      return {
        ...state,
        ui: {
          ...state.ui,
          loading: true,
        },
      };
    case PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_SUCCESS:
      return {
        ...state,
        status: ARCHITECTURE_IN_REVIEW,
      };
    case PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_FETCHING:
    case PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_FETCHING:
      return {
        ...state,
        ui: {
          ...state.ui,
          loading: true,
          error: null,
        },
      };
    case GET_ADMIN_ARCHITECTURE_SUCCESS:
      return getAdminArchitectureSuccess(state, action);
    case PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_SUCCESS:
      return initialState;
    case PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_SUCCESS:
      return {
        ...state,
        ui: {
          ...state.ui,
          loading: false,
          error: null,
        },
      };
    case GET_ADMIN_ARCHITECTURE_ERROR:
    case PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_ERROR:
    case PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_ERROR:
      return {
        ...state,
        ui: {
          ...state.ui,
          loading: false,
          error: action.error,
        },
      };
    case HIGHLIGHT_PACKAGE:
      return highlightPackage(state, action);
    case TOGGLE_ADMIN_ARCHITECTURE_PACKAGE_CHILDREN:
      return toggleAdminArchitecturePackageChildren(state, action);
    case RESET_ADMIN_ARCHITECTURE:
      return initialState;
    default:
      return state;
  }
}

function getAdminArchitectureSuccess(state, action) {
  const { architecture } = action;

  const newTerms = architecture.allPackages
    .filter(p => !p.taxonomy)
    .map(p => ({ packageID: p.id, term: p.term }));

  return {
    ...state,
    ...createArchitectureEntity({ architecture }),
    newTerms,
    ui: {
      ...state.ui,
      loading: false,
      error: null,
    },
  };
}

function highlightPackage(state, action) {
  const updatedPackages = cloneDeep(state.packages);
  (function sub(pkgs) {
    pkgs.forEach((pkg) => {
      if (pkg.id === action.packageID) {
        pkg.highlighted = true;
      } else {
        pkg.highlighted = false;
      }

      if (pkg.packages.length > 0) {
        sub(pkg.packages);
      }
    });
  }(updatedPackages));

  return {
    ...state,
    packages: updatedPackages,
  };
}

function toggleAdminArchitecturePackageChildren(state, action) {
  const { packageID } = action;
  const architecture = { packages: state.packages };

  const newPackages = packageToggleChildren(
    architecture,
    packageID,
  ).tree.packages;

  return {
    ...state,
    packages: newPackages,
  };
}
