import { EditorState } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';
import { stateToHTML } from 'draft-js-export-html';
import { v4 as uuid } from 'uuid';
import { now, timeago } from 'utils/time';
import { createHTMLBlock, isArchitectureBlock } from 'utils/blocks';
import {
  createHTMLBlockEntity,
  createArchitectureBlockEntity,
  createEditorEntity,
  createArchitectureEntity,
} from 'utils/entities';
import {
  packageMoveUp,
  packageMoveDown,
  packageInsertAfter,
  packageInsertBefore,
  packageInsertChild,
  packageUpdateTaxonomyLink,
  packageDeleteTaxonomyLink,
  packageIndent,
  packageUnindent,
  packageToggleChildrenByLevel,
  packageUpdateReferenceLink,
  packageDeleteReferenceLink,
  packageDelete,
  updatePackageTermAt,
  updatePackageOldTermAt,
  updatePackageDescriptionAt,
  packageFindPrevious,
  packageMergeTree,
  packageReplaceTree,
  findPackageInTree,
  findArchitectureOfPackage,
} from 'utils/tree';
import {
  INIT_DOC,
  GET_NEW_DOC,
  GET_DOC_SUCCESS,
  GET_DOC_ERROR,
  RESET_DOC,
  SAVE_DOC_SAVING,
  SAVE_DOC_SUCCESS,
  SAVE_DOC_ERROR,
  SAVE_DOC_CANCELLED,
  UPDATE_DOC_TITLE,
  UPDATE_DOC_TITLE_SUCCESS,
  UPDATE_DOC_TITLE_ERROR,
  REMOVE_DOC_ARCHITECTURE,
  INSERT_DOC_ARCHITECTURE_BLOCK,
  INSERT_DOC_ARCHITECTURE_BLOCK_SUCCESS,
  INSERT_DOC_ARCHITECTURE_BLOCK_ERROR,
  SET_DOC_ACTIVE_EDITOR_QID,
  UNSET_DOC_ACTIVE_EDITOR_QID,
  UPDATE_DOC_EDITOR,
  SET_DOC_ACTIVE_ARCHITECTURE_QID,
  UNSET_DOC_ACTIVE_ARCHITECTURE_QID,
  UPDATE_DOC_ARCHITECTURE,
  UPDATE_DOC_ARCHITECTURE_SUCCESS,
  UPDATE_DOC_ARCHITECTURE_ERROR,
  SET_DOC_ACTIVE_PACKAGE_QID,
  UNSET_DOC_ACTIVE_PACKAGE_QID,
  UPDATE_DOC_ARCHITECTURE_PACKAGE,
  UPDATE_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
  UPDATE_DOC_ARCHITECTURE_PACKAGE_ERROR,
  REMOVE_DOC_ARCHITECTURE_SUCCESS,
  REMOVE_DOC_ARCHITECTURE_ERROR,
  MOVE_DOC_ARCHITECTURE_PACKAGE_UP,
  MOVE_DOC_ARCHITECTURE_PACKAGE_UP_ERROR,
  MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN,
  MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_ERROR,
  INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER,
  INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_ERROR,
  INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE,
  INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_ERROR,
  INDENT_DOC_ARCHITECTURE_PACKAGE,
  INDENT_DOC_ARCHITECTURE_PACKAGE_ERROR,
  UNINDENT_DOC_ARCHITECTURE_PACKAGE,
  UNINDENT_DOC_ARCHITECTURE_PACKAGE_ERROR,
  TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN,
  TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_ERROR,
  TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_BY_LEVEL,
  LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY,
  UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY,
  LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR,
  UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR,
  REVERT_DOC_ARCHITECTURE_TAXONOMY,
  REVERT_DOC_ARCHITECTURE_TAXONOMY_SUCCESS,
  REVERT_DOC_ARCHITECTURE_TAXONOMY_ERROR,
  LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE,
  UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE,
  LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR,
  LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS,
  UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR,
  UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS,
  DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE,
  DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE,
  MOVE_DOC_ARCHITECTURE_PACKAGE_UP_SUCCESS,
  MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_SUCCESS,
  INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_SUCCESS,
  INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_SUCCESS,
  INDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
  UNINDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
  LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS,
  UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS,
  REMOVE_DOC_ARCHITECTURE_PACKAGE,
  REMOVE_DOC_ARCHITECTURE_PACKAGE_SUCCESS,
  REMOVE_DOC_ARCHITECTURE_PACKAGE_ERROR,
  MODIFY_DOC_ARCHITECTURE_BLOCK_ERROR,
  MODIFY_DOC_ARCHITECTURE_BLOCK_SUCCESS,
  UNDO_MERGE_PACKAGE,
  UNDO_INDENT_PACKAGE,
  UNDO_UNINDENT_PACKAGE,
  UNDO_MODIFY_PACKAGE,
  UNDO_INSERT_PACKAGE,
  UNDO_MOVE_PACKAGE_UP,
  UNDO_MOVE_PACKAGE_DOWN,
  UNDO_DELETE_PACKAGE_ONLY_CHILD,
  UNDO_DELETE_PACKAGE_AFTER,
  UNDO_DELETE_PACKAGE_BEFORE,
  UNDO_LINK_TAXONOMY_TO_PACKAGE,
  UNDO_UNLINK_TAXONOMY_TO_PACKAGE,
  UNDO_LINK_REFERENCE_TO_PACKAGE,
  UNDO_UNLINK_REFERENCE_TO_PACKAGE,
  UNDO_ARCHITECTURE_ACTION,
  DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION,
  DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_SUCCESS,
  DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_ERROR,
} from 'actions/activeDoc';
import {
  PATCH_ARCHITECTURE_STATUS_SUBMITTED_SUCCESS,
  PATCH_ARCHITECTURE_STATUS_SUBMITTED_ERROR,
  PATCH_ARCHITECTURE_STATUS_DRAFT_SUCCESS,
  PATCH_ARCHITECTURE_STATUS_DRAFT_ERROR,
} from 'actions/architecture';

const initialState = {
  id: '',
  title: '',
  blocks: [],
  createdAt: '',
  updatedAt: '',
  undoStack: [],
  entities: {
    blocks: {},
    editors: {},
    architectures: {},
  },
  ui: {
    status: '',
    loading: false,
    cancelled: false,
    error: null,
    activeEditorQID: null,
    activeArchitectureQID: null,
    activePackageQID: null,
  },
};

export default function activeDoc(state = initialState, action) {
  switch (action.type) {
    // top-level doc actions
    case INIT_DOC:
      return initDoc(state, action);
    case GET_NEW_DOC:
      return getNewDoc(state, action);
    case GET_DOC_SUCCESS:
      return getDocSuccess(state, action);
    case GET_DOC_ERROR:
      return getDocError(state, action);
    case RESET_DOC:
      return resetDoc();
    case SAVE_DOC_SAVING:
      return saveDocSaving(state, action);
    case SAVE_DOC_SUCCESS:
      return saveDocSuccess(state, action);
    case SAVE_DOC_ERROR:
      return saveDocError(state, action);
    case SAVE_DOC_CANCELLED:
      return saveDocCancelled(state, action);
    case UPDATE_DOC_TITLE:
      return updateDocTitle(state, action);
    case UPDATE_DOC_TITLE_SUCCESS:
      return saveDocSuccess(state, action);
    case UPDATE_DOC_TITLE_ERROR:
      return saveDocError(state, action);
    case INSERT_DOC_ARCHITECTURE_BLOCK:
      return insertDocArchitectureBlock(state, action);
    case INSERT_DOC_ARCHITECTURE_BLOCK_SUCCESS:
      return saveDocSuccess(state, action);
    case INSERT_DOC_ARCHITECTURE_BLOCK_ERROR:
      return saveDocError(state, action);
    case SET_DOC_ACTIVE_EDITOR_QID:
      return setDocActiveEditorQID(state, action);
    case UNSET_DOC_ACTIVE_EDITOR_QID:
      return unsetDocActiveEditorQID(state);
    case UPDATE_DOC_EDITOR:
      return updateDocEditor(state, action);
    case REMOVE_DOC_ARCHITECTURE:
      return removeDocArchitecture(state, action);
    case REMOVE_DOC_ARCHITECTURE_SUCCESS:
      return saveDocSuccess(state, action);
    case REMOVE_DOC_ARCHITECTURE_ERROR:
      return saveDocError(state, action);
    case SET_DOC_ACTIVE_ARCHITECTURE_QID:
      return setDocActiveArchitectureQID(state, action);
    case UNSET_DOC_ACTIVE_ARCHITECTURE_QID:
      return unsetDocActiveArchitectureQID(state);
    case UPDATE_DOC_ARCHITECTURE:
      return updateDocArchitecture(state, action);
    case UPDATE_DOC_ARCHITECTURE_SUCCESS:
      return saveDocSuccess(state, action);
    case UPDATE_DOC_ARCHITECTURE_ERROR:
      return saveDocError(state, action);
    case SET_DOC_ACTIVE_PACKAGE_QID:
      return setDocActivePackageQID(state, action);
    case UNSET_DOC_ACTIVE_PACKAGE_QID:
      return unsetDocActivePackageQID(state);
    // update package description
    case DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION:
      return updateDocArchitecturePackageUpdateDescription(state, action);
    case DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_SUCCESS:
      return saveDocSuccess(state, action);
    case DOC_ARCHITECTURE_PACKAGE_UPDATE_DESCRIPTION_ERROR:
      return saveDocError(state, action);
    // link taxonomy
    case LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY:
      return linkDocArchitecturePackageTaxonomy(state, action);
    case LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS:
      return saveDocSuccess(state, action);
    case LINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR:
      return saveDocError(state, action);
    // unlink taxonomy
    case UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY:
      return unlinkDocArchitecturePackageTaxonomy(state, action);
    case UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_SUCCESS:
      return saveDocSuccess(state, action);
    case UNLINK_DOC_ARCHITECTURE_PACKAGE_TAXONOMY_ERROR:
      return saveDocError(state, action);
    // link reference package
    case LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE:
      return linkDocArchitecturePackageReference(state, action);
    case LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS:
      return saveDocSuccess(state, action);
    case LINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR:
      return saveDocError(state, action);
    // unlink reference package
    case UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE:
      return unlinkDocArchitecturePackageReference(state, action);
    case UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_SUCCESS:
      return saveDocSuccess(state, action);
    case UNLINK_DOC_ARCHITECTURE_PACKAGE_REFERENCE_ERROR:
      return saveDocError(state, action);
    // revert taxonomy
    case REVERT_DOC_ARCHITECTURE_TAXONOMY:
      return revertDocArchitectureTaxonomy(state, action);
    case REVERT_DOC_ARCHITECTURE_TAXONOMY_SUCCESS:
      return saveDocSuccess(state, action);
    case REVERT_DOC_ARCHITECTURE_TAXONOMY_ERROR:
      return saveDocError(state, action);
      // remove package
    case REMOVE_DOC_ARCHITECTURE_PACKAGE:
      return removeDocArchitecturePackage(state, action);
    case REMOVE_DOC_ARCHITECTURE_PACKAGE_SUCCESS:
      return saveDocSuccess(state, action);
    case REMOVE_DOC_ARCHITECTURE_PACKAGE_ERROR:
      return saveDocError(state, action);
    // insert taxonomy tree
    case DOC_ARCHITECTURE_PACKAGE_INSERT_TAXONOMY_TREE:
      return mergePackage(state, action);
    // insert reference tree
    case DOC_ARCHITECTURE_PACKAGE_INSERT_REFERENCE_TREE:
      return mergePackage(state, action);
    // move package up
    case MOVE_DOC_ARCHITECTURE_PACKAGE_UP:
      return movePackageUp(state, action);
    case MOVE_DOC_ARCHITECTURE_PACKAGE_UP_SUCCESS:
      return saveDocSuccess(state, action);
    case MOVE_DOC_ARCHITECTURE_PACKAGE_UP_ERROR:
      return saveDocError(state, action);
    // move package down
    case MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN:
      return movePackageDown(state, action);
    case MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_SUCCESS:
      return saveDocSuccess(state, action);
    case MOVE_DOC_ARCHITECTURE_PACKAGE_DOWN_ERROR:
      return saveDocError(state, action);
    // insert package after
    case INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER:
      return insertPackageAfter(state, action);
    case INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_SUCCESS:
      return saveDocSuccess(state, action);
    case INSERT_DOC_ARCHITECTURE_PACKAGE_AFTER_ERROR:
      return saveDocError(state, action);
    // insert package before
    case INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE:
      return insertPackageBefore(state, action);
    case INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_SUCCESS:
      return saveDocSuccess(state, action);
    case INSERT_DOC_ARCHITECTURE_PACKAGE_BEFORE_ERROR:
      return saveDocError(state, action);
    // indent package
    case INDENT_DOC_ARCHITECTURE_PACKAGE:
      return indentDocArchitecturePackage(state, action);
    case INDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS:
      return saveDocSuccess(state, action);
    case INDENT_DOC_ARCHITECTURE_PACKAGE_ERROR:
      return saveDocError(state, action);
    // unindent package
    case UNINDENT_DOC_ARCHITECTURE_PACKAGE:
      return unindentDocArchitecturePackage(state, action);
    case UNINDENT_DOC_ARCHITECTURE_PACKAGE_SUCCESS:
      return saveDocSuccess(state, action);
    case UNINDENT_DOC_ARCHITECTURE_PACKAGE_ERROR:
      return saveDocError(state, action);
    // toggle package children
    case TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN:
      return toggleDocArchitecturePackageChildren(state, action);
    case TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_ERROR:
      return saveDocError(state, action);
    case TOGGLE_DOC_ARCHITECTURE_PACKAGE_CHILDREN_BY_LEVEL:
      return toggleDocArchitecturePackageChildrenByLevel(state, action);
    // patch arch status
    case PATCH_ARCHITECTURE_STATUS_SUBMITTED_SUCCESS:
      return updateDocArchitectureStatus(state, action);
    case PATCH_ARCHITECTURE_STATUS_SUBMITTED_ERROR:
      return patchArchitectureStatusError(state, action);
    case PATCH_ARCHITECTURE_STATUS_DRAFT_SUCCESS:
      return updateDocArchitectureStatus(state, action);
    case PATCH_ARCHITECTURE_STATUS_DRAFT_ERROR:
      return patchArchitectureStatusError(state, action);
    // update package
    case UPDATE_DOC_ARCHITECTURE_PACKAGE:
      return updateDocArchitecturePackage(state, action);
    case UPDATE_DOC_ARCHITECTURE_PACKAGE_SUCCESS:
      return updateDocArchitecturePackageSuccess(state, action);
    case UPDATE_DOC_ARCHITECTURE_PACKAGE_ERROR:
      return saveDocError(state, action);
    // modify architecture block
    case MODIFY_DOC_ARCHITECTURE_BLOCK_SUCCESS:
      return saveDocSuccess(state, action);
    case MODIFY_DOC_ARCHITECTURE_BLOCK_ERROR:
      return saveDocError(state, action);
    // undo actions
    case UNDO_MERGE_PACKAGE:
      return replacePackage(state, action);
    case UNDO_INDENT_PACKAGE:
      return unindentDocArchitecturePackage(state, action);
    case UNDO_UNINDENT_PACKAGE:
      return indentDocArchitecturePackage(state, action);
    case UNDO_MODIFY_PACKAGE:
      return updateDocArchitecturePackage(state, action);
    case UNDO_INSERT_PACKAGE:
      return removeDocArchitecturePackage(state, action);
    case UNDO_DELETE_PACKAGE_ONLY_CHILD:
      return insertPackageChild(state, action);
    case UNDO_DELETE_PACKAGE_AFTER:
      return insertPackageAfter(state, action);
    case UNDO_DELETE_PACKAGE_BEFORE:
      return insertPackageBefore(state, action);
    case UNDO_MOVE_PACKAGE_UP:
      return movePackageDown(state, action);
    case UNDO_MOVE_PACKAGE_DOWN:
      return movePackageUp(state, action);
    case UNDO_LINK_TAXONOMY_TO_PACKAGE:
      return linkDocArchitecturePackageTaxonomy(state, action);
    case UNDO_UNLINK_TAXONOMY_TO_PACKAGE:
      return linkDocArchitecturePackageTaxonomy(state, action);
    case UNDO_LINK_REFERENCE_TO_PACKAGE:
      return linkDocArchitecturePackageReference(state, action);
    case UNDO_UNLINK_REFERENCE_TO_PACKAGE:
      return linkDocArchitecturePackageReference(state, action);
    case UNDO_ARCHITECTURE_ACTION:
      return undoArchitectureActionSuccess(state, action);
    default:
      return state;
  }
}

/** doc initializers, getters, setters, and persistent data actions */

function initDoc() {
  return {
    ...initialState,
    ui: {
      ...initialState.ui,
      loading: true,
      status: 'Loading...',
    },
  };
}

function getNewDoc(state) {
  const createdAt = now();
  const updatedAt = createdAt;
  const htmlBlock = createHTMLBlock();
  const htmlBlockEntity = createHTMLBlockEntity(htmlBlock);
  const editorEntity = createEditorEntity(htmlBlock);

  return {
    ...state,
    id: uuid(),
    blocks: [htmlBlock.id],
    createdAt,
    updatedAt,
    entities: {
      ...state.entities,
      blocks: {
        [htmlBlockEntity.id]: htmlBlockEntity,
      },
      editors: {
        [editorEntity.id]: editorEntity,
      },
    },
    ui: {
      ...state.ui,
      loading: false,
      status: 'New Document',
    },
  };
}

function getDocSuccess(state, action) {
  const { doc } = action;
  const {
    id,
    title,
    createdAt,
    updatedAt,
    blocks,
  } = doc;
  const { entities } = state;

  const mutableBlocks = blocks.map((b) => {
    const block = {
      id: uuid(),
      ...b,
    };

    if (!isArchitectureBlock(block)) {
      const htmlBlockEntity = createHTMLBlockEntity(block);
      const editorEntity = createEditorEntity(block);

      entities.blocks[htmlBlockEntity.id] = htmlBlockEntity;
      entities.editors[editorEntity.id] = editorEntity;
    } else {
      const architectureBlockEntity = createArchitectureBlockEntity(block);
      const architectureEntity = createArchitectureEntity(block);

      entities.blocks[architectureBlockEntity.id] = architectureBlockEntity;
      entities.architectures[architectureEntity.id] = architectureEntity;
    }

    return block;
  });

  return {
    ...state,
    id,
    title,
    blocks: mutableBlocks.map(b => b.id),
    createdAt,
    updatedAt,
    entities,
    ui: {
      ...state.ui,
      loading: false,
      status: timeago(updatedAt, 'Updated'),
      error: null,
    },
  };
}

function getDocError(state, action) {
  return {
    ...state,
    ui: {
      ...state.ui,
      status: 'Error loading document...',
      loading: false,
      error: action.error,
    },
  };
}

function patchArchitectureStatusError(state, action) {
  return {
    ...state,
    ui: {
      ...state.ui,
      status: 'Error updating architecture...',
      loading: false,
      error: action.error,
    },
  };
}

function resetDoc() {
  return initialState;
}

function saveDocSaving(state) {
  return {
    ...state,
    ui: {
      ...state.ui,
      loading: true,
      status: 'Saving document...',
    },
  };
}

function saveDocSuccess(state, action) {
  const updatedAt = now();

  return {
    ...state,
    updatedAt,
    undoStack: [...state.undoStack, action],
    ui: {
      ...state.ui,
      loading: false,
      status: timeago(updatedAt, 'Updated'),
      error: null,
    },
  };
}

function undoArchitectureActionSuccess(state) {
  const updatedAt = now();

  return {
    ...state,
    updatedAt,
    ui: {
      ...state.ui,
      loading: false,
      status: timeago(updatedAt, 'Updated'),
      error: null,
    },
  };
}

function updateDocArchitecturePackageSuccess(state, action) {
  const updatedAt = now();

  const architectureEntity = state.entities.architectures[state.ui.activeArchitectureQID];
  const newPackages = updatePackageOldTermAt(architectureEntity, action.pkg).tree
    .packages;

  return {
    ...state,
    updatedAt,
    ui: {
      ...state.ui,
      loading: false,
      status: timeago(updatedAt, 'Updated'),
      error: null,
    },
    undoStack: [...state.undoStack, action],
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [state.ui.activeArchitectureQID]: {
          ...state.entities.architectures[state.ui.activeArchitectureQID],
          packages: newPackages,
        },
      },
    },
  };
}

function updateDocArchitecturePackageUpdateDescription(state, action) {
  const updatedAt = now();

  let architectureEntity = state.entities.architectures[state.ui.activeArchitectureQID];

  if (!architectureEntity) {
    architectureEntity = findArchitectureOfPackage(
      state.entities.architectures,
      action.pkg,
    );
  }

  // if null have to find current architecture through current package?

  const newPackages = updatePackageDescriptionAt(architectureEntity, action.pkg)
    .tree.packages;

  return {
    ...state,
    updatedAt,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [state.ui.activeArchitectureQID]: {
          ...state.entities.architectures[state.ui.activeArchitectureQID],
          packages: newPackages,
        },
      },
    },
  };
}

function saveDocError(state, action) {
  return {
    ...state,
    ui: {
      ...state.ui,
      status: 'Error saving document...',
      error: action.error,
    },
  };
}

function saveDocCancelled(state) {
  return {
    ...state,
    ui: {
      ...state.ui,
      cancelled: true,
    },
  };
}

/** top level doc stuff */

function updateDocTitle(state, action) {
  const updatedAt = now();

  return {
    ...state,
    title: action.title,
    updatedAt,
    ui: {
      ...state.ui,
      status: timeago(updatedAt, 'Updated'),
      error: null,
    },
  };
}

/** doc block stuff */

function insertDocArchitectureBlock(state, action) {
  const architectureBlockEntity = createArchitectureBlockEntity(
    action.architectureBlock,
  );
  const architectureEntity = createArchitectureEntity(action.architectureBlock);
  const htmlBlockEntity = createHTMLBlockEntity(action.htmlBlock);
  const editorEntity = createEditorEntity(action.htmlBlock);

  return {
    ...state,
    blocks: [...state.blocks, architectureBlockEntity.id, htmlBlockEntity.id],
    entities: {
      ...state.entities,
      blocks: {
        ...state.entities.blocks,
        [architectureBlockEntity.id]: architectureBlockEntity,
        [htmlBlockEntity.id]: htmlBlockEntity,
      },
      architectures: {
        ...state.entities.architectures,
        [architectureEntity.id]: architectureEntity,
      },
      editors: {
        ...state.entities.editors,
        [editorEntity.id]: editorEntity,
      },
    },
  };
}

/** doc editor(s) stuff */

function setDocActiveEditorQID(state, action) {
  return {
    ...state,
    ui: {
      ...state.ui,
      activeEditorQID: action.qid,
    },
  };
}

function unsetDocActiveEditorQID(state) {
  return {
    ...state,
    ui: {
      ...state.ui,
      activeEditorQID: null,
    },
  };
}

function updateDocEditor(state, action) {
  const { qid } = action;
  const { editorState } = action;
  const editor = state.entities.editors[qid];

  return {
    ...state,
    entities: {
      ...state.entities,
      editors: {
        ...state.entities.editors,
        [qid]: {
          ...editor,
          editorState,
        },
      },
    },
  };
}

/** doc architecture(s) stuff */

function setDocActiveArchitectureQID(state, action) {
  return {
    ...state,
    ui: {
      ...state.ui,
      activeArchitectureQID: action.qid,
    },
  };
}

function unsetDocActiveArchitectureQID(state) {
  return {
    ...state,
    ui: {
      ...state.ui,
      activeArchitectureQID: null,
    },
  };
}

function updateDocArchitecture(state, action) {
  const { id } = action.architecture;

  return {
    ...state,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [id]: {
          ...action.architecture,
        },
      },
    },
  };
}

function removeDocArchitecture(state, action) {
  const { blockQID } = action;
  const deleteInd = state.blocks.indexOf(blockQID);
  const architectureQid = state.entities.blocks[state.blocks[deleteInd]].architecture;

  // Assumption here is that the architecture being removed
  // has an element before and after it to merge.
  const beforeEditor = state.entities.editors[state.blocks[deleteInd - 1]];
  const afterEditor = state.entities.editors[state.blocks[deleteInd + 1]];

  const joinedHTML = stateToHTML(beforeEditor.editorState.getCurrentContent())
    + stateToHTML(afterEditor.editorState.getCurrentContent());

  beforeEditor.editorState = EditorState.createWithContent(
    stateFromHTML(joinedHTML),
  );

  delete state.entities.blocks[state.blocks[deleteInd + 1]];
  delete state.entities.blocks[blockQID];
  delete state.entities.editors[state.blocks[deleteInd + 1]];
  delete state.entities.editors[blockQID];
  delete state.entities.architectures[architectureQid];

  state.blocks.splice(deleteInd, 2);

  // This is faster than return state;
  // I think it's because diffing tool React uses
  // takes longer to figure out diffs than a straight up replace?
  return {
    ...state,
  };
}

function updateDocArchitectureStatus(state, action) {
  const updatedAt = now();

  return {
    ...state,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [action.qid]: {
          ...state.entities.architectures[action.qid],
          status: action.status,
          updatedAt,
        },
      },
    },
  };
}

/** doc architecture packages stuff */

function setDocActivePackageQID(state, action) {
  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: action.qid,
    },
  };
}

function unsetDocActivePackageQID(state) {
  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: null,
    },
  };
}

function mergePackage(state, action) {
  const updatedAt = now();

  const { activeArchitectureQID } = state.ui;
  const architectureEntity = state.entities.architectures[activeArchitectureQID];

  const newUndoStack = state.undoStack;

  // if it's an undo action, don't push onto undo stack.
  if (!action.undo) {
    const oldPackage = findPackageInTree(architectureEntity, action.pkg.id);

    state.undoStack.push({
      ...action,
      action: 'mergePackage',
      oldPackage,
      activePackageQID: oldPackage.id,
      activeArchitectureQID,
    });
  } else {
    newUndoStack.pop();
  }

  const newPackages = packageMergeTree(architectureEntity, action.pkg).tree
    .packages;

  return {
    ...state,
    undoStack: newUndoStack,
    ui: {
      ...state.ui,
      loading: false,
      status: timeago(updatedAt, 'Updated'),
      error: null,
    },
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [activeArchitectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function replacePackage(state, action) {
  // this should only be used for undoing a merge action.

  if (!action.undo) return state;

  const updatedAt = now();

  const newUndoStack = state.undoStack;
  const undoAction = newUndoStack.pop();

  const { activeArchitectureQID } = undoAction;
  const architectureEntity = state.entities.architectures[activeArchitectureQID];

  const newPackages = packageReplaceTree(
    architectureEntity,
    undoAction.oldPackage,
  ).tree.packages;

  return {
    ...state,
    undoStack: newUndoStack,
    ui: {
      ...state.ui,
      activePackageQID: undoAction.activePackageQID,
      loading: false,
      status: timeago(updatedAt, 'Updated'),
      error: null,
    },
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [activeArchitectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function updateDocArchitecturePackage(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;

  const { activeArchitectureQID } = state.ui;
  const architectureEntity = state.entities.architectures[activeArchitectureQID];

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();
  }

  const newTerm = action.undo ? undoAction.pkg.oldTerm : action.pkg.term;
  const packageQID = action.undo ? undoAction.pkg.id : action.pkg.id;

  const newPackages = updatePackageTermAt(architectureEntity, packageQID, newTerm)
    .tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: action.undo
        ? undoAction.activePackageQID
        : state.ui.activePackageQID,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [activeArchitectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function movePackageUp(state, action) {
  const newUndoStack = state.undoStack;
  let undoAction;

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();
  }

  const packageQID = action.undo
    ? undoAction.activePackageQID
    : action.packageQID;
  const architectureQID = action.undo
    ? undoAction.activeArchitectureQID
    : action.architectureQID;

  const architectureEntity = state.entities.architectures[architectureQID];
  const newPackages = packageMoveUp(architectureEntity, packageQID).tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: action.undo
        ? undoAction.activePackageQID
        : state.ui.activePackageQID,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function movePackageDown(state, action) {
  const newUndoStack = state.undoStack;
  let undoAction;

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();
  }

  const packageQID = action.undo
    ? undoAction.activePackageQID
    : action.packageQID;
  const architectureQID = action.undo
    ? undoAction.activeArchitectureQID
    : action.architectureQID;

  const architectureEntity = state.entities.architectures[architectureQID];
  const newPackages = packageMoveDown(architectureEntity, packageQID).tree
    .packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: action.undo
        ? undoAction.activePackageQID
        : state.ui.activePackageQID,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function insertPackageChild(state) {
  const newUndoStack = state.undoStack;
  const undoAction = newUndoStack.pop();

  const architectureQID = undoAction.activeArchitectureQID;
  const targetQID = undoAction.targetPackage.id;
  const newPackage = undoAction.pkg;

  const architectureEntity = state.entities.architectures[architectureQID];
  const newPackages = packageInsertChild(
    architectureEntity,
    targetQID,
    newPackage,
  ).tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: newPackage.id,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function insertPackageAfter(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();
  }

  const architectureQID = action.undo
    ? undoAction.activeArchitectureQID
    : state.ui.activeArchitectureQID;
  const targetQID = action.undo
    ? undoAction.targetPackage.id
    : state.ui.activePackageQID;
  const newPackage = action.undo ? undoAction.pkg : action.pkg;

  const architectureEntity = state.entities.architectures[architectureQID];
  const newPackages = packageInsertAfter(
    architectureEntity,
    targetQID,
    newPackage,
  ).tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: newPackage.id,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function insertPackageBefore(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();
  }

  const architectureQID = action.undo
    ? undoAction.activeArchitectureQID
    : state.ui.activeArchitectureQID;
  const targetQID = action.undo
    ? undoAction.targetPackage.id
    : state.ui.activePackageQID;
  const newPackage = action.undo ? undoAction.pkg : action.pkg;

  const architectureEntity = state.entities.architectures[architectureQID];

  const newPackages = packageInsertBefore(
    architectureEntity,
    targetQID,
    newPackage,
  ).tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: newPackage.id,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function indentDocArchitecturePackage(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();
  }

  const architectureQID = action.undo
    ? undoAction.activeArchitectureQID
    : action.architectureQID;
  const packageQID = action.undo
    ? undoAction.activePackageQID
    : action.packageQID;

  const architectureEntity = state.entities.architectures[architectureQID];
  const newPackages = packageIndent(architectureEntity, packageQID).tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: action.undo
        ? undoAction.activePackageQID
        : state.ui.activePackageQID,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function unindentDocArchitecturePackage(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();
  }

  const architectureQID = action.undo
    ? undoAction.activeArchitectureQID
    : action.architectureQID;
  const packageQID = action.undo
    ? undoAction.activePackageQID
    : action.packageQID;

  const architectureEntity = state.entities.architectures[architectureQID];
  const newPackages = packageUnindent(architectureEntity, packageQID).tree
    .packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: action.undo
        ? undoAction.activePackageQID
        : state.ui.activePackageQID,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function toggleDocArchitecturePackageChildren(state, action) {
  const { architectureQID } = action;
  const architectureEntity = state.entities.architectures[architectureQID];

  return {
    ...state,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: action.packages,
        },
      },
    },
  };
}

function toggleDocArchitecturePackageChildrenByLevel(state, action) {
  const { architectureQID } = action;
  const { packageQID } = action;
  const { level } = action;

  const architectureEntity = state.entities.architectures[architectureQID];
  const { tree, activePackageQID } = packageToggleChildrenByLevel(
    architectureEntity,
    packageQID,
    level,
  );
  const newPackages = tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID,
    },
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function linkDocArchitecturePackageTaxonomy(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;
  let activeArchitectureQID;
  let activePackageQID;
  let activeArchitectureEntity;
  let taxonomyToLink;

  if (!action.undo) {
    activeArchitectureQID = action.activeArchitectureQID || state.ui.activeArchitectureQID;
    activePackageQID = action.activePackageQID || state.ui.activePackageQID;
    activeArchitectureEntity = state.entities.architectures[activeArchitectureQID];
    taxonomyToLink = action.taxonomy;
  } else {
    undoAction = newUndoStack.pop();

    ({ activeArchitectureQID, activePackageQID } = undoAction);

    activeArchitectureEntity = state.entities.architectures[activeArchitectureQID];
    taxonomyToLink = undoAction.oldPackage.taxonomy;
  }

  const newPackages = packageUpdateTaxonomyLink(
    activeArchitectureEntity,
    activePackageQID,
    taxonomyToLink,
  ).tree.packages;

  if (!activeArchitectureQID || !activePackageQID) return state;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [activeArchitectureQID]: {
          ...activeArchitectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function revertDocArchitectureTaxonomy(state, action) {
  const { activeArchitectureQID } = state.ui;
  const architectureEntity = state.entities.architectures[activeArchitectureQID];

  const newPackages = packageUpdateTaxonomyLink(
    architectureEntity,
    action.pkg.id,
    action.pkg.taxonomy,
  ).tree.packages;

  return {
    ...state,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [activeArchitectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function unlinkDocArchitecturePackageTaxonomy(state, action) {
  const activeArchitectureQID = action.activeArchitectureQID || state.ui.activeArchitectureQID;
  const activePackageQID = action.activePackageQID || state.ui.activePackageQID;
  const activeArchitectureEntity = state.entities.architectures[activeArchitectureQID];

  if (!activeArchitectureQID || !activePackageQID) return state;

  const newPackages = packageDeleteTaxonomyLink(
    activeArchitectureEntity,
    activePackageQID,
  ).tree.packages;

  return {
    ...state,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [activeArchitectureQID]: {
          ...activeArchitectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function linkDocArchitecturePackageReference(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;
  let activeArchitectureQID;
  let activePackageQID;
  let referenceToLink;

  // if it's an undo action, don't push onto undo stack.
  if (action.undo) {
    undoAction = newUndoStack.pop();

    ({ activeArchitectureQID, activePackageQID } = undoAction);
    referenceToLink = undoAction.oldPackage.referencePackage;
  } else {
    ({ activeArchitectureQID, activePackageQID } = state.ui);
    referenceToLink = action.reference;
  }

  const activeArchitectureEntity = state.entities.architectures[activeArchitectureQID];

  const newPackages = packageUpdateReferenceLink(
    activeArchitectureEntity,
    activePackageQID,
    referenceToLink,
  ).tree.packages;

  return {
    ...state,
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [activeArchitectureQID]: {
          ...activeArchitectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function unlinkDocArchitecturePackageReference(state, action) {
  const { pkgQID } = action;
  const { architectures } = state.entities;
  const architectureEntity = findArchitectureOfPackage(architectures, { id: pkgQID });

  const newPackages = packageDeleteReferenceLink(
    architectureEntity,
    pkgQID,
  ).tree.packages;

  return {
    ...state,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureEntity.id]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}

function removeDocArchitecturePackage(state, action) {
  let undoAction;
  const newUndoStack = state.undoStack;

  let architectureQID; let packageQID; let newActivePackageQID; let
    architectureEntity;

  // if it's an undo action, don't push onto undo stack.
  if (!action.undo) {
    architectureQID = state.ui.activeArchitectureQID;
    packageQID = action.pkgQID;
    architectureEntity = state.entities.architectures[architectureQID];
    newActivePackageQID = packageFindPrevious(architectureEntity, action.pkgQID)
      .id;
  } else {
    undoAction = newUndoStack.pop();
    architectureQID = undoAction.activeArchitectureQID;
    packageQID = undoAction.pkg.id;
    newActivePackageQID = undoAction.activePackageQID;
    architectureEntity = state.entities.architectures[architectureQID];
  }

  const newPackages = packageDelete(architectureEntity, packageQID).tree.packages;

  return {
    ...state,
    ui: {
      ...state.ui,
      activePackageQID: newActivePackageQID,
    },
    undoStack: newUndoStack,
    entities: {
      ...state.entities,
      architectures: {
        ...state.entities.architectures,
        [architectureQID]: {
          ...architectureEntity,
          packages: newPackages,
        },
      },
    },
  };
}
