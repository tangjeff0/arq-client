import {
  GET_PUBLISHED_ARCHITECTURE_FETCHING,
  GET_PUBLISHED_ARCHITECTURE_SUCCESS,
  GET_PUBLISHED_ARCHITECTURE_ERROR,
} from 'actions/architecture';

const initialState = {
  qid: '',
  title: '',
  status: '',
  packages: [],
  createdAt: '',
  updatedAt: '',
  newTerms: [],
  ui: {
    loading: false,
    error: null,
  },
};

export default function activeArchitecture(state = initialState, action) {
  switch (action.type) {
    case GET_PUBLISHED_ARCHITECTURE_FETCHING:
      return {
        ...state,
        ui: {
          ...state.ui,
          loading: true,
        },
      };
    case GET_PUBLISHED_ARCHITECTURE_SUCCESS:
      return {
        ...action.architecture,
        ui: {
          loading: false,
          error: null,
        },
      };
    case GET_PUBLISHED_ARCHITECTURE_ERROR:
      return {
        ...state,
        ui: {
          loading: false,
          error: action.error,
        },
      };
    default:
      return state;
  }
}
