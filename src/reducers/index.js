import { combineReducers } from 'redux';
import activeDoc from 'reducers/activeDoc';
import sharedDoc from 'reducers/sharedDoc';
import activeArchitecture from 'reducers/activeArchitecture';
import adminArchitecture from 'reducers/adminArchitecture';
import adminArchitectureTerms from 'reducers/adminArchitectureTerms';

const reducer = combineReducers({
  activeArchitecture,
  activeDoc,
  sharedDoc,
  adminArchitecture,
  adminArchitectureTerms,
});

export default reducer;
