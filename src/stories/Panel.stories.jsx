import React from 'react';
import Panel from 'components/Panel/Panel.jsx';
import { storiesOf } from '@storybook/react';

storiesOf('Panel', module)
  .add('with Panel.Content', () => (
    <Panel>
      <Panel.Content>Panel.Content</Panel.Content>
    </Panel>
  ))
  .add('with Panel.Content and Panel.Heading', () => (
    <Panel>
      <Panel.Heading>Panel.Heading</Panel.Heading>
      <Panel.Content>Panel.Content</Panel.Content>
    </Panel>
  ));
