import React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';
import configureStore from 'configureStore';
import { MemoryRouter } from 'react-router-dom';
import {
  architecture,
  architectureSubmitted,
  architectureInReview,
  architectureRejected,
  architecturePublished,
} from 'mocks/architectures';
import { document } from 'mocks/documents';
import ArchitectureToolbar from 'components/Architecture/ArchitectureToolbar/ArchitectureToolbar.jsx';

const store = configureStore();

storiesOf('ArchitectureToolbar', module)
  .addDecorator(story => <Provider store={store}>{story()}</Provider>)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('default (draft)', () => (
    <ArchitectureToolbar architecture={architecture} />
  ))
  .add('default, with clipboard', () => (
    <ArchitectureToolbar architecture={architecture} clipboard />
  ))
  .add('default, with clipboard, actionable', () => (
    <ArchitectureToolbar
      architecture={architecture}
      clipboard
      actionable
    />
  ))
  .add('default, with clipboard, actionable, publishedView', () => (
    <ArchitectureToolbar
      architecture={architecture}
      clipboard
      actionable
      publishedView
    />
  ))
  .add('default, with clipboard, actionable, documentQID', () => (
    <ArchitectureToolbar
      architecture={architecture}
      clipboard
      actionable
      documentQID={document.id}
    />
  ))
  .add('submitted, with clipboard, actionable, documentQID', () => (
    <ArchitectureToolbar
      architecture={architectureSubmitted}
      clipboard
      actionable
      documentQID={document.id}
    />
  ))
  .add('in review, with clipboard, actionable, documentQID', () => (
    <ArchitectureToolbar
      architecture={architectureInReview}
      clipboard
      actionable
      documentQID={document.id}
    />
  ))
  .add('rejected, with clipboard, actionable, documentQID', () => (
    <ArchitectureToolbar
      architecture={architectureRejected}
      clipboard
      actionable
      documentQID={document.id}
    />
  ))
  .add('published, with clipboard, actionable, documentQID', () => (
    <ArchitectureToolbar
      architecture={architecturePublished}
      clipboard
      actionable
      documentQID={document.id}
    />
  ));
