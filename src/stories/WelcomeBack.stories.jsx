import React from 'react';
import { storiesOf } from '@storybook/react';
import WelcomeBack from 'components/WelcomeBack/WelcomeBack.jsx';

const username = 'fwright@email.com';

storiesOf('WelcomeBack', module)
  .add('default', () => <WelcomeBack />)
  .add('appearance dark', () => <WelcomeBack appearance="dark" username={username} />)
  .add('appearance light', () => <WelcomeBack appearance="light" username={username} />);
