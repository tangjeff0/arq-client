import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { editorState } from 'mocks/editors';
import EditorContainer from 'stories/EditorContainer.jsx';

storiesOf('Editor', module)
  .addDecorator(story => (
    <div style={{ padding: '2rem', backgroundColor: '#fff' }}>{story()}</div>
  ))
  .add('default', () => <EditorContainer />)
  .add('with editor state', () => <EditorContainer editorState={editorState} />)
  .add('with onFocus and onBlur callbacks', () => (
    <EditorContainer onFocus={action('onFocus')} onBlur={action('onBlur')} />
  ))
  .add('with placeholder', () => (
    <EditorContainer placeholder="This is your notepad. Use it for notes, checklists or anything. When you're ready, click the Insert Architecture button to start creating your Architecture." />
  ))
  .add('with editor state and readOnly', () => (
    <EditorContainer editorState={editorState} readOnly />
  ));
