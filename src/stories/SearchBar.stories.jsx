import React from 'react';
import { storiesOf } from '@storybook/react';
import SearchBar from 'components/SearchBar/SearchBar.jsx';

storiesOf('SearchBar', module)
  .add('default', () => <SearchBar />)
  .add('secondary appearance', () => <SearchBar appearance="secondary" />)
  .add('read only', () => <SearchBar readOnly />)
  .add('searching', () => <SearchBar searching />)
  .add('read only and searching', () => (
    <SearchBar readOnly searching />
  ));
