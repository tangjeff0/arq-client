import React, { Component } from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { SEARCH_PREFIX } from 'constants/search.js';
import Architecture from 'components/Architecture/Architecture.jsx';
import logger from 'utils/logger';

class ArchitectureContainer extends Component {
  constructor(props) {
    super(props);

    const { architecture } = this.props;

    this.state = {
      entities: {
        architectures: {
          [architecture.id]: architecture,
        },
      },
      ui: {
        activeArchitectureQID: '',
        activePackageQID: '',
      },
    };
  }

  onTitleFocus = () => {}

  onTitleUpdate = (title) => {
    logger(title);
  }

  onStatusChange = (status) => {
    logger(status);
  }

  onDelete = () => {}

  onUpdateActivePackageQID = (qid) => {
    this.setState(prevState => ({
      ...prevState,
      ui: {
        ...prevState.ui,
        activePackageQID: qid,
      },
    }));
  }

  onPackageFocus = (qid) => {
    this.setState(prevState => ({
      ...prevState,
      ui: {
        ...prevState.ui,
        activePackageQID: qid,
      },
    }));
  }

  onPackageBlur = (qid) => {
    logger(qid);
  }

  onPackageTermChange = (term) => {
    if (term.length === 0) {
      logger('empty term');
    } else if (term[0] === SEARCH_PREFIX) {
      logger('slash prefixed term');
    }
  }

  onPackagesUpdate = (newTree, newActivePackageQID) => {
    const { entities } = this.state;

    const architectureEntity = entities.architectures[newTree.id];

    this.setState(prevState => ({
      ...prevState,
      entities: {
        ...prevState.entities,
        architectures: {
          ...prevState.entities.architectures,
          [newTree.id]: {
            ...architectureEntity,
            packages: newTree.packages,
          },
        },
      },
      ui: {
        ...prevState.ui,
        activePackageQID: newActivePackageQID || prevState.ui.activePackageQID,
      },
    }));
  }

  render() {
    const { architecture } = this.props;
    const { entities, ui } = this.state;

    return (
      <MockedProvider>
        <Architecture
          {...this.props}
          architecture={entities.architectures[architecture.id]}
          activePackageQID={ui.activePackageQID}
          onUpdateActivePackageQID={this.onUpdateActivePackageQID}
          onPackageFocus={this.onPackageFocus}
          onPackageBlur={this.onPackageBlur}
          onPackageTermChange={this.onPackageTermChange}
          onPackagesUpdate={this.onPackagesUpdate}
        />
      </MockedProvider>
    );
  }
}

ArchitectureContainer.propTypes = {};

ArchitectureContainer.defaultProps = {};

export default ArchitectureContainer;
