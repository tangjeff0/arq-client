import React from 'react';
import { storiesOf } from '@storybook/react';
import Table from 'components/Table/Table.jsx';
import * as Icons from 'components/Icons/Icons.jsx';

storiesOf('Icons', module).add('font-awesome', () => (
  <Table>
    <Table.Body>
      {Object.keys(Icons)
        .sort((a, b) => {
          if (a.toLowerCase() < b.toLowerCase()) {
            return -1;
          } if (a.toLowerCase() > b.toLowerCase()) {
            return 1;
          }
          return 0;
        })
        .map((key) => {
          const Icon = Icons[key];

          return (
            <Table.Row key={key}>
              <Table.Cell>{key}</Table.Cell>
              <Table.Cell>
                <Icon />
              </Table.Cell>
            </Table.Row>
          );
        })}
    </Table.Body>
  </Table>
));
