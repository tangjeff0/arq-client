import React from 'react';
import { storiesOf } from '@storybook/react';
import ActionList from 'components/ActionList/ActionList.jsx';
import { partialAction } from './utils/partialAction';

storiesOf('ActionList', module)
  .add('with one item', () => (
    <ActionList>
      <ActionList.Item onClick={partialAction('onClick')}>Item</ActionList.Item>
    </ActionList>
  ))
  .add('with many items', () => (
    <ActionList>
      <ActionList.Item onClick={partialAction('onClick')}>Item 1</ActionList.Item>
      <ActionList.Item onClick={partialAction('onClick')}>Item 2</ActionList.Item>
      <ActionList.Item onClick={partialAction('onClick')}>Item 3</ActionList.Item>
    </ActionList>
  ));
