import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { comments } from 'mocks/comments';
import { user } from 'mocks/users';
import { NEW_COMMENT, OLD_COMMENT } from 'constants/commentStates.js';
import CommentBox from 'components/CommentBox/CommentBox.jsx';

const sameUserComment = comments.find(c => c.author.email === user.email);
const differentUserComment = comments.find(c => c.author.email !== user.email);

storiesOf('CommentBox', module)
  .addDecorator(story => <div style={{ maxWidth: '21rem' }}>{story()}</div>)
  .add('default', () => <CommentBox user={user} />)
  .add('status set to new (default)', () => (
    <CommentBox user={user} status={NEW_COMMENT} />
  ))
  .add('status new, onPost and onCancel callbacks', () => (
    <CommentBox
      user={user}
      status={NEW_COMMENT}
      onPost={action('onPost')}
      onCancel={action('onCancel')}
    />
  ))
  .add('status set to new, actions initially hidden', () => (
    <CommentBox user={user} status={NEW_COMMENT} showActions={false} />
  ))
  .add(
    'status set to new, actions initially hidden, custom placeholder',
    () => (
      <CommentBox
        user={user}
        status={NEW_COMMENT}
        showActions={false}
        placeholder="reply..."
      />
    ),
  )
  .add('status set to old, comment author is current user', () => (
    <CommentBox user={user} status={OLD_COMMENT} comment={sameUserComment} />
  ))
  .add('status set to old, comment author is not current user', () => (
    <CommentBox
      user={user}
      status={OLD_COMMENT}
      comment={differentUserComment}
    />
  ))
  .add('status set to new, is submitting', () => (
    <CommentBox user={user} status={NEW_COMMENT} isSubmitting />
  ));
