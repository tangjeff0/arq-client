import React from 'react';
import { storiesOf } from '@storybook/react';
import { AppLogo, PitchbookLogo, SalesforceLogo } from 'components/Logos/Logos.jsx';

storiesOf('Logos/App', module)
  .add('default', () => <AppLogo />)
  .add('size 45', () => <AppLogo size={45} />)
  .add('size 90 and fill #6a71d7', () => <AppLogo size={90} fill="#6a71d7" />);
storiesOf('Logos/Pitchbook', module)
  .add('default', () => <PitchbookLogo />)
  .add('size 45', () => <PitchbookLogo size={45} />)
  .add('size 90 and fill #6a71d7', () => <PitchbookLogo size={90} fill="#6a71d7" />);
storiesOf('Logos/Salesforce', module)
  .add('default', () => <SalesforceLogo />)
  .add('size 45', () => <SalesforceLogo size={45} />)
  .add('size 90 and fill #6a71d7', () => <SalesforceLogo size={90} fill="#6a71d7" />);
