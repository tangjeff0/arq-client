import React from 'react';
import { storiesOf } from '@storybook/react';
import Checkboxes from 'components/Checkbox/Checkboxes.jsx';

const sources = ['One', 'Two', 'Three'];

const sourceFilters = {
  One: false,
  Two: false,
  Three: false,
};

const checkedSourceFilters = {
  One: true,
  Two: true,
  Three: true,
};

storiesOf('Checkboxes', module)
  .add('default', () => (
    <Checkboxes options={sources} checkedMap={sourceFilters} />
  ))
  .add('checked', () => (
    <Checkboxes options={sources} checkedMap={checkedSourceFilters} />
  ));
