import React from 'react';
import { storiesOf } from '@storybook/react';
import Table from 'components/Table/Table.jsx';

const renderContent = content => (
  <div style={{
  }}
  >
    {content}
  </div>
);

storiesOf('Table', module)
  .add('with Table.Body, Table.Row and, Table.Cell', () => (
    <Table>
      <Table.Body>
        <Table.Row>
          <Table.Cell>{renderContent(1)}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  ))
  .add('with multiple rows and cells', () => (
    <Table>
      <Table.Body>
        <Table.Row>
          <Table.Cell>{renderContent('1, 1')}</Table.Cell>
          <Table.Cell>{renderContent('1, 2')}</Table.Cell>
          <Table.Cell>{renderContent('1, 3')}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>{renderContent('2, 1')}</Table.Cell>
          <Table.Cell>{renderContent('2, 2')}</Table.Cell>
          <Table.Cell>{renderContent('2, 3')}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>{renderContent('3, 1')}</Table.Cell>
          <Table.Cell>{renderContent('3, 2')}</Table.Cell>
          <Table.Cell>{renderContent('3, 3')}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  ))
  .add('with header', () => (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.Cell header>{renderContent('Header 1')}</Table.Cell>
          <Table.Cell header>{renderContent('Header 2')}</Table.Cell>
          <Table.Cell header>{renderContent('Header 3')}</Table.Cell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>{renderContent('1, 1')}</Table.Cell>
          <Table.Cell>{renderContent('1, 2')}</Table.Cell>
          <Table.Cell>{renderContent('1, 3')}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>{renderContent('2, 1')}</Table.Cell>
          <Table.Cell>{renderContent('2, 2')}</Table.Cell>
          <Table.Cell>{renderContent('2, 3')}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>{renderContent('3, 1')}</Table.Cell>
          <Table.Cell>{renderContent('3, 2')}</Table.Cell>
          <Table.Cell>{renderContent('3, 3')}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  ))
  .add('with footer', () => (
    <Table>
      <Table.Body>
        <Table.Row>
          <Table.Cell>{renderContent('2, 1')}</Table.Cell>
          <Table.Cell>{renderContent('2, 2')}</Table.Cell>
          <Table.Cell>{renderContent('2, 3')}</Table.Cell>
        </Table.Row>
      </Table.Body>
      <Table.Footer>
        <Table.Row>
          <Table.Cell>{renderContent('Footer 1')}</Table.Cell>
          <Table.Cell>{renderContent('Footer 2')}</Table.Cell>
          <Table.Cell>{renderContent('Footer 3')}</Table.Cell>
        </Table.Row>
      </Table.Footer>
    </Table>
  ))
  .add('with header and footer', () => (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.Cell header>{renderContent('Header 1')}</Table.Cell>
          <Table.Cell header>{renderContent('Header 2')}</Table.Cell>
          <Table.Cell header>{renderContent('Header 3')}</Table.Cell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>{renderContent('2, 1')}</Table.Cell>
          <Table.Cell>{renderContent('2, 2')}</Table.Cell>
          <Table.Cell>{renderContent('2, 3')}</Table.Cell>
        </Table.Row>
      </Table.Body>
      <Table.Footer>
        <Table.Row>
          <Table.Cell>{renderContent('Footer 1')}</Table.Cell>
          <Table.Cell>{renderContent('Footer 2')}</Table.Cell>
          <Table.Cell>{renderContent('Footer 3')}</Table.Cell>
        </Table.Row>
      </Table.Footer>
    </Table>
  ))
  .add('with cells alignRight', () => (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.Cell header alignRight>{renderContent('Header 1')}</Table.Cell>
          <Table.Cell header alignRight>{renderContent('Header 2')}</Table.Cell>
          <Table.Cell header alignRight>{renderContent('Header 3')}</Table.Cell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell alignRight>{renderContent('2, 1')}</Table.Cell>
          <Table.Cell alignRight>{renderContent('2, 2')}</Table.Cell>
          <Table.Cell alignRight>{renderContent('2, 3')}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  ));
