import React from 'react';
import { storiesOf } from '@storybook/react';
import Badge from 'components/Badge/Badge.jsx';

storiesOf('Badge', module)
  .add('default', () => (
    <Badge text="Text" />
  ))
  .add('appearance primary', () => (
    <Badge appearance="primary" text="Text" />
  ))
  .add('appearance secondary', () => (
    <Badge appearance="secondary" text="Text" />
  ));
