import React from 'react';
import { storiesOf } from '@storybook/react';
import Alert from 'components/Alert/Alert.jsx';

storiesOf('Alert', module)
  .add('default', () => (
    <Alert heading="Heading" subheading="Subheading" />
  ))
  .add('padded', () => (
    <Alert heading="Heading" subheading="Subheading" padded />
  ))
  .add('isFullscreen', () => (
    <Alert heading="Heading" subheading="Subheading" isFullscreen />
  ));
