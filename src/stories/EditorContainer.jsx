import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { EditorState } from 'draft-js';
import { v4 as uuid } from 'uuid';
import Editor from 'components/Editor/Editor.jsx';

class EditorContainer extends PureComponent {
  constructor(props) {
    super(props);

    const { editorState } = this.props;

    this.state = {
      qid: uuid(),
      editorState,
    };
  }

  onChange = (qid, editorState) => {
    this.setState({ editorState });
  }

  render() {
    const { placeholder } = this.props;
    const { qid, editorState } = this.state;

    return (
      <Editor
        {...this.props}
        qid={qid}
        editorState={editorState}
        onChange={this.onChange}
        placeholder={placeholder}
      />
    );
  }
}

EditorContainer.propTypes = {
  editorState: PropTypes.object,
  placeholder: PropTypes.string,
};

EditorContainer.defaultProps = {
  editorState: EditorState.createEmpty(),
  placeholder: '',
};

export default EditorContainer;
