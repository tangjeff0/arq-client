import React from 'react';
import { storiesOf } from '@storybook/react';
import Toolbar from 'components/Toolbar/Toolbar.jsx';

storiesOf('Toolbar', module)
  .add('default', () => <Toolbar>This is a toolbar</Toolbar>)
  .add('with groups', () => (
    <Toolbar>
      <Toolbar.Group>Toolbar group 1</Toolbar.Group>
      <Toolbar.Group>Toolbar group 2</Toolbar.Group>
    </Toolbar>
  ))
  .add('with first group filling space', () => (
    <Toolbar>
      <Toolbar.Group fill>Group 1 fill</Toolbar.Group>
      <Toolbar.Group>Group 2</Toolbar.Group>
    </Toolbar>
  ))
  .add('with first group filling space and both padded', () => (
    <Toolbar>
      <Toolbar.Group fill padded>
        Group 1 fill
      </Toolbar.Group>
      <Toolbar.Group padded>Group 2</Toolbar.Group>
    </Toolbar>
  ))
  .add('with padded groups and items', () => (
    <Toolbar>
      <Toolbar.Group padded>
        <Toolbar.Item>Item 1.1</Toolbar.Item>
        <Toolbar.Item>Item 1.2</Toolbar.Item>
      </Toolbar.Group>
      <Toolbar.Group padded>
        <Toolbar.Item>Item 2.1</Toolbar.Item>
        <Toolbar.Item>Item 2.2</Toolbar.Item>
      </Toolbar.Group>
    </Toolbar>
  ));
