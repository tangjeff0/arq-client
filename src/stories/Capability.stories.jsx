import React from 'react';
import { storiesOf } from '@storybook/react';
import Capability from 'components/Capability/Capability.jsx';
import { taxonomy } from 'mocks/taxonomy';

storiesOf('Capability', module)
  .add('default', () => (
    <Capability capability={taxonomy} />
  ))
  .add('size sm', () => (
    <Capability size="sm" capability={taxonomy} />
  ))
  .add('size md', () => (
    <Capability size="md" capability={taxonomy} />
  ));
