import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Popover from 'components/Popover/Popover.jsx';
import Button from 'components/Button/Button.jsx';

storiesOf('Popover', module)
  .add('default', () => (
    <Popover>
      <Popover.Toggle>Toggle Popover</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>Popover box content.</Popover.Box.Content>
      </Popover.Box>
    </Popover>
  ))
  .add('default with lots of content', () => (
    <Popover>
      <Popover.Toggle>Toggle Popover</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas
          veritatis voluptates culpa ipsa quo et vitae, asperiores veniam alias,
          dolorum, aliquam velit dolores ratione ullam nostrum aspernatur non
          nisi? Voluptatibus.
        </Popover.Box.Content>
      </Popover.Box>
    </Popover>
  ))
  .add('size small', () => (
    <Popover size="sm">
      <Popover.Toggle>Toggle Popover</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>Small size</Popover.Box.Content>
      </Popover.Box>
    </Popover>
  ))
  .add('auto width', () => (
    <Popover width="auto">
      <Popover.Toggle>Toggle Popover</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>Auto width.</Popover.Box.Content>
      </Popover.Box>
    </Popover>
  ))
  .add('top left', () => (
    <div>
      <h1>Hello</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
        dolore, eos quia quaerat, cumque mollitia incidunt quibusdam aut
        similique esse accusamus. Nulla dolores fuga illum quasi, error odio
        animi quae.
      </p>
      <Popover leftOrRight="left" topOrBottom="top">
        <Popover.Toggle>Toggle Popover</Popover.Toggle>
        <Popover.Box>
          <Popover.Box.Content>Top left popover.</Popover.Box.Content>
        </Popover.Box>
      </Popover>
    </div>
  ))
  .add('top right', () => (
    <div>
      <h1>Hello</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
        dolore, eos quia quaerat, cumque mollitia incidunt quibusdam aut
        similique esse accusamus. Nulla dolores fuga illum quasi, error odio
        animi quae.
      </p>
      <div style={{ display: 'inlineBlock', textAlign: 'right' }}>
        <Popover leftOrRight="right" topOrBottom="top">
          <Popover.Toggle>Toggle Popover</Popover.Toggle>
          <Popover.Box>
            <Popover.Box.Content>Top right popover.</Popover.Box.Content>
          </Popover.Box>
        </Popover>
      </div>
    </div>
  ))
  .add('bottom left', () => (
    <div>
      <h1>Hello</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
        dolore, eos quia quaerat, cumque mollitia incidunt quibusdam aut
        similique esse accusamus. Nulla dolores fuga illum quasi, error odio
        animi quae.
      </p>
      <Popover leftOrRight="left" topOrBottom="bottom">
        <Popover.Toggle>Toggle Popover</Popover.Toggle>
        <Popover.Box>
          <Popover.Box.Content>Bottom left popover.</Popover.Box.Content>
        </Popover.Box>
      </Popover>
    </div>
  ))
  .add('bottom right', () => (
    <div>
      <h1>Hello</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
        dolore, eos quia quaerat, cumque mollitia incidunt quibusdam aut
        similique esse accusamus. Nulla dolores fuga illum quasi, error odio
        animi quae.
      </p>
      <div style={{ display: 'inlineBlock', textAlign: 'right' }}>
        <Popover leftOrRight="right" topOrBottom="bottom">
          <Popover.Toggle>Toggle Popover</Popover.Toggle>
          <Popover.Box>
            <Popover.Box.Content>Bottom right popover.</Popover.Box.Content>
          </Popover.Box>
        </Popover>
      </div>
    </div>
  ))
  .add('small bottom left', () => (
    <Popover size="sm" leftOrRight="left" topOrBottom="bottom">
      <Popover.Toggle>Toggle Popover</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>Small bottom left popover.</Popover.Box.Content>
      </Popover.Box>
    </Popover>
  ))
  .add('many popover boxes', () => (
    <div>
      <div style={{ marginBottom: '3.6rem' }}>
        <Popover>
          <Popover.Toggle>Toggle Popover 1</Popover.Toggle>
          <Popover.Box>
            <Popover.Box.Content>
              Popover box content number 1!
            </Popover.Box.Content>
          </Popover.Box>
        </Popover>
      </div>
      <div style={{ marginBottom: '3.6rem' }}>
        <Popover>
          <Popover.Toggle>Toggle Popover 2</Popover.Toggle>
          <Popover.Box>
            <Popover.Box.Content>
              Popover box content number 2!
            </Popover.Box.Content>
          </Popover.Box>
        </Popover>
      </div>
      <div>
        <Popover>
          <Popover.Toggle>Toggle Popover 3</Popover.Toggle>
          <Popover.Box>
            <Popover.Box.Content>
              Popover box content number 3!
            </Popover.Box.Content>
          </Popover.Box>
        </Popover>
      </div>
    </div>
  ))
  .add('with actions', () => (
    <Popover hasActions>
      <Popover.Toggle>Toggle Popover</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>Popover box content.</Popover.Box.Content>
        <Popover.Box.Actions>
          <Button.Group fill>
            <Button onClick={action('Action 1 onClick')}>Action 1</Button>
            <Button onClick={action('Action 2 onClick')}>Action 2</Button>
          </Button.Group>
        </Popover.Box.Actions>
      </Popover.Box>
    </Popover>
  ))
  .add('position top, button toggle, and actions', () => (
    <div>
      <h1>Hello</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
        dolore, eos quia quaerat, cumque mollitia incidunt quibusdam aut
        similique esse accusamus. Nulla dolores fuga illum quasi, error odio
        animi quae.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo
        suscipit vel officia, odio quas harum veritatis tempore eius. Ex facere
        repellat quia illum non, iste quaerat placeat corporis, dignissimos
        labore.
      </p>
      <Popover topOrBottom="top" hasActions>
        <Popover.Toggle>
          <Button>Toggle popover</Button>
        </Popover.Toggle>
        <Popover.Box>
          <Popover.Box.Content>Popover box content.</Popover.Box.Content>
          <Popover.Box.Actions>
            <Button.Group fill>
              <Button onClick={action('Action 1 onClick')}>Action 1</Button>
              <Button onClick={action('Action 2 onClick')}>Action 2</Button>
            </Button.Group>
          </Popover.Box.Actions>
        </Popover.Box>
      </Popover>
    </div>
  ))
  .add('with onOpen and onClose callbacks', () => (
    <Popover onOpen={action('onOpen')} onClose={action('onClose')}>
      <Popover.Toggle>Toggle Popover</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>Popover box content.</Popover.Box.Content>
      </Popover.Box>
    </Popover>
  ));
