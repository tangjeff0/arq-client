import React from 'react';
import { storiesOf } from '@storybook/react';
import Button from 'components/Button/Button.jsx';

storiesOf('ButtonGroup', module)
  .add('default (auto fill)', () => (
    <Button.Group>
      <Button>Button 1</Button>
      <Button>Button 2</Button>
    </Button.Group>
  ))
  .add('fill (buttons fill space)', () => (
    <Button.Group fill>
      <Button>Button 1</Button>
      <Button>Button 2</Button>
    </Button.Group>
  ))
  .add('align center', () => (
    <Button.Group align="center">
      <Button>Button 1</Button>
      <Button>Button 2</Button>
    </Button.Group>
  ))
  .add('align right', () => (
    <Button.Group align="right">
      <Button>Button 1</Button>
      <Button>Button 2</Button>
    </Button.Group>
  ))
  .add('sm gutter', () => (
    <Button.Group gutter="sm">
      <Button>Button 1</Button>
      <Button>Button 2</Button>
    </Button.Group>
  ))
  .add('align center sm gutter', () => (
    <Button.Group align="center" gutter="sm">
      <Button>Button 1</Button>
      <Button>Button 2</Button>
    </Button.Group>
  ))
  .add('align right sm gutter', () => (
    <Button.Group align="right" gutter="sm">
      <Button>Button 1</Button>
      <Button>Button 2</Button>
    </Button.Group>
  ));
