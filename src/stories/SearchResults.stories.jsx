import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { elasticResult } from 'mocks/elastic';
import { GET_TAXONOMY_TREE } from 'queries/taxonomies';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import SearchResults from 'components/SearchResults/SearchResults.jsx';
import { IconLink } from 'components/Icons/Icons.jsx';

const hits = elasticResult.hits.hits.slice(0, 2);

const mocks = [
  {
    request: {
      query: GET_TAXONOMY_TREE,
      variables: { id: hits[0]._id }, // eslint-disable-line
    },
    result: {
      data: {
        TaxonomiesByID: {
          id: 'cba49d59-69fc-4366-813b-6f79d41b851e',
          term: 'Planets',
          ancestors: [{
            id: 'c93ac025-fbad-4e8c-9baa-267941bc535a', term: 'Science - general', parentId: null, __typename: 'arq_taxonomies',
          }, {
            id: '2fb74b2c-6cd6-4928-80f6-c64234428218', term: 'Astronomy', parentId: 'c93ac025-fbad-4e8c-9baa-267941bc535a', __typename: 'arq_taxonomies',
          }, {
            id: 'cba49d59-69fc-4366-813b-6f79d41b851e', term: 'Planets', parentId: '2fb74b2c-6cd6-4928-80f6-c64234428218', __typename: 'arq_taxonomies',
          }],
          allChildren: [{
            id: 'cba49d59-69fc-4366-813b-6f79d41b851e', term: 'Planets', parentId: '2fb74b2c-6cd6-4928-80f6-c64234428218', __typename: 'arq_taxonomies',
          }, {
            id: 'e2980641-4e41-41fa-9c69-5963432ba06d', term: 'Extrasolar planets', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: '85877b23-14d7-4bae-8a6d-3ed4edcb696a', term: 'Venus', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0', term: 'Mercury (planets)', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: '46edb04b-ec13-47e7-9b4c-01d53462f675', term: 'Earth', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: '2e9fb4fd-1920-4ac0-88a1-96b112356063', term: 'Sun', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: 'f2427662-bedd-40f4-bfdb-1465bb85cd64', term: 'Mars', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: '72a0c478-383d-4390-820a-32b70428b63e', term: 'Jupiter', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: '85198803-a09c-444f-b79d-dfb685dc53ac', term: 'Saturn', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: '6118baf2-102b-4edd-9561-cdf94d347ca5', term: 'Pluto', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }],
          __typename: 'arq_taxonomies',
        },
      },
    },
  },
  {
    request: {
      query: GET_TAXONOMY_TREE,
      variables: { id: hits[1]._id }, // eslint-disable-line
    },
    result: {
      data: {
        TaxonomiesByID: {
          id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0',
          term: 'Mercury (planets)',
          ancestors: [{
            id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0', term: 'Mercury (planets)', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }, {
            id: 'c93ac025-fbad-4e8c-9baa-267941bc535a', term: 'Science - general', parentId: null, __typename: 'arq_taxonomies',
          }, {
            id: '2fb74b2c-6cd6-4928-80f6-c64234428218', term: 'Astronomy', parentId: 'c93ac025-fbad-4e8c-9baa-267941bc535a', __typename: 'arq_taxonomies',
          }, {
            id: 'cba49d59-69fc-4366-813b-6f79d41b851e', term: 'Planets', parentId: '2fb74b2c-6cd6-4928-80f6-c64234428218', __typename: 'arq_taxonomies',
          }],
          allChildren: [{
            id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0', term: 'Mercury (planets)', parentId: 'cba49d59-69fc-4366-813b-6f79d41b851e', __typename: 'arq_taxonomies',
          }],
          __typename: 'arq_taxonomies',
        },
      },
    },
  },
];

const TreeActions = {
  primary: [
    {
      title: 'Link to this term',
      icon: <IconLink />,
      handler: action('Link to this term'),
    },
  ],
};

storiesOf('SearchResults', module)
  .add('default', () => (
    <MockedProvider mocks={mocks} addTypename={false}>
      <SearchResults
        hits={hits}
        actions={TreeActions}
      />
    </MockedProvider>
  ))
  .add('with hover action', () => (
    <MockedProvider mocks={mocks} addTypename={false}>
      <SearchResults
        hits={hits}
        actions={TreeActions}
        hoverAction={TreeActions.primary[0]}
      />
    </MockedProvider>
  ));
