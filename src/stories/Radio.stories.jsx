import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Radio from 'components/Radio/Radio.jsx';

storiesOf('Radio', module)
  .add('default', () => <Radio id="storybookRadio" />)
  .add('checked', () => <Radio id="storybookRadio" checked />)
  .add('named group', () => (
    <div>
      <Radio id="storybookRadio1" name="storybookRadio" />
      <Radio id="storybookRadio2" name="storybookRadio" />
      <Radio id="storybookRadio3" name="storybookRadio" />
    </div>
  ))
  .add('named group with a default checked', () => (
    <div>
      <Radio id="storybookRadio1" name="storybookRadio" checked />
      <Radio id="storybookRadio2" name="storybookRadio" />
      <Radio id="storybookRadio3" name="storybookRadio" />
    </div>
  ))
  .add('with label', () => <Radio id="storybookRadio" label="Radio button" />)
  .add('with label and inline', () => (
    <div>
      <Radio id="storybookRadio1" label="Radio button 1" isInline />
      <Radio id="storybookRadio2" label="Radio button 2" isInline />
    </div>
  ))
  .add('onChange', () => (
    <Radio id="storybookRadio" onChange={action('onChange')} />
  ));
