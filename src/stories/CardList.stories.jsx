import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { storiesOf } from '@storybook/react';
import Card from 'components/Card/Card.jsx';
import CardList from 'components/CardList/CardList.jsx';

const renderCard = ({ title }) => (
  <Card url="/" key={title}>
    <Card.Header title={title} />
    <Card.Meta>Subheader</Card.Meta>
    <Card.Preview>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,&nbsp;
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi&nbsp;
      ut aliquip ex ea commodo consequat.
    </Card.Preview>
    <Card.Footer>Footer</Card.Footer>
  </Card>
);

const ITEMS = [
  { title: 'Card One' },
  { title: 'Card Two' },
  { title: 'Card Three' },
  { title: 'Card Four' },
  { title: 'Card Five' },
  { title: 'Card Six' },
  { title: 'Card Seven' },
  { title: 'Card Eight' },
  { title: 'Card Nine' },
];

storiesOf('CardList', module)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('default', () => (
    <CardList items={ITEMS} renderCard={renderCard} />
  ))
  .add('limit 3', () => (
    <CardList items={ITEMS} limit={3} renderCard={renderCard} />
  ))
  .add('renderEmpty', () => (
    <CardList
      items={[]}
      renderCard={renderCard}
      renderEmpty={() => <span>Custom empty state!</span>}
    />
  ));
