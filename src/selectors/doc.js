import { stateToHTML } from 'draft-js-export-html';
import { now } from 'utils/time';

export function composeDocFromState(state) {
  const { activeDoc } = state;
  const {
    id, title, blocks, createdAt, entities,
  } = activeDoc;

  return {
    id,
    title,
    createdAt,
    updatedAt: now(),
    blocks: blocks.map((blockQID) => {
      const blockEntity = entities.blocks[blockQID];

      if (blockEntity.type === 'html') {
        const editorQID = blockEntity.editor;
        const editorEntity = entities.editors[editorQID];

        return {
          ...blockEntity,
          body: stateToHTML(editorEntity.editorState.getCurrentContent()),
        };
      } if (blockEntity.type === 'architecture') {
        const architectureQID = blockEntity.architecture;
        const architectureEntity = entities.architectures[architectureQID];

        return {
          ...blockEntity,
          architecture: architectureEntity,
        };
      }

      return blockEntity;
    }),
  };
}

export default composeDocFromState;
