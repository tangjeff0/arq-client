import React from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import { IconRadioOff, IconRadioOn } from 'components/Icons/Icons.jsx';
import 'components/Radio/Radio.scss';
import cx from 'classnames';

const Radio = ({
  checked,
  id,
  isInline,
  label,
  name,
  value,
  onChange,
}) => {
  const handleChange = e => onChange(e.target);

  return (
    <div
      className={cx('Radio', {
        'Radio--isInline': isInline,
      })}
    >
      <input
        type="radio"
        id={id}
        className="Radio-input"
        name={name}
        value={value}
        checked={checked}
        onChange={handleChange}
      />
      <label
        htmlFor={id}
        className={cx('Radio-label', {
          'Radio-label--isInline': isInline,
        })}
      >
        {checked ? (
          <IconRadioOn display={label ? 'inlineBlock' : 'block'} />
        ) : (
          <IconRadioOff display={label ? 'inlineBlock' : 'block'} />
        )}
        {label && <span className="Radio-label-text">{label}</span>}
      </label>
    </div>
  );
};

Radio.propTypes = {
  checked: PropTypes.bool,
  id: PropTypes.string.isRequired,
  isInline: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
};

Radio.defaultProps = {
  checked: false,
  isInline: false,
  label: '',
  name: '',
  onChange: noop,
  value: '',
};

export default Radio;
