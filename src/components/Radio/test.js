import { setUpMount, checkProps } from 'utils/testing';
import Radio from 'components/Radio/Radio.jsx';

describe('<Radio />', () => {
  let wrapper;

  const expectedProps = {
    id: 'radio1',
  };

  beforeEach(() => {
    wrapper = setUpMount(Radio, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Radio, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Radio class', () => {
    expect(wrapper.childAt(0).is('.Radio')).toBe(true);
  });

  it('should accept a checked prop', () => {
    const checked = true;

    wrapper.setProps({ checked });

    const radio = wrapper.find('input');
    expect(radio.props().checked).toEqual(true);
  });

  it('displays a label when passed in', () => {
    const label = 'Label';

    wrapper.setProps({ label });

    const radioLabel = wrapper.find('.Radio-label-text');
    expect(radioLabel.text()).toEqual('Label');
  });

  it('should accept an onChange prop', () => {
    const onChangeFunc = jest.fn();

    wrapper.setProps({ onChange: onChangeFunc });

    wrapper.find('input').simulate('change');
    expect(onChangeFunc).toHaveBeenCalled();
  });
});
