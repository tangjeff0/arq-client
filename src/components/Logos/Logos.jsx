import 'components/Logos/Logos.scss';

export AppLogo from 'components/Logos/AppLogo.jsx';
export PitchbookLogo from 'components/Logos/PitchbookLogo.jsx';
export SalesforceLogo from 'components/Logos/SalesforceLogo.jsx';
