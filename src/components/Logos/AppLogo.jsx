import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import 'components/Logos/Logos.scss';

const AppLogo = ({ display, fill, size }) => (
  <svg
    className={cx('Logo', `Logo--display-${display}`)}
    width={size}
    height={size}
    viewBox="0 0 206 206"
  >
    <path
      d="M148.91 206H0V0h206v149.91h-14V14H14v178h134.91v14z"
      fill={fill}
    />
    <path
      d="M96.73 123.36H65.2l-7.34 25.49H40.72l30.81-94.18h19.59l30.67 94.17h-17.71zM93 110.25l-3.32-11.52c-2.88-9.93-5.76-20.73-8.5-31.1h-.58c-2.6 10.51-5.32 21.17-8.35 31.1l-3.31 11.52zM131.73 78.14h13.68l1.15 12.53h.58c5-9.22 12.53-14.26 20-14.26a18 18 0 0 1 8.06 1.44l-2.88 14.4A23.12 23.12 0 0 0 165 91.1c-5.62 0-12.38 3.89-16.7 14.54v43.2h-16.57zM194 204l-28.12-30.22 10.24-9.56 28 30L204 204h-10z"
      fill={fill}
    />
  </svg>
);

AppLogo.propTypes = {
  display: PropTypes.oneOf(['inlineBlock', 'block']),
  fill: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

AppLogo.defaultProps = {
  display: 'inlineBlock',
  fill: '#fff',
  size: 206,
};

export default AppLogo;
