import React from 'react';
import PropTypes from 'prop-types';
import { companyPropType } from 'proptypes';
import Card from 'components/Card/Card.jsx';
import Capability from 'components/Capability/Capability.jsx';
import 'components/CompanyCard/CompanyCard.scss';

const CAPABILITY_LIMIT = 3;

const CompanyCard = ({
  item,
  highlight,
  searchResult,
}) => {
  const { capabilities = [] } = item;

  const limitedCapabilities = !searchResult
    ? capabilities.slice(0, CAPABILITY_LIMIT)
    : capabilities;

  const more = capabilities.length - CAPABILITY_LIMIT;

  const renderHighlight = name => <span dangerouslySetInnerHTML={{ __html: name }} />; // eslint-disable-line

  return (
    <Card
      className="CompanyCard"
      fullWidth={searchResult}
      url={`/companies/${item.id}`}
    >
      <Card.Header title={highlight ? renderHighlight(highlight.name[0]) : item.name} />
      {item.portfolio && <Card.Meta>Portfolio Company</Card.Meta>}
      <div className="CompanyCard-card-capabilities">
        {limitedCapabilities.map(capability => (
          <div key={capability.id} className="CompanyCard-card-capability">
            <Capability capability={capability} />
          </div>
        ))}
        {!searchResult && more > 0 && <span>{`+ ${more} more`}</span>}
      </div>
    </Card>
  );
};

CompanyCard.propTypes = {
  item: companyPropType,
  searchResult: PropTypes.bool,
  highlight: PropTypes.shape({
    name: PropTypes.array,
  }),
};

CompanyCard.defaultProps = {
  item: {},
  searchResult: false,
  highlight: null,
};

export default CompanyCard;
