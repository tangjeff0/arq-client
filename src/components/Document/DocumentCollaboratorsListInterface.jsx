import React, { useState } from 'react';
import PropTypes from 'prop-types';
import FilterableList from 'components/FilterableList/FilterableList.jsx';
import { INVITATION_NEW } from 'constants/invitationStates';
import useDocCollaborators from 'hooks/useDocCollaborators.jsx';

const DocumentCollaboratorsListInterface = ({ documentQID, onUpdateComplete }) => {
  const [updating, setUpdating] = useState(false);
  const {
    inviteCollaborators,
    selectCollaborator,
    deselectCollaborator,
    allCollaborators,
  } = useDocCollaborators({ documentQID });

  const onSelectCollaborator = ({ email }) => selectCollaborator(email);

  const onDeselectCollaborator = ({ email }) => deselectCollaborator(email);

  const disabled = item => item.status !== INVITATION_NEW;

  const onUpdateCollaborators = async () => {
    setUpdating(true);
    await inviteCollaborators(documentQID);
    setUpdating(false);
    onUpdateComplete();
  };

  return (
    <FilterableList
      collection={allCollaborators}
      loading={!allCollaborators}
      idKey="qid"
      titleKey="email"
      checkedKey="status"
      checkedValue={INVITATION_NEW}
      actionText="Update"
      onSelect={onSelectCollaborator}
      onDeselect={onDeselectCollaborator}
      onSubmit={onUpdateCollaborators}
      updating={updating}
      disabled={disabled}
      scrollable
      allowClickOnRow
    />
  );
};

DocumentCollaboratorsListInterface.propTypes = {
  documentQID: PropTypes.string.isRequired,
  onUpdateComplete: PropTypes.func.isRequired,
};

export default DocumentCollaboratorsListInterface;
