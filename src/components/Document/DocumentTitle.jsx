import React, { Component } from 'react';
import PropTypes from 'prop-types';

class DocumentTitle extends Component {
  onChange = (e) => {
    const { updateDocTitle } = this.props;

    const title = e.target.value;
    updateDocTitle(title);
  }

  render() {
    const { title, onFocus } = this.props;
    return (
      <input
        className="Document-title"
        value={title}
        onChange={this.onChange}
        onFocus={onFocus}
        placeholder="Untitled"
      />
    );
  }
}

DocumentTitle.propTypes = {
  title: PropTypes.string.isRequired,
  updateDocTitle: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
};

DocumentTitle.defaultProps = {};

export default DocumentTitle;
