import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import Architecture from 'components/Architecture/Architecture.jsx';
import ArchitectureToolbar from 'components/Architecture/ArchitectureToolbar/ArchitectureToolbar.jsx';
import DocumentArchitecturePackageInterface from 'components/Document/DocumentArchitecturePackageInterface.jsx';
import { nicetime, timeAfter } from 'utils/time';

import {
  ARCHITECTURE_SUBMITTED,
  ARCHITECTURE_IN_REVIEW,
} from 'constants/architectureStates.js';
import DocumentArchitecturePrompt from './DocumentArchitecturePrompt.jsx';


class DocumentArchitecture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animatePrompt: false,
    };
  }

  onDocArchitectureTitleFocus = () => {
    const {
      architecture,
      activeArchitectureQID,
      activePackageQID,
      setDocActiveArchitectureQID,
      unsetDocActivePackageQID,
    } = this.props;
    if (activeArchitectureQID !== architecture.id) {
      setDocActiveArchitectureQID(architecture.id);
    }
    if (activePackageQID) {
      unsetDocActivePackageQID();
    }
  }

  onDocArchitectureTitleChange = (title) => {
    const { architecture, updateDocArchitecture } = this.props;

    const updatedArchitecture = {
      ...architecture,
      title,
    };

    updateDocArchitecture(updatedArchitecture);
  }

  onDocArchitectureTitleKeyDown = (e) => {
    const { setDocActivePackageQID, architecture } = this.props;
    if (e.key === 'Tab') {
      e.preventDefault();
      setDocActivePackageQID(architecture.packages[0].id);
    }
  }

  onDocArchitectureStatusChange = (status) => {
    const { architecture, patchArchitecture } = this.props;

    const updatedArchitecture = {
      ...architecture,
      status,
    };

    // @todo: better visual feedback
    patchArchitecture(updatedArchitecture);
  }

  onDocArchitectureDelete = () => {
    const { blockQID, architecture, removeDocArchitecture } = this.props;

    // @todo: better visual feedback
    removeDocArchitecture(blockQID, architecture);
  }

  onDocArchitectureUpdateActivePackageQID = (qid) => {
    const { setDocActivePackageQID } = this.props;
    setDocActivePackageQID(qid);
  }

  onDocArchitecturePackageFocus = (qid) => {
    const {
      architecture,
      setDocActiveArchitectureQID,
    } = this.props;

    setDocActiveArchitectureQID(architecture.id);
    this.onDocArchitectureUpdateActivePackageQID(qid);
  }

  onDocArchitectureInputClick = () => {
    if (!this.isEditable()) {
      this.animatePrompt();
    }
  }

  onPackageToggleComments = (pkg) => {
    const {
      architecture,
      setDocActiveArchitectureQID,
      setDocActiveCommentThread,
    } = this.props;

    setDocActiveArchitectureQID(architecture.id);
    setDocActiveCommentThread(pkg);
  }

  animatePrompt = () => {
    this.setState({ animatePrompt: true });
  }

  onPromptAnimationEnd = () => {
    this.setState({ animatePrompt: false });
  }

  isEditable = () => {
    const { architecture } = this.props;
    return (
      architecture.status !== ARCHITECTURE_IN_REVIEW
      && architecture.status !== ARCHITECTURE_SUBMITTED
    );
  }

  render() {
    const {
      documentQID, // @note: this is a temp fix for v1 drawing navigation
      architecture,
      activePackageQID,
      activeCommentThreadPackageQID,
      architectureIndex,
    } = this.props;

    const { animatePrompt } = this.state;

    const editable = this.isEditable();

    return (
      <div className="Document-architecture">
        {architecture.status === ARCHITECTURE_IN_REVIEW && (
          <DocumentArchitecturePrompt
            animate={animatePrompt}
            onAnimationEnd={this.onPromptAnimationEnd}
          >
            Architecture can&apos;t be edited while in review
          </DocumentArchitecturePrompt>
        )}
        {architecture.status === ARCHITECTURE_SUBMITTED && (
          <DocumentArchitecturePrompt
            animate={animatePrompt}
            onAnimationEnd={this.onPromptAnimationEnd}
          >
            Architecture can&apos;t be edited while review pending
          </DocumentArchitecturePrompt>
        )}
        {editable
          && (timeAfter(
            architecture.metadata.publishedAt,
            architecture.metadata.rejectedAt,
          )
            || (!architecture.metadata.rejectedAt
              && architecture.metadata.publishedAt)) && (
              <DocumentArchitecturePrompt>
                Last published on
                {' '}
                {nicetime(architecture.metadata.publishedAt)}
              </DocumentArchitecturePrompt>
        )}
        {editable
          && (timeAfter(
            architecture.metadata.rejectedAt,
            architecture.metadata.publishedAt,
          )
            || (!architecture.metadata.publishedAt
              && architecture.metadata.rejectedAt)) && (
              <DocumentArchitecturePrompt>
                Last rejected on
                {' '}
                {nicetime(architecture.metadata.rejectedAt)}
              </DocumentArchitecturePrompt>
        )}
        <ArchitectureToolbar
          documentQID={documentQID} // @note: this is a temp fix for v1 drawing navigation
          architecture={architecture}
          clipboard
          actionable
          onStatusChange={this.onDocArchitectureStatusChange}
          onDelete={this.onDocArchitectureDelete}
          enableOnboarding={architectureIndex === 0}
        />
        <Architecture
          architecture={architecture}
          editable={editable}
          actionable
          interactive={architecture.status !== ARCHITECTURE_IN_REVIEW}
          commentable
          disabled={!editable}
          onTitleFocus={this.onDocArchitectureTitleFocus}
          onTitleClick={this.onDocArchitectureInputClick}
          onTitleChange={this.onDocArchitectureTitleChange}
          onTitleKeyDown={this.onDocArchitectureTitleKeyDown}
          activePackageQID={activePackageQID}
          activeCommentThreadPackageQID={activeCommentThreadPackageQID}
          onUpdateActivePackageQID={
            this.onDocArchitectureUpdateActivePackageQID
          }
          onPackageFocus={this.onDocArchitecturePackageFocus}
          onPackageClick={this.onDocArchitectureInputClick}
          onPackageToggleComments={this.onPackageToggleComments}
          onContentClick={this.animatePrompt}
          enableOnboarding={architectureIndex === 0}
          renderPackageInterface={props => <DocumentArchitecturePackageInterface {...props} />}
        />
      </div>
    );
  }
}

DocumentArchitecture.propTypes = {
  architecture: architecturePropType.isRequired,
  activeArchitectureQID: PropTypes.string,
  activePackageQID: PropTypes.string,
};

DocumentArchitecture.defaultProps = {
  activeArchitectureQID: null,
  activePackageQID: null,
};

export default DocumentArchitecture;
