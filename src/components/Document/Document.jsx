import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import DocumentControls from 'components/Document/DocumentControls.jsx';
import DocumentMain from 'components/Document/DocumentMain.jsx';
import DocumentStatus from 'components/Document/DocumentStatus.jsx';
import DocumentTitle from 'components/Document/DocumentTitle.jsx';
import DocumentBlocks from 'components/Document/DocumentBlocks.jsx';
import DocumentCommentThread from 'components/Document/DocumentCommentThread.jsx';
import DocumentSearchInterface from 'components/Document/DocumentSearchInterface.jsx';
import { toast } from 'react-toastify';
import 'components/Document/Document.scss';

class Document extends Component {
  constructor(props) {
    super(props);

    // prevent duplicates
    this.toastId = null;
  }

  componentDidUpdate() {
    const { activeDoc } = this.props;
    if (activeDoc.ui.error && !toast.isActive(this.toastId)) {
      this.toastId = toast.error(
        'Error syncing the document on save. Please refresh the page!',
      );
    }
  }

  render() {
    const {
      user,
      activeDoc,
      updateDocTitle,
      updateDocEditor,
      insertDocArchitectureBlock,
      setDocActiveEditorQID,
      unsetDocActiveEditorQID,
      updateDocHTMLBlock,
      setDocActiveArchitectureQID,
      updateDocArchitecture,
      removeDocArchitecture,
      setDocActivePackageQID,
      unsetDocActivePackageQID,
      setDocActiveCommentThread,
      unsetDocActiveCommentThread,
      addDocPackageComment,
      deleteDocPackageComment,
      activeComments,
      patchArchitecture,
    } = this.props;

    const {
      title, blocks, entities, ui,
    } = activeDoc;

    const activeCommentsPackageQID = activeComments.packageQID;
    const activeCommentsThread = activeCommentsPackageQID
      ? activeComments.commentsThread
      : {};

    return (
      <div className="Document">
        <Dashboard>
          <Dashboard.Header showBreadcrumbs />
          <Dashboard.Section fit>
            <Dashboard.Column fit theme="light">
              <Dashboard.Toolbar>
                <DocumentControls
                  entities={entities}
                  activeEditorQID={ui.activeEditorQID}
                  activeDocQID={activeDoc.id}
                  insertDocArchitectureBlock={insertDocArchitectureBlock}
                  updateDocEditor={updateDocEditor}
                />
              </Dashboard.Toolbar>
              <Dashboard.Section fit>
                <Dashboard.Column fit>
                  <Dashboard.Content padding={['top', 'right', 'left']}>
                    <DocumentMain>
                      <DocumentStatus status={ui.status} />
                      {!ui.error && (
                        <DocumentTitle
                          title={title || ''}
                          updateDocTitle={updateDocTitle}
                          onFocus={unsetDocActivePackageQID}
                        />
                      )}
                      {!ui.error && (
                        <DocumentBlocks
                          documentQID={activeDoc.id} // @note: temp fix for v1 drawing navigation
                          blocks={blocks}
                          entities={entities}
                          setDocActiveEditorQID={setDocActiveEditorQID}
                          unsetDocActiveEditorQID={unsetDocActiveEditorQID}
                          updateDocEditor={updateDocEditor}
                          updateDocHTMLBlock={updateDocHTMLBlock}
                          setDocActiveArchitectureQID={
                            setDocActiveArchitectureQID
                          }
                          updateDocArchitecture={updateDocArchitecture}
                          setDocActiveCommentThread={setDocActiveCommentThread}
                          removeDocArchitecture={removeDocArchitecture}
                          setDocActivePackageQID={setDocActivePackageQID}
                          unsetDocActivePackageQID={unsetDocActivePackageQID}
                          activeArchitectureQID={ui.activeArchitectureQID}
                          activePackageQID={ui.activePackageQID}
                          activeCommentThreadPackageQID={activeCommentsPackageQID}
                          patchArchitecture={patchArchitecture}
                        />
                      )}
                    </DocumentMain>
                    <Dashboard.Footer />
                  </Dashboard.Content>
                </Dashboard.Column>
                {ui.activeArchitectureQID
                  && activeCommentsPackageQID && (
                  <Dashboard.Column fit size="lg">
                    <Dashboard.Content padding={['top', 'right', 'bottom']}>
                      <DocumentCommentThread
                        user={user}
                        thread={activeCommentsThread}
                        activeCommentThreadPackageQID={activeCommentsPackageQID}
                        addDocPackageComment={addDocPackageComment}
                        deleteDocPackageComment={deleteDocPackageComment}
                        onDismiss={unsetDocActiveCommentThread}
                      />
                    </Dashboard.Content>
                  </Dashboard.Column>
                )}
              </Dashboard.Section>
            </Dashboard.Column>
            <Dashboard.Column fit minWidth="md" size="lg" theme="white">
              <DocumentSearchInterface />
            </Dashboard.Column>
          </Dashboard.Section>
        </Dashboard>
      </div>
    );
  }
}

Document.propTypes = {
  activeDoc: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string,
    ui: PropTypes.object,
    entities: PropTypes.shape({
      architectures: PropTypes.object,
    }),
  }).isRequired,
};

Document.defaultProps = {};

export default Document;
