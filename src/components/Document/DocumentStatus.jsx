import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import StatusText from 'components/Text/StatusText.jsx';

class DocumentStatus extends PureComponent {
  render() {
    const { status } = this.props;
    return (
      <div className="Document-status">
        <StatusText>{status}</StatusText>
      </div>
    );
  }
}

DocumentStatus.propTypes = {
  status: PropTypes.string,
};

DocumentStatus.defaultProps = {
  status: null,
};

export default DocumentStatus;
