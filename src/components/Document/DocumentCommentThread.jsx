import React, { PureComponent } from 'react';
import { userPropType } from 'proptypes';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';
import CommentThread from 'components/CommentThread/CommentThread.jsx';

class DocumentCommentThread extends PureComponent {
  addDocPackageComment = (text) => {
    const {
      user,
      activeCommentThreadPackageQID,
      addDocPackageComment,
    } = this.props;

    addDocPackageComment(
      user,
      text,
      activeCommentThreadPackageQID,
    );
  }

  deleteDocPackageComment = (comment) => {
    const { deleteDocPackageComment, activeCommentThreadPackageQID } = this.props;

    deleteDocPackageComment(activeCommentThreadPackageQID, comment);
  }

  render() {
    const {
      user, thread, onDismiss,
    } = this.props;

    return (
      <div className="Document-commentThread">
        <div className="Document-commentThread-dismiss">
          <Button unstyled onClick={onDismiss}>
            Dismiss
            <IconClose position="right" height={12} />
          </Button>
        </div>
        <div className="Document-commentThread-comments">
          <CommentThread
            user={user}
            comments={thread.comments}
            onAddNewComment={this.addDocPackageComment}
            onDeleteComment={this.deleteDocPackageComment}
          />
        </div>
      </div>
    );
  }
}

DocumentCommentThread.propTypes = {
  user: userPropType.isRequired,
  thread: PropTypes.shape({
    comments: PropTypes.arrayOf(PropTypes.shape({})),
  }).isRequired,
  activeCommentThreadPackageQID: PropTypes.string.isRequired,
  addDocPackageComment: PropTypes.func.isRequired,
  deleteDocPackageComment: PropTypes.func.isRequired,
  onDismiss: PropTypes.func.isRequired,
};

DocumentCommentThread.defaultProps = {};

export default DocumentCommentThread;
