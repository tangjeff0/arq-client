import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import cx from 'classnames';

class DocumentControl extends Component {
  onMouseDown = (e) => {
    e.preventDefault();
  }

  onClick = () => {
    const { onToggle, controlStyle } = this.props;
    onToggle(controlStyle);
  }

  render() {
    const {
      active,
      iconOnly,
      textOnly,
      icon,
      text,
    } = this.props;

    return (
      <button
        className={cx('Document-control', {
          'is-active': active,
          'Document-control--iconOnly': iconOnly,
          'Document-control--textOnly': textOnly,
        })}
        onMouseDown={this.onMouseDown}
        onClick={this.onClick}
        type="button"
      >
        {iconOnly && icon}
        {textOnly && text}
      </button>
    );
  }
}

DocumentControl.propTypes = {
  iconOnly: PropTypes.bool,
  textOnly: PropTypes.bool,
  onToggle: PropTypes.func,
};

DocumentControl.defaultProps = {
  onToggle: noop,
  iconOnly: false,
  textOnly: false,
};

export default DocumentControl;
