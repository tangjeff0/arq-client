import React from 'react';
import DocumentBlock from 'components/Document/DocumentBlock.jsx';
import DocumentEditor from 'components/Document/DocumentEditor.jsx';
import DocumentArchitecture from 'components/Document/DocumentArchitecture.jsx';

const DocumentBlocks = ({
  documentQID, // @note: this is a temp fix for v1 drawing navigation
  blocks,
  entities,
  setDocActiveEditorQID,
  unsetDocActiveEditorQID,
  updateDocEditor,
  updateDocHTMLBlock,
  setDocActiveArchitectureQID,
  updateDocArchitecture,
  setDocActiveCommentThread,
  removeDocArchitecture,
  setDocActivePackageQID,
  activeArchitectureQID,
  activePackageQID,
  activeCommentThreadPackageQID,
  patchArchitecture,
  unsetDocActivePackageQID,
}) => {
  let architectureIndex = -1;

  return (
    <div className="Document-blocks">
      {blocks.map((qid, index) => {
        const block = entities.blocks[qid];

        if (block.type === 'html') {
          const editorQID = block.editor;
          const editorEntity = entities.editors[editorQID];

          return (
            <DocumentBlock key={qid}>
              <DocumentEditor
                editor={editorEntity}
                blockQID={qid}
                updateDocEditor={updateDocEditor}
                setDocActiveEditorQID={setDocActiveEditorQID}
                unsetDocActiveEditorQID={unsetDocActiveEditorQID}
                unsetDocActivePackageQID={unsetDocActivePackageQID}
                updateDocHTMLBlock={updateDocHTMLBlock}
                index={index}
              />
            </DocumentBlock>
          );
        }
        const blockQID = block.id;
        const architectureQID = block.architecture;
        const architectureEntity = entities.architectures[architectureQID];

        architectureIndex += 1;

        return (
          <DocumentBlock key={qid}>
            <DocumentArchitecture
              documentQID={documentQID} // @todo: this is a temp fix for v1 drawing navigation
              blockQID={blockQID}
              architecture={architectureEntity}
              setDocActiveArchitectureQID={setDocActiveArchitectureQID}
              updateDocArchitecture={updateDocArchitecture}
              setDocActivePackageQID={setDocActivePackageQID}
              unsetDocActivePackageQID={unsetDocActivePackageQID}
              setDocActiveCommentThread={setDocActiveCommentThread}
              removeDocArchitecture={removeDocArchitecture}
              activeArchitectureQID={activeArchitectureQID}
              activePackageQID={activePackageQID}
              activeCommentThreadPackageQID={activeCommentThreadPackageQID}
              patchArchitecture={patchArchitecture}
              architectureIndex={architectureIndex}
            />
          </DocumentBlock>
        );
      })}
    </div>
  );
};

DocumentBlocks.propTypes = {};

DocumentBlocks.defaultProps = {};

export default DocumentBlocks;
