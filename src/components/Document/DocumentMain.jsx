import React from 'react';
import PropTypes from 'prop-types';

const DocumentMain = ({ children }) => <div className="Document-main">{children}</div>;

DocumentMain.propTypes = {
  children: PropTypes.node,
};

DocumentMain.defaultProps = {
  children: null,
};

export default DocumentMain;
