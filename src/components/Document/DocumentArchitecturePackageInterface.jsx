/* eslint-disable import/no-cycle */
import React from 'react';
import { packagePropType } from 'proptypes';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  updateDocArchitecturePackage,
  updateDocArchitecturePackageBE,
  moveDocArchitecturePackageUp,
  moveDocArchitecturePackageDown,
  insertDocArchitecturePackageBefore,
  insertDocArchitecturePackageAfter,
  indentDocArchitecturePackage,
  unindentDocArchitecturePackage,
  packageToggleChildren,
  packageToggleChildrenByLevel,
  unlinkDocArchitecturePackageTaxonomy,
  unlinkDocArchitecturePackageReference,
  removeDocArchitecturePackage,
  undoArchitectureAction,
  updateDocArchitecturePackageDescription,
  unsetDocActivePackageQID,
  setDocActivePackageQID,
} from 'actions/activeDoc';
import usePackageCommentCount from 'hooks/usePackageCommentCount.jsx';
import ArchitecturePackage from 'components/Architecture/ArchitecturePackage.jsx';

const DocumentArchitecturePackageInterface = (props) => {
  const { pkg } = props;
  const { commentCount } = usePackageCommentCount(pkg);

  return (
    <ArchitecturePackage
      commentCount={commentCount}
      {...props}
    />
  );
};

DocumentArchitecturePackageInterface.propTypes = {
  pkg: packagePropType.isRequired,
};

DocumentArchitecturePackageInterface.defaultProps = {};

function mapStateToProps(state) {
  return {
    activeDocUILoading: state.activeDoc.ui.loading,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateDocArchitecturePackage,
      moveDocArchitecturePackageUp,
      moveDocArchitecturePackageDown,
      insertDocArchitecturePackageBefore,
      insertDocArchitecturePackageAfter,
      indentDocArchitecturePackage,
      unindentDocArchitecturePackage,
      packageToggleChildren,
      packageToggleChildrenByLevel,
      unlinkDocArchitecturePackageTaxonomy,
      unlinkDocArchitecturePackageReference,
      removeDocArchitecturePackage,
      updateDocArchitecturePackageBE,
      undoArchitectureAction,
      updateDocArchitecturePackageDescription,
      unsetDocActivePackageQID,
      setDocActivePackageQID,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(
  DocumentArchitecturePackageInterface,
);
