import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import Tabs from 'components/Tabs/Tabs.jsx';

const TabsInterface = ({
  children,
  compact,
  selected,
  setSourceTabSelectedIndex,
  tabOptions,
}) => {
  const tabsCount = React.Children.count(children);

  const [selectedTab, setSelectedTab] = useState(selected && selected < tabsCount ? selected : 0);

  const onTabChange = (index) => {
    if (setSourceTabSelectedIndex) {
      setSourceTabSelectedIndex(index);
    }

    setSelectedTab(index);
  };

  useEffect(() => {
    if (selected) {
      setSelectedTab(
        selected < tabsCount || setSourceTabSelectedIndex ? selected : 0,
      );
    }
  }, [selected, tabsCount, setSourceTabSelectedIndex]);

  return (
    <Tabs
      compact={compact}
      onTabChange={onTabChange}
      selectedTab={selectedTab}
      tabOptions={tabOptions}
    >
      {children}
    </Tabs>
  );
};

TabsInterface.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired,
  compact: PropTypes.bool,
  selected: PropTypes.number,
  setSourceTabSelectedIndex: PropTypes.func,
  tabOptions: PropTypes.shape({
    color: PropTypes.string,
    textAlign: PropTypes.string,
  }),
};

TabsInterface.defaultProps = {
  compact: false,
  selected: null,
  setSourceTabSelectedIndex: noop,
  tabOptions: null,
};

export default TabsInterface;
