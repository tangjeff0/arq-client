import React from 'react';
import { setUpMount, checkProps } from 'utils/testing';
import Tabs from './Tabs.jsx';

describe('<Tabs />', () => {
  let wrapper;

  const expectedProps = {
    selectedTab: 1,
    children: <Tabs.Pane>Tab 1 Pane</Tabs.Pane>,
  };

  beforeEach(() => {
    wrapper = setUpMount(Tabs, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Tabs, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Tabs class', () => {
    expect(wrapper.childAt(0).is('.Tabs')).toBe(true);
  });
});
