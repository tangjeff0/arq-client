import React from 'react';
import { setUp, checkProps } from 'utils/testing';
import Navigation from './Navigation.jsx';

describe('<Navigation />', () => {
  let wrapper;

  const expectedProps = {
    children: <Navigation.Item>Test</Navigation.Item>,
  };

  beforeEach(() => {
    wrapper = setUp(Navigation, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Navigation, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Navigation class', () => {
    expect(wrapper.is('.Navigation')).toBe(true);
  });

  it('should render a .Navigation-items list', () => {
    expect(wrapper.exists('.Navigation-items')).toBe(true);
  });
});

describe('<NavigationItem />', () => {
  let wrapper;

  const expectedProps = {
    children: 'Test',
    to: '/',
  };

  beforeEach(() => {
    wrapper = setUp(Navigation.Item, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Navigation.Item, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Navigation-item class', () => {
    expect(wrapper.is('.Navigation-item')).toBe(true);
  });
});
