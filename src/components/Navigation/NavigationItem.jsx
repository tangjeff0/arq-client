import React from 'react';
import PropTypes from 'prop-types';
import Link from 'components/Link/Link.jsx';

function NavigationItem({ children, to, external }) {
  return (
    <li className="Navigation-item">
      {to ? (
        <Link to={to} className="Navigation-link" external={external}>
          {children}
        </Link>
      ) : (
        <span className="Navigation-link">{children}</span>
      )}
    </li>
  );
}

NavigationItem.propTypes = {
  children: PropTypes.node,
  external: PropTypes.bool,
  to: PropTypes.string,
};

NavigationItem.defaultProps = {
  children: null,
  external: false,
  to: null,
};

export default NavigationItem;
