import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import NavigationItem from './NavigationItem.jsx';
import 'components/Navigation/Navigation.scss';

const Navigation = ({ children, align }) => (
  <nav
    className={cx('Navigation', {
      [`Navigation--align-${align}`]: align,
    })}
  >
    <ul className="Navigation-items">{children}</ul>
  </nav>
);

Navigation.Item = NavigationItem;

Navigation.propTypes = {
  children: PropTypes.node.isRequired,
  align: PropTypes.string,
};

Navigation.defaultProps = {
  align: null,
};

export default Navigation;
