import React from 'react';
import LearningModuleMain from 'components/LearningModule/LearningModuleMain.jsx';
import LearningModuleSideNav from 'components/LearningModule/LearningModuleSideNav.jsx';

const LearningModule = () => (
  <div className="LearningModuleContainer">
    <LearningModuleSideNav />
    <LearningModuleMain />
  </div>
);

LearningModule.propTypes = {};

LearningModule.defaultProps = {};

export default LearningModule;
