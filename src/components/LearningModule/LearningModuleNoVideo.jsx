import React from 'react';

const LearningModuleNoVideo = () => (
  <div className="LearningModule--no-video">
    Oops! It looks like we can&apos;t play this video in your browser. Google
    Chrome is the recommended browser for ArQ.
  </div>
);

export default LearningModuleNoVideo;
