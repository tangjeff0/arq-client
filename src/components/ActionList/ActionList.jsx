import React from 'react';
import PropTypes from 'prop-types';
import ActionListItem from './ActionListItem.jsx';
import 'components/ActionList/ActionList.scss';

function ActionList({ children }) {
  return <ul className="ActionList">{children}</ul>;
}

ActionList.Item = ActionListItem;

ActionList.propTypes = {
  children: PropTypes.node,
};

ActionList.defaultProps = {
  children: null,
};

export default ActionList;
