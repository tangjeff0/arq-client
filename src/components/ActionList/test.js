import React from 'react';
import ActionList from 'components/ActionList/ActionList.jsx';

describe('<ActionList />', () => {
  it('should render with .ActionList class', () => {
    const wrapper = shallow(<ActionList />);
    expect(wrapper.is('.ActionList')).toBe(true);
  });

  it('should render children when passed in', () => {
    const wrapper = mount(
      <ActionList>
        <ActionList.Item onClick={() => {}}>Content</ActionList.Item>
      </ActionList>,
    );
    expect(wrapper.find('.ActionList-item').text()).toBe('Content');
  });
});
