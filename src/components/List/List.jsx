import React from 'react';
import PropTypes from 'prop-types';
import ListItem from 'components/List/ListItem.jsx';
import 'components/List/List.scss';

const List = ({ children }) => <ul className="List">{children}</ul>;

List.propTypes = {
  children: PropTypes.node,
};

List.defaultProps = {
  children: null,
};

List.Item = ListItem;

export default List;
