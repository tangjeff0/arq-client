import React from 'react';
import PropTypes from 'prop-types';

const ListItem = ({ children }) => (
  <li className="List-item">{children}</li>
);

ListItem.propTypes = {
  children: PropTypes.node,
};

ListItem.defaultProps = {
  children: null,
};

export default ListItem;
