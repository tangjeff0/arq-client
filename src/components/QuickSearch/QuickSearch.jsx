import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { esSearchResultPropType } from 'proptypes';
import SearchBar from 'components/SearchBar/SearchBar.jsx';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';
import QuickSearchHit from 'components/QuickSearch/QuickSearchHit.jsx';
import 'components/QuickSearch/QuickSearch.scss';

const QuickSearch = ({
  onSearch,
  hits,
  searching,
  onSelect,
  onCancel,
}) => {
  const [term, setTerm] = useState('');

  const onChange = (e) => {
    const newTerm = e.target.value;
    setTerm(newTerm);
    onSearch(newTerm);
  };

  return (
    <div className="QuickSearch">
      <SearchBar
        placeholder="Search Company Name"
        onChange={onChange}
        searching={searching}
        value={term}
        hideIcon
      />
      <Button unstyled iconOnly onClick={onCancel}>
        <IconClose />
      </Button>
      {term.length > 0 && (
        <div className="QuickSearch-hits">
          {hits.length === 0 && <div className="QuickSearch-no-results">No results.</div>}
          {hits.length > 0 && hits.map((hit) => {
            const { _source: { id } } = hit;

            return <QuickSearchHit key={id} hit={hit} onSelect={onSelect} />;
          })}
        </div>
      )}
    </div>
  );
};

QuickSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
  searching: PropTypes.bool.isRequired,
  hits: PropTypes.arrayOf(esSearchResultPropType).isRequired,
  onCancel: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default QuickSearch;
