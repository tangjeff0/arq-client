import { useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { GET_NAVIGATION_STATE } from 'queries/navigation';
import { onboardingModuleShownVar, toggleUserShownVar } from 'apollo/cache';
import { toggleUser } from 'services/users';

const useDashboardHeaderNavigation = () => {
  const { data } = useQuery(GET_NAVIGATION_STATE);
  const { onboardingModuleShown, toggleUserShown } = data;

  const showOnboardingModule = () => onboardingModuleShownVar(false);
  const closeOnboardingModule = () => onboardingModuleShownVar(true);

  const showToggleUser = () => toggleUserShownVar(true);
  const closeToggleUser = () => toggleUserShownVar(false);

  const history = useHistory();
  const toggleUserAction = (u) => {
    toggleUser(u);
    // eslint-disable-next-line no-alert
    alert('Switched to a new user!');
    history.push('/');
    window.location.reload();
  };

  return {
    closeOnboardingModule,
    closeToggleUser,
    onboardingModuleShown,
    showOnboardingModule,
    showToggleUser,
    toggleUserShown,
    toggleUserAction,
  };
};

export default useDashboardHeaderNavigation;
