import React from 'react';
import PropTypes from 'prop-types';
import { userPropType } from 'proptypes';
import { ENV_LOCAL } from 'constants/environments.js';
import Navigation from 'components/Navigation/Navigation.jsx';
import Modal from 'components/Modal/Modal.jsx';
import Button from 'components/Button/Button.jsx';
import LearningModule from 'components/LearningModule/LearningModule.jsx';
import ToggleUserModal from 'components/ToggleUserModal/ToggleUserModal.jsx';

const DashboardHeaderNavigation = ({
  closeOnboardingModule,
  closeToggleUser,
  onboardingModuleShown,
  showOnboardingModule,
  showToggleUser,
  toggleUserShown,
  toggleUserAction,
  user,
  users,
}) => (
  <Navigation align="right">
    <Navigation.Item>
      <Button unstyled onClick={showOnboardingModule}>
        Learn
      </Button>
    </Navigation.Item>
    {ZENDESK_ENABLED && (
    <Navigation.Item
      to={`https://${ZENDESK_HOST}${ZENDESK_ARQ_SECTION}`}
      external
    >
      Help
    </Navigation.Item>
    )}
    {ENV === ENV_LOCAL && (
      <Navigation.Item>
        <Button unstyled onClick={showToggleUser}>
          Toggle User
        </Button>
      </Navigation.Item>
    )}
    {ENV === ENV_LOCAL && toggleUserShown && (
      <ToggleUserModal
        closeToggleUser={closeToggleUser}
        toggleUserShown={toggleUserShown}
        toggleUserAction={toggleUserAction}
        user={user}
        users={users}
      />
    )}
    <Modal
      className="DocumentsLibrary--onboarding-modal"
      title="Welcome to ArQ"
      isOpened={!onboardingModuleShown}
      closeModal={closeOnboardingModule}
      learningModule
    >
      <LearningModule />
    </Modal>
  </Navigation>
);

DashboardHeaderNavigation.propTypes = {
  closeOnboardingModule: PropTypes.func.isRequired,
  closeToggleUser: PropTypes.func.isRequired,
  onboardingModuleShown: PropTypes.bool.isRequired,
  showOnboardingModule: PropTypes.func.isRequired,
  showToggleUser: PropTypes.func.isRequired,
  toggleUserShown: PropTypes.bool.isRequired,
  toggleUserAction: PropTypes.func.isRequired,
  user: userPropType,
  users: PropTypes.arrayOf(userPropType).isRequired,
};

DashboardHeaderNavigation.defaultProps = {
  user: null,
};

export default DashboardHeaderNavigation;
