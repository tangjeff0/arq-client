import React from 'react';
import PropTypes from 'prop-types';
import 'components/Text/Text.scss';
import cx from 'classnames';

const StatusText = ({
  children,
}) => (
  <span className={cx('StatusText', {
    'StatusText-Error': children.toLowerCase().includes('error'),
  })}
  >
    {children}
  </span>
);

StatusText.propTypes = {
  children: PropTypes.node.isRequired,
};

export default StatusText;
