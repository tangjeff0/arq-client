import React from 'react';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Radio from 'components/Radio/Radio.jsx';
import usePitchbookIntersectionHooks from 'components/PitchbookIntersection/PitchbookIntersectionHooks.jsx';
import PitchbookIntersectionTable from 'components/PitchbookIntersection/PitchbookIntersectionTable.jsx';
import PitchbookUpload from 'components/PitchbookIntersection/PitchbookUpload.jsx';
import PitchbookIntersectionEmptyState from 'components/PitchbookIntersection/PitchbookIntersectionEmptyState.jsx';
import cx from 'classnames';
import 'components/PitchbookIntersection/PitchbookIntersection.scss';

const PitchbookIntersection = () => {
  const {
    error,
    filterToggle,
    setFilterToggle,
    intersection,
    intersectionIds,
    intersectionByPitchbookId,
    loading,
    pitchbookById,
    pitchbookIds,
    refetch,
    searchTerm,
    setPitchbookById,
    setPitchbookIds,
    setSearchTerm,
  } = usePitchbookIntersectionHooks();

  const setA = 'Pitchbook';
  const setB = 'Salesforce';
  const setABIntersection = `${setA} ∩ ${setB}`;

  const handleOptionChange = () => {
    setFilterToggle(!filterToggle);
  };

  return (
    <div className="PitchbookIntersection">
      <Dashboard>
        <Dashboard.Header />
        <Dashboard.Section fit>
          <Dashboard.Column padded size="sm" theme="dark">
            <Grid direction="column" justify="spaceBetween">
              <SidebarNavigationInterface active="pitchbook" />
              <Dashboard.Footer>
                <WelcomeBackInterface />
              </Dashboard.Footer>
            </Grid>
          </Dashboard.Column>
          <Dashboard.Column fit theme="light">
            {!loading
              && !error
              && !!pitchbookIds.length && (
              <Dashboard.Toolbar padded>
                <Grid wrap="nowrap" align="center" justify="spaceBetween">
                  <Grid.Cell>{/* library navigation */}</Grid.Cell>
                  <Grid.Cell>
                    <PitchbookUpload
                      hasIcon
                      refetch={refetch}
                      setA={setA}
                      setPitchbookById={setPitchbookById}
                      setPitchbookIds={setPitchbookIds}
                      setSearchTerm={setSearchTerm}
                    />
                  </Grid.Cell>
                </Grid>
              </Dashboard.Toolbar>
            )}
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              {!loading
                && !error
                && !pitchbookIds.length && (
                <PitchbookIntersectionEmptyState
                  refetch={refetch}
                  setA={setA}
                  setB={setB}
                  setABIntersection={setABIntersection}
                  setPitchbookById={setPitchbookById}
                  setPitchbookIds={setPitchbookIds}
                  setSearchTerm={setSearchTerm}
                />
              )}
              {error && 'Error!'}
              {!error && loading && (
                <Loader size={48} isCentered isAbsolute />
              )}
              {!loading
                && !error
                && !!pitchbookIds.length && (
                <div className={cx('', {
                  'PitchbookIntersection-content':
                  intersection,
                })}
                >
                  <h2 className={cx('PitchbookIntersection-title')}>
                    {searchTerm}
                  </h2>

                  <div>
                    <Radio
                      id={setA}
                      name={setA}
                      label={setA}
                      checked={!filterToggle}
                      onChange={handleOptionChange}
                      isInline
                    />
                    <Radio
                      id={setABIntersection}
                      name={setABIntersection}
                      label={setABIntersection}
                      checked={filterToggle}
                      onChange={handleOptionChange}
                      isInline
                    />
                  </div>

                  <div className="PitchbookIntersection-match-info">
                    {`matched ${intersection.length} out of ${pitchbookIds.length} ${setA} companies`}
                  </div>

                  {!!pitchbookIds.length && (
                    <PitchbookIntersectionTable
                      ids={!filterToggle ? pitchbookIds : intersectionIds}
                      intersectionByPitchbookId={intersectionByPitchbookId}
                      pitchbookById={pitchbookById}
                      setA={setA}
                      setB={setB}
                    />
                  )}
                </div>
              )}
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

export default PitchbookIntersection;
