import React from 'react';
import PropTypes from 'prop-types';
import ReactFileReader from 'react-file-reader';
import Button from 'components/Button/Button.jsx';
import { IconUpload } from 'components/Icons/Icons.jsx';
import csv from 'csv';
import { keyBy } from 'lodash';

const PitchbookUpload = ({
  hasIcon,
  refetch,
  setA,
  setPitchbookById,
  setPitchbookIds,
  setSearchTerm,
}) => {
  const handleFiles = (files) => {
    const reader = new FileReader();
    const matchPitchbookId = /^\d+-\d{2}$/;

    reader.onload = function get() {
      const pitchbookSet = [];
      csv.parse(reader.result, (err, data) => {
        data.forEach((row) => {
          if (row[0].match(/Search Criteria/g)) {
            const term = row[1].replace('; ', '');
            setSearchTerm(term);
            refetch();
          }
          if (matchPitchbookId.test(row[0])) {
            pitchbookSet.push({
              id: row[0],
              pitchbookId: row[0],
              name: row[1],
              __typename: 'Organization',
            });
          }
        });
        const pitchbookKeyedByID = keyBy(pitchbookSet, 'id');
        setPitchbookById(pitchbookKeyedByID);
        const arrayOfPitchbookIds = pitchbookSet.map(item => item.id);
        setPitchbookIds(arrayOfPitchbookIds);
      });
    };
    reader.readAsText(files[0]);
  };

  return (
    <ReactFileReader handleFiles={handleFiles} fileTypes=".csv">
      <Button>
        {`Upload ${setA} CSV`}
        {hasIcon && (
          <IconUpload position="right" height={14} fill="#fff" />
        )}
      </Button>
    </ReactFileReader>
  );
};

PitchbookUpload.propTypes = {
  hasIcon: PropTypes.bool,
  refetch: PropTypes.func.isRequired,
  setA: PropTypes.string.isRequired,
  setPitchbookById: PropTypes.func.isRequired,
  setPitchbookIds: PropTypes.func.isRequired,
  setSearchTerm: PropTypes.func.isRequired,
};

PitchbookUpload.defaultProps = {
  hasIcon: false,
};

export default PitchbookUpload;
