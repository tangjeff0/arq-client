import React from 'react';
import PropTypes from 'prop-types';
import PitchbookUpload from 'components/PitchbookIntersection/PitchbookUpload.jsx';
import { IconUpload } from 'components/Icons/Icons.jsx';
import 'components/PitchbookIntersection/PitchbookIntersection.scss';

const PitchbookIntersectionTable = ({
  refetch,
  setA,
  setB,
  setABIntersection,
  setPitchbookById,
  setPitchbookIds,
  setSearchTerm,
}) => (
  <div className="PitchbookIntersection-empty">
    <IconUpload height={100} fill="#6a71d7" />
    <span className="PitchbookIntersection-empty-header">
      {setABIntersection}
    </span>
    <p className="PitchbookIntersection-empty-description">
      {`To view a table of ${setA} companies that exist in ${setB}`}
    </p>
    <PitchbookUpload
      refetch={refetch}
      setA={setA}
      setPitchbookById={setPitchbookById}
      setPitchbookIds={setPitchbookIds}
      setSearchTerm={setSearchTerm}
    />
  </div>
);

PitchbookIntersectionTable.propTypes = {
  refetch: PropTypes.func.isRequired,
  setA: PropTypes.string.isRequired,
  setB: PropTypes.string.isRequired,
  setABIntersection: PropTypes.string.isRequired,
  setPitchbookById: PropTypes.func.isRequired,
  setPitchbookIds: PropTypes.func.isRequired,
  setSearchTerm: PropTypes.func.isRequired,
};

PitchbookIntersectionTable.defaultProps = {};

export default PitchbookIntersectionTable;
