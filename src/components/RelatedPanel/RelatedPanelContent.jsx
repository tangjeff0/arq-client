import React from 'react';
import PropTypes from 'prop-types';
import {
  apolloErrorPropType,
  companyPropType,
  problemPropType,
  taxonomyPropType,
} from 'proptypes';
import Panel from 'components/Panel/Panel.jsx';
import CardList from 'components/CardList/CardList.jsx';
import ProblemCard from 'components/ProblemCard/ProblemCard.jsx';
import Loader from 'components/Loader/Loader.jsx';
import CompanyCard from 'components/CompanyCard/CompanyCard.jsx';
import ArchitecturesLibraryCard from 'components/ArchitecturesLibrary/ArchitecturesLibraryCard.jsx';
import Button from 'components/Button/Button.jsx';
import { IconPlus } from 'components/Icons/Icons.jsx';

const RelatedPanelContent = ({
  loading, error, resource, resourceName, createDoc,
}) => (
  <Panel.Content>
    <div className={`Related${resourceName}List`}>
      {loading && <Loader isCentered size={24} />}
      {!loading && error && 'Error!'}
      {!loading && resource && resourceName === 'architectures' && (
        <CardList
          items={resource}
          renderCard={item => <ArchitecturesLibraryCard key={item.id} architecture={item} />}
          renderEmpty={() => (
            <>
              {`No ${resourceName}`}
              {createDoc && (
                <p>
                  <Button onClick={createDoc}>
                    New Document
                    <IconPlus position="right" fill="#ffffff" />
                  </Button>
                </p>
              )}
            </>
          )}
        />
      )}
      {!loading && resource && resourceName === 'companies' && (
        <CardList
          items={resource}
          renderCard={item => <CompanyCard key={item.id} item={item} />}
          renderEmpty={() => `No ${resourceName}`}
        />
      )}
      {!loading && resource && resourceName === 'problems' && (
        <CardList
          items={resource}
          renderCard={item => <ProblemCard key={item.id} problem={item} />}
          renderEmpty={() => `No ${resourceName}`}
        />
      )}
    </div>
  </Panel.Content>
);

RelatedPanelContent.propTypes = {
  createDoc: PropTypes.func,
  loading: PropTypes.bool.isRequired,
  error: apolloErrorPropType,
  resource: PropTypes.oneOfType([
    PropTypes.arrayOf(companyPropType),
    PropTypes.arrayOf(problemPropType),
    PropTypes.arrayOf(taxonomyPropType),
  ]).isRequired,
  resourceName: PropTypes.string.isRequired,
};

RelatedPanelContent.defaultProps = {
  createDoc: null,
  error: null,
};

export default RelatedPanelContent;
