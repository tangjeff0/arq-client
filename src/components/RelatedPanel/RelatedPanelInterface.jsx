import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { queryPropType } from 'proptypes';
import RelatedPanel from 'components/RelatedPanel/RelatedPanel.jsx';
import useProblemSearch from 'hooks/useProblemSearch.jsx';
import useCompanySearch from 'hooks/useCompanySearch.jsx';
import useRelatedPanel from 'components/RelatedPanel/useRelatedPanel.jsx';
import useCreateDoc from 'hooks/useCreateDoc.jsx';

const RelatedPanelInterface = ({
  id,
  childKey,
  query,
  parentKey,
  parentSearchLink,
}) => {
  const { createDoc } = useCreateDoc();
  const history = useHistory();

  const RELATED_SECTIONS = {
    architectures: {
      resourceName: 'architectures',
      hasSearch: false,
    },
    companies: {
      resourceName: 'companies',
      hasSearch: true,
    },
    problemSets: {
      resourceName: 'problems',
      hasSearch: true,
    },
  };

  const { hasSearch, resourceName } = RELATED_SECTIONS[childKey];

  const {
    loading,
    error,
    resource,
    searchLink,
  } = useRelatedPanel({
    query,
    id,
    parentKey,
    childKey,
    resourceName,
    hasSearch,
  });

  const { setTaxonomiesOperator: setProblemsFilterOperator } = useProblemSearch();

  const { setTaxonomiesOperator: setCompaniesFilterOperator } = useCompanySearch();

  const onSearchLinkClick = () => {
    if (resourceName === 'problems') setProblemsFilterOperator('OR');
    if (resourceName === 'companies') setCompaniesFilterOperator('OR');

    history.push(searchLink);
  };

  return (
    <RelatedPanel
      loading={loading}
      error={error}
      resource={resource}
      resourceName={resourceName}
      searchLink={searchLink || parentSearchLink}
      onSearchLinkClick={onSearchLinkClick}
      createDoc={createDoc}
    />
  );
};

RelatedPanelInterface.propTypes = {
  childKey: PropTypes.string.isRequired,
  parentKey: PropTypes.string.isRequired,
  parentSearchLink: PropTypes.string,
  id: PropTypes.string.isRequired,
  query: queryPropType.isRequired,
};

RelatedPanelInterface.defaultProps = {
  parentSearchLink: null,
};

export default RelatedPanelInterface;
