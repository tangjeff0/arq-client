import { useQuery } from '@apollo/client';
import { get } from 'lodash';
import {
  getCapabilitiesUniqueResource,
  getCapabilitiesUniquePublishedArchitectures,
  getPackagesUniquePublishedArchitectures,
} from 'utils/gql';
import { generateSearchLink } from 'utils/links';
import {
  COMPANY_QUERY_ROOT,
  PROBLEM_QUERY_ROOT,
  TAXONOMY_QUERY_ROOT,
  CUSTOMER_QUERY_ROOT,
} from 'constants/gql';

const useRelatedPanel = ({
  query,
  id,
  parentKey,
  childKey,
  resourceName,
  hasSearch,
}) => {
  const {
    loading,
    error,
    data,
  } = useQuery(query, {
    variables: { id },
    fetchPolicy: 'cache-and-network',
  });

  const parent = get(data, parentKey);

  let resource = [];
  let capabilities = [];
  let searchLink = null;

  if (parent) {
    if (parentKey === COMPANY_QUERY_ROOT || parentKey === PROBLEM_QUERY_ROOT) {
      ({ capabilities } = parent);

      switch (resourceName) {
        case 'companies':
          resource = getCapabilitiesUniqueResource(capabilities, childKey);
          break;
        case 'problems':
          resource = getCapabilitiesUniqueResource(capabilities, childKey);
          break;
        case 'architectures':
          resource = getCapabilitiesUniquePublishedArchitectures(capabilities);
          break;
        default:
          break;
      }
    }

    if (parentKey === TAXONOMY_QUERY_ROOT) {
      capabilities = [parent];

      switch (resourceName) {
        case 'companies':
          resource = parent ? parent[childKey] : [];
          break;
        case 'problems':
          resource = parent ? parent[childKey] : [];
          break;
        case 'architectures':
          resource = parent
            ? getPackagesUniquePublishedArchitectures(parent.referencedBy)
            : [];
          break;
        default:
          break;
      }
    }

    if (parentKey === CUSTOMER_QUERY_ROOT) {
      resource = parent ? parent[childKey] : [];
    }

    resource = resource.filter(item => item.id !== id);
  }

  if (resource.length && hasSearch && parentKey !== CUSTOMER_QUERY_ROOT) {
    searchLink = generateSearchLink('taxonomies', resourceName, 'term', capabilities);
  }

  return {
    loading,
    error,
    resource,
    searchLink,
  };
};

export default useRelatedPanel;
