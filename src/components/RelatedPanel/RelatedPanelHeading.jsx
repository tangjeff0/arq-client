import React from 'react';
import PropTypes from 'prop-types';
import Panel from 'components/Panel/Panel.jsx';
import Heading from 'components/Heading/Heading.jsx';
import Button from 'components/Button/Button.jsx';

const RelatedPanelHeading = ({
  searchLink,
  resourceName,
  onSearchLinkClick,
}) => (
  <Panel.Heading>
    <Heading level={1}>
      {`Related ${resourceName}`}
    </Heading>
    {searchLink && (
      <Button unstyled onClick={onSearchLinkClick}>
        <span style={{ color: '#6a71d7' }}>View In Search</span>
      </Button>
    )}
  </Panel.Heading>
);

RelatedPanelHeading.propTypes = {
  resourceName: PropTypes.string,
  searchLink: PropTypes.string,
  onSearchLinkClick: PropTypes.func,
};

RelatedPanelHeading.defaultProps = {
  resourceName: null,
  searchLink: null,
  onSearchLinkClick: null,
};

export default RelatedPanelHeading;
