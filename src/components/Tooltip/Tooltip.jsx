import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TooltipWrapper from 'components/Tooltip/TooltipWrapper.jsx';
import TooltipContent from 'components/Tooltip/TooltipContent.jsx';
import 'components/Tooltip/Tooltip.scss';

class Tooltip extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  showTooltip = () => {
    this.setState({ isVisible: true });
  }

  hideTooltip = () => {
    this.setState({ isVisible: false });
  }

  render() {
    const {
      children,
      content,
      leftOrRight,
      topDistance,
    } = this.props;

    const { isVisible } = this.state;

    return (
      <div className="Tooltip">
        {React.Children.map(children, (child) => {
          if (child.type === TooltipWrapper) {
            return React.cloneElement(child, {
              showTooltip: this.showTooltip,
              hideTooltip: this.hideTooltip,
            });
          }

          return null;
        })}
        {isVisible && (
          <TooltipContent
            content={content}
            leftOrRight={leftOrRight}
            topDistance={topDistance}
          />
        )}
      </div>
    );
  }
}

Tooltip.propTypes = {
  children: PropTypes.node.isRequired,
  content: PropTypes.string,
  topDistance: PropTypes.oneOf(['xs', 'sm', 'md']),
  leftOrRight: PropTypes.oneOf(['left', 'right']),
};

Tooltip.defaultProps = {
  content: '',
  topDistance: 'md',
  leftOrRight: 'left',
};

Tooltip.Wrapper = TooltipWrapper;

export default Tooltip;
