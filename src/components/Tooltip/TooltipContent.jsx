import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

function TooltipContent({ content, leftOrRight, topDistance }) {
  return (
    <div
      className={cx('Tooltip-content', {
        [`Tooltip-content--position-${leftOrRight}`]: leftOrRight,
        [`Tooltip-content--top-distance-${topDistance}`]: topDistance,
      })}
    >
      {content}
    </div>
  );
}

TooltipContent.propTypes = {
  content: PropTypes.string.isRequired,
  leftOrRight: PropTypes.string.isRequired,
  topDistance: PropTypes.string.isRequired,
};

TooltipContent.defaultProps = {};

export default TooltipContent;
