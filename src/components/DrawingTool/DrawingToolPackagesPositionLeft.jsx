/* eslint-disable import/no-cycle */
import React from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import { POSITION_LEFT } from 'constants/drawingTool';
import DrawingToolPackage from 'components/DrawingTool/DrawingToolPackage.jsx';

const DrawingToolPackagesPositionLeft = ({
  packagesByQID,
  packagesPositionLeft,
  level,
  selectedTheme,
  setFocusID,
  editable,
}) => {
  if (level > 1 || packagesPositionLeft.length === 0) {
    return null;
  }

  return (
    <div className={cx('DrawingTool-packagesPositionLeft', `level-${level}`)}>
      {packagesPositionLeft.map(pkg => (
        <DrawingToolPackage
          key={pkg.id}
          pkg={pkg}
          packagesByQID={packagesByQID}
          level={level}
          position={POSITION_LEFT}
          selectedTheme={selectedTheme}
          setFocusID={setFocusID}
          editable={editable}
        />
      ))}
    </div>
  );
};

DrawingToolPackagesPositionLeft.propTypes = {
  packagesByQID: PropTypes.shape({}).isRequired,
  packagesPositionLeft: PropTypes.arrayOf(packagePropType).isRequired,
  level: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackagesPositionLeft.defaultProps = {};

export default DrawingToolPackagesPositionLeft;
