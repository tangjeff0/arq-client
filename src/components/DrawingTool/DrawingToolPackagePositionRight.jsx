import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import DrawingToolPackageControls from 'components/DrawingTool/DrawingToolPackageControls/DrawingToolPackageControls.jsx';

class DrawingToolPackagePositionRight extends Component {
  onDoubleClick = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const { pkg, level, setFocusID } = this.props;

    if (level === 1 || level === 2) {
      setFocusID(pkg.id);
    }
  }

  render() {
    const {
      pkg,
      level,
      position,
      selectedTheme,
      setFocusID,
      editable,
    } = this.props;

    return (
      <div
        className={cx('DrawingTool-packagePositionRight', `level-${level}`)}
        onDoubleClick={this.onDoubleClick}
      >
        {level <= 1 && (
          <DrawingToolPackageControls
            pkg={pkg}
            level={level}
            position={position}
            selectedTheme={selectedTheme}
            setFocusID={setFocusID}
            editable={editable}
          />
        )}
        <div
          className={cx(
            'DrawingTool-packagePositionRight-header',
            `level-${level}`,
          )}
        >
          <h2
            className={cx(
              'DrawingTool-packagePositionRight-heading',
              `level-${level}`,
            )}
          >
            {pkg.term}
          </h2>
        </div>
      </div>
    );
  }
}

DrawingToolPackagePositionRight.propTypes = {
  pkg: packagePropType.isRequired,
  level: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackagePositionRight.defaultProps = {};

export default DrawingToolPackagePositionRight;
