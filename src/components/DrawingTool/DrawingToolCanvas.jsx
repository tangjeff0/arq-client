import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { drawingRasterPropsPropType } from 'proptypes';
import rasterizeHTML from 'rasterizehtml';
import FileSaver from 'file-saver';

class DrawingToolCanvas extends Component {
  draw = () => {
    const { drawingToolClassNames, drawingRasterProps } = this.props;

    const { html, devicePixelRatio, gutter } = drawingRasterProps;

    rasterizeHTML
      .drawHTML(
        `
        <html>
        <head>
          <link rel="stylesheet" href="${CLIENT_BASE_URL}/styles.css">
          <style>
            .DrawingTool-architecture {
              margin: ${gutter / 10}rem;
            }
          </style>
        </head>
        <body>
          <div class="${drawingToolClassNames}">
            <div class="DrawingTool-architecture">
              ${html}
            </div>
          </div>
        </body>
        </html>
      `,
        this.$canvas,
        {
          zoom: devicePixelRatio,
        },
      )
      .then(
        () => {
          this.$canvas.toBlob((blob) => {
            FileSaver.saveAs(blob, 'Screenshot from ArQ Drawing Tool.png');
            this.clear();
          });
        },
        () => {
          // @todo: handle error
        },
      );
  }

  clear = () => {
    rasterizeHTML.drawHTML('', this.$canvas, {}).then(
      () => {},
      () => {
        // @todo: handle error
      },
    );
  }

  render() {
    const { drawingRasterProps } = this.props;

    const {
      width,
      height,
      devicePixelRatio,
      gutter,
    } = drawingRasterProps;

    return (
      <div className="DrawingTool-canvas">
        <canvas
          ref={($canvas) => {
            this.$canvas = $canvas;
          }}
          width={width * devicePixelRatio + gutter * devicePixelRatio * 2}
          height={height * devicePixelRatio + gutter * devicePixelRatio * 2}
        />
      </div>
    );
  }
}

DrawingToolCanvas.propTypes = {
  drawingToolClassNames: PropTypes.string.isRequired,
  drawingRasterProps: drawingRasterPropsPropType.isRequired,
};

DrawingToolCanvas.defaultProps = {};

export default DrawingToolCanvas;
