import React from 'react';
import PropTypes from 'prop-types';
import sizeMe from 'react-sizeme';
import { drawingToolThemePropType } from 'proptypes';
import DrawingToolOverlayEditor from 'components/DrawingTool/DrawingToolOverlay/DrawingToolOverlayEditor.jsx';
import usePackageAnnotations from 'components/DrawingTool/hooks/usePackageAnnotations.jsx';

const DrawingToolOverlay = ({
  packageQID,
  size,
  selectedTheme,
  selectedAnnotationID,
  setSelectedAnnotationID,
  editable,
}) => {
  const {
    annotations,
    removePackageAnnotation,
    updatePackageAnnotation,
    rotatePackageAnnotation,
  } = usePackageAnnotations({ packageQID, editable });

  return (
    <div className="DrawingTool-overlay">
      <DrawingToolOverlayEditor
        annotations={annotations}
        updateAnnotation={updatePackageAnnotation}
        rotateAnnotation={rotatePackageAnnotation}
        selectedAnnotationID={selectedAnnotationID}
        setSelectedAnnotationID={setSelectedAnnotationID}
        removeAnnotation={removePackageAnnotation}
        selectedTheme={selectedTheme}
        size={size}
      />
    </div>
  );
};

DrawingToolOverlay.propTypes = {
  packageQID: PropTypes.string.isRequired,
  size: PropTypes.shape({}).isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  selectedAnnotationID: PropTypes.string,
  setSelectedAnnotationID: PropTypes.func.isRequired,
  editable: PropTypes.bool.isRequired,
};

DrawingToolOverlay.defaultProps = {
  selectedAnnotationID: null,
};

export default sizeMe({
  monitorWidth: true,
  monitorHeight: true,
})(DrawingToolOverlay);
