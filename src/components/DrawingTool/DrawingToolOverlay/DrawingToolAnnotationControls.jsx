import React from 'react';
import PropTypes from 'prop-types';
import { IconTrash, IconRotateRight } from 'components/Icons/Icons.jsx';
import Button from 'components/Button/Button.jsx';

const DrawingToolAnnotationControls = ({
  onDelete,
  onRotate,
  selectedTheme,
}) => {
  const themeAccentFill = selectedTheme.swatches.find(
    s => s.name === 'ACCENT',
  ).fill;

  return (
    <div className="DrawingTool-annotation-controls">
      <Button.Group gutter="sm">
        {onRotate && (
          <Button unstyled onClick={onRotate}>
            <IconRotateRight display="block" fill={themeAccentFill} />
          </Button>
        )}
        <Button unstyled onClick={onDelete}>
          <IconTrash display="block" fill={themeAccentFill} />
        </Button>
      </Button.Group>
    </div>
  );
};

DrawingToolAnnotationControls.propTypes = {
  onDelete: PropTypes.func.isRequired,
  onRotate: PropTypes.func,
  selectedTheme: PropTypes.shape({
    swatches: PropTypes.array,
  }).isRequired,
};

DrawingToolAnnotationControls.defaultProps = {
  onRotate: null,
};

export default DrawingToolAnnotationControls;
