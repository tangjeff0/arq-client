import React from 'react';
import AppLogo from 'components/Logos/AppLogo.jsx';

const DrawingToolFooter = () => (
  <div className="DrawingTool-footer">
    Made With
    <AppLogo size={18} fill="#6a71d7" />
  </div>
);

export default DrawingToolFooter;
