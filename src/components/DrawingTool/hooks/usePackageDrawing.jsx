import fragments from 'queries/fragments';
import { useMutation, useApolloClient } from '@apollo/client';
import { UPDATE_DRAWING_SPEC } from 'queries/drawing';
import { PACKAGE_TYPENAME } from 'constants/gql.js';

const usePackageDrawing = ({ editable }) => {
  const client = useApolloClient();

  // functions to pass down component tree
  const [updatePackageMutationBE] = useMutation(UPDATE_DRAWING_SPEC);
  const updatePackageDrawings = (packageDrawings) => {
    packageDrawings.forEach((packageDrawing) => {
      // get existing drawing spec from cache
      const cachedPackageDrawing = client.readFragment({
        fragment: fragments.packageDrawingFields,
        id: `${PACKAGE_TYPENAME}:${packageDrawing.id}`,
      });

      const data = {
        ...cachedPackageDrawing,
        ...packageDrawing,
      };

      // override with any new values and write to cache
      client.writeFragment({
        fragment: fragments.packageDrawingFields,
        id: `${PACKAGE_TYPENAME}:${packageDrawing.id}`,
        data,
      });

      // if editable, update DB as well
      const { __typename, ...variables } = data;
      editable && updatePackageMutationBE({ variables });
    });
  };

  return { updatePackageDrawings };
};

export default usePackageDrawing;
