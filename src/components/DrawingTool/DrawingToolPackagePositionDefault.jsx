/* eslint-disable import/no-cycle */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import DrawingToolPackages from 'components/DrawingTool/DrawingToolPackages.jsx';
import DrawingToolPackageControls from 'components/DrawingTool/DrawingToolPackageControls/DrawingToolPackageControls.jsx';
import DrawingToolOverlay from 'components/DrawingTool/DrawingToolOverlay/DrawingToolOverlay.jsx';

const DrawingToolPackagePositionDefault = ({
  pkg,
  level,
  packagesByQID,
  position,
  selectedTheme,
  setFocusID,
  numColumns: col,
  editable,
}) => {
  const [selectedAnnotationID, setSelectedAnnotationID] = useState(null);

  const onMouseDown = (e) => {
    setSelectedAnnotationID(null);
    if (level === 2) {
      e.preventDefault();
      e.stopPropagation();
    }
  };

  const onDoubleClick = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (level === 1 || level === 2) {
      setFocusID(pkg.id);
    }
  };

  const childPackageQIDs = pkg.packages.map(childPkg => childPkg.id);

  const titlePosition = pkg[`level${level}titlePosition`];

  // TODO: clean up
  const { level1columns, level1hideChildren: hideChildren } = pkg;
  const numColumns = level1columns > 0 ? level1columns : 1;

  return (
    <div
      className={cx(
        'DrawingTool-packagePositionDefault',
        `level-${level}`,
        `title-${titlePosition}`,
        `col-${col}`,
        { hasChildren: childPackageQIDs.length > 0 },
      )}
      onMouseDown={onMouseDown}
      onDoubleClick={onDoubleClick}
      role="presentation"
    >
      {level <= 1 && (
        <DrawingToolPackageControls
          pkg={pkg}
          level={level}
          position={position}
          selectedTheme={selectedTheme}
          setFocusID={setFocusID}
          numColumns={numColumns}
          hideChildren={hideChildren}
          editable={editable}
        />
      )}
      <div
        className={cx(
          'DrawingTool-packagePositionDefault-header',
          `level-${level}`,
          `title-${titlePosition}`,
        )}
      >
        <h2
          className={cx(
            'DrawingTool-packagePositionDefault-heading',
            `level-${level}`,
            `title-${titlePosition}`,
          )}
        >
          {pkg.term}
        </h2>
        {level === 0 && (
          <span
            className={cx(
              'DrawingTool-packagePositionDefault-subHeading',
              `level-${level}`,
              `title-${titlePosition}`,
            )}
          >
            {`Children: ${childPackageQIDs.length}`}
          </span>
        )}
      </div>
      {childPackageQIDs.length > 0 && level === 0 && (
        <DrawingToolOverlay
          packageQID={pkg.id}
          selectedTheme={selectedTheme}
          selectedAnnotationID={selectedAnnotationID}
          setSelectedAnnotationID={setSelectedAnnotationID}
          editable={editable}
        />
      )}
      {childPackageQIDs.length > 0
        && level < 2 && !hideChildren && (
        <DrawingToolPackages
          packageQIDs={childPackageQIDs}
          packagesByQID={packagesByQID}
          selectedTheme={selectedTheme}
          level={level + 1}
          setFocusID={setFocusID}
          numColumns={numColumns}
          editable={editable}
        />
      )}
      {level === 1 && (
        <div
          className={cx(
            'DrawingTool-packagesPositionDefault-additional',
            `level-${level}`,
          )}
        >
          {`Children: ${childPackageQIDs.length}`}
        </div>
      )}
    </div>
  );
};

DrawingToolPackagePositionDefault.propTypes = {
  pkg: packagePropType.isRequired,
  packagesByQID: PropTypes.shape({}).isRequired,
  level: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  numColumns: PropTypes.number,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackagePositionDefault.defaultProps = {
  numColumns: null,
};

export default DrawingToolPackagePositionDefault;
