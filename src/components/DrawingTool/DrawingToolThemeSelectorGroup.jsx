import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { drawingToolThemePropType } from 'proptypes';
import { swatchSize, swatchCount } from 'constants/drawingTool.js';
import Radio from 'components/Radio/Radio.jsx';

class DrawingToolThemeSelectorGroup extends Component {
  onChange = () => {
    const { theme, onThemeUpdate } = this.props;

    onThemeUpdate(theme);
  }

  render() {
    const { theme, selectedTheme } = this.props;

    return (
      <div className="DrawingTool-themeSelector-group">
        <svg
          className="DrawingTool-themeSelector-theme"
          width={swatchSize * swatchCount}
          height={swatchSize}
          viewBox={`0 0 ${swatchSize * swatchCount} ${swatchSize}`}
        >
          {theme.swatches.map((swatch, index) => (
            <rect
              key={swatch.name}
              x={index * swatchSize}
              y={0}
              width={swatchSize}
              height={swatchSize}
              fill={swatch.fill}
            />
          ))}
        </svg>
        <div className="DrawingTool-themeSelector-radio">
          <Radio
            id={theme.name}
            name="drawingToolTheme"
            checked={selectedTheme.name === theme.name}
            onChange={this.onChange}
          />
        </div>
      </div>
    );
  }
}

DrawingToolThemeSelectorGroup.propTypes = {
  theme: drawingToolThemePropType.isRequired,
  onThemeUpdate: PropTypes.func.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
};

DrawingToolThemeSelectorGroup.defaultProps = {};

export default DrawingToolThemeSelectorGroup;
