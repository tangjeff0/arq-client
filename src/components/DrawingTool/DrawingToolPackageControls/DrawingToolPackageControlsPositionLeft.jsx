import React from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import Popover from 'components/Popover/Popover.jsx';
import Button from 'components/Button/Button.jsx';
import {
  IconAlignCenter,
  IconAlignRight,
  IconSearch,
  IconMoreVertical,
} from 'components/Icons/Icons.jsx';
import Tooltip from 'components/Tooltip/Tooltip.jsx';

const DrawingToolPackageControlsPositionLeft = ({
  pkg,
  level,
  selectedTheme,
  controlsOpen,
  onControlsOpen,
  onControlsClose,
  packagePositionDefault,
  packagePositionRight,
  setFocusID,
}) => {
  const themeAccentSwatch = selectedTheme.swatches.find(
    s => s.name === 'ACCENT',
  );
  const themeAccentFill = themeAccentSwatch.fill;

  return (
    <div
      className={cx(
        'DrawingTool-package-controlsPositionLeft',
        `level-${level}`,
        {
          'is-open': controlsOpen,
        },
      )}
    >
      <Popover
        size="sm"
        width="auto"
        topOrBottom="top"
        leftOrRight="right"
        onOpen={onControlsOpen}
        onClose={onControlsClose}
      >
        <Popover.Toggle>
          <IconMoreVertical
            display="block"
            height={15}
            fill="#000"
            opacity={0.5}
          />
        </Popover.Toggle>
        <Popover.Box>
          <Popover.Box.Content>
            <Button.Group gutter="sm">
              <Button unstyled onClick={packagePositionDefault}>
                <Tooltip content="Position default">
                  <Tooltip.Wrapper>
                    <IconAlignCenter display="block" fill={themeAccentFill} />
                  </Tooltip.Wrapper>
                </Tooltip>
              </Button>
              <Button unstyled onClick={packagePositionRight}>
                <Tooltip content="Position right">
                  <Tooltip.Wrapper>
                    <IconAlignRight display="block" fill={themeAccentFill} />
                  </Tooltip.Wrapper>
                </Tooltip>
              </Button>
              <Button unstyled onClick={() => setFocusID(pkg.id)}>
                <Tooltip content="Zoom In">
                  <Tooltip.Wrapper>
                    <IconSearch display="block" fill={themeAccentFill} />
                  </Tooltip.Wrapper>
                </Tooltip>
              </Button>
            </Button.Group>
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    </div>
  );
};

DrawingToolPackageControlsPositionLeft.propTypes = {
  pkg: packagePropType.isRequired,
  level: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  packagePositionDefault: PropTypes.func.isRequired,
  packagePositionRight: PropTypes.func.isRequired,
  controlsOpen: PropTypes.bool.isRequired,
  onControlsOpen: PropTypes.func.isRequired,
  onControlsClose: PropTypes.func.isRequired,
};

DrawingToolPackageControlsPositionLeft.defaultProps = {};

export default DrawingToolPackageControlsPositionLeft;
