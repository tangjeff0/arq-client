import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import Popover from 'components/Popover/Popover.jsx';
import Button from 'components/Button/Button.jsx';
import {
  IconAlignLeft,
  IconAlignRight,
  IconSearch,
  IconMoreVertical,
  IconArrowUp,
  IconRotateLeft,
  IconRotateRight,
  IconColumnsAdd,
  IconColumnsRemove,
  IconEye,
  IconEyeSlash,
  IconPencil,
  IconImage,
} from 'components/Icons/Icons.jsx';
import Tooltip from 'components/Tooltip/Tooltip.jsx';
import QuickSearchInterface from 'components/QuickSearch/QuickSearchInterface.jsx';

const DrawingToolPackageControlsPositionDefault = ({
  pkg,
  level,
  selectedTheme,
  controlsOpen,
  onControlsOpen,
  onControlsClose,
  setFocusID,
  packagePositionLeft,
  packagePositionRight,
  packageTitlePositionLeft,
  packageTitlePositionRight,
  packageTitlePositionDefault,
  packageToggleChildren,
  packageAddAnnotation,
  packageAddImage,
  numColumns,
  setNumColumns,
}) => {
  const [inputOpen, setInputOpen] = useState(false);

  const onMouseDown = e => e.stopPropagation();

  const themeAccentFill = selectedTheme.swatches.find(
    s => s.name === 'ACCENT',
  ).fill;

  const themeDisabledFill = selectedTheme.swatches.find(
    s => s.name === 'DISABLED',
  ).fill;

  const titlePosition = pkg[`level${level}titlePosition`];

  const COLUMN_LIMIT = 5;
  const canRemoveColumn = numColumns > 1;
  const canAddColumn = numColumns < COLUMN_LIMIT;

  const onColumnRemove = () => canRemoveColumn && setNumColumns(numColumns - 1);

  const onColumnAdd = () => canAddColumn && setNumColumns(numColumns + 1);

  const titleTopDisabled = titlePosition === 0;
  const titleLeftDisabled = titlePosition === 1;
  const titleRightDisabled = titlePosition === 2;

  const { level1hideChildren: hideChildren } = pkg;

  const handleControlsOpen = () => {
    onControlsOpen();
  };

  const handleControlsClose = () => {
    setInputOpen(false);
    onControlsClose();
  };

  return (
    <div
      className={cx(
        'DrawingTool-package-controlsPositionDefault',
        `level-${level}`,
        {
          'is-open': controlsOpen,
        },
      )}
      onMouseDown={onMouseDown}
      onDoubleClick={onMouseDown}
      role="presentation"
    >
      <Popover
        size="sm"
        width="auto"
        topOrBottom="top"
        leftOrRight="right"
        onOpen={handleControlsOpen}
        onClose={handleControlsClose}
      >
        <Popover.Toggle>
          <IconMoreVertical
            display="block"
            height={15}
            fill="#000"
            opacity={0.5}
          />
        </Popover.Toggle>
        <Popover.Box>
          <Popover.Box.Content>
            {level === 0 && inputOpen ? (
              <QuickSearchInterface
                onCancel={() => setInputOpen(false)}
                onSelect={packageAddImage}
              />
            ) : (
              <Button.Group gutter="sm">
                <Button unstyled onClick={packagePositionLeft}>
                  <Tooltip content="Position left">
                    <Tooltip.Wrapper>
                      <IconAlignLeft display="block" fill={themeAccentFill} />
                    </Tooltip.Wrapper>
                  </Tooltip>
                </Button>
                <Button unstyled onClick={packagePositionRight}>
                  <Tooltip content="Position right">
                    <Tooltip.Wrapper>
                      <IconAlignRight display="block" fill={themeAccentFill} />
                    </Tooltip.Wrapper>
                  </Tooltip>
                </Button>
                <Button
                  unstyled
                  onClick={packageTitlePositionLeft}
                  readOnly={titleLeftDisabled}
                >
                  <Tooltip content="Title left">
                    <Tooltip.Wrapper>
                      <IconRotateLeft display="block" fill={titleLeftDisabled ? themeDisabledFill : themeAccentFill} />
                    </Tooltip.Wrapper>
                  </Tooltip>
                </Button>
                <Button
                  unstyled
                  onClick={packageTitlePositionDefault}
                  readOnly={titleTopDisabled}
                >
                  <Tooltip content="Title top">
                    <Tooltip.Wrapper>
                      <IconArrowUp display="block" fill={titleTopDisabled ? themeDisabledFill : themeAccentFill} />
                    </Tooltip.Wrapper>
                  </Tooltip>
                </Button>
                <Button
                  unstyled
                  onClick={packageTitlePositionRight}
                  readOnly={titleRightDisabled}
                >
                  <Tooltip content="Title right">
                    <Tooltip.Wrapper>
                      <IconRotateRight display="block" fill={titleRightDisabled ? themeDisabledFill : themeAccentFill} />
                    </Tooltip.Wrapper>
                  </Tooltip>
                </Button>
                {level === 0 && (
                  <>
                    <Button unstyled onClick={packageAddAnnotation}>
                      <Tooltip content="Add arrow">
                        <Tooltip.Wrapper>
                          <IconPencil display="block" fill={themeAccentFill} />
                        </Tooltip.Wrapper>
                      </Tooltip>
                    </Button>
                    <Button unstyled onClick={() => setInputOpen(true)}>
                      <Tooltip content="Add image">
                        <Tooltip.Wrapper>
                          <IconImage display="block" fill={themeAccentFill} />
                        </Tooltip.Wrapper>
                      </Tooltip>
                    </Button>
                  </>
                )}
                {level === 1 && (
                  <Button
                    unstyled
                    onClick={onColumnRemove}
                    readOnly={!canRemoveColumn}
                  >
                    <Tooltip content="Remove column">
                      <Tooltip.Wrapper>
                        <IconColumnsRemove display="block" fill={canRemoveColumn ? themeAccentFill : themeDisabledFill} />
                      </Tooltip.Wrapper>
                    </Tooltip>
                  </Button>
                )}
                {level === 1 && (
                  <Button
                    unstyled
                    onClick={onColumnAdd}
                    readOnly={!canAddColumn}
                  >
                    <Tooltip content="Add column">
                      <Tooltip.Wrapper>
                        <IconColumnsAdd display="block" fill={canAddColumn ? themeAccentFill : themeDisabledFill} />
                      </Tooltip.Wrapper>
                    </Tooltip>
                  </Button>
                )}
                {level === 1 && (
                  <Button
                    unstyled
                    onClick={packageToggleChildren}
                  >
                    <Tooltip content={hideChildren ? 'Show children' : 'Hide children'}>
                      <Tooltip.Wrapper>
                        {hideChildren
                          ? <IconEye fill={themeAccentFill} />
                          : <IconEyeSlash fill={themeAccentFill} />
                        }
                      </Tooltip.Wrapper>
                    </Tooltip>
                  </Button>
                )}
                <Button unstyled onClick={() => setFocusID(pkg.id)}>
                  <Tooltip content="Zoom in">
                    <Tooltip.Wrapper>
                      <IconSearch display="block" fill={themeAccentFill} />
                    </Tooltip.Wrapper>
                  </Tooltip>
                </Button>
              </Button.Group>
            )}
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    </div>
  );
};

DrawingToolPackageControlsPositionDefault.propTypes = {
  pkg: packagePropType.isRequired,
  level: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  packagePositionLeft: PropTypes.func.isRequired,
  packagePositionRight: PropTypes.func.isRequired,
  packageTitlePositionLeft: PropTypes.func.isRequired,
  packageTitlePositionRight: PropTypes.func.isRequired,
  packageTitlePositionDefault: PropTypes.func.isRequired,
  packageToggleChildren: PropTypes.func.isRequired,
  packageAddAnnotation: PropTypes.func.isRequired,
  packageAddImage: PropTypes.func.isRequired,
  controlsOpen: PropTypes.bool.isRequired,
  onControlsOpen: PropTypes.func.isRequired,
  onControlsClose: PropTypes.func.isRequired,
  numColumns: PropTypes.number.isRequired,
  setNumColumns: PropTypes.func.isRequired,
};

DrawingToolPackageControlsPositionDefault.defaultProps = {};

export default DrawingToolPackageControlsPositionDefault;
