import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { drawingToolThemePropType } from 'proptypes';
import { themes } from 'constants/drawingTool';
import DrawingToolThemeSelectorGroup from 'components/DrawingTool/DrawingToolThemeSelectorGroup.jsx';

class DrawingToolThemeSelector extends Component {
  onThemeUpdate = (theme) => {
    const { setSelectedTheme } = this.props;

    setSelectedTheme(theme);
  }

  render() {
    const { selectedTheme } = this.props;

    return (
      <div className="DrawingTool-themeSelector">
        {themes.map(theme => (
          <DrawingToolThemeSelectorGroup
            key={theme.name}
            theme={theme}
            onThemeUpdate={this.onThemeUpdate}
            selectedTheme={selectedTheme}
          />
        ))}
      </div>
    );
  }
}

DrawingToolThemeSelector.propTypes = {
  selectedTheme: drawingToolThemePropType.isRequired,
  setSelectedTheme: PropTypes.func.isRequired,
};

DrawingToolThemeSelector.defaultProps = {};

export default DrawingToolThemeSelector;
