/* eslint-disable import/no-cycle */
import React from 'react';
import PropTypes from 'prop-types';
import { drawingToolThemePropType } from 'proptypes';
import { sortBy } from 'lodash';
import cx from 'classnames';
import {
  POSITION_DEFAULT,
  POSITION_LEFT,
  POSITION_RIGHT,
} from 'constants/drawingTool.js';
import DrawingToolPackagesPositionDefault from 'components/DrawingTool/DrawingToolPackagesPositionDefault.jsx';
import DrawingToolPackagesPositionLeft from 'components/DrawingTool/DrawingToolPackagesPositionLeft.jsx';
import DrawingToolPackagesPositionRight from 'components/DrawingTool/DrawingToolPackagesPositionRight.jsx';

const DrawingToolPackages = ({
  packageQIDs,
  packagesByQID,
  selectedTheme,
  level,
  setFocusID,
  numColumns,
  editable,
}) => {
  // TODO: sort further up?
  const packagesPositionDefault = sortBy(packageQIDs
    .filter(qid => (
      packagesByQID[qid][`level${level}position`]
        === POSITION_DEFAULT
    ))
    .map(qid => packagesByQID[qid]),
  p => p.sequence);

  const packagesPositionLeft = sortBy(packageQIDs
    .filter(qid => (
      packagesByQID[qid][`level${level}position`]
        === POSITION_LEFT
    ))
    .map(qid => packagesByQID[qid]),
  p => p.sequence);

  const packagesPositionRight = sortBy(packageQIDs
    .filter(qid => (
      packagesByQID[qid][`level${level}position`]
        === POSITION_RIGHT
    ))
    .map(qid => packagesByQID[qid]),
  p => p.sequence);

  return (
    <div className={cx('DrawingTool-packages', `level-${level}`)}>
      <DrawingToolPackagesPositionLeft
        packagesByQID={packagesByQID}
        packagesPositionLeft={packagesPositionLeft}
        level={level}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        editable={editable}
      />
      <DrawingToolPackagesPositionDefault
        packagesByQID={packagesByQID}
        packagesPositionDefault={packagesPositionDefault}
        level={level}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        numColumns={numColumns}
        editable={editable}
      />
      <DrawingToolPackagesPositionRight
        packagesByQID={packagesByQID}
        packagesPositionRight={packagesPositionRight}
        level={level}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        editable={editable}
      />
    </div>
  );
};

DrawingToolPackages.propTypes = {
  packageQIDs: PropTypes.arrayOf(PropTypes.string).isRequired,
  packagesByQID: PropTypes.shape({}).isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  level: PropTypes.number,
  setFocusID: PropTypes.func.isRequired,
  numColumns: PropTypes.number,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackages.defaultProps = {
  level: 0,
  numColumns: null,
};

export default DrawingToolPackages;
