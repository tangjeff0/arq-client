/* eslint-disable import/no-cycle */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { partition } from 'lodash';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import {
  POSITION_DEFAULT,
  COLUMNS,
  ROW_HEIGHT,
  HEIGHT_DEFAULT,
  WIDTH_DEFAULT,
  itemProps,
} from 'constants/drawingTool.js';
import sizeMe from 'react-sizeme';
import ReactGridLayout from 'react-grid-layout';
import DrawingToolPackage from 'components/DrawingTool/DrawingToolPackage.jsx';
import usePackageDrawing from 'components/DrawingTool/hooks/usePackageDrawing.jsx';

const DrawingToolPackagesPositionDefaultDraggable = ({
  packagesByQID,
  packagesPositionDefault,
  level,
  selectedTheme,
  setFocusID,
  size,
  editable,
}) => {
  const [width, setWidth] = useState(size.width);

  useEffect(() => {
    setWidth(size.width);
  }, [size.width]);

  const { updatePackageDrawings } = usePackageDrawing({ editable });

  const inferLayoutFromPackages = (packages, startY) => {
    let currentIndex = 0;
    let lastPkgWidth;
    let currentPkgY = startY;

    const layout = packages.map((pkg, index) => {
      currentIndex += index === 0 ? 0 : lastPkgWidth;
      const currentPkgWidth = WIDTH_DEFAULT;
      lastPkgWidth = currentPkgWidth;

      if ((currentIndex + currentPkgWidth) / COLUMNS > 1) {
        currentPkgY += 1;
        currentIndex = 0;
      }

      return {
        i: pkg.id,
        x: currentIndex % COLUMNS,
        y: currentPkgY,
        w: currentPkgWidth,
        h: HEIGHT_DEFAULT,
        ...itemProps,
      };
    });

    return layout;
  };

  const getLayoutFromPackages = (packages) => {
    let yIndex = 0;
    const layout = packages.map((pkg) => {
      const {
        [`level${level}x`]: x,
        [`level${level}y`]: y,
        [`level${level}width`]: w,
        [`level${level}height`]: h,
      } = pkg;

      yIndex = Math.max(yIndex, y + h);
      return {
        i: pkg.id, x, y, w, h, ...itemProps,
      };
    });
    return { layout, height: yIndex };
  };

  const onLayoutChange = (newLayout) => {
    const gridItems = [];

    newLayout.forEach((item) => {
      const {
        i: id, x, y, w, h,
      } = item;
      const pkg = packagesByQID[id];

      gridItems.push({
        id: pkg.id,
        level1x: x,
        level1y: y,
        level1width: w,
        level1height: h,
        level1drawn: true,
      });
    });
    updatePackageDrawings(gridItems);
  };

  const [defaultPackages, positionedPackages] = partition(
    packagesPositionDefault,
    pkg => !pkg.level1drawn,
  );

  const { layout: positionedLayout, height } = getLayoutFromPackages(positionedPackages);
  const inferredLayout = inferLayoutFromPackages(defaultPackages, height);

  const layout = [
    ...positionedLayout,
    ...inferredLayout,
  ];

  if (!layout.length) return null;

  return (
    <ReactGridLayout
      className="layout"
      layout={layout}
      cols={COLUMNS}
      rowHeight={ROW_HEIGHT}
      margin={[9, 9]}
      containerPadding={[0, 0]}
      width={width}
      compactType={null}
      onLayoutChange={onLayoutChange}
    >
      {packagesPositionDefault.map(pkg => (
        <div key={pkg.id}>
          <DrawingToolPackage
            pkg={pkg}
            packagesByQID={packagesByQID}
            level={level}
            position={POSITION_DEFAULT}
            selectedTheme={selectedTheme}
            updatePackageDrawings={updatePackageDrawings}
            setFocusID={setFocusID}
            editable={editable}
          />
        </div>
      ))}
    </ReactGridLayout>
  );
};

DrawingToolPackagesPositionDefaultDraggable.propTypes = {
  packagesByQID: PropTypes.shape({}).isRequired,
  packagesPositionDefault: PropTypes.arrayOf(packagePropType).isRequired,
  level: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  size: PropTypes.shape({
    width: PropTypes.number,
  }),
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackagesPositionDefaultDraggable.defaultProps = {
  size: {},
};

export default sizeMe({
  monitorWidth: true,
})(DrawingToolPackagesPositionDefaultDraggable);
