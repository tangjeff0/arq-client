import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'components/Modal/Modal.jsx';

const DrawingToolCompanyModal = ({
  companyModalOpen,
  setCompanyModalOpen,
}) => (
  <Modal
    title="No Company Logo"
    isOpened={companyModalOpen}
    closeModal={() => setCompanyModalOpen(false)}
  >
    Oops! There&apos;s no logo available for this company in ArQ.
    {SALESFORCE_URL && (
      <>
        &nbsp;Add one in&nbsp;
        <a href={SALESFORCE_URL} target="_blank" rel="noopener noreferrer">Salesforce</a>
        .
      </>
    )}
  </Modal>
);

DrawingToolCompanyModal.propTypes = {
  companyModalOpen: PropTypes.bool.isRequired,
  setCompanyModalOpen: PropTypes.func.isRequired,
};

export default DrawingToolCompanyModal;
