import React from 'react';
import PropTypes from 'prop-types';
import { drawingToolBreadcrumbPropType } from 'proptypes';
import DrawingToolBreadcrumbsItem from 'components/DrawingTool/DrawingToolBreadcrumbsItem.jsx';

const DrawingToolBreadcrumbs = ({ breadcrumbs, setFocusID }) => (
  breadcrumbs.length > 0 && (
    <ul className="DrawingTool-breadcrumbs">
      {breadcrumbs.map(breadcrumb => (
        <DrawingToolBreadcrumbsItem
          key={breadcrumb.id}
          breadcrumb={breadcrumb}
          setFocusID={setFocusID}
        />
      ))}
    </ul>
  )
);

DrawingToolBreadcrumbs.propTypes = {
  breadcrumbs: PropTypes.arrayOf(drawingToolBreadcrumbPropType).isRequired,
  setFocusID: PropTypes.func.isRequired,
};

DrawingToolBreadcrumbs.defaultProps = {};

export default DrawingToolBreadcrumbs;
