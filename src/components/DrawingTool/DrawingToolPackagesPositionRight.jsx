/* eslint-disable import/no-cycle */
import React from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import { POSITION_RIGHT } from 'constants/drawingTool';
import DrawingToolPackage from 'components/DrawingTool/DrawingToolPackage.jsx';

const DrawingToolPackagesPositionRight = ({
  packagesByQID,
  packagesPositionRight,
  level,
  selectedTheme,
  setFocusID,
  editable,
}) => {
  if (level > 1 || packagesPositionRight.length === 0) {
    return null;
  }

  return (
    <div
      className={cx('DrawingTool-packagesPositionRight', `level-${level}`)}
    >
      {packagesPositionRight.map(pkg => (
        <DrawingToolPackage
          key={pkg.id}
          pkg={pkg}
          packagesByQID={packagesByQID}
          level={level}
          position={POSITION_RIGHT}
          selectedTheme={selectedTheme}
          setFocusID={setFocusID}
          editable={editable}
        />
      ))}
    </div>
  );
};

DrawingToolPackagesPositionRight.propTypes = {
  packagesByQID: PropTypes.shape({}).isRequired,
  packagesPositionRight: PropTypes.arrayOf(packagePropType).isRequired,
  level: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackagesPositionRight.defaultProps = {};

export default DrawingToolPackagesPositionRight;
