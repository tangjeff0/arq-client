/* eslint-disable import/no-cycle */
import React from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import { POSITION_DEFAULT } from 'constants/drawingTool';
import DrawingToolPackagesPositionDefaultDraggable from 'components/DrawingTool/DrawingToolPackagesPositionDefaultDraggable.jsx';
import DrawingToolPackage from 'components/DrawingTool/DrawingToolPackage.jsx';

const DrawingToolPackagesPositionDefault = ({
  packagesByQID,
  packagesPositionDefault,
  level,
  selectedTheme,
  setFocusID,
  numColumns,
  editable,
}) => (
  <div
    className={cx('DrawingTool-packagesPositionDefault', `level-${level}`)}
  >
    {level === 0 && (
      packagesPositionDefault.map(pkg => (
        <DrawingToolPackage
          key={pkg.id}
          pkg={pkg}
          packagesByQID={packagesByQID}
          level={level}
          position={POSITION_DEFAULT}
          selectedTheme={selectedTheme}
          setFocusID={setFocusID}
          editable={editable}
        />
      ))
    )}
    {level === 1 && (
      <DrawingToolPackagesPositionDefaultDraggable
        packagesByQID={packagesByQID}
        packagesPositionDefault={packagesPositionDefault}
        level={level}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        editable={editable}
      />
    )}
    {level === 2 && (
      packagesPositionDefault.map(pkg => (
        <DrawingToolPackage
          key={pkg.id}
          pkg={pkg}
          packagesByQID={packagesByQID}
          level={level}
          position={POSITION_DEFAULT}
          selectedTheme={selectedTheme}
          setFocusID={setFocusID}
          numColumns={numColumns}
          editable={editable}
        />
      ))
    )}
  </div>
);

DrawingToolPackagesPositionDefault.propTypes = {
  packagesByQID: PropTypes.shape({}).isRequired,
  packagesPositionDefault: PropTypes.arrayOf(packagePropType).isRequired,
  level: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  numColumns: PropTypes.number,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackagesPositionDefault.defaultProps = {
  numColumns: null,
};

export default DrawingToolPackagesPositionDefault;
