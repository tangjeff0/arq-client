import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';

function Link({
  children, to, className, external,
}) {
  if (external) {
    return (
      <a
        href={to}
        className={className}
        target="_blank"
        rel="noopener noreferrer"
      >
        {children}
      </a>
    );
  }
  return (
    <RouterLink to={to} className={className}>
      {children}
    </RouterLink>
  );
}

Link.propTypes = {
  to: PropTypes.string,
  className: PropTypes.string,
  external: PropTypes.bool,
};

Link.defaultProps = {
  external: false,
};

export default Link;
