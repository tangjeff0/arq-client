import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import 'components/CardList/CardList.scss';

const CardList = ({
  items,
  renderCard,
  renderEmpty,
  limit,
}) => {
  const [currentLimit, setCurrentLimit] = useState(limit);

  const limitedItems = items.slice(0, currentLimit);

  return (
    <div className="CardList">
      {items.length > 0 && (
        <div className="CardList-items">
          {limitedItems.map(item => renderCard(item))}
        </div>
      )}
      {items.length === 0 && renderEmpty && (
        <div style={{ textAlign: 'center' }}>{renderEmpty()}</div>
      )}
      {currentLimit < items.length && (
        <div className="CardList-loadMore">
          <Button
            readOnly={currentLimit >= items.length}
            onClick={() => setCurrentLimit(currentLimit + 9)}
          >
            Load More
          </Button>
        </div>
      )}
    </div>
  );
};

CardList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
  renderCard: PropTypes.func,
  renderEmpty: PropTypes.func,
  limit: PropTypes.number,
};

CardList.defaultProps = {
  items: [],
  renderCard: () => {},
  renderEmpty: null,
  limit: 6,
};

export default CardList;
