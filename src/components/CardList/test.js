import React from 'react';
import CardList from 'components/CardList/CardList.jsx';

describe('<CardList />', () => {
  it('should render with .CardList class', () => {
    const wrapper = shallow(<CardList />);
    expect(wrapper.is('.CardList')).toBe(true);
  });
});
