import React from 'react';
import PropTypes from 'prop-types';
import CommentThreadComments from 'components/CommentThread/CommentThreadComments.jsx';
import CommentThreadReply from 'components/CommentThread/CommentThreadReply.jsx';
import 'components/CommentThread/CommentThread.scss';

const CommentThread = ({
  user,
  comments,
  onAddNewComment,
  onDeleteComment,
  shared,
}) => (
  <div className="CommentThread">
    <CommentThreadComments
      user={user}
      comments={comments}
      onDeleteComment={onDeleteComment}
      shared={shared}
    />
    <CommentThreadReply
      user={user}
      comments={comments}
      onAddNewComment={onAddNewComment}
    />
  </div>
);

CommentThread.propTypes = {
  user: PropTypes.shape({
    qid: PropTypes.string,
    email: PropTypes.string,
  }).isRequired,
  comments: PropTypes.array,
  onAddNewComment: PropTypes.func.isRequired,
  onDeleteComment: PropTypes.func.isRequired,
  shared: PropTypes.bool,
};

CommentThread.defaultProps = {
  comments: [],
  shared: false,
};

export default CommentThread;
