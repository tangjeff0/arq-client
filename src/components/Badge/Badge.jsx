import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import 'components/Badge/Badge.scss';

const Badge = ({ text, appearance }) => (
  <span className={cx('Badge', [`Badge--appearance-${appearance}`])}>{text}</span>
);

Badge.propTypes = {
  text: PropTypes.string.isRequired,
  appearance: PropTypes.oneOf(['primary', 'secondary']),
};

Badge.defaultProps = {
  appearance: 'primary',
};

export default Badge;
