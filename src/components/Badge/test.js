import React from 'react';
import Badge from 'components/Badge/Badge.jsx';

describe('<Badge />', () => {
  it('should render with .Badge class', () => {
    const wrapper = shallow(<Badge text="Test" />);
    expect(wrapper.is('.Badge')).toBe(true);
  });
});
