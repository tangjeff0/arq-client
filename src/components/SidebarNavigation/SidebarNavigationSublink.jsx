import React from 'react';
import PropTypes from 'prop-types';
import Link from 'components/Link/Link.jsx';
import 'components/SidebarNavigation/SidebarNavigation.scss';

const SidebarNavigationSublink = ({
  linkName,
  linkPath,
  unseenItems,
}) => (
  <Link to={linkPath} className="SidebarNavigation-sublink">
    {linkName}
    {!!unseenItems && unseenItems > 0 && (
      ` (${unseenItems} NEW)`
    )}
  </Link>
);

SidebarNavigationSublink.propTypes = {
  linkName: PropTypes.string.isRequired,
  linkPath: PropTypes.string.isRequired,
  unseenItems: PropTypes.number,
};

SidebarNavigationSublink.defaultProps = {
  unseenItems: null,
};

export default SidebarNavigationSublink;
