import { useQuery } from '@apollo/client';
import { get } from 'lodash';
import useIntegrationFlags from 'hooks/useIntegrationFlags.jsx';
import useUser from 'hooks/useUser.jsx';
import { GET_SIDEBAR_ITEMS } from 'queries/sidebar';

const useSidebarNavigation = () => {
  const { data } = useQuery(GET_SIDEBAR_ITEMS, {
    fetchPolicy: 'cache-and-network',
    pollInterval: 10000,
  });

  const unseenArchitectures = get(data, 'arq_architectures_statuses_aggregate.aggregate.count', 0);
  const unseenSharedDocs = get(data, 'arq_my_collaboration_invitations_aggregate.aggregate.count', 0);

  const unseenItems = {
    shared: unseenSharedDocs,
    admin: unseenArchitectures,
  };

  const { salesforceEnabled } = useIntegrationFlags();

  const links = [
    {
      name: 'Documents',
      shortname: 'home',
      path: '/',
      sublinks: [
        {
          name: 'My Documents',
          shortname: 'home',
          path: '/',
        },
        {
          name: 'Shared Documents',
          shortname: 'shared',
          path: '/shared',
        },
      ],
    },
    {
      name: 'Architectures',
      shortname: 'published',
      path: '/published_architectures',
    },
    {
      name: 'Taxonomies',
      shortname: 'taxonomies',
      path: '/taxonomies',
      sublinks: [
        {
          name: 'All Taxonomies',
          shortname: 'taxonomies',
          path: '/taxonomies',
        },
        {
          name: 'Tech Capability Tags',
          shortname: 'capabilities',
          path: '/published_architectures/1763fca5-22a5-4209-a698-bfeb230274aa',
        },
      ],
    },
    {
      name: 'Customers',
      shortname: 'customers',
      path: '/customers',
      sublinks: [
        {
          name: 'All Customers',
          shortname: 'customers',
          path: '/customers',
        },
        {
          name: 'Problem Sets',
          shortname: 'problems',
          path: '/problems',
        },
      ],
    },
    {
      name: 'Companies',
      shortname: 'companies',
      path: '/companies',
    },
    {
      name: 'Admin',
      shortname: 'admin',
      path: '/admin/architectures',
      permissions: 'admin',
      sublinks: [
        {
          name: 'Architecture Review',
          shortname: 'admin',
          path: '/admin/architectures',
        },
        {
          name: 'Merge Taxonomies',
          shortname: 'merge',
          path: '/admin/merge',
        },
        {
          name: 'Taxonomy Editor',
          shortname: 'editor',
          path: '/admin/merge/',
        },
      ],
    },
  ];

  if (salesforceEnabled) {
    links.push({
      name: 'Pitchbook ∩ Salesforce',
      shortname: 'pitchbook',
      path: '/pitchbook',
    });
  }

  const { user } = useUser();

  return { links, unseenItems, user };
};

export default useSidebarNavigation;
