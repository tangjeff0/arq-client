import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import Loader from 'components/Loader/Loader.jsx';

function FilterableListFooter({ actionText, onSubmit, updating }) {
  return actionText ? (
    <div className="FilterableList-item-footer">
      <Button display="block" size="md" onClick={onSubmit}>
        {updating ? <Loader size={21} isCentered /> : actionText}
      </Button>
    </div>
  ) : null;
}

FilterableListFooter.propTypes = {
  actionText: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
};

export default FilterableListFooter;
