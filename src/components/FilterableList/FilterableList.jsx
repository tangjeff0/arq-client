import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import cx from 'classnames';
import FilterableListSearch from 'components/FilterableList/FilterableListSearch.jsx';
import FilterableListContent from 'components/FilterableList/FilterableListContent.jsx';
import FilterableListCollection from 'components/FilterableList/FilterableListCollection.jsx';
import FilterableListFooter from 'components/FilterableList/FilterableListFooter.jsx';
import Loader from 'components/Loader/Loader.jsx';
import 'components/FilterableList/FilterableList.scss';

class FilterableList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filterTerm: '',
    };
  }

  onSearch = (filterTerm) => {
    const { onSearch } = this.props;
    this.setState({ filterTerm });
    onSearch(filterTerm);
  }

  onSelect = (item) => {
    const { onSelect } = this.props;
    onSelect(item);
  }

  onDeselect = (item) => {
    const { onDeselect } = this.props;
    onDeselect(item);
  }

  onSubmit = () => {
    const { onSubmit } = this.props;
    onSubmit();
  }

  render() {
    const {
      collection,
      loading,
      idKey,
      titleKey,
      checkedKey,
      checkedValue,
      actionText,
      disabled,
      scrollable,
      allowClickOnRow,
      updating,
    } = this.props;

    const { filterTerm } = this.state;

    return (
      <div
        className={cx('FilterableList', {
          'FilterableList--scrollable': scrollable,
        })}
      >
        {!loading && <FilterableListSearch onSearch={this.onSearch} />}
        <FilterableListContent>
          {loading ? (
            <Loader size={21} isCentered />
          ) : (
            <FilterableListCollection
              collection={collection}
              idKey={idKey}
              titleKey={titleKey}
              checkedKey={checkedKey}
              checkedValue={checkedValue}
              filterTerm={filterTerm}
              onSelect={this.onSelect}
              onDeselect={this.onDeselect}
              disabled={disabled}
              allowClickOnRow={allowClickOnRow}
            />
          )}
        </FilterableListContent>
        <FilterableListFooter
          updating={updating}
          actionText={actionText}
          onSubmit={this.onSubmit}
        />
      </div>
    );
  }
}

FilterableList.propTypes = {
  collection: PropTypes.arrayOf(PropTypes.shape({})),
  loading: PropTypes.bool,
  idKey: PropTypes.string.isRequired,
  titleKey: PropTypes.string.isRequired,
  checkedKey: PropTypes.string.isRequired,
  checkedValue: PropTypes.string.isRequired,
  actionText: PropTypes.string.isRequired,
  onSearch: PropTypes.func,
  onSelect: PropTypes.func,
  onDeselect: PropTypes.func,
  onSubmit: PropTypes.func,
  allowClickOnRow: PropTypes.bool,
  disabled: PropTypes.func,
  scrollable: PropTypes.bool,
  updating: PropTypes.bool,
};

FilterableList.defaultProps = {
  collection: [],
  loading: false,
  onSearch: noop,
  onSelect: noop,
  onDeselect: noop,
  onSubmit: noop,
  allowClickOnRow: false,
  disabled: () => true,
  scrollable: false,
  updating: false,
};

export default FilterableList;
