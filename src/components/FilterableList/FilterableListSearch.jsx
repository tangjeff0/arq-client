import React, { Component } from 'react';
import PropTypes from 'prop-types';

class FilterableListSearch extends Component {
  onChange = (e) => {
    const { onSearch } = this.props;
    const term = e.target.value;
    onSearch(term);
  }

  render() {
    return (
      <div className="FilterableList-search">
        <input
          className="FilterableList-search-input"
          placeholder="Search people"
          onChange={this.onChange}
        />
      </div>
    );
  }
}

FilterableListSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default FilterableListSearch;
