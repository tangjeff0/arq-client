import React from 'react';
import PropTypes from 'prop-types';
import FilterableListItem from 'components/FilterableList/FilterableListItem.jsx';

const FilterableListCollection = ({
  collection,
  idKey,
  titleKey,
  checkedKey,
  checkedValue,
  filterTerm,
  onSelect,
  onDeselect,
  disabled,
  allowClickOnRow,
}) => (
  <ul className="FilterableList-collection">
    {collection.map((item) => {
      const visible = item[titleKey]
        .toLowerCase()
        .includes(filterTerm.toLowerCase());

      return (
        <FilterableListItem
          key={item[idKey]}
          id={item[idKey]}
          visible={visible}
          item={item}
          titleKey={titleKey}
          checkedKey={checkedKey}
          checkedValue={checkedValue}
          filterTerm={filterTerm}
          onSelect={onSelect}
          onDeselect={onDeselect}
          disabled={disabled}
          allowClickOnRow={allowClickOnRow}
        />
      );
    })}
  </ul>
);

FilterableListCollection.propTypes = {
  collection: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  idKey: PropTypes.string.isRequired,
  titleKey: PropTypes.string.isRequired,
  checkedKey: PropTypes.string.isRequired,
  checkedValue: PropTypes.string.isRequired,
  filterTerm: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
  onDeselect: PropTypes.func.isRequired,
  disabled: PropTypes.func.isRequired,
  allowClickOnRow: PropTypes.bool.isRequired,
};

export default FilterableListCollection;
