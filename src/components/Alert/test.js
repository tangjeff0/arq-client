import React from 'react';
import { setUp, checkProps } from 'utils/testing';
import Alert from 'components/Alert/Alert.jsx';

describe('<Alert />', () => {
  let wrapper;

  const expectedProps = {
    heading: 'Heading',
    subheading: 'Subheading',
  };

  beforeEach(() => {
    wrapper = setUp(Alert, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Alert, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Alert class', () => {
    expect(wrapper.is('.Alert')).toBe(true);
  });

  it('should render with .Alert-content class', () => {
    expect(wrapper.exists('.Alert-content')).toBe(true);
  });

  it('should render an .Alert-heading', () => {
    expect(wrapper.exists('.Alert-heading')).toBe(true);
  });

  it('should render heading text', () => {
    const heading = wrapper.find('.Alert-heading');
    expect(heading.text()).toEqual('Heading');
  });

  it('should render with .Alert-subheading class', () => {
    expect(wrapper.exists('.Alert-subheading')).toBe(true);
  });

  it('should render subheading text', () => {
    const subheading = wrapper.find('.Alert-subheading');
    expect(subheading.text()).toEqual('Subheading');
  });

  it('accepts isFullscreen and padded props', () => {
    const isFullscreen = true;
    const padded = true;

    // need to mount to check props
    const wrapperMount = mount(
      <Alert heading="Heading" subheading="Subheading" isFullscreen={isFullscreen} padded={padded} />,
    );
    expect(wrapperMount.props().isFullscreen).toEqual(isFullscreen);
    expect(wrapperMount.props().padded).toEqual(padded);
  });
});
