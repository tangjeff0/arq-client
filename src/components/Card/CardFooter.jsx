import React from 'react';
import PropTypes from 'prop-types';

const CardFooter = ({ isNew, children }) => (
  <footer className="Card-card-footer">
    {isNew && (
      <div className="Card-card-footer-status">New!</div>
    )}
    <div className="Card-card-footer-content">{children}</div>
  </footer>
);

CardFooter.propTypes = {
  isNew: PropTypes.bool,
  children: PropTypes.node,
};

CardFooter.defaultProps = {
  isNew: false,
  children: null,
};

export default CardFooter;
