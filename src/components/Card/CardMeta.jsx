import React from 'react';
import PropTypes from 'prop-types';

const CardMeta = ({ children }) => (
  <span className="Card-card-meta">{children}</span>
);

CardMeta.propTypes = {
  children: PropTypes.node,
};

CardMeta.defaultProps = {
  children: null,
};

export default CardMeta;
