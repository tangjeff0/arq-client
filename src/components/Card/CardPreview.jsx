import React from 'react';
import PropTypes from 'prop-types';

const CardPreview = ({ children }) => (
  <div className="Card-card-preview">{children}</div>
);

CardPreview.propTypes = {
  children: PropTypes.node,
};

CardPreview.defaultProps = {
  children: null,
};

export default CardPreview;
