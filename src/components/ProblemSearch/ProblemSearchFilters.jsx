import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { RefinementListFilter } from 'searchkit';
import SearchKitRefinementListContainer from 'components/SearchKit/SearchKitRefinementListContainer.jsx';
import RefinementListFilterExtended from 'components/SearchKit/RefinementListFilterExtended.jsx';
import SearchKitRefinementListTools from 'components/SearchKit/SearchKitRefinementListTools.jsx';
import SearchKitRefinementListOperatorToggle from 'components/SearchKit/SearchKitRefinementListOperatorToggle.jsx';
import SearchKitRefinementListSortToggle from 'components/SearchKit/SearchKitRefinementListSortToggle.jsx';
import SearchKitRefinementListSearchBar from 'components/SearchKit/SearchKitRefinementListSearchBar.jsx';
import SearchKitRefinementListSearchButton from 'components/SearchKit/SearchKitRefinementListSearchButton.jsx';
import SearchKit from 'components/SearchKit/SearchKit.jsx';
import {
  LABEL_YEAR,
  LABEL_CUSTOMER_AGENCY,
  LABEL_CUSTOMER_PRIORITY,
  LABEL_CATEGORY_OF_NEED,
  LABEL_TECH_TAGS,
} from 'constants/problems.js';

const ProblemSearchFilters = ({
  taxonomiesOperator,
  setTaxonomiesOperator,
  taxonomiesSort,
  setTaxonomiesSort,
  filter,
  setFilter,
  include,
  filterOpen,
  setFilterOpen,
}) => {
  const [bucketNumber, setBucketNumber] = useState(0);

  return (
    <div className="ProblemSearch-filters">
      <RefinementListFilter
        id="year"
        title={LABEL_YEAR}
        field="year"
        operator="OR"
        size={10}
        orderKey="_term"
        orderDirection="asc"
        itemComponent={SearchKit.FilterOption}
      />
      <RefinementListFilter
        id="agencies"
        title={LABEL_CUSTOMER_AGENCY}
        field="customer.raw"
        operator="OR"
        size={3}
        itemComponent={SearchKit.FilterOption}
      />
      <RefinementListFilter
        id="priority"
        title={LABEL_CUSTOMER_PRIORITY}
        field="priority.raw"
        operator="OR"
        size={4}
        orderKey="_count"
        orderDirection="desc"
        itemComponent={SearchKit.FilterOption}
      />
      <RefinementListFilter
        id="categoryofneed"
        title={LABEL_CATEGORY_OF_NEED}
        field="categoryOfNeed.raw"
        operator="OR"
        size={2}
        itemComponent={SearchKit.FilterOption}
      />
      <RefinementListFilterExtended
        id="taxonomies"
        title={LABEL_TECH_TAGS}
        field="taxonomies.term.raw"
        fieldOptions={{ type: 'nested', options: { path: 'taxonomies' } }}
        operator={taxonomiesOperator}
        operatorName="taxonomiesOperator"
        orderKey={taxonomiesSort.key}
        orderDirection={taxonomiesSort.direction}
        include={include}
        filter={filter}
        size={filter.length > 0 ? 999 : 4}
        showMore={filter.length <= 0}
        itemComponent={SearchKit.FilterOption}
        setBucketNumber={setBucketNumber}
        containerComponent={
          (
            <SearchKitRefinementListContainer
              rightComponent={(
                <SearchKitRefinementListTools>
                  <SearchKitRefinementListOperatorToggle
                    taxonomiesOperator={taxonomiesOperator}
                    setTaxonomiesOperator={setTaxonomiesOperator}
                  />
                  <SearchKitRefinementListSortToggle
                    taxonomiesSort={taxonomiesSort}
                    setTaxonomiesSort={setTaxonomiesSort}
                  />
                  <SearchKitRefinementListSearchButton
                    filter={filter}
                    filterOpen={filterOpen}
                    setFilterOpen={setFilterOpen}
                    bucketNumber={bucketNumber}
                  />
                </SearchKitRefinementListTools>
              )}
              bottomComponent={(
                <SearchKitRefinementListSearchBar
                  value={filter}
                  setFilter={setFilter}
                  open={filterOpen}
                />
              )}
            />
          )
        }
      />
    </div>
  );
};

ProblemSearchFilters.propTypes = {
  taxonomiesOperator: PropTypes.string.isRequired,
  setTaxonomiesOperator: PropTypes.func.isRequired,
  taxonomiesSort: PropTypes.shape({
    term: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    direction: PropTypes.string.isRequired,
  }).isRequired,
  setTaxonomiesSort: PropTypes.func.isRequired,
  filter: PropTypes.string,
  setFilter: PropTypes.func.isRequired,
  include: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
  ]).isRequired,
  filterOpen: PropTypes.bool.isRequired,
  setFilterOpen: PropTypes.func.isRequired,
};

ProblemSearchFilters.defaultProps = {
  filter: '',
};


export default ProblemSearchFilters;
