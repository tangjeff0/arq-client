import React from 'react';
import PropTypes from 'prop-types';
import { createProblemFromElasticHit } from 'utils/elastic';
import Problem from 'components/Problem/Problem.jsx';
import Capability from 'components/Capability/Capability.jsx';
import Link from 'components/Link/Link.jsx';

const ProblemSearchProblem = ({ result }) => {
  const problem = createProblemFromElasticHit(result);

  return (
    <div className="ProblemSearch-problem">
      <Problem problem={problem} customer={problem.customer} highlight linkTitle />
      <div className="ProblemSearch-problem-capabilities">
        {problem.taxonomies
          && problem.taxonomies.map(t => (
            <Link
              className="ProblemSearch-problem-capability"
              key={t.id}
              to={`/taxonomies/${t.id}`}
            >
              <Capability capability={t} />
            </Link>
          ))}
      </div>
    </div>
  );
};

ProblemSearchProblem.propTypes = {
  result: PropTypes.shape({
    _id: PropTypes.string,
    _index: PropTypes.string,
    _score: PropTypes.number,
    _source: PropTypes.shape({}),
    _type: PropTypes.string,
  }).isRequired,
  highlight: PropTypes.shape({
    description: PropTypes.arrayOf(),
    number: PropTypes.arrayOf(),
  }),
};

ProblemSearchProblem.defaultProps = {
  highlight: null,
};

export default ProblemSearchProblem;
