import React from 'react';
import {
  SearchkitManager,
  SearchBox,
  Pagination,
  NoHits,
  ViewSwitcherHits,
  HitsStats,
  ResetFilters,
  SelectedFilters,
} from 'searchkit';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Alert from 'components/Alert/Alert.jsx';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import ProblemSearchProblem from 'components/ProblemSearch/ProblemSearchProblem.jsx';
import SearchKit from 'components/SearchKit/SearchKit.jsx';
import ProblemSearchFilters from 'components/ProblemSearch/ProblemSearchFilters.jsx';
import useProblemSearch from 'hooks/useProblemSearch.jsx';
import 'components/ProblemSearch/ProblemSearch.scss';

const searchkit = new SearchkitManager(`${ES_HOST}/problem_sets`);

const ProblemSearch = () => {
  const {
    error,
    loading,
    setTaxonomiesOperator,
    taxonomiesOperator,
    taxonomiesSort,
    setTaxonomiesSort,
    filter,
    setFilter,
    include,
    filterOpen,
    setFilterOpen,
  } = useProblemSearch();

  const queryBuilder = (query) => {
    if (query.slice(0, 3) === 'id:') {
      return {
        match: {
          number: {
            query: `${query.toLowerCase().slice(3)}*`,
          },
        },
      };
    }

    return {
      bool: {
        should: [
          {
            match: {
              description: {
                query,
                fuzziness: 'AUTO',
              },
            },
          },
          {
            wildcard: {
              description: {
                value: `*${query.toLowerCase()}*`,
              },
            },
          },
          {
            wildcard: {
              number: {
                value: `*${query.toLowerCase()}*`,
              },
            },
          },
          {
            nested: {
              path: 'taxonomies',
              query: {
                match: {
                  'taxonomies.term': {
                    query,
                    fuzziness: 'AUTO',
                  },
                },
              },
            },
          },
        ],
      },
    };
  };

  return (
    <div className="ProblemSearch">
      <Dashboard>
        <Dashboard.Header />
        <Dashboard.Section fit>
          <Dashboard.Column padded size="sm" theme="dark">
            <Grid direction="column" justify="spaceBetween">
              <SidebarNavigationInterface active="problems" />
              <Dashboard.Footer>
                <WelcomeBackInterface />
              </Dashboard.Footer>
            </Grid>
          </Dashboard.Column>
          {error && (
            <Alert
              padded
              heading="Uh Oh..."
              subheading="There was an error. Check your network connection and try again."
            />
          )}
          {!error && loading && <Loader size={36} isFullscreen />}
          {!loading && !error && taxonomiesOperator && (
            <SearchKit searchkit={searchkit}>
              <>
                <Dashboard.Column fit>
                  <Dashboard.Toolbar>
                    <SearchBox
                      autofocus
                      searchOnChange
                      placeholder="Search problems... Hint: Use 'id:' to search by ID only"
                      queryBuilder={queryBuilder}
                    />
                  </Dashboard.Toolbar>
                  <div className="ProblemSearch-results">
                    <SearchKit.ActionBar>
                      <HitsStats
                        translations={{
                          'hitstats.results_found': '{hitCount} problem(s) found',
                        }}
                      />
                      <ResetFilters component={SearchKit.Reset} />
                    </SearchKit.ActionBar>
                    <SelectedFilters itemComponent={SearchKit.SelectedFilter} />
                    <ViewSwitcherHits
                      hitsPerPage={12}
                      highlightFields={['description', 'number']}
                      customHighlight={{ number_of_fragments: 0 }}
                      hitComponents={[
                        {
                          key: 'list',
                          title: 'List',
                          itemComponent: ProblemSearchProblem,
                          defaultOption: true,
                        },
                      ]}
                      scrollTo="body"
                    />
                    <NoHits
                      translations={{
                        'NoHits.NoResultsFound': 'No Problems were found.',
                      }}
                      suggestionsField="description"
                      errorComponent={SearchKit.Error}
                    />
                    <Pagination showNumbers />
                  </div>
                </Dashboard.Column>
                <Dashboard.Column fit size="lg" theme="white">
                  <Dashboard.Content>
                    <ProblemSearchFilters
                      taxonomiesOperator={taxonomiesOperator}
                      setTaxonomiesOperator={setTaxonomiesOperator}
                      taxonomiesSort={taxonomiesSort}
                      setTaxonomiesSort={setTaxonomiesSort}
                      filter={filter}
                      setFilter={setFilter}
                      include={include}
                      filterOpen={filterOpen}
                      setFilterOpen={setFilterOpen}
                    />
                  </Dashboard.Content>
                </Dashboard.Column>
              </>
            </SearchKit>
          )}
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

export default ProblemSearch;
