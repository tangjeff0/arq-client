import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import cx from 'classnames';
import ArchitectureReviewTermSearch from 'components/ArchitectureReviewTerms/ArchitectureReviewTermSearch.jsx';
import Button from 'components/Button/Button.jsx';
import {
  IconCheck,
  IconCheckOutlined,
  IconX,
  IconXOutlined,
} from 'components/Icons/Icons.jsx';
import 'components/ArchitectureReviewTerms/ArchitectureReviewTerm.scss';

const ArchitectureReviewTerm = ({
  termEntity,
  onTermExpand,
  onTermCollapse,
  onReject,
  onUnreject,
  onSearch,
  onInsertTermBelowTaxonomy,
  onRemoveTermFromTaxonomy,
  onLinkTermToTaxonomy,
  onUnlinkTermFromTaxonomy,
  highlightPackage,
  expandedTermID,
  searches,
  onApprove,
}) => {
  const [isSearchOpened, setIsSearchOpened] = useState(false);

  const toggleSearch = () => {
    isSearchOpened ? closeSearch() : openSearch();
  };

  const openSearch = () => {
    onTermExpand(termEntity.packageID);
    highlightPackage(termEntity.packageID);
    setIsSearchOpened(true);
  };

  const closeSearch = () => {
    onTermCollapse();
    highlightPackage(null);
    setIsSearchOpened(false);
  };

  const handleReject = () => {
    if (termEntity.approved === false) {
      onUnreject(termEntity.packageID);
    } else {
      onReject(termEntity.packageID);
    }

    closeSearch();
  };

  const handleSearch = (e) => {
    const contains = e.target.value;

    onSearch(contains, termEntity.packageID);
  };

  const handleInsertTermBelowTaxonomy = (taxonomy) => {
    onInsertTermBelowTaxonomy(termEntity, taxonomy);
  };

  const handleRemoveTermFromTaxonomy = () => {
    onRemoveTermFromTaxonomy(termEntity);
  };

  const handleLinkTermToTaxonomy = (taxonomy) => {
    onLinkTermToTaxonomy(termEntity, taxonomy);
  };

  const handleUnlinkTermFromTaxonomy = () => {
    onUnlinkTermFromTaxonomy(termEntity);
  };

  useEffect(() => {
    if (expandedTermID !== termEntity.packageID) setIsSearchOpened(false);
  }, [expandedTermID, termEntity.packageID]);

  return (
    <div className="ArchitectureReviewTerm">
      <div className="ArchitectureReviewTerm-header">
        <h4
          className={cx('ArchitectureReviewTerm-term', {
            'ArchitectureReviewTerm-term--isRejected':
              termEntity.approved === false,
          })}
        >
          <span>{termEntity.term}</span>
        </h4>
        <div className="ArchitectureReviewTerm-actions">
          <Button.Group align="right" gutter="lg">
            <Button unstyled onClick={toggleSearch}>
              {termEntity.approved === true ? (
                <IconCheck />
              ) : (
                <IconCheckOutlined />
              )}
            </Button>
            <Button unstyled onClick={handleReject}>
              {termEntity.approved === false ? <IconX /> : <IconXOutlined />}
            </Button>
          </Button.Group>
        </div>
      </div>
      {isSearchOpened && (
        <ArchitectureReviewTermSearch
          id={termEntity.packageID}
          onApprove={onApprove}
          search={searches.entities[termEntity.packageID]}
          onInsertTermBelowTaxonomy={handleInsertTermBelowTaxonomy}
          onRemoveTermFromTaxonomy={handleRemoveTermFromTaxonomy}
          onLinkTermToTaxonomy={handleLinkTermToTaxonomy}
          onUnlinkTermFromTaxonomy={handleUnlinkTermFromTaxonomy}
          onSearch={handleSearch}
          closeSearch={closeSearch}
        />
      )}
    </div>
  );
};

ArchitectureReviewTerm.propTypes = {
  highlightPackage: PropTypes.func,
  onApprove: PropTypes.func,
  onInsertTermBelowTaxonomy: PropTypes.func,
  onReject: PropTypes.func,
  onRemoveTermFromTaxonomy: PropTypes.func,
  onLinkTermToTaxonomy: PropTypes.func,
  onUnlinkTermFromTaxonomy: PropTypes.func,
  onSearch: PropTypes.func,
  onTermCollapse: PropTypes.func,
  onTermExpand: PropTypes.func,
  onUnreject: PropTypes.func,
  searches: PropTypes.shape({
    byID: PropTypes.arrayOf(PropTypes.string),
    entities: PropTypes.shape({}),
  }),
  termEntity: PropTypes.shape({
    packageID: PropTypes.string,
    term: PropTypes.string,
    approved: PropTypes.bool,
  }),
  expandedTermID: PropTypes.string,
};

ArchitectureReviewTerm.defaultProps = {
  highlightPackage: noop,
  onApprove: noop,
  onReject: noop,
  onUnreject: noop,
  onSearch: noop,
  onInsertTermBelowTaxonomy: noop,
  onRemoveTermFromTaxonomy: noop,
  onLinkTermToTaxonomy: noop,
  onUnlinkTermFromTaxonomy: noop,
  onTermExpand: noop,
  onTermCollapse: noop,
  searches: {},
  termEntity: {},
  expandedTermID: null,
};

export default ArchitectureReviewTerm;
