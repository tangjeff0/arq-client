import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';
import {
  IconClose,
  IconInsertBelow,
  IconLink,
  IconBrokenLink,
} from 'components/Icons/Icons.jsx';

const ArchitectureReviewTermSearchResults = ({
  results,
  onInsertTermBelowTaxonomy,
  onRemoveTermFromTaxonomy,
  onLinkTermToTaxonomy,
  onUnlinkTermFromTaxonomy,
}) => {
  const onTaxonomyInsertTerm = (taxonomy) => {
    onInsertTermBelowTaxonomy(taxonomy);
  };

  const onTaxonomyRemoveTerm = () => {
    onRemoveTermFromTaxonomy();
  };

  const onTaxonomyLinkTerm = (taxonomy) => {
    onLinkTermToTaxonomy(taxonomy);
  };

  const onTaxonomyUnlinkTerm = () => {
    onUnlinkTermFromTaxonomy();
  };

  const taxonomyTreeActions = {
    primary: [
      {
        title: 'Link',
        icon: <IconLink display="block" />,
        handler: onTaxonomyLinkTerm,
      },
      {
        title: 'Insert below',
        icon: <IconInsertBelow rotation={90} display="block" />,
        handler: onTaxonomyInsertTerm,
      },
    ],
    temp: [
      {
        title: 'Remove',
        icon: <IconClose display="block" fill="#cad2d3" />,
        handler: onTaxonomyRemoveTerm,
      },
      {},
    ],
    unlink: [
      {
        title: 'Unlink',
        icon: <IconBrokenLink display="block" fill="#cad2d3" />,
        handler: onTaxonomyUnlinkTerm,
      },
      {},
    ],
  };

  return (
    <div className="ArchitectureReviewTerm-search-results">
      {results.taxonomies.length ? (
        results.taxonomies.map(taxonomy => (
          <div
            key={taxonomy.id}
            className="ArchitectureReviewTerm-search-results-group"
          >
            <TaxonomyTree
              taxonomy={taxonomy}
              actions={taxonomyTreeActions}
              actionFlags={['temp', 'unlink']}
            />
          </div>
        ))
      ) : (
        <p className="ArchitectureReviewTerm-search-results-message">
          No results.
        </p>
      )}
    </div>
  );
};

ArchitectureReviewTermSearchResults.propTypes = {
  results: PropTypes.shape({
    taxonomies: PropTypes.arrayOf(taxonomyPropType),
  }).isRequired,
  onInsertTermBelowTaxonomy: PropTypes.func.isRequired,
  onRemoveTermFromTaxonomy: PropTypes.func.isRequired,
};

ArchitectureReviewTermSearchResults.defaultProps = {};

export default ArchitectureReviewTermSearchResults;
