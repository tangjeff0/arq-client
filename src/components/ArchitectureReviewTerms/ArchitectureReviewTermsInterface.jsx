import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { debounce } from 'lodash';
import Button from 'components/Button/Button.jsx';
import Loader from 'components/Loader/Loader.jsx';
import {
  initTerms as initTermsRaw,
  approveTerm as approveTermRaw,
  rejectTerm as rejectTermRaw,
  unrejectTerm as unrejectTermRaw,
  searchForParentTerm as searchForParentTermRaw,
  submitApprovedTerms as submitApprovedTermsRaw,
  insertTermBelowTaxonomy as insertTermBelowTaxonomyRaw,
  removeTermFromTaxonomy as removeTermFromTaxonomyRaw,
  linkTermToTaxonomy as linkTermToTaxonomyRaw,
  unlinkTermFromTaxonomy as unlinkTermFromTaxonomyRaw,
} from 'actions/adminArchitectureTerms';
import { highlightPackage as highlightPackageRaw } from 'actions/adminArchitecture';
import ArchitectureReviewTerms from 'components/ArchitectureReviewTerms/ArchitectureReviewTerms.jsx';

const ArchitectureReviewTermsInterface = ({
  initTerms,
  approveTerm,
  rejectTerm,
  unrejectTerm,
  searchForParentTerm,
  submitApprovedTerms,
  insertTermBelowTaxonomy,
  removeTermFromTaxonomy,
  linkTermToTaxonomy,
  unlinkTermFromTaxonomy,
  highlightPackage,
  adminArchitectureTerms,
}) => {
  useEffect(
    () => { initTerms(); },
    [initTerms],
  );

  const delayedSearchForParentTerm = debounce(
    (contains, termID) => searchForParentTerm(contains, termID),
    1000,
  );

  const submitApprovedTermsAsync = async () => {
    await submitApprovedTerms();
  };

  if (!adminArchitectureTerms.terms) return null;

  return (
    <div>
      <ArchitectureReviewTerms
        terms={adminArchitectureTerms.terms}
        searches={adminArchitectureTerms.searches}
        approveTerm={approveTerm}
        rejectTerm={rejectTerm}
        unrejectTerm={unrejectTerm}
        searchForParentTerm={delayedSearchForParentTerm}
        insertTermBelowTaxonomy={insertTermBelowTaxonomy}
        removeTermFromTaxonomy={removeTermFromTaxonomy}
        linkTermToTaxonomy={linkTermToTaxonomy}
        unlinkTermFromTaxonomy={unlinkTermFromTaxonomy}
        highlightPackage={highlightPackage}
      />
      {adminArchitectureTerms.submitting ? (
        <Loader size={21} isCentered />
      ) : (
        <Button onClick={submitApprovedTermsAsync} display="block">
          Submit approved terms
        </Button>
      )}
    </div>
  );
};

ArchitectureReviewTermsInterface.propTypes = {
  initTerms: PropTypes.func.isRequired,
  approveTerm: PropTypes.func.isRequired,
  rejectTerm: PropTypes.func.isRequired,
  unrejectTerm: PropTypes.func.isRequired,
  searchForParentTerm: PropTypes.func.isRequired,
  submitApprovedTerms: PropTypes.func.isRequired,
  insertTermBelowTaxonomy: PropTypes.func.isRequired,
  removeTermFromTaxonomy: PropTypes.func.isRequired,
  linkTermToTaxonomy: PropTypes.func.isRequired,
  unlinkTermFromTaxonomy: PropTypes.func.isRequired,
  highlightPackage: PropTypes.func.isRequired,
  adminArchitectureTerms: PropTypes.shape({
    terms: PropTypes.shape({}),
    searches: PropTypes.shape({}),
    submitting: PropTypes.bool,
  }).isRequired,
};

ArchitectureReviewTermsInterface.defaultProps = {};

function mapStateToProps(state) {
  return {
    adminArchitectureTerms: state.adminArchitectureTerms,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      initTerms: initTermsRaw,
      approveTerm: approveTermRaw,
      rejectTerm: rejectTermRaw,
      unrejectTerm: unrejectTermRaw,
      searchForParentTerm: searchForParentTermRaw,
      submitApprovedTerms: submitApprovedTermsRaw,
      insertTermBelowTaxonomy: insertTermBelowTaxonomyRaw,
      removeTermFromTaxonomy: removeTermFromTaxonomyRaw,
      linkTermToTaxonomy: linkTermToTaxonomyRaw,
      unlinkTermFromTaxonomy: unlinkTermFromTaxonomyRaw,
      highlightPackage: highlightPackageRaw,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ArchitectureReviewTermsInterface,
);
