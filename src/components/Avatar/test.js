import React from 'react';
import Avatar from 'components/Avatar/Avatar.jsx';

describe('<Avatar />', () => {
  it('should render with .Avatar class', () => {
    const wrapper = shallow(<Avatar />);
    expect(wrapper.is('.Avatar')).toBe(true);
  });
});

describe('<AvatarGroup />', () => {
  it('should render with .AvatarGroup class', () => {
    const wrapper = shallow(<Avatar.Group />);
    expect(wrapper.is('.AvatarGroup')).toBe(true);
  });
});
