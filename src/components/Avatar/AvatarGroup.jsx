/* eslint-disable import/no-cycle */
// Allow export of related components from main file
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { userPropType } from 'proptypes';
import cx from 'classnames';
import Avatar from 'components/Avatar/Avatar.jsx';

class AvatarGroup extends PureComponent {
  render() {
    const { users, align } = this.props;

    return (
      <div className={cx('AvatarGroup', `AvatarGroup--align-${align}`)}>
        {users.map(user => <Avatar key={user.email} user={user} />)}
      </div>
    );
  }
}

AvatarGroup.propTypes = {
  users: PropTypes.arrayOf(userPropType),
  align: PropTypes.string,
};

AvatarGroup.defaultProps = {
  users: [],
  align: 'left',
};

export default AvatarGroup;
