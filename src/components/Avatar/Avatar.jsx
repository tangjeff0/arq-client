/* eslint-disable import/no-cycle */
// Allow export of related components from main file
import React from 'react';
import PropTypes from 'prop-types';
import { userPropType } from 'proptypes';
import cx from 'classnames';
import Tooltip from 'components/Tooltip/Tooltip.jsx';
import AvatarGroup from 'components/Avatar/AvatarGroup.jsx';
import 'components/Avatar/Avatar.scss';

const Avatar = ({
  user,
  size,
  tooltipLeftOrRight,
}) => {
  const initials = user.email ? user.email.split('@')[0].substr(0, 2) : '?';

  return (
    <div className={cx('Avatar', `Avatar--size-${size}`)}>
      <Tooltip content={user.email} leftOrRight={tooltipLeftOrRight} topDistance="sm">
        <Tooltip.Wrapper>{initials}</Tooltip.Wrapper>
      </Tooltip>
    </div>
  );
};

Avatar.propTypes = {
  user: userPropType,
  size: PropTypes.oneOf(['sm', 'md', 'lg', 'xl']),
  tooltipLeftOrRight: PropTypes.oneOf(['left', 'right']),
};

Avatar.defaultProps = {
  user: {},
  size: 'sm',
  tooltipLeftOrRight: 'right',
};

Avatar.Group = AvatarGroup;

export default Avatar;
