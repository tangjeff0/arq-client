import { setUp, checkProps } from 'utils/testing';
import SharedDocument from 'components/SharedDocument/SharedDocument.jsx';

describe('<SharedDocument />', () => {
  let wrapper;

  const expectedProps = {
    user: {},
    data: {},
    entities: {},
    activeComments: {},
    ui: {},
    setSharedDocActiveComments: () => {},
    unsetSharedDocActiveComments: () => {},
    addSharedDocPackageComment: () => {},
    deleteSharedDocPackageComment: () => {},
  };

  beforeEach(() => {
    wrapper = setUp(SharedDocument, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(SharedDocument, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .SharedDocument class', () => {
    expect(wrapper.is('.SharedDocument')).toBe(true);
  });
});
