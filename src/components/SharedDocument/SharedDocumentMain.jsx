import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class SharedDocumentMain extends PureComponent {
  render() {
    const { children } = this.props;

    return <div className="SharedDocument-main">{children}</div>;
  }
}

SharedDocumentMain.propTypes = {
  children: PropTypes.node,
};

SharedDocumentMain.defaultProps = {
  children: null,
};

export default SharedDocumentMain;
