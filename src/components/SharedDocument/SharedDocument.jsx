import React from 'react';
import PropTypes from 'prop-types';
import { userPropType } from 'proptypes';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import SharedDocumentMain from 'components/SharedDocument/SharedDocumentMain.jsx';
import SharedDocumentStatus from 'components/SharedDocument/SharedDocumentStatus.jsx';
import SharedDocumentTitle from 'components/SharedDocument/SharedDocumentTitle.jsx';
import SharedDocumentBlocks from 'components/SharedDocument/SharedDocumentBlocks.jsx';
import SharedDocumentCommentThread from 'components/SharedDocument/SharedDocumentCommentThread.jsx';
import AvatarGroup from 'components/Avatar/AvatarGroup.jsx';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import { IconComment } from 'components/Icons/Icons.jsx';
import 'components/SharedDocument/SharedDocument.scss';

const SharedDocument = ({
  user,
  data,
  entities,
  activeComments,
  ui,
  setSharedDocActiveComments,
  unsetSharedDocActiveComments,
  addSharedDocPackageComment,
  deleteSharedDocPackageComment,
  publishedView,
}) => {
  const activeCommentsPackageQID = activeComments.packageQID;
  const activeCommentsThread = activeCommentsPackageQID
    ? activeComments.commentsThread
    : {};

  return (
    <div className="SharedDocument">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section>
          <Dashboard.Column>
            <Toolbar fill>
              {!publishedView && (
                <Toolbar.Group fill padded>
                  <Toolbar.Item>
                    <IconComment display="inlineBlock" position="left" />
                    Comment Only
                  </Toolbar.Item>
                </Toolbar.Group>
              )}
              <Toolbar.Group padded>
                <Toolbar.Item>
                  Author:
                  {' '}
                  {data.author ? data.author.email : 'unknown@email.com'}
                </Toolbar.Item>
              </Toolbar.Group>
              <Toolbar.Group padded>
                <Toolbar.Item>
                  <AvatarGroup align="right" users={entities.collaborators} />
                </Toolbar.Item>
              </Toolbar.Group>
            </Toolbar>
          </Dashboard.Column>
        </Dashboard.Section>
        <Dashboard.Section fit>
          <Dashboard.Column fit offsetLeft="sm">
            <Dashboard.Content padding={['top']}>
              <SharedDocumentMain>
                <SharedDocumentStatus status={ui.status} />
                {!ui.loading
                  && !ui.error
                  && data.title && <SharedDocumentTitle title={data.title} />}
                {!ui.loading
                  && !ui.error
                  && entities.allBlocks && (
                  <SharedDocumentBlocks
                    blocks={entities.allBlocks}
                    entities={entities}
                    activeComments={activeComments}
                    documentQID={data.id}
                    setSharedDocActiveComments={setSharedDocActiveComments}
                    unsetSharedDocActiveComments={
                      unsetSharedDocActiveComments
                    }
                    publishedView={publishedView}
                  />
                )}
              </SharedDocumentMain>
            </Dashboard.Content>
          </Dashboard.Column>
          <Dashboard.Column fit size="sm">
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              {activeComments.packageQID && (
                <SharedDocumentCommentThread
                  user={user}
                  activeCommentsThread={activeCommentsThread}
                  activeComments={activeComments}
                  addSharedDocPackageComment={addSharedDocPackageComment}
                  deleteSharedDocPackageComment={deleteSharedDocPackageComment}
                  onDismiss={unsetSharedDocActiveComments}
                />
              )}
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

SharedDocument.propTypes = {
  user: userPropType.isRequired,
  data: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    author: userPropType,
  }).isRequired,
  entities: PropTypes.shape({
    allBlocks: PropTypes.arrayOf(PropTypes.string),
    collaborators: PropTypes.arrayOf(userPropType),
  }).isRequired,
  activeComments: PropTypes.shape({
    packageQID: PropTypes.string,
    commentsThread: PropTypes.shape({}),
  }).isRequired,
  ui: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.shape({}),
    status: PropTypes.string,
  }).isRequired,
  setSharedDocActiveComments: PropTypes.func.isRequired,
  unsetSharedDocActiveComments: PropTypes.func.isRequired,
  addSharedDocPackageComment: PropTypes.func.isRequired,
  deleteSharedDocPackageComment: PropTypes.func.isRequired,
  publishedView: PropTypes.bool,
};

SharedDocument.defaultProps = {
  publishedView: false,
};

export default SharedDocument;
