/* eslint-disable import/no-cycle */
import React from 'react';
import { packagePropType } from 'proptypes';
import usePackageCommentCount from 'hooks/usePackageCommentCount.jsx';
import useSharedDocumentPackage from 'hooks/useSharedDocumentPackage.jsx';
import ArchitecturePackage from 'components/Architecture/ArchitecturePackage.jsx';

// TODO: convert to interface
const SharedDocumentArchitecturePackageInterface = (props) => {
  const { pkg } = props;

  const { commentCount } = usePackageCommentCount(pkg);
  const { packageToggleChildren } = useSharedDocumentPackage();

  return (
    <ArchitecturePackage
      commentCount={commentCount}
      packageToggleChildren={packageToggleChildren}
      {...props}
    />
  );
};

SharedDocumentArchitecturePackageInterface.propTypes = {
  pkg: packagePropType.isRequired,
};

SharedDocumentArchitecturePackageInterface.defaultProps = {};

export default SharedDocumentArchitecturePackageInterface;
