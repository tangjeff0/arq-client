import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class SharedDocumentTitle extends PureComponent {
  render() {
    const { title } = this.props;

    return <h2 className="SharedDocument-title">{title}</h2>;
  }
}

SharedDocumentTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

SharedDocumentTitle.defaultProps = {};

export default SharedDocumentTitle;
