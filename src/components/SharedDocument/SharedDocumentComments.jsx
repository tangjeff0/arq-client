import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';

class SharedDocumentComments extends PureComponent {
  render() {
    const { packageQID, onDismiss } = this.props;

    return (
      <div className="SharedDocument-comments">
        <div className="SharedDocument-comments-dismiss">
          <Button unstyled onClick={onDismiss}>
            <IconClose position="left" />
            Dismiss
          </Button>
        </div>
        <div className="SharedDocument-comments-thread">{packageQID}</div>
      </div>
    );
  }
}

SharedDocumentComments.propTypes = {
  onDismiss: PropTypes.func,
};

SharedDocumentComments.defaultProps = {
  onDismiss: noop,
};

export default SharedDocumentComments;
