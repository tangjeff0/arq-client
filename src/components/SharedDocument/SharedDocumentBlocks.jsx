import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import SharedDocumentBlock from 'components/SharedDocument/SharedDocumentBlock.jsx';
import SharedDocumentEditor from 'components/SharedDocument/SharedDocumentEditor.jsx';
import SharedDocumentArchitecture from 'components/SharedDocument/SharedDocumentArchitecture.jsx';

class SharedDocumentBlocks extends PureComponent {
  render() {
    const {
      blocks,
      entities,
      activeComments,
      setSharedDocActiveComments,
      unsetSharedDocActiveComments,
      documentQID,
      publishedView,
    } = this.props;

    return (
      <div className="SharedDocument-blocks">
        {blocks.map((qid) => {
          const block = entities.blocks[qid];
          const { type } = block;

          switch (type) {
            case 'html': {
              const editorQID = block.editor;
              const editorEntity = entities.editors[editorQID];

              return (
                <SharedDocumentBlock key={qid}>
                  <SharedDocumentEditor editor={editorEntity} />
                </SharedDocumentBlock>
              );
            }
            case 'architecture': {
              const architectureQID = block.architecture;
              const architectureEntity = entities.architectures[architectureQID];

              return (
                <SharedDocumentBlock key={qid}>
                  <SharedDocumentArchitecture
                    documentQID={documentQID}
                    architecture={architectureEntity}
                    activeCommentsPackageQID={activeComments.packageQID}
                    setSharedDocActiveComments={setSharedDocActiveComments}
                    unsetSharedDocActiveComments={unsetSharedDocActiveComments}
                    publishedView={publishedView}
                  />
                </SharedDocumentBlock>
              );
            }
            default:
              return null;
          }
        })}
      </div>
    );
  }
}

SharedDocumentBlocks.propTypes = {
  documentQID: PropTypes.string.isRequired,
  blocks: PropTypes.arrayOf(PropTypes.string).isRequired,
  entities: PropTypes.shape({}).isRequired,
  activeComments: PropTypes.shape({}).isRequired,
  setSharedDocActiveComments: PropTypes.func.isRequired,
  unsetSharedDocActiveComments: PropTypes.func.isRequired,
  publishedView: PropTypes.bool.isRequired,
};

SharedDocumentBlocks.defaultProps = {};

export default SharedDocumentBlocks;
