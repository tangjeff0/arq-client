import React from 'react';
import PropTypes from 'prop-types';

function TableHeader({ children, ...props }) {
  return (
    <thead className="Table-header" {...props}>
      {children}
    </thead>
  );
}

TableHeader.propTypes = {
  children: PropTypes.node,
};

TableHeader.defaultProps = {
  children: null,
};

export default TableHeader;
