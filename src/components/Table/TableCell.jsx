import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

function TableCell({
  children,
  header,
  alignRight,
  ...props
}) {
  if (header) {
    return (
      <th
        className={cx('Table-cell', {
          'Table-cell--alignRight': alignRight,
        })}
        {...props}
      >
        {children}
      </th>
    );
  }
  return (
    <td
      className={cx('Table-cell', {
        'Table-cell--alignRight': alignRight,
      })}
      {...props}
    >
      {children}
    </td>
  );
}

TableCell.propTypes = {
  children: PropTypes.node,
  header: PropTypes.bool,
  alignRight: PropTypes.bool,
};

TableCell.defaultProps = {
  children: null,
  header: false,
  alignRight: false,
};

TableCell.defaultProps = {};

export default TableCell;
