import React from 'react';
import PropTypes from 'prop-types';

function TableFooter({ children, ...props }) {
  return (
    <tfoot className="Table-footer" {...props}>
      {children}
    </tfoot>
  );
}

TableFooter.propTypes = {
  children: PropTypes.node,
};

TableFooter.defaultProps = {
  children: null,
};

export default TableFooter;
