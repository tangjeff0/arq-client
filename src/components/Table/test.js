import React from 'react';
import Table from 'components/Table/Table.jsx';

describe('<Table />', () => {
  it('should render with .Table class', () => {
    const wrapper = shallow(<Table capability={{}} />);
    expect(wrapper.is('.Table')).toBe(true);
  });

  it('should render children when passed in', () => {
    const wrapper = mount(
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.Cell header />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell />
          </Table.Row>
        </Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.Cell />
          </Table.Row>
        </Table.Footer>
      </Table>,
    );

    expect(wrapper.exists('.Table-row')).toEqual(true);
    expect(wrapper.exists('.Table-cell')).toEqual(true);
    expect(wrapper.exists('.Table-header')).toEqual(true);
    expect(wrapper.exists('.Table-footer')).toEqual(true);
  });
});
