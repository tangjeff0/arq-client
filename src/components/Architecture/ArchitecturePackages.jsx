/* eslint-disable import/no-cycle */
// Rewrite without cycle?
import React from 'react';
import PropTypes from 'prop-types';
import { packagePropType } from 'proptypes';
import cx from 'classnames';
import ArchitecturePackage from 'components/Architecture/ArchitecturePackage.jsx';

const ArchitecturePackages = (props) => {
  const { packages, depth, renderPackageInterface } = props;

  const renderPackage = (packageProps) => {
    if (renderPackageInterface) return renderPackageInterface(packageProps);

    return <ArchitecturePackage {...packageProps} />;
  };

  return (
    <ul
      className={cx(
        'Architecture-packages',
        `Architecture-packages--depth-${depth}`,
      )}
    >
      {packages.map((pkg, ind) => (
        pkg.show !== false && renderPackage({
          pkg,
          key: pkg.id,
          indentable: ind !== 0,
          canMoveUp: ind !== 0,
          canMoveDown: ind < packages.length - 1,
          ...props,
        })
      ))}
    </ul>
  );
};

ArchitecturePackages.propTypes = {
  depth: PropTypes.number,
  packages: PropTypes.arrayOf(packagePropType).isRequired,
  renderPackageInterface: PropTypes.func,
};

ArchitecturePackages.defaultProps = {
  depth: 0,
  renderPackageInterface: null,
};

export default ArchitecturePackages;
