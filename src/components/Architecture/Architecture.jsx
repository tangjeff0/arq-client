import React from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import { noop } from 'lodash';
import cx from 'classnames';
import ArchitectureHeader from 'components/Architecture/ArchitectureHeader.jsx';
import ArchitectureTree from 'components/Architecture/ArchitectureTree.jsx';
import 'components/Architecture/Architecture.scss';

const Architecture = ({
  architecture,
  size,
  editable,
  actionable,
  interactive,
  commentable,
  disabled,
  onTitleFocus,
  onTitleClick,
  onTitleChange,
  onTitleKeyDown,
  activePackageQID,
  activeCommentThreadPackageQID,
  onUpdateActivePackageQID,
  onPackageFocus,
  onPackageClick,
  onPackageToggleComments,
  publishedView,
  enableOnboarding,
  renderPackageInterface,
}) => (
  <div
    className={cx('Architecture', {
      [`Architecture--${size}`]: size,
      'Architecture--disabled': disabled,
    })}
  >
    <div className="Architecture-content">
      <ArchitectureHeader
        architecture={architecture}
        editable={editable}
        onTitleFocus={onTitleFocus}
        onTitleClick={onTitleClick}
        onTitleChange={onTitleChange}
        onTitleKeyDown={onTitleKeyDown}
        enableOnboarding={enableOnboarding}
      />
      <ArchitectureTree
        architecture={architecture}
        editable={editable}
        interactive={interactive}
        actionable={actionable}
        commentable={commentable}
        activePackageQID={activePackageQID}
        activeCommentThreadPackageQID={activeCommentThreadPackageQID}
        onUpdateActivePackageQID={onUpdateActivePackageQID}
        onPackageFocus={onPackageFocus}
        onPackageClick={onPackageClick}
        onPackageToggleComments={onPackageToggleComments}
        publishedView={publishedView}
        // Example: props => <MyCustomPackage {...props} />
        // MyCustomPackage should render an <ArchitecturePackage /> with custom package props
        renderPackageInterface={renderPackageInterface}
      />
    </div>
  </div>
);

Architecture.propTypes = {
  actionable: PropTypes.bool,
  activeCommentThreadPackageQID: PropTypes.string,
  activePackageQID: PropTypes.string,
  architecture: architecturePropType.isRequired,
  commentable: PropTypes.bool,
  disabled: PropTypes.bool,
  editable: PropTypes.bool,
  enableOnboarding: PropTypes.bool,
  interactive: PropTypes.bool,
  onPackageClick: PropTypes.func,
  onPackageFocus: PropTypes.func,
  onPackageToggleComments: PropTypes.func,
  onTitleChange: PropTypes.func,
  onTitleClick: PropTypes.func,
  onTitleFocus: PropTypes.func,
  onTitleKeyDown: PropTypes.func,
  onUpdateActivePackageQID: PropTypes.func,
  publishedView: PropTypes.bool,
  size: PropTypes.string,
  renderPackageInterface: PropTypes.func,
};

Architecture.defaultProps = {
  size: '',
  editable: false,
  enableOnboarding: false,
  interactive: false,
  actionable: false,
  commentable: false,
  disabled: false,
  publishedView: false,
  onTitleFocus: noop,
  onTitleClick: noop,
  onTitleChange: noop,
  onTitleKeyDown: noop,
  activePackageQID: '',
  activeCommentThreadPackageQID: '',
  onUpdateActivePackageQID: noop,
  onPackageFocus: noop,
  onPackageClick: noop,
  onPackageToggleComments: noop,
  renderPackageInterface: null,
};

export default Architecture;
