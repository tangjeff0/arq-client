import React, { Component } from 'react';
import { architecturePropType } from 'proptypes';
import clipboard from 'clipboard-js';
import { architectureToClipboard } from 'utils/architecture';
import { IconClipboard } from 'components/Icons/Icons.jsx';

import ArchitectureToolbarButton from './ArchitectureToolbarButton.jsx';

class ArchitectureToolbarClipboard extends Component {
  onClick = (e) => {
    e.preventDefault();

    const { architecture } = this.props;

    const clipboardData = architectureToClipboard(architecture);

    clipboard.copy({
      'text/plain': clipboardData.text,
      'text/html': clipboardData.html,
    });

    alert('Architecture to clipboard!');
  }

  render() {
    return (
      <ArchitectureToolbarButton onClick={this.onClick}>
        <IconClipboard display="block" />
        {' '}
        Copy to clipboard
      </ArchitectureToolbarButton>
    );
  }
}

ArchitectureToolbarClipboard.propTypes = {
  architecture: architecturePropType.isRequired,
};

ArchitectureToolbarClipboard.defaultProps = {};

export default ArchitectureToolbarClipboard;
