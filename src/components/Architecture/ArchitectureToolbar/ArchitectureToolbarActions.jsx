import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import { noop } from 'lodash';
import {
  ARCHITECTURE_DRAFT,
  ARCHITECTURE_SUBMITTED,
  ARCHITECTURE_IN_REVIEW,
  ARCHITECTURE_REJECTED,
} from 'constants/architectureStates.js';
import Button from 'components/Button/Button.jsx';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import ActionBox from 'components/ActionBox/ActionBox.jsx';
import ActionList from 'components/ActionList/ActionList.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Modal from 'components/Modal/Modal.jsx';
import { IconCaretDown, IconSettings } from 'components/Icons/Icons.jsx';

const ArchitectureToolbarActions = ({
  architecture,
  onActionOpen,
  onActionClose,
  onDelete,
  onStatusChange,
}) => {
  const [architectureDeleteModalOpened, setArchitectureDeleteModalOpened] = useState(false);
  const [architectureDeleting, setArchitectureDeleting] = useState(false);
  const [architectureStatusUpdating, setArchitectureStatusUpdating] = useState(false);

  useEffect(() => {
    setArchitectureStatusUpdating(false);
  }, [architecture.status]);

  const architectureDeleteModal = () => {
    const actions = (
      <Button.Group align="right" gutter="sm">
        <Button onClick={architectureDeleteConfirm}>
          Yes, Delete Architecture
        </Button>
        <Button onClick={architectureDeleteCancel} appearance="secondary">
          Cancel
        </Button>
      </Button.Group>
    );
    const deleteModalMesssage = 'Deleting this is a permanent action, and it can\'t be recovered.';

    return (
      <Modal
        title="Delete Architecture?"
        actions={actions}
        isOpened={architectureDeleteModalOpened}
        isProgressing={architectureDeleting}
      >
        {deleteModalMesssage}
      </Modal>
    );
  };

  const architectureDeleteModalShow = () => {
    setArchitectureDeleteModalOpened(true);
  };

  const architectureDeleteCancel = () => {
    setArchitectureDeleteModalOpened(false);
  };

  const architectureDeleteConfirm = () => {
    setArchitectureDeleting(true);
    onDelete();
  };

  const architectureSubmit = () => {
    setArchitectureStatusUpdating(true);
    onStatusChange(ARCHITECTURE_SUBMITTED);
  };

  const architectureRetract = () => {
    setArchitectureStatusUpdating(true);
    onStatusChange(ARCHITECTURE_DRAFT);
  };

  return (
    <div className="Architecture-actions">
      <Dropdown onOpen={onActionOpen} onClose={onActionClose}>
        {(() => {
          switch (architecture.status) {
            case ARCHITECTURE_DRAFT:
            case ARCHITECTURE_REJECTED:
              return (
                <Dropdown.Toggle>
                  <div className="Architecture-action">
                    <Button>
                      Submit
                      <IconCaretDown position="right" fill="#fff" />
                    </Button>
                  </div>
                </Dropdown.Toggle>
              );
            case ARCHITECTURE_SUBMITTED:
              return (
                <Dropdown.Toggle>
                  <div className="Architecture-action">
                    <Button>
                      Pending
                      <IconCaretDown position="right" fill="#fff" />
                    </Button>
                  </div>
                </Dropdown.Toggle>
              );
            case ARCHITECTURE_IN_REVIEW:
              return (
                <Dropdown.Toggle>
                  <div className="Architecture-action">
                    <Button readOnly>In Review</Button>
                  </div>
                </Dropdown.Toggle>
              );
            default:
              return (
                <div className="Architecture-action">
                  <Button readOnly>Published</Button>
                </div>
              );
          }
        })()}
        <Dropdown.Menu position="right">
          {architectureStatusUpdating && (
            <div className="Modal-progress">
              <Loader size={15} />
            </div>
          )}
          {(() => {
            switch (architecture.status) {
              case ARCHITECTURE_DRAFT:
              case ARCHITECTURE_REJECTED:
                return (
                  <div className="Architecture-action-content">
                    <ActionBox>
                      <ActionBox.Content>
                        <p>Submit this Architecture for review</p>
                      </ActionBox.Content>
                      <Button
                        onClick={architectureSubmit}
                        display="block"
                      >
                        Request review
                      </Button>
                    </ActionBox>
                  </div>
                );
              case ARCHITECTURE_SUBMITTED:
                return (
                  <div className="Architecture-action-content">
                    <ActionBox>
                      <ActionBox.Content>
                        <p>Retract this Architecture from review</p>
                      </ActionBox.Content>
                      <Button
                        onClick={architectureRetract}
                        display="block"
                      >
                        Remove from review
                      </Button>
                    </ActionBox>
                  </div>
                );
              default:
                return null;
            }
          })()}
        </Dropdown.Menu>
      </Dropdown>
      {architecture.status !== ARCHITECTURE_IN_REVIEW && (
        <Dropdown onOpen={onActionOpen} onClose={onActionClose}>
          <Dropdown.Toggle>
            <div className="Architecture-action">
              <Button iconOnly>
                <IconSettings fill="#fff" />
              </Button>
            </div>
          </Dropdown.Toggle>
          <Dropdown.Menu position="right">
            <div className="Architecture-action-content">
              <ActionList>
                <ActionList.Item onClick={architectureDeleteModalShow}>
                  Delete
                </ActionList.Item>
              </ActionList>
            </div>
          </Dropdown.Menu>
        </Dropdown>
      )}
      {architectureDeleteModal()}
    </div>
  );
};

ArchitectureToolbarActions.propTypes = {
  architecture: architecturePropType.isRequired,
  onActionClose: PropTypes.func,
  onActionOpen: PropTypes.func,
  onDelete: PropTypes.func.isRequired,
  onStatusChange: PropTypes.func.isRequired,
};

ArchitectureToolbarActions.defaultProps = {
  onActionClose: noop,
  onActionOpen: noop,
};

export default ArchitectureToolbarActions;
