import React from 'react';
import Button from 'components/Button/Button.jsx';

const ArchitectureToolbarButton = ({ ...props }) => <Button unstyled icon display="flex" {...props} />;

export default ArchitectureToolbarButton;
