import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { packagePropType } from 'proptypes';
import { IconBullet, IconCaretRight } from 'components/Icons/Icons.jsx';

class ArchitecturePackageIcon extends PureComponent {
  render() {
    const { childPackages, onClick } = this.props;
    return (
      <div className="Architecture-package-icon" onClick={onClick}>
        {childPackages && childPackages.length === 0 && (
          <IconBullet display="block" height={6} fill="#5d5c6b" />
        )}
        {childPackages && childPackages.length > 0 && (
          <IconCaretRight
            display="block"
            rotate={childPackages[0].show ? 90 : 0}
            fill="#5d5c6b"
          />
        )}
      </div>
    );
  }
}

ArchitecturePackageIcon.propTypes = {
  childPackages: PropTypes.arrayOf(packagePropType),
  onClick: PropTypes.func.isRequired,
};

ArchitecturePackageIcon.defaultProps = {
  childPackages: null,
};

export default ArchitecturePackageIcon;
