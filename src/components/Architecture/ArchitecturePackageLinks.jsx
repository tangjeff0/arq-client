import React from 'react';
import PropTypes from 'prop-types';
import { packagePropType } from 'proptypes';
import ArchitecturePackageTaxonomyLinkInterface from 'components/Architecture/ArchitecturePackageTaxonomyLinkInterface.jsx';
import ArchitecturePackageReferenceLink from 'components/Architecture/ArchitecturePackageReferenceLink.jsx';

const ArchitecturePackageLinks = ({
  pkg,
  editable,
  interactive,
  actionable,
  onPackageReferenceLinkDelete,
  architectureQID,
}) => (
  <div
    className="Architecture-package-links"
    data-term={pkg.term.replace(/\s/g, '\u00a0')}
  >
    {pkg.taxonomy && (
      <ArchitecturePackageTaxonomyLinkInterface
        pkg={pkg}
        architectureQID={architectureQID}
        editable={editable}
        interactive={interactive}
        actionable={actionable}
      />
    )}
    {pkg.referencePackage && (
      <ArchitecturePackageReferenceLink
        pkg={pkg}
        editable={editable}
        interactive={interactive}
        actionable={actionable}
        onPackageReferenceLinkDelete={onPackageReferenceLinkDelete}
      />
    )}
  </div>
);

ArchitecturePackageLinks.propTypes = {
  actionable: PropTypes.bool.isRequired,
  architectureQID: PropTypes.string.isRequired,
  editable: PropTypes.bool.isRequired,
  interactive: PropTypes.bool.isRequired,
  onPackageReferenceLinkDelete: PropTypes.func.isRequired,
  pkg: packagePropType.isRequired,
};

ArchitecturePackageLinks.defaultProps = {};

export default ArchitecturePackageLinks;
