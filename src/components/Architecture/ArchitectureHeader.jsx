import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import { noop } from 'lodash';
import CreateArchitecturePopup from 'components/OnboardingPopups/CreateArchitecturePopup.jsx';

class ArchitectureHeader extends Component {
  onFocus = () => {
    const { onTitleFocus } = this.props;
    onTitleFocus();
  }

  onClick = () => {
    const { onTitleClick } = this.props;
    onTitleClick();
  }

  onChange = (e) => {
    const title = e.target.value;
    const { onTitleChange } = this.props;
    onTitleChange(title);
  }

  onKeyDown = (e) => {
    const { onTitleKeyDown } = this.props;
    onTitleKeyDown(e);
  }

  render() {
    const {
      architecture,
      editable,
      enableOnboarding,
    } = this.props;

    return (
      <div className="Architecture-header">
        {enableOnboarding
          && architecture.packages.length === 1
          && architecture.packages[0].packages.length === 0 && (
          <CreateArchitecturePopup />
        )}
        <input
          className="Architecture-title"
          type="text"
          placeholder="Untitled"
          value={architecture.title || ''}
          onFocus={this.onFocus}
          onClick={this.onClick}
          onChange={this.onChange}
          onKeyDown={this.onKeyDown}
          readOnly={!editable}
        />
      </div>
    );
  }
}

ArchitectureHeader.propTypes = {
  architecture: architecturePropType.isRequired,
  editable: PropTypes.bool.isRequired,
  onTitleFocus: PropTypes.func,
  onTitleClick: PropTypes.func,
  onTitleChange: PropTypes.func,
  onTitleKeyDown: PropTypes.func,
  enableOnboarding: PropTypes.bool,
};

ArchitectureHeader.defaultProps = {
  onTitleFocus: noop,
  onTitleClick: noop,
  onTitleChange: noop,
  onTitleKeyDown: noop,
  enableOnboarding: false,
};

export default ArchitectureHeader;
