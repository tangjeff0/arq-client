import React, { useCallback } from 'react';
import { packagePropType } from 'proptypes';
import { get } from 'lodash';
import { useLazyQuery } from '@apollo/client';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  unlinkDocArchitecturePackageTaxonomy,
  revertDocArchitectureTaxonomy,
} from 'actions/activeDoc';
import { GET_TAXONOMY_DETAILS } from 'queries/taxonomies';
import { TAXONOMY_QUERY_ROOT } from 'constants/gql';
import ArchitecturePackageTaxonomyLink from 'components/Architecture/ArchitecturePackageTaxonomyLink.jsx';

const ArchitecturePackageTaxonomyLinkInterface = ({ pkg, ...props }) => {
  const [
    getTaxonomyDetails,
    { loading, error, data },
  ] = useLazyQuery(GET_TAXONOMY_DETAILS);

  const getTaxonomyTree = useCallback(
    id => getTaxonomyDetails({ variables: { id } }),
    [getTaxonomyDetails],
  );

  const taxonomy = get(data, [TAXONOMY_QUERY_ROOT]);

  return (
    <ArchitecturePackageTaxonomyLink
      pkg={pkg}
      getTaxonomyTree={getTaxonomyTree}
      loading={loading}
      error={error}
      taxonomy={taxonomy}
      {...props}
    />
  );
};

ArchitecturePackageTaxonomyLinkInterface.propTypes = {
  pkg: packagePropType.isRequired,
};

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      unlinkDocArchitecturePackageTaxonomy,
      revertDocArchitectureTaxonomy,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ArchitecturePackageTaxonomyLinkInterface,
);
