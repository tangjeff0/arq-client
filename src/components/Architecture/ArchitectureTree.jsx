import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import { architecturePropType } from 'proptypes';
import {
  packageFindPrevious,
  packageFindNext,
} from 'utils/tree';
import ArchitecturePackages from 'components/Architecture/ArchitecturePackages.jsx';

class ArchitectureTree extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shiftKeyDown: false, // @note: handled here to "keep shift pressed" during tree re-renders
      metaKeyDown: false, // For cmd down
    };
  }

  onKeyDown = (e) => {
    const { metaKeyDown } = this.state;
    switch (e.key) {
      case 'Shift':
        this.setState({ shiftKeyDown: true });
        break;
      case 'Meta':
        this.setState({ metaKeyDown: true });
        break;
      // For windows users
      case 'Control':
        this.setState({ metaKeyDown: true });
        break;
      case 'ArrowUp':
        if (!metaKeyDown) {
          const {
            architecture,
            activePackageQID,
            onUpdateActivePackageQID,
          } = this.props;
          const previousPackage = packageFindPrevious(
            architecture,
            activePackageQID,
          );
          if (previousPackage) {
            onUpdateActivePackageQID(previousPackage.id);
          }
        }
        break;
      case 'ArrowDown':
        if (!metaKeyDown) {
          const {
            architecture,
            activePackageQID,
            onUpdateActivePackageQID,
          } = this.props;
          const nextPackage = packageFindNext(architecture, activePackageQID);
          if (nextPackage) {
            onUpdateActivePackageQID(nextPackage.id);
          }
        }
        break;
      default:
        break;
    }
  }

  onKeyUp = (e) => {
    switch (e.key) {
      case 'Shift':
        this.setState({ shiftKeyDown: false });
        break;
      case 'Meta':
        this.setState({ metaKeyDown: false });
        break;
      /*
       * For windows users:
       *  the only edge case is if someone is trying to use both Control AND
       * Command and key ups on one but not the other.
       * */
      case 'Control':
        this.setState({ metaKeyDown: false });
        break;
      default:
        break;
    }
  }

  render() {
    const {
      architecture,
      editable,
      interactive,
      actionable,
      commentable,
      activePackageQID,
      activeCommentThreadPackageQID,
      onPackageFocus,
      onPackageClick,
      onPackageToggleComments,
      publishedView,
      renderPackageInterface,
    } = this.props;

    const { metaKeyDown, shiftKeyDown } = this.state;

    return (
      <div
        className="Architecture-tree"
        onKeyDownCapture={this.onKeyDown}
        onKeyUpCapture={this.onKeyUp}
      >
        <ArchitecturePackages
          packages={architecture.packages}
          editable={editable}
          interactive={interactive}
          actionable={actionable}
          architectureQID={architecture.id}
          commentable={commentable}
          activePackageQID={activePackageQID}
          activeCommentThreadPackageQID={activeCommentThreadPackageQID}
          onPackageFocus={onPackageFocus}
          onPackageClick={onPackageClick}
          onPackageToggleComments={onPackageToggleComments}
          onPackageInsertBefore={this.onPackageInsertBefore}
          onPackageInsertAfter={this.onPackageInsertAfter}
          shiftKeyDown={shiftKeyDown}
          metaKeyDown={metaKeyDown}
          publishedView={publishedView}
          renderPackageInterface={renderPackageInterface}
        />
      </div>
    );
  }
}

ArchitectureTree.propTypes = {
  actionable: PropTypes.bool,
  activeCommentThreadPackageQID: PropTypes.string,
  activePackageQID: PropTypes.string,
  architecture: architecturePropType.isRequired,
  commentable: PropTypes.bool,
  editable: PropTypes.bool,
  interactive: PropTypes.bool,
  onPackageClick: PropTypes.func,
  onPackageFocus: PropTypes.func,
  onPackageToggleComments: PropTypes.func,
  onUpdateActivePackageQID: PropTypes.func,
  publishedView: PropTypes.bool,
  renderPackageInterface: PropTypes.func,
};

ArchitectureTree.defaultProps = {
  actionable: false,
  activeCommentThreadPackageQID: '',
  activePackageQID: '',
  commentable: false,
  editable: false,
  interactive: false,
  onPackageClick: noop,
  onPackageFocus: noop,
  onPackageToggleComments: noop,
  onUpdateActivePackageQID: noop,
  publishedView: false,
  renderPackageInterface: null,
};

export default ArchitectureTree;
