import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Button from 'components/Button/Button.jsx';

import './ArchitecturePackageTool.scss';

class ArchitecturePackageTool extends PureComponent {
  render() {
    const { className, Icon, ...props } = this.props;
    return (
      <div className={cx('Architecture-package-tool-button', className)}>
        <Button display="block" unstyled {...props}>
          <Icon display="block" height={15} />
        </Button>
      </div>
    );
  }
}

ArchitecturePackageTool.propTypes = {
  Icon: PropTypes.func.isRequired,
  className: PropTypes.string,
};

ArchitecturePackageTool.defaultProps = {
  className: null,
};

export default ArchitecturePackageTool;
