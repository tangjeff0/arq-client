import React from 'react';
import PropTypes from 'prop-types';
import { IconSearch } from 'components/Icons/Icons.jsx';

import ArchitecturePackageTool from './ArchitecturePackageTool.jsx';

const ArchitecturePackageToolSearch = ({ onClick }) => (
  <ArchitecturePackageTool Icon={IconSearch} onClick={onClick} />
);

ArchitecturePackageToolSearch.propTypes = {
  onClick: PropTypes.func.isRequired,
};

ArchitecturePackageToolSearch.defaultProps = {};

export default ArchitecturePackageToolSearch;
