import React from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@apollo/client';
import Link from 'components/Link/Link.jsx';
import Loader from 'components/Loader/Loader.jsx';
import { getProblemShortname } from 'utils/problem';
import { GET_TAXONOMY_PROBLEMS_NAMES } from 'queries/taxonomies';
import { TAXONOMY_QUERY_ROOT } from 'constants/gql';

const ActivePackageProblemsList = ({ id }) => {
  const { loading, error, data } = useQuery(GET_TAXONOMY_PROBLEMS_NAMES, { variables: { id } });

  if (loading) return <Loader />;
  if (error) return <div>Error!</div>; // @todo: Error state

  const { problemSets } = data[TAXONOMY_QUERY_ROOT];

  if (!problemSets.length) return 'No related customer problems.';

  return (problemSets.map(problem => (
    <div key={problem.id}>
      <Link to={`/problems/${problem.id}`}>
        {getProblemShortname(problem, problem.customer)}
      </Link>
    </div>
  )));
};

ActivePackageProblemsList.propTypes = {
  id: PropTypes.string.isRequired,
};

ActivePackageProblemsList.defaultProps = {};

export default ActivePackageProblemsList;
