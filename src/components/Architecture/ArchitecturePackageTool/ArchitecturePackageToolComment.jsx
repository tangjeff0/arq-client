import React from 'react';
import PropTypes from 'prop-types';
import { IconComment } from 'components/Icons/Icons.jsx';

import ArchitecturePackageTool from './ArchitecturePackageTool.jsx';

const ArchitecturePackageToolComment = ({ onClick }) => (
  <ArchitecturePackageTool
    className="Architecture-package-tool-button--comment"
    Icon={IconComment}
    onClick={onClick}
  />
);

ArchitecturePackageToolComment.propTypes = {
  onClick: PropTypes.func.isRequired,
};

ArchitecturePackageToolComment.defaultProps = {};

export default ArchitecturePackageToolComment;
