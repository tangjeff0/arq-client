/* eslint-disable import/no-cycle */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { packagePropType } from 'proptypes';
import cx from 'classnames';
import { debounce, noop } from 'lodash';

import ArchitecturePackages from 'components/Architecture/ArchitecturePackages.jsx';
import ArchitecturePackageLinks from 'components/Architecture/ArchitecturePackageLinks.jsx';
import ArchitecturePackageWrapper from 'components/Architecture/ArchitecturePackageWrapper.jsx';
import ArchitecturePackageContent from 'components/Architecture/ArchitecturePackageContent.jsx';
import ArchitecturePackageIcon from 'components/Architecture/ArchitecturePackageIcon.jsx';

import ArchitecturePackageToolComment from './ArchitecturePackageTool/ArchitecturePackageToolComment.jsx';
import ArchitecturePackageToolExternalReference from './ArchitecturePackageTool/ArchitecturePackageToolExternalReference.jsx';
import ArchitecturePackageToolProblems from './ArchitecturePackageTool/ArchitecturePackageToolProblems.jsx';

class ArchitecturePackage extends PureComponent {
  debounceUpdateDocArchitecturePackageBE = debounce(() => {
    const { updateDocArchitecturePackageBE, pkg } = this.props;
    updateDocArchitecturePackageBE(pkg);
  }, 1000);

  componentDidMount() {
    const { pkg, activePackageQID } = this.props;

    if (pkg.id === activePackageQID) {
      this.focus();
    }
  }

  componentDidUpdate() {
    const { pkg, activePackageQID } = this.props;

    if (pkg.id === activePackageQID) {
      this.focus();
    }
  }

  focus = () => {
    this.input.focus();
  }

  onClick = () => {
    const { pkg, onPackageFocus, onPackageClick } = this.props;
    onPackageFocus(pkg.id);
    onPackageClick();
  }

  onSearchTermChange = (pkg, term, breakLink) => {
    const { updateDocArchitecturePackage } = this.props;

    const newPkg = pkg;

    newPkg.term = term;

    if (breakLink) {
      newPkg.linkBroken = true;
    }

    updateDocArchitecturePackage(newPkg);
    this.debounceUpdateDocArchitecturePackageBE();
  }

  onChange = (term) => {
    const { pkg } = this.props;
    const breakLink = pkg.taxonomy && !pkg.linkBroken;

    this.updatePackage(this.onSearchTermChange, pkg, term, breakLink);
  }

  onKeyDown = (e) => {
    const {
      pkg,
      architectureQID,
      metaKeyDown,
      shiftKeyDown,
      activeDocUILoading,
      packageToggleChildren,
      packageToggleChildrenByLevel,
      unlinkDocArchitecturePackageTaxonomy,
      undoArchitectureAction,
    } = this.props;

    switch (e.key) {
      case 'Enter':
        this.onEnterKey(e);
        break;
      case 'Backspace':
        this.onBackspaceKey(e);
        break;
      case 'ArrowDown':
        this.onArrowDownKey(e);
        break;
      case 'ArrowUp':
        this.onArrowUpKey(e);
        break;
      case 'Tab':
        this.onTabKey(e);
        break;
      case ',':
        if (metaKeyDown) {
          e.preventDefault();
          packageToggleChildren(architectureQID, pkg.id);
        }
        break;
      case '<':
        if (metaKeyDown && shiftKeyDown) {
          e.preventDefault();
          packageToggleChildrenByLevel(0);
        }
        break;
      case '!':
        if (metaKeyDown && shiftKeyDown) {
          e.preventDefault();
          packageToggleChildrenByLevel(1);
        }
        break;
      case '@':
        if (metaKeyDown && shiftKeyDown) {
          e.preventDefault();
          packageToggleChildrenByLevel(2);
        }
        break;
      case '#':
        if (metaKeyDown && shiftKeyDown) {
          e.preventDefault();
          packageToggleChildrenByLevel(3);
        }
        break;
      case '$':
        if (metaKeyDown && shiftKeyDown) {
          e.preventDefault();
          packageToggleChildrenByLevel(4);
        }
        break;
      case '%':
        if (metaKeyDown && shiftKeyDown) {
          e.preventDefault();
          packageToggleChildrenByLevel(5);
        }
        break;
      case 'u':
        if (metaKeyDown) {
          e.preventDefault();
          unlinkDocArchitecturePackageTaxonomy(pkg.id);
        }
        break;
      case 'z':
        // disable undo if doc is saving still
        if (metaKeyDown && !activeDocUILoading) {
          e.preventDefault();
          undoArchitectureAction();
        }
        break;
      default:
        break;
    }
  }

  onEnterKey = (e) => {
    e.preventDefault();

    const {
      pkg,
      shiftKeyDown,
      insertDocArchitecturePackageBefore,
      insertDocArchitecturePackageAfter,
    } = this.props;

    if (shiftKeyDown) {
      this.debounceUpdateDocArchitecturePackageBE.flush();
      this.updatePackage(insertDocArchitecturePackageBefore, pkg.id);
    } else {
      this.debounceUpdateDocArchitecturePackageBE.flush();
      this.updatePackage(insertDocArchitecturePackageAfter, pkg.id);
    }
  }

  onBackspaceKey = (e) => {
    const term = e.target.value;
    const { pkg, removeDocArchitecturePackage } = this.props;

    if (term.length === 0) {
      e.preventDefault();
      this.debounceUpdateDocArchitecturePackageBE.flush();
      this.updatePackage(removeDocArchitecturePackage, pkg.id);
    }
  }

  onTabKey = (e) => {
    e.preventDefault();
    const {
      shiftKeyDown,
      indentable,
      indentDocArchitecturePackage,
      unindentDocArchitecturePackage,
    } = this.props;

    if (shiftKeyDown) {
      this.debounceUpdateDocArchitecturePackageBE.flush();
      this.updatePackage(unindentDocArchitecturePackage);
    } else if (indentable) {
      this.debounceUpdateDocArchitecturePackageBE.flush();
      this.updatePackage(indentDocArchitecturePackage);
    }
  }

  onArrowDownKey = (e) => {
    e.preventDefault();
    const {
      canMoveDown,
      metaKeyDown,
      moveDocArchitecturePackageDown,
    } = this.props;
    if (metaKeyDown && canMoveDown) {
      this.debounceUpdateDocArchitecturePackageBE.flush();
      this.updatePackage(moveDocArchitecturePackageDown);
    }
  }

  onArrowUpKey = (e) => {
    e.preventDefault();
    const { canMoveUp, metaKeyDown, moveDocArchitecturePackageUp } = this.props;
    if (metaKeyDown && canMoveUp) {
      this.debounceUpdateDocArchitecturePackageBE.flush();
      this.updatePackage(moveDocArchitecturePackageUp);
    }
  }

  updatePackage = (callback, ...args) => {
    const { editable } = this.props;

    if (editable) {
      callback.apply(this, args);
    }
  }

  onIconClick = () => {
    const { architectureQID, pkg, packageToggleChildren } = this.props;
    packageToggleChildren(architectureQID, pkg.id);
  }

  onToggleComments = () => {
    const { pkg, onPackageFocus, onPackageToggleComments } = this.props;
    onPackageFocus(pkg.id);
    onPackageToggleComments(pkg);
  }

  render() {
    const {
      pkg,
      editable,
      interactive,
      actionable,
      commentable,
      activePackageQID,
      architectureQID,
      onPackageFocus,
      onPackageClick,
      unlinkDocArchitecturePackageReference,
      updateDocArchitecturePackageDescription,
      activeCommentThreadPackageQID,
      onPackageToggleComments,
      shiftKeyDown,
      metaKeyDown,
      depth,
      unsetDocActivePackageQID,
      setDocActivePackageQID,
      publishedView,
      commentCount,
      renderPackageInterface,
    } = this.props;

    return (
      <div className="Architecture-package">
        <ArchitecturePackageWrapper
          pkg={pkg}
          commentable={commentable}
          activeCommentThreadPackageQID={activeCommentThreadPackageQID}
          commentCount={commentCount}
        >
          <ArchitecturePackageContent>
            <ArchitecturePackageIcon
              childPackages={pkg.packages}
              onClick={this.onIconClick}
            />
            <div className="Architecture-package-input">
              <input
                ref={(c) => { this.input = c; }}
                className={cx('Architecture-package-text', {
                  'Architecture-package-text--references': pkg.referencePackage,
                  'Architecture-package-text--linked': pkg.taxonomy,
                  'Architecture-package-text--highlighted': pkg.highlighted,
                })}
                type="text"
                placeholder={
                  editable
                    ? 'Use search and link terms'
                    : null
                }
                value={pkg.term || ''}
                onClick={this.onClick}
                onChange={e => this.onChange(e.target.value)}
                onKeyDown={this.onKeyDown}
                readOnly={!editable}
              />
              {interactive && (
                <ArchitecturePackageLinks
                  pkg={pkg}
                  architectureQID={architectureQID}
                  editable={editable}
                  interactive={interactive}
                  actionable={actionable}
                  onPackageReferenceLinkDelete={
                    unlinkDocArchitecturePackageReference
                  }
                />
              )}
            </div>
          </ArchitecturePackageContent>
          {commentable
            && !publishedView && (
            <ArchitecturePackageToolComment onClick={this.onToggleComments} />
          )}
          {pkg.taxonomy
            && interactive && (
            <ArchitecturePackageToolProblems taxonomy={pkg.taxonomy} />
          )}
          {interactive && (
            <ArchitecturePackageToolExternalReference
              pkg={pkg}
              updateDocArchitecturePackageDescription={
                updateDocArchitecturePackageDescription
              }
              unsetDocActivePackageQID={unsetDocActivePackageQID}
              setDocActivePackageQID={setDocActivePackageQID}
              actionable={actionable}
              editable={editable}
            />
          )}
        </ArchitecturePackageWrapper>
        {pkg.packages && pkg.packages.length > 0 && (
          <ArchitecturePackages
            packages={pkg.packages}
            editable={editable}
            interactive={interactive}
            actionable={actionable}
            commentable={commentable}
            activePackageQID={activePackageQID}
            architectureQID={architectureQID}
            onPackageFocus={onPackageFocus}
            onPackageClick={onPackageClick}
            activeCommentThreadPackageQID={activeCommentThreadPackageQID}
            onPackageToggleComments={onPackageToggleComments}
            shiftKeyDown={shiftKeyDown}
            metaKeyDown={metaKeyDown}
            depth={depth + 1}
            unsetDocActivePackageQID={unsetDocActivePackageQID}
            renderPackageInterface={renderPackageInterface}
          />
        )}
      </div>
    );
  }
}

ArchitecturePackage.propTypes = {
  pkg: packagePropType.isRequired,
  editable: PropTypes.bool.isRequired,
  interactive: PropTypes.bool.isRequired,
  actionable: PropTypes.bool.isRequired,
  commentable: PropTypes.bool.isRequired,
  onPackageFocus: PropTypes.func.isRequired,
  onPackageClick: PropTypes.func.isRequired,
  shiftKeyDown: PropTypes.bool.isRequired,
  metaKeyDown: PropTypes.bool.isRequired,
  depth: PropTypes.number.isRequired,
  architectureQID: PropTypes.string.isRequired,
  activePackageQID: PropTypes.string,
  activeCommentThreadPackageQID: PropTypes.string,
  updateDocArchitecturePackageBE: PropTypes.func,
  updateDocArchitecturePackage: PropTypes.func,
  insertDocArchitecturePackageBefore: PropTypes.func,
  insertDocArchitecturePackageAfter: PropTypes.func,
  removeDocArchitecturePackage: PropTypes.func,
  indentable: PropTypes.bool.isRequired,
  indentDocArchitecturePackage: PropTypes.func,
  unindentDocArchitecturePackage: PropTypes.func,
  activeDocUILoading: PropTypes.bool,
  packageToggleChildren: PropTypes.func,
  packageToggleChildrenByLevel: PropTypes.func,
  unlinkDocArchitecturePackageTaxonomy: PropTypes.func,
  canMoveUp: PropTypes.bool.isRequired,
  moveDocArchitecturePackageUp: PropTypes.func,
  canMoveDown: PropTypes.bool.isRequired,
  moveDocArchitecturePackageDown: PropTypes.func,
  onPackageToggleComments: PropTypes.func,
  unlinkDocArchitecturePackageReference: PropTypes.func,
  updateDocArchitecturePackageDescription: PropTypes.func,
  setDocActivePackageQID: PropTypes.func,
  unsetDocActivePackageQID: PropTypes.func,
  publishedView: PropTypes.bool,
  undoArchitectureAction: PropTypes.func,
  commentCount: PropTypes.number,
  renderPackageInterface: PropTypes.func,
};

ArchitecturePackage.defaultProps = {
  activePackageQID: null,
  activeCommentThreadPackageQID: null,
  publishedView: false,
  // package edits
  updateDocArchitecturePackageBE: noop,
  updateDocArchitecturePackage: noop,
  insertDocArchitecturePackageBefore: noop,
  insertDocArchitecturePackageAfter: noop,
  removeDocArchitecturePackage: noop,
  indentDocArchitecturePackage: noop,
  unindentDocArchitecturePackage: noop,
  activeDocUILoading: false,
  packageToggleChildren: noop,
  packageToggleChildrenByLevel: noop,
  unlinkDocArchitecturePackageTaxonomy: noop,
  moveDocArchitecturePackageUp: noop,
  moveDocArchitecturePackageDown: noop,
  onPackageToggleComments: noop,
  unlinkDocArchitecturePackageReference: noop,
  updateDocArchitecturePackageDescription: noop,
  setDocActivePackageQID: noop,
  unsetDocActivePackageQID: noop,
  undoArchitectureAction: noop,
  commentCount: 0,
  renderPackageInterface: null,
};

export default ArchitecturePackage;
