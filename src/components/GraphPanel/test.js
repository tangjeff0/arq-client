import { TREE } from 'constants/graph';
import { taxonomy } from 'mocks/taxonomy';
import { setUp, checkProps } from 'utils/testing';
import GraphPanel from 'components/GraphPanel/GraphPanel.jsx';

describe('<GraphPanel />', () => {
  let wrapper;

  const expectedProps = {
    loading: false,
    resource: taxonomy,
    client: { query: () => {} },
    layout: TREE,
    setLayout: () => {},
  };

  beforeEach(() => {
    wrapper = setUp(GraphPanel, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(GraphPanel, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render a Panel', () => {
    expect(wrapper.is('Panel')).toBe(true);
  });

  it('should render .GraphPanel-content', () => {
    expect(wrapper.exists('.GraphPanel-content')).toBe(true);
  });
});
