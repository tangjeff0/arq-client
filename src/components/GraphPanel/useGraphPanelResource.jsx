import { useQuery } from '@apollo/client';
import { get } from 'lodash';

const useGraphPanelResource = ({ query, id, type }) => {
  const {
    loading,
    error,
    data,
    client,
  } = useQuery(query, {
    variables: { id },
    fetchPolicy: 'cache-and-network',
  });

  const resource = get(data, `${type}`, {});

  return {
    client,
    resource,
    error,
    loading,
  };
};

export default useGraphPanelResource;
