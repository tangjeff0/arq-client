import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useApolloClient } from '@apollo/client';
import { queryPropType } from 'proptypes';
import GraphPanel from 'components/GraphPanel/GraphPanel.jsx';
import {
  COMPANY_TYPENAME,
  CUSTOMER_TYPENAME,
  PROBLEM_TYPENAME,
  TAXONOMY_TYPENAME,
} from 'constants/gql';
import { GRID } from 'constants/graph';
import useGraphPanelResource from 'components/GraphPanel/useGraphPanelResource.jsx';

const GraphPanelInterface = ({
  id,
  query,
  type,
}) => {
  const {
    resource,
    error,
    loading,
  } = useGraphPanelResource({ query, id, type });

  const client = useApolloClient();

  const [layout, setLayout] = useState(GRID);

  let fullscreenLink;

  const { __typename: typename } = resource;

  switch (typename) {
    case COMPANY_TYPENAME:
      fullscreenLink = `/companies/${id}/graph`;
      break;
    case PROBLEM_TYPENAME:
      fullscreenLink = `/problems/${id}/graph`;
      break;
    case TAXONOMY_TYPENAME:
      fullscreenLink = `/taxonomies/${id}/graph`;
      break;
    case CUSTOMER_TYPENAME:
      fullscreenLink = `/customers/${id}/graph`;
      break;
    default:
      fullscreenLink = '/';
      break;
  }

  return (
    <GraphPanel
      loading={loading}
      error={error}
      resource={resource}
      client={client}
      fullscreenLink={fullscreenLink}
      layout={layout}
      setLayout={setLayout}
    />
  );
};

GraphPanelInterface.propTypes = {
  id: PropTypes.string.isRequired,
  query: queryPropType.isRequired,
  type: PropTypes.string.isRequired,
};

export default GraphPanelInterface;
