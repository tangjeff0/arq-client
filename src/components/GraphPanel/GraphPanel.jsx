import React from 'react';
import PropTypes from 'prop-types';
import Heading from 'components/Heading/Heading.jsx';
import { apolloErrorPropType, apolloClientPropType } from 'proptypes';
import Loader from 'components/Loader/Loader.jsx';
import Graph from 'components/Graph/Graph.jsx';
import Panel from 'components/Panel/Panel.jsx';
import Link from 'components/Link/Link.jsx';

const GraphPanel = ({
  loading,
  error,
  resource,
  client,
  fullscreenLink,
  layout,
  setLayout,
}) => (
  <Panel>
    <Panel.Heading>
      <Heading level={1}>Graph</Heading>
      <Link to={fullscreenLink}>
        View Larger
      </Link>
    </Panel.Heading>
    <div className="GraphPanel-content">
      {loading && <Loader isCentered size={24} />}
      {!loading && error && 'Error!'}
      {!loading && resource && (
        <Graph
          item={resource}
          client={client}
          layout={layout}
          setLayout={setLayout}
        />
      )}
    </div>
  </Panel>
);

GraphPanel.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: apolloErrorPropType,
  resource: PropTypes.shape({}),
  client: apolloClientPropType.isRequired,
  fullscreenLink: PropTypes.string,
  layout: PropTypes.string.isRequired,
  setLayout: PropTypes.func.isRequired,
};

GraphPanel.defaultProps = {
  error: null,
  resource: null,
  fullscreenLink: null,
};

export default GraphPanel;
