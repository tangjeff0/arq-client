import { setUp, checkProps } from 'utils/testing';
import { user } from 'mocks/users';
import ToggleUserModal from 'components/ToggleUserModal/ToggleUserModal.jsx';

describe('<ToggleUserModal />', () => {
  let wrapper;

  const expectedProps = {
    user,
    users: [],
    toggleUserAction: () => {},
  };

  beforeEach(() => {
    wrapper = setUp(ToggleUserModal, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ToggleUserModal, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render without error', () => {
    expect(wrapper.exists('Modal')).toBe(true);
  });
});
