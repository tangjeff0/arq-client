import React from 'react';
import PropTypes from 'prop-types';
import WelcomeBack from 'components/WelcomeBack/WelcomeBack.jsx';
import useUser from 'hooks/useUser.jsx';
import { toUserName } from 'utils/user';

const WelcomeBackInterface = ({ appearance }) => {
  const { user } = useUser();
  const username = toUserName(user.email || null);

  return (
    <WelcomeBack
      appearance={appearance}
      username={username}
    />
  );
};

WelcomeBackInterface.propTypes = {
  appearance: PropTypes.oneOf(['dark', 'light']),
};

WelcomeBackInterface.defaultProps = {
  appearance: 'light',
};

export default WelcomeBackInterface;
