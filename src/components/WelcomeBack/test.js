import React from 'react';
import WelcomeBack from './WelcomeBack.jsx';

describe('<WelcomeBack />', () => {
  const user = 'nick';
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<WelcomeBack username={user} />);
  });

  it('should render with .WelcomeBack class', () => {
    expect(wrapper.is('.WelcomeBack')).toBe(true);
  });

  it('accepts username and appearance props', () => {
    const shade = 'dark';
    const wrapperMount = mount(<WelcomeBack username={user} appearance={shade} />);
    expect(wrapperMount.props().username).toEqual(user);
    expect(wrapperMount.props().appearance).toEqual(shade);
  });

  it('should render the <IconSun /> component', () => {
    expect(wrapper.find('IconSun').length).toEqual(1);
  });

  it('should render with .WelcomeBack-text class', () => {
    const welcome = wrapper.find('.WelcomeBack-text');
    expect(welcome.length).toEqual(1);
  });

  it('should render a username when username passed in', () => {
    expect(wrapper.text()).toEqual(`<IconSun />Welcome back,${user}`);
  });
});
