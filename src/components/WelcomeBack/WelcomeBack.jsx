import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { IconSun } from 'components/Icons/Icons.jsx';
import 'components/WelcomeBack/WelcomeBack.scss';

const WelcomeBack = ({ appearance, username }) => (
  <div className={cx('WelcomeBack', `WelcomeBack--appearance-${appearance}`)}>
    <span className="WelcomeBack-icon">
      <IconSun
        display="block"
        height={21}
        fill={appearance === 'light' ? '#fff' : '#3a3945'}
      />
    </span>
    <div className="WelcomeBack-text">
      Welcome back,
      <br />
      {username}
    </div>
  </div>
);

WelcomeBack.propTypes = {
  appearance: PropTypes.oneOf(['dark', 'light']),
  username: PropTypes.string.isRequired,
};

WelcomeBack.defaultProps = {
  appearance: 'light',
};

export default WelcomeBack;
