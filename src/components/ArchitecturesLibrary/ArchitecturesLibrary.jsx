import React from 'react';
import PropTypes from 'prop-types';
import { apolloErrorPropType, viewportPropType } from 'proptypes';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Alert from 'components/Alert/Alert.jsx';
import ArchitecturesLibraryContent from 'components/ArchitecturesLibrary/ArchitecturesLibraryContent.jsx';
import 'components/ArchitecturesLibrary/ArchitecturesLibrary.scss';

const ArchitecturesLibrary = ({
  loading,
  error,
  publishedArchitectures,
  viewport,
}) => (
  <div className="ArchitecturesLibrary">
    <Dashboard>
      <Dashboard.Header />
      <Dashboard.Section fit>
        <Dashboard.Column padded minWidth="sm" size="sm" theme="dark">
          <Grid direction="column" justify="spaceBetween">
            <SidebarNavigationInterface active="published" />
            <Dashboard.Footer>
              <WelcomeBackInterface />
            </Dashboard.Footer>
          </Grid>
        </Dashboard.Column>
        <Dashboard.Column fit theme="light">
          <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
            {loading && <Loader size={36} isCentered isHeight100 />}
            {!loading && error && (
              <Alert
                heading="Uh Oh..."
                subheading="There was an error fetching architectures."
              />
            )}
            {!loading && !error && publishedArchitectures && (
              <ArchitecturesLibraryContent
                publishedArchitectures={publishedArchitectures}
                column={viewport.desktopSm}
              />
            )}
          </Dashboard.Content>
        </Dashboard.Column>
      </Dashboard.Section>
    </Dashboard>
  </div>
);

ArchitecturesLibrary.propTypes = {
  publishedArchitectures: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  error: apolloErrorPropType,
  loading: PropTypes.bool,
  viewport: viewportPropType,
};

ArchitecturesLibrary.defaultProps = {
  error: null,
  loading: false,
  viewport: {},
};

export default ArchitecturesLibrary;
