import React from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import LibraryCard from 'components/LibraryCard/LibraryCard.jsx';

const ArchitecturesLibraryCard = ({ architecture, fullWidth }) => (
  <LibraryCard
    item={architecture}
    link={`/published_architectures/${architecture.id}`}
    previewArchitecture={architecture}
    author={architecture.author}
    createdAt={architecture.createdAt}
    publishedAt={architecture.publishedAt}
    fullWidth={fullWidth}
  />
);

ArchitecturesLibraryCard.propTypes = {
  architecture: architecturePropType.isRequired,
  fullWidth: PropTypes.bool,
};

ArchitecturesLibraryCard.defaultProps = {
  fullWidth: false,
};

export default ArchitecturesLibraryCard;
