import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { documentPropType } from 'proptypes';
import { isArchitectureBlock } from 'utils/blocks';
import { now } from 'utils/time';
import { SHARED_DOC } from 'constants/doctypes.js';
import {
  ARCHITECTURE_DRAFT,
  ARCHITECTURE_SUBMITTED,
  ARCHITECTURE_IN_REVIEW,
  ARCHITECTURE_PUBLISHED,
} from 'constants/architectureStates.js';
import { INVITATION_UNSEEN } from 'constants/invitationStates';

import Button from 'components/Button/Button.jsx';
import Modal from 'components/Modal/Modal.jsx';
import LibraryCard from 'components/LibraryCard/LibraryCard.jsx';

class DocumentsLibraryCard extends Component {
  constructor(props) {
    super(props);

    const { doc } = this.props;

    this.previewBlock = {};
    const architecturesStatus = {};

    doc.blocks.forEach((block) => {
      if (isArchitectureBlock(block)) {
        if (!this.previewBlock.id) {
          this.previewBlock = block;
        }

        if (block.architecture) {
          switch (block.architecture.status) {
            case ARCHITECTURE_DRAFT:
              if (architecturesStatus.draft) {
                architecturesStatus.draft += 1;
              } else {
                architecturesStatus.draft = 1;
              }
              break;
            case ARCHITECTURE_SUBMITTED:
              if (architecturesStatus.pending) {
                architecturesStatus.pending += 1;
              } else {
                architecturesStatus.pending = 1;
              }
              break;
            case ARCHITECTURE_IN_REVIEW:
              if (architecturesStatus.pending) {
                architecturesStatus.pending += 1;
              } else {
                architecturesStatus.pending = 1;
              }
              break;
            case ARCHITECTURE_PUBLISHED:
              if (architecturesStatus.published) {
                architecturesStatus.published += 1;
              } else {
                architecturesStatus.published = 1;
              }
              break;
            default:
              break;
          }
        }
      }
    });

    // Create aggregated architecture status display
    this.architecturesStatusString = '';

    // Each status display has leading , and space
    Object.keys(architecturesStatus).forEach((key) => {
      this.architecturesStatusString += `, ${key}: ${architecturesStatus[key]}`;
    });

    // Remove the leading , and space for first status
    this.architecturesStatusString = this.architecturesStatusString.substr(1);

    // If no architectures, set to a default
    if (this.architecturesStatusString === '') {
      this.architecturesStatusString = 'no architectures';
    }

    this.state = {
      documentArchiveModalOpen: false,
      documentArchiving: false,
    };
  }

  renderDocumentArchiveModal = () => {
    const { documentArchiveModalOpen, documentArchiving } = this.state;

    const actions = (
      <Button.Group align="right" gutter="sm">
        <Button onClick={this.documentArchiveConfirm}>
          Yes, Delete Document
        </Button>
        <Button onClick={this.documentArchiveCancel} appearance="secondary">
          Cancel
        </Button>
      </Button.Group>
    );

    return (
      <Modal
        title="Delete Document?"
        actions={actions}
        isOpened={documentArchiveModalOpen}
        isProgressing={documentArchiving}
      >
        Deleting this is a permanent action, and it can&apos;t be recovered.
      </Modal>
    );
  }

  onArchiveClick = () => {
    this.setState({ documentArchiveModalOpen: true });
  }

  documentArchiveConfirm = async () => {
    const { doc, archiveDoc, refetch } = this.props;

    this.setState({ documentArchiving: true });
    await archiveDoc({
      variables: {
        id: doc.id,
        deletedAt: now(),
      },
    });
    refetch();
  }

  documentArchiveCancel = () => {
    this.setState({ documentArchiveModalOpen: false });
  }

  render() {
    const { doc, doctype, fullWidth } = this.props;

    const link = (() => {
      switch (doctype) {
        case SHARED_DOC:
          return `/shared/${doc.id}`;
        default:
          return `/documents/${doc.id}`;
      }
    })();

    const isNew = doctype === SHARED_DOC && doc.invitation.status === INVITATION_UNSEEN;
    const deletable = doctype !== SHARED_DOC;

    return (
      <LibraryCard
        link={link}
        item={doc}
        statusString={this.architecturesStatusString}
        previewArchitecture={this.previewBlock.architecture}
        onArchiveClick={deletable ? this.onArchiveClick : null}
        renderArchiveModal={deletable ? this.renderDocumentArchiveModal : null}
        createdAt={doc.createdAt}
        updatedAt={doc.updatedAt}
        isNew={isNew}
        fullWidth={fullWidth}
      />
    );
  }
}

DocumentsLibraryCard.propTypes = {
  doc: documentPropType.isRequired,
  doctype: PropTypes.string.isRequired,
  archiveDoc: PropTypes.func,
  refetch: PropTypes.func.isRequired,
  fullWidth: PropTypes.bool,
};

DocumentsLibraryCard.defaultProps = {
  archiveDoc: null,
  fullWidth: false,
};

export default DocumentsLibraryCard;
