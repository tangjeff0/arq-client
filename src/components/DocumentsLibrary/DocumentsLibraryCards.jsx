import React from 'react';
import PropTypes from 'prop-types';
import { documentPropType } from 'proptypes';
import DocumentsLibraryCard from 'components/DocumentsLibrary//DocumentsLibraryCard.jsx';

const DocumentsLibraryCards = ({
  docs,
  doctype,
  archiveDoc,
  refetch,
  column,
}) => (
  <div className="DocumentsLibrary-cards">
    {docs.map(doc => (
      <DocumentsLibraryCard
        key={doc.id}
        doc={doc}
        doctype={doctype}
        archiveDoc={archiveDoc}
        refetch={refetch}
        fullWidth={column}
      />
    ))}
  </div>
);

DocumentsLibraryCards.propTypes = {
  docs: PropTypes.arrayOf(documentPropType).isRequired,
  doctype: PropTypes.string.isRequired,
  archiveDoc: PropTypes.func,
  refetch: PropTypes.func.isRequired,
  column: PropTypes.bool,
};

DocumentsLibraryCards.defaultProps = {
  archiveDoc: null,
  column: false,
};

export default DocumentsLibraryCards;
