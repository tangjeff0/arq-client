import React from 'react';
import Button from 'components/Button/Button.jsx';
import { onboardingModuleShownVar } from 'apollo/cache';

const DocumentsLibraryLearnMoreButton = () => (
  <Button
    onClick={() => onboardingModuleShownVar(false)}
  >
    Learn How
  </Button>
);

export default DocumentsLibraryLearnMoreButton;
