import React, { PureComponent } from 'react';
import { IconEmptyDocument } from 'components/Icons/Icons.jsx';
import DocumentsLibraryLearnMoreButton from 'components/DocumentsLibrary/DocumentsLibraryLearnMoreButton.jsx';

class DocumentsLibraryEmptyState extends PureComponent {
  render() {
    return (
      <div className="DocumentsLibrary-empty">
        <IconEmptyDocument height={100} fill="#6a71d7" />
        <span className="DocumentsLibrary-empty-header">Create a Document</span>
        <p className="DocumentsLibrary-empty-description">
          ArQ is a tool to help you and your colleagues collaborate by utilizing
          common terminology from trusted soruces as well as create and share
          architectural outlines
        </p>
        <DocumentsLibraryLearnMoreButton />
      </div>
    );
  }
}

DocumentsLibraryEmptyState.propTypes = {};

DocumentsLibraryEmptyState.defaultProps = {};

export default DocumentsLibraryEmptyState;
