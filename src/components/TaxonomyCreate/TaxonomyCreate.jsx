import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { buildNewTermTree } from 'utils/tree';
import Textbox from 'components/Textbox/Textbox.jsx';
import Button from 'components/Button/Button.jsx';
import 'components/TaxonomyCreate/TaxonomyCreate.scss';

const TaxonomyCreate = ({ onApprove }) => {
  const [hierarchyInputValue, setHierarchyInputValue] = useState(null);

  const setTreeNotation = (queryString) => {
    const terms = queryString
      .trim()
      .split('>')
      .map(str => str.trim())
      .filter(str => str.length);

    return buildNewTermTree(terms);
  };

  const hierarchyInputSubmit = () => {
    if (hierarchyInputValue.trim().length) {
      onApprove(setTreeNotation(hierarchyInputValue));
    }
  };

  return (
    <div className="TaxonomyCreate">
      <Textbox
        appearance="secondary"
        placeholder="Term 1 > Term 2 > Term 3"
        onEnter={hierarchyInputSubmit}
        onChange={value => setHierarchyInputValue(value)}
      />
      <Button
        display="block"
        onClick={hierarchyInputSubmit}
        readOnly={!hierarchyInputValue}
      >
        Accept
      </Button>
    </div>
  );
};

TaxonomyCreate.propTypes = {
  onApprove: PropTypes.func.isRequired,
};

export default TaxonomyCreate;
