import { elasticResult } from 'mocks/elastic';
import { setUp, checkProps } from 'utils/testing';
import SearchResults from 'components/SearchResults/SearchResults.jsx';
import SearchResult from 'components/SearchResults/SearchResult.jsx';

describe('<SearchResults />', () => {
  let wrapper;

  const expectedProps = {
    hits: elasticResult.hits.hits,
    actions: {},
  };

  beforeEach(() => {
    wrapper = setUp(SearchResults, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(SearchResults, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .SearchResults class', () => {
    expect(wrapper.is('.SearchResults')).toBe(true);
  });
});

describe('<SearchResult />', () => {
  let wrapper;

  const expectedProps = {
    taxonomy: elasticResult.hits.hits[0]._source, // eslint-disable-line
    actions: {},
  };

  beforeEach(() => {
    wrapper = setUp(SearchResult, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(SearchResult, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .SearchResult class', () => {
    expect(wrapper.is('.SearchResult')).toBe(true);
  });
});
