import React from 'react';
import PropTypes from 'prop-types';
import { esSearchResultPropType, treeItemActionPropType } from 'proptypes';
import SearchResult from 'components/SearchResults/SearchResult.jsx';

const SearchResults = ({ hits, actions, hoverAction }) => (
  <div className="SearchResults">
    {hits.map(({ _source: taxonomy }) => (
      <SearchResult
        key={taxonomy.id}
        taxonomy={taxonomy}
        actions={actions}
        hoverAction={hoverAction}
      />
    ))}
  </div>
);

SearchResults.propTypes = {
  hits: PropTypes.arrayOf(esSearchResultPropType).isRequired,
  actions: PropTypes.shape({
    primary: PropTypes.arrayOf(treeItemActionPropType),
  }).isRequired,
  hoverAction: treeItemActionPropType,
};

SearchResults.defaultProps = {
  hoverAction: null,
};

export default SearchResults;
