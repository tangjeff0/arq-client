import React from 'react';
import PropTypes from 'prop-types';
import 'components/Heading/Heading.scss';

function Heading({ children, level }) {
  switch (level) {
    case 1:
      return <h1 className="Heading Heading--level-1">{children}</h1>;
    case 2:
      return <h2 className="Heading Heading--level-2">{children}</h2>;
    case 3:
      return <h3 className="Heading Heading--level-3">{children}</h3>;
    case 4:
      return <h4 className="Heading Heading--level-4">{children}</h4>;
    case 5:
      return <h5 className="Heading Heading--level-5">{children}</h5>;
    case 6:
      return <h6 className="Heading Heading--level-6">{children}</h6>;
    default:
      return <h1 className="Heading Heading--level-1">{children}</h1>;
  }
}

Heading.propTypes = {
  children: PropTypes.node,
  level: PropTypes.number,
};

Heading.defaultProps = {
  children: null,
  level: 1,
};

export default Heading;
