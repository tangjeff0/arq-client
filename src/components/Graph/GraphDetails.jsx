import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import Button from 'components/Button/Button.jsx';
import ActionBox from 'components/ActionBox/ActionBox.jsx';
import GraphDetailsTaxonomy from 'components/Graph/GraphDetailsTaxonomy.jsx';
import GraphDetailsProblem from 'components/Graph/GraphDetailsProblem.jsx';
import GraphDetailsCompany from 'components/Graph/GraphDetailsCompany.jsx';
import GraphDetailsArchitecture from 'components/Graph/GraphDetailsArchitecture.jsx';
import GraphDetailsPackage from 'components/Graph/GraphDetailsPackage.jsx';
import GraphDetailsCustomer from 'components/Graph/GraphDetailsCustomer.jsx';

import {
  TAXONOMY_TYPENAME,
  PROBLEM_TYPENAME,
  COMPANY_TYPENAME,
  ARCHITECTURE_TYPENAME,
  PACKAGE_TYPENAME,
  CUSTOMER_TYPENAME,
} from 'constants/gql';

class GraphDetails extends Component {
  constructor(props) {
    super(props);

    const { node } = this.props;

    this.state = {
      interactive: node.hasClass('interactive'),
    };
  }

  componentDidUpdate(prevProps) {
    const { node: prevNode } = prevProps;
    const { node } = this.props;

    if (node.id() !== prevNode.id()) {
      this.onUpdateNode(node);
    }
  }

  onUpdateNode = (node) => {
    this.setState({ interactive: node.hasClass('interactive') });
  }

  handleExpandClick = () => {
    const { onExpandClick, node } = this.props;
    onExpandClick(node);

    this.setState({ interactive: false });
  }

  render() {
    const { node } = this.props;
    const { interactive } = this.state;

    const elementData = node.data();
    const { content, entity, label } = elementData;

    let NodeDetails;
    switch (label) {
      case TAXONOMY_TYPENAME:
        NodeDetails = GraphDetailsTaxonomy;
        break;
      case PROBLEM_TYPENAME:
        NodeDetails = GraphDetailsProblem;
        break;
      case COMPANY_TYPENAME:
        NodeDetails = GraphDetailsCompany;
        break;
      case ARCHITECTURE_TYPENAME:
        NodeDetails = GraphDetailsArchitecture;
        break;
      case PACKAGE_TYPENAME:
        NodeDetails = GraphDetailsPackage;
        break;
      case CUSTOMER_TYPENAME:
        NodeDetails = GraphDetailsCustomer;
        break;
      default:
        break;
    }

    return (
      <div className="Graph-nodeDetails">
        <ActionBox>
          <ActionBox.Content>
            {NodeDetails && <NodeDetails content={content} entity={entity} />}
            {!NodeDetails && (
              <div className="Graph-nodeDetails-content">{content}</div>
            )}
          </ActionBox.Content>
          <Button
            display="block"
            onClick={this.handleExpandClick}
            readOnly={!interactive}
          >
            Expand
          </Button>
        </ActionBox>
      </div>
    );
  }
}

GraphDetails.propTypes = {
  onExpandClick: PropTypes.func,
  node: PropTypes.shape({
    data: PropTypes.func,
    id: PropTypes.func,
    hasClass: PropTypes.func,
  }).isRequired,
};

GraphDetails.defaultProps = {
  onExpandClick: noop,
};

export default GraphDetails;
