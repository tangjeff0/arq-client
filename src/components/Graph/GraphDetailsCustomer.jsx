import React from 'react';
import Link from 'components/Link/Link.jsx';
import GraphDetailsType from 'components/Graph/GraphDetailsType.jsx';
import { customerPropType } from 'proptypes';

const GraphDetailsCustomer = ({ entity }) => (
  <>
    <GraphDetailsType>
      Customer
    </GraphDetailsType>
    <div className="Graph-nodeDetails-content">
      <div className="Graph-nodeDetails-content-title">
        <Link to={`/customers/${entity.id}`}>
          {entity.name}
        </Link>
      </div>
    </div>
  </>
);

GraphDetailsCustomer.propTypes = {
  entity: customerPropType.isRequired,
};

export default GraphDetailsCustomer;
