import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import noop from 'lodash';
import 'components/Textbox/Textbox.scss';

class Textbox extends Component {
  constructor(props) {
    super(props);

    const { value } = this.props;

    this.state = {
      value,
    };
  }

  onChange = (e) => {
    const { value } = e.target;
    const { onChange } = this.props;
    this.setState({ value }, () => onChange(value));
  }

  onKeyDown = (e) => {
    const { onEnter } = this.props;
    const { value } = this.state;

    if (e.key === 'Enter') {
      e.preventDefault();
      onEnter(value);
    }
  }

  render() {
    const { appearance, placeholder } = this.props;
    const { value } = this.state;

    return (
      <textarea
        rows="3"
        className={cx('Textbox', {
          [`Textbox--appearance-${appearance}`]: appearance,
        })}
        placeholder={placeholder}
        value={value}
        onChange={this.onChange}
        onKeyDown={this.onKeyDown}
      />
    );
  }
}

Textbox.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  onEnter: PropTypes.func,
};

Textbox.defaultProps = {
  value: '',
  onChange: noop,
  onEnter: noop,
};

export default Textbox;
