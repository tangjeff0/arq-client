import React, { useState } from 'react';
import PropTypes from 'prop-types';
import SearchBar from 'components/SearchBar/SearchBar.jsx';
import TaxonomySearchResults from 'components/TaxonomySearch/TaxonomySearchResults.jsx';
import 'components/TaxonomySearch/TaxonomySearch.scss';

const TaxonomySearch = ({ onSearch, hits, searching }) => {
  const [resultsOpen, setResultsOpen] = useState(false);
  const [value, setValue] = useState('');

  const onChange = (e) => {
    const val = e.target.value;
    setValue(val);
    onSearch(val);
    setResultsOpen(true);
  };

  return (
    <div className="TaxonomySearch">
      <SearchBar
        value={value}
        onChange={onChange}
        placeholder="Search taxonomies"
        searching={searching}
      />
      {resultsOpen && (
        <TaxonomySearchResults
          hits={hits}
          handleClickOutside={() => setResultsOpen(false)}
        />
      )}
    </div>
  );
};

TaxonomySearch.propTypes = {
  onSearch: PropTypes.func,
  hits: PropTypes.arrayOf(PropTypes.shape({})),
  searching: PropTypes.bool,
};

TaxonomySearch.defaultProps = {
  onSearch: () => {},
  hits: [],
  searching: false,
};

export default TaxonomySearch;
