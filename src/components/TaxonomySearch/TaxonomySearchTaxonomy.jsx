import React from 'react';
import { taxonomyPropType } from 'proptypes';
import { sourceNames } from 'constants/sources';
import { orderAncestorsByParentId } from 'utils/elastic';
import Link from 'components/Link/Link.jsx';

const TaxonomySearchTaxonomy = ({ taxonomy }) => {
  const {
    id,
    term,
    ancestors,
    source_name: sourceName,
  } = taxonomy;

  const orderedAncestors = orderAncestorsByParentId(ancestors);

  return (
    <div
      key={taxonomy.id}
      className="TaxonomySearch-taxonomy"
    >
      <Link to={`/taxonomies/${id}`}>
        <span className="TaxonomySearch-taxonomy-breadcrumb">{`${sourceNames[sourceName]} > `}</span>
        {orderedAncestors.map((t, i) => {
          const ancestor = (i > 0 && i < orderedAncestors.length - 2) ? '...' : t.term;
          if (i + 1 === orderedAncestors.length) return null;
          return (
            <span className="TaxonomySearch-taxonomy-breadcrumb" key={t.id}>{`${ancestor} > `}</span>
          );
        })}
        {term}
      </Link>
    </div>
  );
};

TaxonomySearchTaxonomy.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
};

export default TaxonomySearchTaxonomy;
