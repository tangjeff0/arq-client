import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const DashboardColumn = ({
  children,
  fit,
  size,
  offsetLeft,
  padded,
  minWidth,
  theme,
  scroll,
}) => (
  <div
    className={cx('Dashboard-column', {
      'Dashboard-column--fit': fit,
      [`Dashboard-column--size-${size}`]: size,
      [`Dashboard-column--offsetLeft-${offsetLeft}`]: offsetLeft,
      'Dashboard-column--padded': padded,
      [`Dashboard-column--minWidth-${minWidth}`]: minWidth,
      [`Dashboard-column--theme-${theme}`]: theme,
      'Dashboard-column--scroll': scroll,
    })}
  >
    {children}
  </div>
);

DashboardColumn.propTypes = {
  children: PropTypes.node,
  fit: PropTypes.bool,
  size: PropTypes.string,
  offsetLeft: PropTypes.string,
  padded: PropTypes.bool,
  minWidth: PropTypes.string,
  theme: PropTypes.string,
  scroll: PropTypes.bool,
};

DashboardColumn.defaultProps = {
  children: null,
  fit: false,
  size: null,
  offsetLeft: null,
  padded: false,
  minWidth: null,
  theme: null,
  scroll: false,
};

export default DashboardColumn;
