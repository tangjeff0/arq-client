import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const DashboardToolbar = ({ children, padded, display }) => (
  <div
    className={cx('Dashboard-toolbar', {
      'Dashboard-toolbar--padded': padded,
      [`Dashboard-toolbar--display-${display}`]: display,
    })}
  >
    {children}
  </div>
);

DashboardToolbar.propTypes = {
  children: PropTypes.node,
  padded: PropTypes.bool,
  display: PropTypes.oneOf(['flex']),
};

DashboardToolbar.defaultProps = {
  children: null,
  padded: false,
  display: null,
};

export default DashboardToolbar;
