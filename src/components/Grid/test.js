import React from 'react';
import Grid from './Grid.jsx';

describe('<Grid />', () => {
  it('should render with .Grid class', () => {
    const wrapper = shallow(<Grid />);
    expect(wrapper.is('.Grid')).toBe(true);
  });
});
