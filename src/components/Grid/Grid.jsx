import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import GridCell from './GridCell.jsx';
import 'components/Grid/Grid.scss';

const Grid = ({
  children, display, wrap, direction, align, justify,
}) => (
  <div
    className={cx('Grid', {
      [`Grid--display-${display}`]: display,
      [`Grid--wrap-${wrap}`]: wrap,
      [`Grid--direction-${direction}`]: direction,
      [`Grid--align-${align}`]: align,
      [`Grid--justify-${justify}`]: justify,
    })}
  >
    {children}
  </div>
);

Grid.propTypes = {
  children: PropTypes.node,
  display: PropTypes.oneOf(['flex', 'inlineFlex']),
  wrap: PropTypes.oneOf(['nowrap', 'wrap', 'wrapReverse']),
  direction: PropTypes.oneOf(['row', 'rowReverse', 'column', 'columnReverse']),
  align: PropTypes.oneOf([
    'flexStart',
    'flexEnd',
    'center',
    'baseline',
    'stretch',
  ]),
  justify: PropTypes.oneOf([
    'center',
    'start',
    'end',
    'flexStart',
    'flexEnd',
    'left',
    'right',
    'baseline',
    'firstBaseline',
    'lastBaseline',
    'spaceBetween',
    'spaceAround',
    'spaceEvenly',
    'stretch',
    'safeCenter',
    'unsafeCenter',
  ]),
};

Grid.defaultProps = {
  children: null,
};

Grid.Cell = GridCell;

export default Grid;
