import React from 'react';
import ActionBox from 'components/ActionBox/ActionBox.jsx';

describe('<ActionBox />', () => {
  it('should render with .ActionBox class', () => {
    const wrapper = shallow(<ActionBox />);
    expect(wrapper.is('.ActionBox')).toBe(true);
  });

  it('should render children when passed in', () => {
    const wrapper = mount(
      <ActionBox>
        <ActionBox.Content>Content</ActionBox.Content>
      </ActionBox>,
    );
    expect(wrapper.find('.ActionBox-content').text()).toBe('Content');
  });
});
