import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { userPropType, commentPropType } from 'proptypes';
import { noop } from 'lodash';
import { NEW_COMMENT, OLD_COMMENT } from 'constants/commentStates.js';
import Avatar from 'components/Avatar/Avatar.jsx';
import CommentBoxInputField from 'components/CommentBox/CommentBoxInputField.jsx';
import CommentBoxActions from 'components/CommentBox/CommentBoxActions.jsx';
import CommentBoxTools from 'components/CommentBox/CommentBoxTools.jsx';
import useOnClickOutside from 'hooks/useOnClickOutside.jsx';
import 'components/CommentBox/CommentBox.scss';

const CommentBox = ({
  comment,
  status,
  showActions: showActionsProp,
  showTools: showToolsProp,
  onPost,
  onCancel,
  user,
  placeholder,
  isSubmitting,
  onDeleteComment,
}) => {
  const commentBoxInputField = useRef(null);
  const commentBox = useRef(null);

  const [text, setText] = useState((comment && comment.text) || '');
  const [editable, setEditable] = useState(status === NEW_COMMENT);
  const [showActions, setShowActions] = useState(status === NEW_COMMENT && showActionsProp);
  const [showTools] = useState(status === OLD_COMMENT && showToolsProp);

  useOnClickOutside(commentBox, () => handleClickOutside());

  const handleClickOutside = () => {
    if (!showActionsProp) {
      // @note: check actions from props to remember the original intent
      setShowActions(false);
    }

    if (status !== NEW_COMMENT) {
      // @note: this check reverts editing comments to original state
      setEditable(false);
    }
  };

  const focus = () => {
    commentBoxInputField.current.focus();
  };

  const onFocus = () => {
    if (!showActionsProp) {
      // @note: check actions from props to remember the original intent
      setShowActions(true);
    }
  };

  const onChange = (e) => {
    const newText = e.target.value;
    setText(newText);
  };

  const handlePost = () => {
    onPost(text);
    setText('');
  };

  const handleCancel = () => {
    setText('');
    onCancel();
    focus();
  };

  useEffect(() => {
    setShowActions(status === NEW_COMMENT && showActionsProp);
  }, [showActionsProp, status]);

  return (
    <div className="CommentBox" ref={commentBox}>
      {showTools && (
        <div className="CommentBox-tools">
          <CommentBoxTools
            comment={comment}
            onDeleteComment={onDeleteComment}
          />
        </div>
      )}
      <div className="CommentBox-avatar">
        <Avatar
          user={status === NEW_COMMENT ? user : comment.author}
          tooltipLeftOrRight="left"
        />
      </div>
      <div className="CommentBox-input">
        <CommentBoxInputField
          ref={commentBoxInputField}
          placeholder={placeholder}
          editable={editable}
          text={text}
          onFocus={onFocus}
          onChange={onChange}
          isSubmitting={isSubmitting}
        />
      </div>
      {showActions && (
        <CommentBoxActions
          onPost={handlePost}
          onCancel={handleCancel}
          isSubmitting={isSubmitting}
        />
      )}
    </div>
  );
};

CommentBox.propTypes = {
  user: userPropType.isRequired,
  status: PropTypes.oneOf([NEW_COMMENT, OLD_COMMENT]),
  comment: commentPropType,
  placeholder: PropTypes.string,
  showActions: PropTypes.bool,
  onPost: PropTypes.func,
  onCancel: PropTypes.func,
  isSubmitting: PropTypes.bool,
  onDeleteComment: PropTypes.func,
  showTools: PropTypes.bool,
};

CommentBox.defaultProps = {
  status: NEW_COMMENT,
  comment: null,
  showActions: true,
  placeholder: 'New comment...',
  onPost: noop,
  onCancel: noop,
  isSubmitting: false,
  onDeleteComment: noop,
  showTools: false,
};

export default CommentBox;
