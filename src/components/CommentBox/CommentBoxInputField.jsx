import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Linkify from 'react-linkify';

class CommentBoxInputField extends Component {
  focus = () => {
    this.input.focus();
  }

  render() {
    const {
      editable,
      text,
      placeholder,
      onFocus,
      onChange,
      isSubmitting,
    } = this.props;

    return editable ? (
      <textarea
        ref={(c) => { this.input = c; }}
        className="CommentBox-input-field"
        value={text}
        placeholder={placeholder}
        onFocus={onFocus}
        onChange={onChange}
        disabled={isSubmitting}
      />
    ) : (
      <Linkify properties={{ target: '_blank' }}>
        <span className="CommentBox-input-field CommentBox-input-field--disabled">
          {text}
        </span>
      </Linkify>
    );
  }
}

CommentBoxInputField.propTypes = {
  editable: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onFocus: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
};

CommentBoxInputField.defaultProps = {};

export default CommentBoxInputField;
