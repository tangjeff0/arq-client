import { setUp, checkProps } from 'utils/testing';
import { user } from 'mocks/users';
import CommentBox from 'components/CommentBox/CommentBox.jsx';

describe('<CommentBox />', () => {
  let wrapper;

  const expectedProps = {
    user,
  };

  beforeEach(() => {
    wrapper = setUp(CommentBox, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(CommentBox, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .CommentBox class', () => {
    expect(wrapper.is('.CommentBox')).toBe(true);
  });
});
