import React, { Component } from 'react';
import { onboardingPopupPropType } from 'proptypes';
import PropTypes from 'prop-types';
import Popover from 'components/Popover/Popover.jsx';
import PopoverBoxActionsNav from 'components/Popover/PopoverBoxActionsNav.jsx';
import architecturePopupHOC from 'components/OnboardingPopups/hoc/architecturePopupHOC.jsx';

class CreateArchitecturePopup extends Component {
  setPanel = ind => () => {
    const { setArchitecturePopupPanel } = this.props;
    setArchitecturePopupPanel(ind);
  }

  render() {
    const { architecturePopupSeen, architecturePopup, controlsPopup } = this.props;
    const { completed, panel } = architecturePopup;

    if (!controlsPopup.completed || completed || panel !== 0) return null;

    return (
      <Popover
        width="md"
        opened
        topMargin
        leftMargin
        preventCloseOnClickOutside
      >
        <Popover.Box>
          <Popover.Box.Content>
            <div className="Document-architecture-popup">
              <div className="Document-architecture-popup--content">
                <div className="Document-architecture-popup--header">
                  {' '}
                  Creating your architecture
                  {' '}
                </div>
                <p>
                  {' '}
                  This outliner will help you search for and link to existing
                  taxonomies to better communicate your problem set.
                  {' '}
                </p>
              </div>
              <div className="Document-architecture-popup--actions">
                <PopoverBoxActionsNav hide />
                <ul className="Document-architecture-popup--breadcrumbs">
                  <li
                    className="Document-architecture-popup--bullet Document-architecture-popup--bullet-active"
                    onClick={this.setPanel(0)}
                  />
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(1)}
                  />
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(2)}
                  />
                </ul>
                <PopoverBoxActionsNav
                  onClick={architecturePopupSeen}
                >
                  Next
                </PopoverBoxActionsNav>
              </div>
            </div>
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    );
  }
}

CreateArchitecturePopup.propTypes = {
  controlsPopup: onboardingPopupPropType.isRequired,
  architecturePopup: onboardingPopupPropType.isRequired,
  setArchitecturePopupPanel: PropTypes.func.isRequired,
  architecturePopupSeen: PropTypes.func.isRequired,
};

CreateArchitecturePopup.defaultProps = {};

export default architecturePopupHOC(CreateArchitecturePopup);
