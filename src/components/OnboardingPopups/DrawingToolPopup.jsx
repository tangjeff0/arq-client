import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { onboardingPopupPropType } from 'proptypes';
import Popover from 'components/Popover/Popover.jsx';
import PopoverBoxActionsNav from 'components/Popover/PopoverBoxActionsNav.jsx';
import LearningModuleButton from 'components/OnboardingPopups/LearningModuleButton.jsx';
import architecturePopupHOC from 'components/OnboardingPopups/hoc/architecturePopupHOC.jsx';

class DrawingToolPopup extends Component {
  setPanel = ind => () => {
    const { setArchitecturePopupPanel } = this.props;
    setArchitecturePopupPanel(ind);
  }

  render() {
    const { drawingToolPopupSeen, architecturePopup } = this.props;
    const { completed, panel } = architecturePopup;

    if (completed || panel !== 2) return null;

    return (
      <Popover
        width="md"
        opened
        leftMargin
        preventCloseOnClickOutside
      >
        <Popover.Box>
          <Popover.Box.Content>
            <div className="Document-architecture-popup">
              <div className="Document-architecture-popup--content">
                <div className="Document-architecture-popup--header">
                  {' '}
                  Drawing
                  {' '}
                </div>
                <p>
                  {' '}
                  View your outline visually with this drawing tool. Export to
                  .ppt to further refine your Architecture.
                  {' '}
                </p>
                <p>
                  {' '}
                  Learn more by clicking the
                  {' '}
                  <LearningModuleButton />
                  {' '}
                  button at the top right of this page.
                  {' '}
                </p>
              </div>
              <div className="Document-architecture-popup--actions">
                <PopoverBoxActionsNav onClick={this.setPanel(1)}>
                  Back
                </PopoverBoxActionsNav>
                <ul className="Document-architecture-popup--breadcrumbs">
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(0)}
                  />
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(1)}
                  />
                  <li
                    className="Document-architecture-popup--bullet Document-architecture-popup--bullet-active"
                    onClick={this.setPanel(2)}
                  />
                </ul>
                <PopoverBoxActionsNav onClick={drawingToolPopupSeen}>
                  Finish
                </PopoverBoxActionsNav>
              </div>
            </div>
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    );
  }
}

DrawingToolPopup.propTypes = {
  architecturePopup: onboardingPopupPropType.isRequired,
  setArchitecturePopupPanel: PropTypes.func.isRequired,
  drawingToolPopupSeen: PropTypes.func.isRequired,
};

DrawingToolPopup.defaultProps = {};

export default architecturePopupHOC(DrawingToolPopup);
