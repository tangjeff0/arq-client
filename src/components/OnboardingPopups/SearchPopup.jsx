import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { onboardingPopupPropType } from 'proptypes';
import Popover from 'components/Popover/Popover.jsx';
import PopoverBoxActionsNav from 'components/Popover/PopoverBoxActionsNav.jsx';
import architecturePopupHOC from 'components/OnboardingPopups/hoc/architecturePopupHOC.jsx';

class SearchPopup extends Component {
  setPanel = ind => () => {
    const { setArchitecturePopupPanel } = this.props;
    setArchitecturePopupPanel(ind);
  }

  render() {
    const { searchPopupSeen, architecturePopup } = this.props;
    const { completed, panel } = architecturePopup;

    if (completed || panel !== 1) return null;

    return (
      <Popover
        width="80"
        opened
        topMargin
        leftMargin
        preventCloseOnClickOutside
      >
        <Popover.Box>
          <Popover.Box.Content>
            <div className="Document-architecture-popup">
              <div className="Document-architecture-popup--content">
                <div className="Document-architecture-popup--header">
                  {' '}
                  Search
                  {' '}
                </div>
                <p>
                  {' '}
                  Find officially recognized terms to use in our Architectures
                  to better communicate your outlines.
                  {' '}
                </p>
              </div>
              <div className="Document-architecture-popup--actions">
                <PopoverBoxActionsNav onClick={this.setPanel(0)}>
                  Back
                </PopoverBoxActionsNav>
                <ul className="Document-architecture-popup--breadcrumbs">
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(0)}
                  />
                  <li
                    className="Document-architecture-popup--bullet Document-architecture-popup--bullet-active"
                    onClick={this.setPanel(1)}
                  />
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(2)}
                  />
                </ul>
                <PopoverBoxActionsNav onClick={searchPopupSeen}>
                  Next
                </PopoverBoxActionsNav>
              </div>
            </div>
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    );
  }
}

SearchPopup.propTypes = {
  architecturePopup: onboardingPopupPropType.isRequired,
  setArchitecturePopupPanel: PropTypes.func.isRequired,
  searchPopupSeen: PropTypes.func.isRequired,
};

SearchPopup.defaultProps = {};

export default architecturePopupHOC(SearchPopup);
