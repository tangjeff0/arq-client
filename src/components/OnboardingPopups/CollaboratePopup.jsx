import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { onboardingPopupPropType } from 'proptypes';
import Popover from 'components/Popover/Popover.jsx';
import PopoverBoxActionsNav from 'components/Popover/PopoverBoxActionsNav.jsx';
import LearningModuleButton from 'components/OnboardingPopups/LearningModuleButton.jsx';
import controlsPopupHOC from 'components/OnboardingPopups/hoc/controlsPopupHOC.jsx';

class CollaboratePopup extends PureComponent {
  setPanel = ind => () => {
    const { setControlsPopupPanel } = this.props;
    setControlsPopupPanel(ind);
  }

  render() {
    const { collaboratePopupSeen, controlsPopup } = this.props;
    const { completed, panel } = controlsPopup;

    if (completed || panel !== 1) return null;

    return (
      <Popover width="md" opened preventCloseOnClickOutside>
        <Popover.Box>
          <Popover.Box.Content>
            <div className="Document-architecture-popup">
              <div className="Document-architecture-popup--content">
                <div className="Document-architecture-popup--header">
                  {' '}
                  Collaborate
                  {' '}
                </div>
                <p>
                  {' '}
                  Invite your peers to collaborate as you build your
                  architecture.
                  {' '}
                </p>
                <p>
                  {' '}
                  Learn more by clicking the
                  {' '}
                  <LearningModuleButton />
                  {' '}
                  button at
                  the top right of this page.
                  {' '}
                </p>
              </div>
              <div className="Document-architecture-popup--actions">
                <PopoverBoxActionsNav onClick={this.setPanel(0)}>
                  Back
                </PopoverBoxActionsNav>
                <ul className="Document-architecture-popup--breadcrumbs">
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(0)}
                  />
                  <li
                    className="Document-architecture-popup--bullet Document-architecture-popup--bullet-active"
                    onClick={this.setPanel(0)}
                  />
                </ul>
                <PopoverBoxActionsNav onClick={collaboratePopupSeen}>
                  Finish
                </PopoverBoxActionsNav>
              </div>
            </div>
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    );
  }
}

CollaboratePopup.propTypes = {
  controlsPopup: onboardingPopupPropType.isRequired,
  setControlsPopupPanel: PropTypes.func.isRequired,
  collaboratePopupSeen: PropTypes.func.isRequired,
};

CollaboratePopup.defaultProps = {};

export default controlsPopupHOC(CollaboratePopup);
