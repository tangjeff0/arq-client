import React from 'react';
import PropTypes from 'prop-types';
import { block } from 'searchkit';

const SearchKitRefinementListContainer = ({
  title,
  mod,
  className,
  disabled,
  children,
  rightComponent,
  bottomComponent,
}) => {
  const bemBlocks = {
    container: block(mod),
  };

  const containerBlock = bemBlocks.container.el;
  const containerClass = containerBlock().mix(className).state({ disabled });
  const titleDiv = (
    <div className="sk-panel__header">
      {title}
      {rightComponent}
    </div>
  );

  return (
    <div className={containerClass}>
      {titleDiv}
      <div className="sk-panel__content">
        {bottomComponent}
        {children}
      </div>
    </div>
  );
};

SearchKitRefinementListContainer.propTypes = {
  title: PropTypes.string,
  disabled: PropTypes.bool,
  children: PropTypes.arrayOf(PropTypes.node),
  rightComponent: PropTypes.node,
  bottomComponent: PropTypes.node,
  mod: PropTypes.string,
  className: PropTypes.string,
};

SearchKitRefinementListContainer.defaultProps = {
  title: '',
  disabled: false,
  children: [],
  rightComponent: null,
  bottomComponent: null,
  mod: 'sk-panel',
  className: '',
};

export default SearchKitRefinementListContainer;
