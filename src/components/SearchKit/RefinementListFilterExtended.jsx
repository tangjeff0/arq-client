import { RefinementListFilter } from 'searchkit';

class RefinementListFilterExtended extends RefinementListFilter {
  hasOptions() {
    return this.accessor.getBuckets().length >= 0;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.operator !== this.props.operator) {
      this.accessor.options.operator = this.props.operator;
      this.searchkit.performSearch(true, false);
    }

    if (prevProps.orderKey !== this.props.orderKey) {
      this.accessor.options.orderKey = this.props.orderKey;
      this.accessor.options.orderDirection = this.props.orderDirection;
      this.searchkit.performSearch(true, false);
    }

    if (prevProps.filter !== this.props.filter) {
      this.accessor.options.include = this.props.include;
      this.accessor.size = this.props.size;
      this.searchkit.performSearch(true, false);
    }

    this.props.setBucketNumber(this.accessor.getBuckets().length);
  }
}

export default RefinementListFilterExtended;
