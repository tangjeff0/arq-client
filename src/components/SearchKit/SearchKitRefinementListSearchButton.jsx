import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import { IconFilter } from 'components/Icons/Icons.jsx';

const SearchKitRefinementListSearchButton = ({
  filter,
  filterOpen,
  setFilterOpen,
  bucketNumber,
}) => (
  <Button unstyled onClick={() => setFilterOpen(!filterOpen)}>
    <IconFilter opacity={filterOpen ? 1 : 0.5} />
    {filter.length > 0 && <sub>{bucketNumber}</sub>}
  </Button>
);

SearchKitRefinementListSearchButton.propTypes = {
  filter: PropTypes.string.isRequired,
  filterOpen: PropTypes.bool.isRequired,
  setFilterOpen: PropTypes.func.isRequired,
  bucketNumber: PropTypes.number,
};

SearchKitRefinementListSearchButton.defaultProps = {
  bucketNumber: null,
};

export default SearchKitRefinementListSearchButton;
