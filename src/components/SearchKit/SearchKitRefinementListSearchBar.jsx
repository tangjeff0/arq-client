import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import SearchBar from 'components/SearchBar/SearchBar.jsx';

const SearchKitRefinementListSearchBar = ({ value, setFilter, open }) => (
  <div className={cx('SearchKit-refinement-list-search-bar', {
    ['SearchKit-refinement-list-search-bar--open']: open, // eslint-disable-line no-useless-computed-key
  })}
  >
    <SearchBar
      appearance="secondary"
      placeholder="Filter tags"
      onChange={e => setFilter(e.target.value)}
      value={value}
      hideIcon
    />
  </div>
);

SearchKitRefinementListSearchBar.propTypes = {
  value: PropTypes.string.isRequired,
  setFilter: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default SearchKitRefinementListSearchBar;
