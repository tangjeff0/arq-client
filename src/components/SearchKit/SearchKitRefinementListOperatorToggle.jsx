import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { OPERATOR_OPTIONS } from 'constants/elastic.js';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import ActionBox from 'components/ActionBox/ActionBox.jsx';
import ActionList from 'components/ActionList/ActionList.jsx';

const SearchKitRefinementListOperatorToggle = ({
  taxonomiesOperator,
  setTaxonomiesOperator,
}) => {
  const dropdown = useRef();

  const handleOptionSelect = (option) => {
    setTaxonomiesOperator(option);
    dropdown && dropdown.current.close();
  };

  return (
    <Dropdown ref={dropdown}>
      <Dropdown.Toggle>
        <span className="SearchKit-refinement-list-operator-toggle">{taxonomiesOperator}</span>
      </Dropdown.Toggle>
      <Dropdown.Menu position="right">
        <ActionBox>
          <ActionList>
            {OPERATOR_OPTIONS.map(option => (
              <ActionList.Item
                key={option}
                onClick={() => handleOptionSelect(option)}
              >
                <span style={{ whiteSpace: 'nowrap' }}>{option}</span>
              </ActionList.Item>
            ))}
          </ActionList>
        </ActionBox>
      </Dropdown.Menu>
    </Dropdown>
  );
};

SearchKitRefinementListOperatorToggle.propTypes = {
  taxonomiesOperator: PropTypes.string.isRequired,
  setTaxonomiesOperator: PropTypes.func.isRequired,
};

export default SearchKitRefinementListOperatorToggle;
