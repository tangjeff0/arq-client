import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Button from 'components/Button/Button.jsx';

const SearchKitReset = ({
  hasFilters,
  resetFilters,
}) => (
  <div className={cx('SearchKit-reset', { 'SearchKit-reset--disabled': !hasFilters })}>
    <Button unstyled onClick={resetFilters}>
      Clear All Filters
    </Button>
  </div>
);

SearchKitReset.propTypes = {
  resetFilters: PropTypes.func.isRequired,
  hasFilters: PropTypes.bool.isRequired,
};

export default SearchKitReset;
