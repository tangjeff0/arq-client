import React from 'react';
import PropTypes from 'prop-types';

const SearchKitRefinementListTools = ({ children }) => (
  <div className="SearchKit-refinement-list-tools">
    {React.Children.map(children, c => <div className="SearchKit-refinement-list-tool">{c}</div>)}
  </div>
);

SearchKitRefinementListTools.propTypes = {
  children: PropTypes.node.isRequired,
};

export default SearchKitRefinementListTools;
