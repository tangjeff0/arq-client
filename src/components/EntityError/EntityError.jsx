import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import 'components/EntityError/EntityError.scss';

const EntityError = ({
  error,
  entityName,
  canRequestAccess,
}) => {
  const titleCaseName = entityName.charAt(0).toUpperCase() + entityName.slice(1).toLowerCase();

  // TODO: Update for Apollo errors
  const authorEmail = get(error, 'data.internalError.data.author.email');

  return (
    <div className="EntityError">
      <img className="EntityError-image" src="/404@2x.png" alt="Error" />
      {error.status === 403 && (
        <div className="EntityError-main">
          <span>
            You do not have permissions to view this
            {' '}
            {entityName.toLowerCase()}
            .
          </span>
          {canRequestAccess
            && authorEmail && (
            <span>
              Please request access from
              {' '}
              <a href={`mailto:${authorEmail}`}>{authorEmail}</a>
            </span>
          )}
        </div>
      )}
      {error.status === 404 && (
        <div className="EntityError-main">
          <span>
            {titleCaseName}
            {' '}
            does not exist
          </span>
        </div>
      )}
      <div className="EntityError-cta">
        Return to your available&nbsp;
        <a href="/">documents</a>
      </div>
    </div>
  );
};

EntityError.propTypes = {
  error: PropTypes.shape({
    status: PropTypes.number,
  }).isRequired,
  entityName: PropTypes.string.isRequired,
  canRequestAccess: PropTypes.bool,
};

EntityError.defaultProps = {
  canRequestAccess: false,
};

export default EntityError;
