import { setUp, checkProps } from 'utils/testing';
import Checkbox from 'components/Checkbox/Checkbox.jsx';
import Checkboxes from 'components/Checkbox/Checkboxes.jsx';

describe('<Checkbox />', () => {
  let wrapper;

  const expectedProps = {
    checked: false,
    id: 'testId',
  };

  beforeEach(() => {
    wrapper = setUp(Checkbox, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Checkbox, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Checkbox class', () => {
    expect(wrapper.is('.Checkbox')).toBe(true);
  });
});

describe('<Checkboxes />', () => {
  let wrapper;

  const expectedProps = {};

  beforeEach(() => {
    wrapper = setUp(Checkboxes, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Checkboxes, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Checkboxes class', () => {
    expect(wrapper.is('.Checkboxes')).toBe(true);
  });
});
