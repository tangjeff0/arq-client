import React from 'react';
import PropTypes from 'prop-types';
import { getTaxonomyRootAncestor } from 'utils/taxonomies';
import Loader from 'components/Loader/Loader.jsx';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';
import useTaxonomyDetails from 'hooks/useTaxonomyDetails.jsx';

const TaxonomyTreeInterface = ({ id, ...treeProps }) => {
  // NOTE: No cache is used here because the ancestors popover on the
  // graph details taxonomies/packages was triggering a re-render of the
  // parent.
  // Possible cause: https://github.com/apollographql/react-apollo/issues/2202
  // Bypassing the cache on this query prevents the page-level hook from changing
  const {
    loading,
    error,
    taxonomy,
  } = useTaxonomyDetails({ id, fetchPolicy: 'no-cache' });

  return (
    <>
      {loading && <Loader />}
      {!loading && error && 'Error!'}
      {!loading && taxonomy && (
        <TaxonomyTree
          taxonomy={getTaxonomyRootAncestor(taxonomy.ancestors)}
          allTaxonomies={taxonomy.ancestors}
          expanded
          {...treeProps}
        />
      )}
    </>
  );
};

TaxonomyTreeInterface.propTypes = {
  id: PropTypes.string.isRequired,
};

TaxonomyTreeInterface.defaultProps = {};

export default TaxonomyTreeInterface;
