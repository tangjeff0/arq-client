/* eslint-disable import/no-cycle */
// TODO: rewrite tree to eliminate cycle?
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType, treeItemActionsPropType } from 'proptypes';
import cx from 'classnames';
import { v4 as uuid } from 'uuid';
import { sourceNames } from 'constants/sources';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';
import TaxonomyTreeItemAction from 'components/TaxonomyTree/TaxonomyTreeItemAction.jsx';
import {
  IconCaretDown,
  IconCaretRight,
  IconBullet,
} from 'components/Icons/Icons.jsx';

class TaxonomyTreeItem extends PureComponent {
  render() {
    const {
      taxonomy,
      allTaxonomies,
      depth,
      expanded,
      actionFlags,
      renderItem,
    } = this.props;

    const { actions } = this.props;
    let actionList = actions.primary;

    if (actionList && actionFlags.length > 0) {
      for (let i = 0; i < actionFlags.length; i++) {
        const actionFlag = actionFlags[i];

        if (actions[actionFlag] && taxonomy[actionFlag]) {
          actionList = actions[actionFlag];
          break;
        }
      }
    }

    const {
      id,
      source,
      expand,
      architecture,
      source_name: esSourceName, // es returns source name on hit
    } = taxonomy;

    const children = allTaxonomies.filter(tax => tax.parentId === id);

    const expandItem = expand || expanded;

    return (
      <li className="TaxonomyTree-item">
        {depth === 0 && (
          <span className="TaxonomyTree-item-source">
            {architecture ? architecture.title : sourceNames[esSourceName || source.name]}
          </span>
        )}
        {actionList
          && actionList.length > 0 && (
          <div className="TaxonomyTree-item-actions">
            {actionList.map(action => (
              <TaxonomyTreeItemAction
                key={uuid()}
                taxonomy={taxonomy}
                action={action}
              />
            ))}
          </div>
        )}
        <div
          className={cx('TaxonomyTree-item-content', {
            'TaxonomyTree-item-content--hasActions':
            actionList && actionList.length > 0,
          })}
        >
          <span className="TaxonomyTree-item-term">
            <span className="TaxonomyTree-item-term-icon">
              {(() => {
                if (children.length > 0) {
                  return expandItem ? (
                    <IconCaretDown display="block" fill="#cad2d3" />
                  ) : (
                    <IconCaretRight display="block" fill="#cad2d3" />
                  );
                }

                return (
                  <IconBullet
                    display="block"
                    height={6}
                    fill="#cad2d3"
                  />
                );
              })()}
            </span>
            <span
              className={cx('TaxonomyTree-item-term-text', {
                'TaxonomyTree-item-term-text--highlight':
                  children.length === 0 || !expandItem,
              })}
            >
              {renderItem(taxonomy)}
            </span>
          </span>
        </div>

        {expandItem
          && children.length > 0
          && children.map(child => (
            <TaxonomyTree
              key={child.id}
              taxonomy={child}
              allTaxonomies={allTaxonomies}
              depth={depth + 1}
              expanded={expanded}
              actions={actions}
              actionFlags={actionFlags}
              renderItem={renderItem}
            />
          ))}
      </li>
    );
  }
}

TaxonomyTreeItem.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
  allTaxonomies: PropTypes.arrayOf(taxonomyPropType),
  actions: treeItemActionsPropType.isRequired,
  actionFlags: PropTypes.arrayOf(PropTypes.string).isRequired,
  depth: PropTypes.number.isRequired,
  expanded: PropTypes.bool,
  renderItem: PropTypes.func.isRequired,
};

TaxonomyTreeItem.defaultProps = {
  allTaxonomies: [],
  expanded: false,
};

export default TaxonomyTreeItem;
