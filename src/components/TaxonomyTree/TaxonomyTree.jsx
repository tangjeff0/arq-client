/* eslint-disable import/no-cycle */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import cx from 'classnames';
import TaxonomyTreeItem from 'components/TaxonomyTree/TaxonomyTreeItem.jsx';
import 'components/TaxonomyTree/TaxonomyTree.scss';

class TaxonomyTree extends PureComponent {
  render() {
    const {
      taxonomy,
      allTaxonomies,
      depth,
      expanded,
      actions,
      actionFlags,
      renderItem,
      noWrap,
    } = this.props;

    return (
      <ul
        className={cx('TaxonomyTree', `TaxonomyTree--depth-${depth}`, {
          'TaxonomyTree--nowrap': noWrap,
        })}
      >
        <TaxonomyTreeItem
          taxonomy={taxonomy}
          allTaxonomies={allTaxonomies}
          depth={depth}
          expanded={expanded}
          actions={actions}
          actionFlags={actionFlags}
          renderItem={renderItem}
        />
      </ul>
    );
  }
}

TaxonomyTree.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
  allTaxonomies: PropTypes.arrayOf(taxonomyPropType),
  depth: PropTypes.number,
  expanded: PropTypes.bool,
  actionFlags: PropTypes.arrayOf(PropTypes.string),
  actions: PropTypes.shape({
    primary: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  renderItem: PropTypes.func,
  noWrap: PropTypes.bool,
};

TaxonomyTree.defaultProps = {
  allTaxonomies: [],
  depth: 0,
  expanded: false,
  actions: {},
  actionFlags: [],
  renderItem: item => item.term,
  noWrap: false,
};

export default TaxonomyTree;
