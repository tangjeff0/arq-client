import React from 'react';
import Button from 'components/Button/Button.jsx';

describe('<Button />', () => {
  const defaultProps = {
    onClick() {},
  };

  it('should render with .Button class', () => {
    const wrapper = shallow(<Button {...defaultProps}>child</Button>);
    expect(wrapper.is('.Button')).toBe(true);
  });

  it('should render children when passed in', () => {
    const wrapper = shallow(
      <Button {...defaultProps}>
        <div className="test" />
      </Button>,
    );
    expect(wrapper.find('div.test').length).toEqual(1);
  });

  it('should render text string when passed in as child', () => {
    const wrapper = shallow(<Button {...defaultProps}>hello world</Button>);
    expect(wrapper.text()).toEqual('hello world');
  });

  it('allows us to set display prop', () => {
    const wrapper = mount(<Button display="block" {...defaultProps}>child</Button>);
    expect(wrapper.props().display).toEqual('block');
  });

  it('allows us to set appearance prop', () => {
    const wrapper = mount(<Button appearance="primary" {...defaultProps}>child</Button>);
    expect(wrapper.props().appearance).toEqual('primary');
  });

  it('allows us to set size prop', () => {
    const wrapper = mount(<Button size="sm" {...defaultProps}>child</Button>);
    expect(wrapper.props().size).toEqual('sm');
  });
});
