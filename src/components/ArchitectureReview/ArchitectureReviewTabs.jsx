import React from 'react';
import PropTypes from 'prop-types';
import Tabs from 'components/Tabs/Tabs.jsx';
import TabsInterface from 'components/Tabs/TabsInterface.jsx';
import ArchitectureReviewTermsInterface from 'components/ArchitectureReviewTerms/ArchitectureReviewTermsInterface.jsx';
import Button from 'components/Button/Button.jsx';

const ArchitectureReviewTabs = ({
  patchAdminArchitectureStatusRejected,
  patchAdminArchitectureStatusPublished,
}) => (
  <div className="ArchitectureReview-tabs">
    <TabsInterface compact>
      <Tabs.Pane label="Term Review">
        <ArchitectureReviewTermsInterface />
      </Tabs.Pane>
      <Tabs.Pane label="Arch Review">
        <Button.Group fill>
          <Button onClick={patchAdminArchitectureStatusPublished}>
            Accept
          </Button>
          <Button onClick={patchAdminArchitectureStatusRejected}>
            Reject
          </Button>
        </Button.Group>
      </Tabs.Pane>
    </TabsInterface>
  </div>
);

ArchitectureReviewTabs.propTypes = {
  patchAdminArchitectureStatusPublished: PropTypes.func.isRequired,
  patchAdminArchitectureStatusRejected: PropTypes.func.isRequired,
};

ArchitectureReviewTabs.defaultProps = {};

export default ArchitectureReviewTabs;
