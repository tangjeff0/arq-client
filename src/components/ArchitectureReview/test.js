import { setUp, checkProps } from 'utils/testing';
import ArchitectureReview from 'components/ArchitectureReview/ArchitectureReview.jsx';

describe('<ArchitectureReview />', () => {
  let wrapper;

  const expectedProps = {
    adminArchitecture: {
      ui: {},
    },
    patchAdminArchitectureStatusPublished: () => {},
    patchAdminArchitectureStatusRejected: () => {},
  };

  beforeEach(() => {
    wrapper = setUp(ArchitectureReview, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ArchitectureReview, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .ArchitectureReview class', () => {
    expect(wrapper.is('.ArchitectureReview')).toBe(true);
  });
});
