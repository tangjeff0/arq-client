import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';

const TreeItemTools = ({ tools, item }) => (
  <div className="Tree-item-tools">
    {tools.map(tool => (
      <div className="Tree-item-tool" key={tool.key}>
        <tool.renderTool item={item} />
      </div>
    ))}
  </div>
);

TreeItemTools.propTypes = {
  tools: PropTypes.arrayOf(
    PropTypes.shape({
      renderTool: PropTypes.func.isRequired,
      key: PropTypes.string.isRequired,
    }),
  ).isRequired,
  item: taxonomyPropType.isRequired,
};

export default TreeItemTools;
