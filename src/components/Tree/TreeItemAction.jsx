import React from 'react';
import { treeItemActionPropType, treeItemPropType } from 'proptypes';
import Tooltip from 'components/Tooltip/Tooltip.jsx';
import Button from 'components/Button/Button.jsx';

const TreeItemAction = ({ item, action }) => {
  const handleClick = () => {
    const { handler } = action;
    handler(item);
  };

  const {
    title,
    icon,
    renderButton,
  } = action;

  return (
    <div className="Tree-item-action">
      <Tooltip content={title}>
        <Tooltip.Wrapper>
          {action.renderButton ? (
            renderButton(item)
          ) : (
            <Button unstyled onClick={handleClick}>
              {icon}
            </Button>
          )}
        </Tooltip.Wrapper>
      </Tooltip>
    </div>
  );
};

TreeItemAction.propTypes = {
  item: treeItemPropType.isRequired,
  action: treeItemActionPropType.isRequired,
};

TreeItemAction.defaultProps = {};

export default TreeItemAction;
