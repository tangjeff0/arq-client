import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { taxonomyPropType } from 'proptypes';
import Tree from 'components/Tree/Tree.jsx'; // eslint-disable-line import/no-cycle
import TreeItemWrapper from 'components/Tree/TreeItemWrapper.jsx';
import TreeItemAction from 'components/Tree/TreeItemAction.jsx';
import Badge from 'components/Badge/Badge.jsx';
import { IconCaret, IconBullet } from 'components/Icons/Icons.jsx';

const TreeItem = ({
  actions,
  actionFlags,
  bulletFill,
  depth,
  item,
  renderItem,
  renderBadges,
  expanded,
  selected,
  onSelect,
  moveTaxonomy,
  deleteTaxonomy,
  deleteTaxonomies,
  tools,
  highlightItem,
  allItems,
}) => {
  const [expand, setExpand] = useState(expanded);
  const [focusItem, setFocusItem] = useState(null);

  const childItems = allItems
    ? allItems.filter(i => i.parentId === item.id)
    : (item.children || []);

  const onToggleExpand = (e) => {
    e.preventDefault();
    setExpand(!expand);
  };

  useEffect(() => {
    // Set expand and focus to true if treeItem found in deleteTaxonomies
    if (deleteTaxonomy) {
      if (deleteTaxonomies && deleteTaxonomies.includes(item.id)) {
        setExpand(true);
        setFocusItem(true);
      } else {
        setFocusItem(false);
      }
    }
    if (!deleteTaxonomy) {
      if (deleteTaxonomies && deleteTaxonomies.includes(item.id)) {
        setFocusItem(false);
      }
    }
  }, [item, deleteTaxonomies, deleteTaxonomy, setExpand]);

  let itemActions;
  if (actions) {
    itemActions = actions.primary;
    if (itemActions && actionFlags.length > 0) {
      actionFlags.forEach((i) => {
        const actionFlag = actionFlags[i];
        if (actions[actionFlag] && item[actionFlag]) {
          itemActions = actions[actionFlag];
        }
      });
    }
  }

  return (
    <li className="Tree-item">
      {itemActions
        && itemActions.length > 0 && (
        <div className="Tree-item-actions">
          {itemActions.map(action => (
            <TreeItemAction key={action.title} item={item} action={action} />
          ))}
        </div>
      )}
      <TreeItemWrapper
        item={item}
        onSelect={onSelect}
        selected={selected}
        moveTaxonomy={moveTaxonomy}
        tools={tools}
      >
        <div
          className={cx('Tree-item-content', {
            'Tree-item-content--hasActions': itemActions && itemActions.length > 0,
          })}
        >
          <span className="Tree-item-term">
            <span
              className="Tree-item-term-icon"
              onClick={onToggleExpand}
              role="presentation"
            >
              {childItems.length > 0 ? (
                <IconCaret
                  display="block"
                  height={6}
                  rotate={expand ? 90 : null}
                  fill={bulletFill || '#000000'}
                />
              ) : (
                <IconBullet
                  display="block"
                  height={6}
                  fill={bulletFill || '#000000'}
                />
              )}
            </span>
            <span className={cx('Tree-item-term-text', {
              'Tree-item-term-text--highlight': highlightItem && highlightItem(item),
              'Tree-item-term-text--focus': focusItem,
            })}
            >
              {renderItem(item)}
            </span>
            {renderBadges && (
              <span className="Tree-item-term-badges">
                {renderBadges(item).map(badge => (
                  <Badge key={badge} text={badge} />
                ))}
              </span>
            )}
          </span>
        </div>
      </TreeItemWrapper>
      {expand
        && childItems.length > 0
        && childItems.map(childItem => (
          <Tree
            key={childItem.id}
            item={childItem}
            depth={depth + 1}
            renderItem={renderItem}
            renderBadges={renderBadges}
            actions={actions}
            actionFlags={actionFlags}
            bulletFill={bulletFill}
            expanded={expanded}
            selected={selected}
            onSelect={onSelect}
            moveTaxonomy={moveTaxonomy}
            deleteTaxonomy={deleteTaxonomy}
            deleteTaxonomies={deleteTaxonomies}
            tools={tools}
            highlightItem={highlightItem}
            allItems={allItems}
          />
        ))}
    </li>
  );
};

TreeItem.propTypes = {
  actionFlags: PropTypes.arrayOf(PropTypes.string).isRequired,
  actions: PropTypes.shape({
    primary: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  bulletFill: PropTypes.string,
  depth: PropTypes.number.isRequired,
  item: taxonomyPropType.isRequired,
  renderItem: PropTypes.func.isRequired,
  renderBadges: PropTypes.func,
  expanded: PropTypes.bool.isRequired,
  selected: taxonomyPropType,
  onSelect: PropTypes.func,
  moveTaxonomy: taxonomyPropType,
  deleteTaxonomy: taxonomyPropType,
  deleteTaxonomies: PropTypes.arrayOf(PropTypes.string),
  tools: PropTypes.arrayOf(PropTypes.shape({})),
  highlightItem: PropTypes.func,
  allItems: PropTypes.arrayOf(taxonomyPropType),
};

TreeItem.defaultProps = {
  actions: null,
  bulletFill: null,
  renderBadges: null,
  selected: null,
  onSelect: null,
  moveTaxonomy: null,
  deleteTaxonomy: null,
  deleteTaxonomies: [],
  tools: null,
  highlightItem: null,
  allItems: null,
};

export default TreeItem;
