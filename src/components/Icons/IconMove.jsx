import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import OpenWithOutlinedIcon from '@material-ui/icons/OpenWithOutlined';

const IconMove = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <OpenWithOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconMove.propTypes = {
  fill: PropTypes.string,
};

IconMove.defaultProps = {
  fill: '#6a71d7',
};

export default IconMove;
