import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import KeyboardTabOutlinedIcon from '@material-ui/icons/KeyboardTabOutlined';

const IconAlignRight = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <KeyboardTabOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconAlignRight.propTypes = {
  fill: PropTypes.string,
};

IconAlignRight.defaultProps = {
  fill: '#6a71d7',
};

export default IconAlignRight;
