import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import ViewListOutlinedIcon from '@material-ui/icons/ViewListOutlined';

const IconWordLinked = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <ViewListOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconWordLinked.propTypes = {
  fill: PropTypes.string,
};

IconWordLinked.defaultProps = {
  fill: '#6a71d7',
};

export default IconWordLinked;
