import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';

const IconEyeSlash = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <VisibilityOffOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconEyeSlash.propTypes = {
  fill: PropTypes.string,
};

IconEyeSlash.defaultProps = {
  fill: '#6a71d7',
};

export default IconEyeSlash;
