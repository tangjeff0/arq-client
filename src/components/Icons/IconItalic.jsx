import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FormatItalicOutlinedIcon from '@material-ui/icons/FormatItalicOutlined';

const IconItalic = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FormatItalicOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconItalic.propTypes = {
  fill: PropTypes.string,
};

IconItalic.defaultProps = {
  fill: '#6a71d7',
};

export default IconItalic;
