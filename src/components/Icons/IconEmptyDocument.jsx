import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';

const IconEmptyDocument = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <DescriptionOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconEmptyDocument.propTypes = {
  fill: PropTypes.string,
};

IconEmptyDocument.defaultProps = {
  fill: '#6a71d7',
};

export default IconEmptyDocument;
