import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import RotateLeftOutlinedIcon from '@material-ui/icons/RotateLeftOutlined';

const IconRotateLeft = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <RotateLeftOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconRotateLeft.propTypes = {
  fill: PropTypes.string,
};

IconRotateLeft.defaultProps = {
  fill: '#6a71d7',
};

export default IconRotateLeft;
