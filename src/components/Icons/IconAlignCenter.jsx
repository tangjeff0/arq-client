import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FullscreenOutlinedIcon from '@material-ui/icons/FullscreenOutlined';

const IconAlignCenter = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FullscreenOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconAlignCenter.propTypes = {
  fill: PropTypes.string,
};

IconAlignCenter.defaultProps = {
  fill: '#6a71d7',
};

export default IconAlignCenter;
