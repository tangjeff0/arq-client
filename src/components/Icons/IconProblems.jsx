import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import ErrorOutlineOutlinedIcon from '@material-ui/icons/ErrorOutlineOutlined';

const IconProblems = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <ErrorOutlineOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconProblems.propTypes = {
  fill: PropTypes.string,
};

IconProblems.defaultProps = {
  fill: '#6a71d7',
};

export default IconProblems;
