import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FormatBoldOutlinedIcon from '@material-ui/icons/FormatBoldOutlined';

const IconBold = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FormatBoldOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconBold.propTypes = {
  fill: PropTypes.string,
};

IconBold.defaultProps = {
  fill: '#6a71d7',
};

export default IconBold;
