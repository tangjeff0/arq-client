import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import BlockIcon from '@material-ui/icons/Block';

const IconBlock = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <BlockIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconBlock.propTypes = {
  fill: PropTypes.string,
};

IconBlock.defaultProps = {
  fill: '#6a71d7',
};

export default IconBlock;
