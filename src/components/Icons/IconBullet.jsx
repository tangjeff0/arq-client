import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const IconBullet = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FiberManualRecordIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconBullet.propTypes = {
  fill: PropTypes.string,
};

IconBullet.defaultProps = {
  fill: '#6a71d7',
};

export default IconBullet;
