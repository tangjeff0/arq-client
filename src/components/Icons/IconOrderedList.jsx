import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FormatListNumberedOutlinedIcon from '@material-ui/icons/FormatListNumberedOutlined';

const IconUnorderedList = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FormatListNumberedOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconUnorderedList.propTypes = {
  fill: PropTypes.string,
};

IconUnorderedList.defaultProps = {
  fill: '#6a71d7',
};

export default IconUnorderedList;
