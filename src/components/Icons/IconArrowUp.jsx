import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import ArrowUpwardOutlinedIcon from '@material-ui/icons/ArrowUpwardOutlined';

const IconArrowUp = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <ArrowUpwardOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconArrowUp.propTypes = {
  fill: PropTypes.string,
};

IconArrowUp.defaultProps = {
  fill: '#6a71d7',
};

export default IconArrowUp;
