import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import LinkOffOutlinedIcon from '@material-ui/icons/LinkOffOutlined';

const IconBrokenLink = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <LinkOffOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconBrokenLink.propTypes = {
  fill: PropTypes.string,
};

IconBrokenLink.defaultProps = {
  fill: '#6a71d7',
};

export default IconBrokenLink;
