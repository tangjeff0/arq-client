import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';

const IconHome = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <HomeOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconHome.propTypes = {
  fill: PropTypes.string,
};

IconHome.defaultProps = {
  fill: '#6a71d7',
};

export default IconHome;
