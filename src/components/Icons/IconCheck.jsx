import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

const IconCheck = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <CheckCircleIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconCheck.propTypes = {
  fill: PropTypes.string,
};

IconCheck.defaultProps = {
  fill: '#6a71d7',
};

export default IconCheck;
