import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Icon = ({
  height,
  opacity,
  position,
  display,
  rotate,
  flip,
  children,
}) => (
  <span
    style={{
      fontSize: height ? `${height}px` : '1.4em',
      height: height ? `${height}px` : 'auto',
      opacity,
    }}
    className={cx('Icon', {
      [`Icon--position-${position}`]: position,
      [`Icon--display-${display}`]: display,
      [`Icon--rotate-${rotate}`]: rotate !== null,
      'Icon--flip': flip,
    })}
  >
    {children}
  </span>
);

Icon.propTypes = {
  height: PropTypes.number,
  display: PropTypes.string,
  position: PropTypes.string,
  opacity: PropTypes.number,
  rotate: PropTypes.number,
  children: PropTypes.node.isRequired,
  flip: PropTypes.bool,
};

Icon.defaultProps = {
  height: null,
  display: 'inlineBlock',
  position: null,
  opacity: 1,
  rotate: null,
  flip: null,
};

export default Icon;
