import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

const IconCaretRight = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <KeyboardArrowRightIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconCaretRight.propTypes = {
  fill: PropTypes.string,
};

IconCaretRight.defaultProps = {
  fill: '#6a71d7',
};

export default IconCaretRight;
