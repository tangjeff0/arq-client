import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import ScatterPlotOutlinedIcon from '@material-ui/icons/ScatterPlotOutlined';

const IconGraph = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <ScatterPlotOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconGraph.propTypes = {
  fill: PropTypes.string,
};

IconGraph.defaultProps = {
  fill: '#6a71d7',
};

export default IconGraph;
