import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined';

const IconUpload = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <CloudUploadOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconUpload.propTypes = {
  fill: PropTypes.string,
};

IconUpload.defaultProps = {
  fill: '#6a71d7',
};

export default IconUpload;
