import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';

const IconPlus = ({ fill, height, ...iconProps }) => (
  <Icon height={height} {...iconProps}>
    <AddOutlinedIcon
      style={{ color: fill }}
      fontSize="inherit"
      viewBox="2 2 20 20"
    />
  </Icon>
);

IconPlus.propTypes = {
  fill: PropTypes.string,
  height: PropTypes.number,
};

IconPlus.defaultProps = {
  fill: '#6a71d7',
  height: 12,
};

export default IconPlus;
