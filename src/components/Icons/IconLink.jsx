import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import LinkOutlinedIcon from '@material-ui/icons/LinkOutlined';

const IconLink = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <LinkOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconLink.propTypes = {
  fill: PropTypes.string,
};

IconLink.defaultProps = {
  fill: '#6a71d7',
};

export default IconLink;
