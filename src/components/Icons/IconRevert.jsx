import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import UndoOutlinedIcon from '@material-ui/icons/UndoOutlined';

const IconRevert = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <UndoOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconRevert.propTypes = {
  fill: PropTypes.string,
};

IconRevert.defaultProps = {
  fill: '#6a71d7',
};

export default IconRevert;
