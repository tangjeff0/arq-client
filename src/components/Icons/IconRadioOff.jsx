import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';

const IconRadioOff = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <RadioButtonUncheckedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconRadioOff.propTypes = {
  fill: PropTypes.string,
};

IconRadioOff.defaultProps = {
  fill: '#6a71d7',
};

export default IconRadioOff;
