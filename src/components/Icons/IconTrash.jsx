import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

const IconTrash = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <DeleteOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconTrash.propTypes = {
  fill: PropTypes.string,
};

IconTrash.defaultProps = {
  fill: '#6a71d7',
};

export default IconTrash;
