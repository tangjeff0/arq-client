import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';

const IconCaret = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <ArrowRightIcon
      style={{ color: fill }}
      fontSize="inherit"
      viewBox="10 7 5 10"
    />
  </Icon>
);

IconCaret.propTypes = {
  fill: PropTypes.string,
};

IconCaret.defaultProps = {
  fill: '#6a71d7',
};

export default IconCaret;
