import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import MoreVertOutlinedIcon from '@material-ui/icons/MoreVertOutlined';

const IconMoreVertical = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <MoreVertOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconMoreVertical.propTypes = {
  fill: PropTypes.string,
};

IconMoreVertical.defaultProps = {
  fill: '#6a71d7',
};

export default IconMoreVertical;
