import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import KeyboardTabOutlinedIcon from '@material-ui/icons/KeyboardTabOutlined';

const IconAlignLeft = ({ fill, ...iconProps }) => (
  <Icon flip {...iconProps}>
    <KeyboardTabOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconAlignLeft.propTypes = {
  fill: PropTypes.string,
};

IconAlignLeft.defaultProps = {
  fill: '#6a71d7',
};

export default IconAlignLeft;
