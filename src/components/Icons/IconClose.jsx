import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';

const IconClose = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <CloseOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconClose.propTypes = {
  fill: PropTypes.string,
};

IconClose.defaultProps = {
  fill: '#6a71d7',
};

export default IconClose;
