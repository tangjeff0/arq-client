import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';

const IconEye = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <VisibilityOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconEye.propTypes = {
  fill: PropTypes.string,
};

IconEye.defaultProps = {
  fill: '#6a71d7',
};

export default IconEye;
