import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';

const IconDisc = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <RadioButtonUncheckedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconDisc.propTypes = {
  fill: PropTypes.string,
};

IconDisc.defaultProps = {
  fill: '#6a71d7',
};

export default IconDisc;
