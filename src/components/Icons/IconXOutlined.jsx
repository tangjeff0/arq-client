import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import HighlightOffOutlinedIcon from '@material-ui/icons/HighlightOffOutlined';

const IconXOutlined = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <HighlightOffOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconXOutlined.propTypes = {
  fill: PropTypes.string,
};

IconXOutlined.defaultProps = {
  fill: '#6a71d7',
};

export default IconXOutlined;
