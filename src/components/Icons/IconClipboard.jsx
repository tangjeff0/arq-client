import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';

const IconClipboard = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <AssignmentOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconClipboard.propTypes = {
  fill: PropTypes.string,
};

IconClipboard.defaultProps = {
  fill: '#6a71d7',
};

export default IconClipboard;
