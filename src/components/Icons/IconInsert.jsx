import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import InputOutlinedIcon from '@material-ui/icons/InputOutlined';

const IconInsert = ({ fill, ...iconProps }) => (
  <Icon {...iconProps} flip>
    <InputOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconInsert.propTypes = {
  fill: PropTypes.string,
};

IconInsert.defaultProps = {
  fill: '#6a71d7',
};

export default IconInsert;
