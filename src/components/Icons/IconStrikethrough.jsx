import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import StrikethroughSOutlinedIcon from '@material-ui/icons/StrikethroughSOutlined';

const IconStrikethrough = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <StrikethroughSOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconStrikethrough.propTypes = {
  fill: PropTypes.string,
};

IconStrikethrough.defaultProps = {
  fill: '#6a71d7',
};

export default IconStrikethrough;
