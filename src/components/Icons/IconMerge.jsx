import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import MergeTypeOutlinedIcon from '@material-ui/icons/MergeTypeOutlined';

const IconMerge = ({ fill, ...iconProps }) => (
  <Icon {...iconProps} rotate={270}>
    <MergeTypeOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconMerge.propTypes = {
  fill: PropTypes.string,
};

IconMerge.defaultProps = {
  fill: '#6a71d7',
};

export default IconMerge;
