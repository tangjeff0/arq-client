import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import { getPackagesUniquePublishedArchitectures } from 'utils/gql';
import { sourceNames } from 'constants/sources';
import Card from 'components/Card/Card.jsx';
import Tree from 'components/Tree/Tree.jsx';
import 'components/CapabilityCard/CapabilityCard.scss';

const CapabilityCard = ({ capability, onArchiveClick }) => (
  <Card
    className="CapabilityCard"
    url={`/taxonomies/${capability.id}`}
    onArchiveClick={onArchiveClick ? () => onArchiveClick(capability) : null}
  >
    <div className="CapabilityCard-content">
      <div className="CapabilityCard-content-title">
        {capability.term}
      </div>
      <Card.Meta>{sourceNames[capability.source.name]}</Card.Meta>
      <div className="CapabilityCard-content-tree">
        <Tree
          item={capability}
          allItems={capability.ancestors}
        />
      </div>
      <div className="CapabilityCard-content-links">
        Architectures:
        {` ${getPackagesUniquePublishedArchitectures(capability.referencedBy).length}`}
        <br />
        Problems:
        {` ${capability.problemSets.length}`}
        <br />
        Companies:
        {` ${capability.companies.length}`}
      </div>
    </div>
  </Card>
);

CapabilityCard.propTypes = {
  capability: taxonomyPropType.isRequired,
  onArchiveClick: PropTypes.func,
};

CapabilityCard.defaultProps = {
  onArchiveClick: null,
};

export default CapabilityCard;
