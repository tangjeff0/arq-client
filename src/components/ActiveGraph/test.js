import { layouts, TREE } from 'constants/graph.js';
import { taxonomy } from 'mocks/taxonomy';
import { setUp, checkProps } from 'utils/testing';
import ActiveGraph from 'components/ActiveGraph/ActiveGraph.jsx';
import ActiveGraphToolbar from 'components/ActiveGraph/ActiveGraphToolbar.jsx';
import ActiveGraphLayoutSelector from 'components/ActiveGraph/ActiveGraphLayoutSelector.jsx';
import ActiveGraphLayoutSelectorGroup from 'components/ActiveGraph/ActiveGraphLayoutSelectorGroup.jsx';

describe('<ActiveGraph />', () => {
  let wrapper;

  const expectedProps = {
    client: { query: () => {} },
    setLayout: () => {},
    layout: TREE,
  };

  beforeEach(() => {
    wrapper = setUp(ActiveGraph, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ActiveGraph, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .ActiveGraph class', () => {
    expect(wrapper.is('.ActiveGraph')).toBe(true);
  });

  it('should render a Dashboard', () => {
    expect(wrapper.exists('Dashboard')).toBe(true);
  });

  it('should render a Loader if loading', () => {
    wrapper.setProps({ loading: true });
    expect(wrapper.exists('Loader')).toBe(true);
  });

  it('should render a Graph if not loading, no error', () => {
    wrapper.setProps({ loading: false, entity: taxonomy });
    expect(wrapper.exists('Graph')).toBe(true);
  });
});

describe('<ActiveGraphToolbar />', () => {
  let wrapper;

  const expectedProps = {
    layout: TREE,
    setLayout: () => {},
    backLink: {
      url: '/',
      text: 'Link',
    },
  };

  beforeEach(() => {
    wrapper = setUp(ActiveGraphToolbar, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ActiveGraphToolbar, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render a Toolbar', () => {
    expect(wrapper.is('Toolbar')).toBe(true);
  });
});

describe('<ActiveGraphLayoutSelector />', () => {
  let wrapper;

  const expectedProps = {
    selectedLayout: TREE,
    updateSelectedLayout: jest.fn(),
  };

  beforeEach(() => {
    wrapper = setUp(ActiveGraphLayoutSelector, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ActiveGraphLayoutSelector, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .ActiveGraph-layoutSelector class', () => {
    expect(wrapper.is('.ActiveGraph-layoutSelector')).toBe(true);
  });

  it('should render the correct number of ActiveGraphLayoutSelectorGroup options', () => {
    const options = wrapper.find('ActiveGraphLayoutSelectorGroup');
    expect(options.length).toBe(Object.values(layouts).length);
  });
});

describe('<ActiveGraphLayoutSelectorGroup />', () => {
  let wrapper;

  const expectedProps = {
    layout: TREE,
    layoutName: 'Layout Name',
    selectedLayout: TREE,
    onLayoutUpdate: () => {},
  };

  beforeEach(() => {
    wrapper = setUp(ActiveGraphLayoutSelectorGroup, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ActiveGraphLayoutSelectorGroup, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .ActiveGraph-layoutSelector-group class', () => {
    expect(wrapper.is('.ActiveGraph-layoutSelector-group')).toBe(true);
  });
});
