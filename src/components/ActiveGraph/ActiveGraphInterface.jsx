import React from 'react';
import PropTypes from 'prop-types';
import ActiveGraph from 'components/ActiveGraph/ActiveGraph.jsx';
import useActiveGraph from 'components/ActiveGraph/useActiveGraph.jsx';

const ActiveGraphInterface = ({
  id,
  type,
  documentQID,
  published,
  shared,
}) => {
  const {
    loading,
    error,
    entity,
    title,
    client,
    layout,
    setLayout,
    backLink,
  } = useActiveGraph({
    id,
    type,
    documentQID,
    published,
    shared,
  });

  return (
    <ActiveGraph
      loading={loading}
      error={error}
      entity={entity}
      title={title}
      client={client}
      layout={layout}
      setLayout={setLayout}
      backLink={backLink}
    />
  );
};

ActiveGraphInterface.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  documentQID: PropTypes.string,
  published: PropTypes.bool,
  shared: PropTypes.bool,
};

ActiveGraphInterface.defaultProps = {
  documentQID: null,
  published: false,
  shared: false,
};

export default ActiveGraphInterface;
