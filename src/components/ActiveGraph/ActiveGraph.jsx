import React from 'react';
import PropTypes from 'prop-types';
import { apolloErrorPropType, apolloClientPropType } from 'proptypes';
import cx from 'classnames';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Graph from 'components/Graph/Graph.jsx';
import Loader from 'components/Loader/Loader.jsx';
import ActiveGraphToolbar from 'components/ActiveGraph/ActiveGraphToolbar.jsx';
import 'components/ActiveGraph/ActiveGraph.scss';

const ActiveGraph = ({
  loading,
  error,
  entity,
  title,
  client,
  layout,
  setLayout,
  backLink,
}) => (
  <div className="ActiveGraph">
    <Dashboard>
      <Dashboard.Header showBreadcrumbs />
      {!loading
        && !error
        && entity && (
        <Dashboard.Section above>
          <Dashboard.Toolbar>
            <ActiveGraphToolbar
              backLink={backLink}
            />
          </Dashboard.Toolbar>
        </Dashboard.Section>
      )}
      <Dashboard.Section fit>
        <Dashboard.Column fit>
          <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
            <div className="ActiveGraph-content">
              {loading && <Loader size={36} isCentered isAbsolute />}
              {!loading
                && !error
                && entity && (
                <>
                  <h2 className={cx('ActiveGraph-title', {
                    'ActiveGraph-title--untitled': !title,
                  })}
                  >
                    {title || 'Untitled'}
                  </h2>
                  <Graph
                    layout={layout}
                    setLayout={setLayout}
                    item={entity}
                    client={client}
                  />
                </>
              )}
            </div>
            <Dashboard.Footer />
          </Dashboard.Content>
        </Dashboard.Column>
      </Dashboard.Section>
    </Dashboard>
  </div>
);

ActiveGraph.propTypes = {
  loading: PropTypes.bool,
  error: apolloErrorPropType,
  entity: PropTypes.shape({}),
  title: PropTypes.string,
  client: apolloClientPropType.isRequired,
  layout: PropTypes.string.isRequired,
  setLayout: PropTypes.func.isRequired,
  backLink: PropTypes.shape({
    url: PropTypes.string,
    text: PropTypes.string,
  }),
};

ActiveGraph.defaultProps = {
  loading: false,
  error: null,
  entity: null,
  title: null,
  backLink: null,
};

export default ActiveGraph;
