import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

function DropdownMenu({ children, position }) {
  return (
    <div className={cx('Dropdown-menu', `Dropdown-menu--${position}`)}>
      {children}
    </div>
  );
}

DropdownMenu.propTypes = {
  children: PropTypes.any,
  position: PropTypes.string,
};

DropdownMenu.defaultProps = {
  children: null,
  position: 'left',
};

export default DropdownMenu;
