import React from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';

function DropdownToggle({ children, onToggle }) {
  return (
    <div className="Dropdown-toggle" onClick={onToggle} role="button" tabIndex={0}>
      {children}
    </div>
  );
}

DropdownToggle.propTypes = {
  children: PropTypes.any,
  onToggle: PropTypes.func,
};

DropdownToggle.defaultProps = {
  children: null,
  onToggle: noop,
};

export default DropdownToggle;
