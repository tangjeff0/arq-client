import React from 'react';
import Button from 'components/Button/Button.jsx';

export default ({ hide, ...props }) => (
  <div className="Popover-box-actions--nav">
    {!hide && <Button unstyled {...props} />}
  </div>
);
