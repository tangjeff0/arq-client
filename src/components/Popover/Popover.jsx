import React, {
  useState,
  useEffect,
  useRef,
  forwardRef,
  useImperativeHandle,
} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { noop } from 'lodash';
import useOnClickOutside from 'hooks/useOnClickOutside.jsx';
import PopoverToggle from 'components/Popover/PopoverToggle.jsx';
import PopoverBox from 'components/Popover/PopoverBox.jsx';
import PopoverBoxContent from 'components/Popover/PopoverBoxContent.jsx';
import PopoverBoxActions from 'components/Popover/PopoverBoxActions.jsx';
import 'components/Popover/Popover.scss';

const Popover = forwardRef(({
  size,
  width,
  leftOrRight,
  topOrBottom,
  hasActions,
  inline,
  leftMargin,
  topMargin,
  children,
  opened,
  onOpen,
  onClose,
  preventCloseOnClickOutside,
}, ref) => {
  const popover = useRef();
  useImperativeHandle(ref, () => ({ close }));

  const [isOpened, setIsOpened] = useState(opened);

  useOnClickOutside(popover, () => !preventCloseOnClickOutside && close());

  useEffect(() => {
    isOpened && onOpen();
    !isOpened && onClose();
  }, [isOpened, onOpen, onClose]);

  const onToggle = (e) => {
    e.preventDefault();
    e.stopPropagation();
    isOpened ? close() : open();
  };

  const open = () => !isOpened && setIsOpened(true);

  const close = () => isOpened && setIsOpened(false);

  return (
    <div
      ref={popover}
      className={cx('Popover', `Popover--size-${size}`, {
        'Popover--isOpened': isOpened,
        'Popover--hasActions': hasActions,
        'Popover--inline': inline,
        'Popover--left': leftMargin,
        'Popover--top': topMargin,
      })}
    >
      {React.Children.map(children, (child) => {
        if (child.type === PopoverToggle) {
          return React.cloneElement(child, { onToggle });
        }

        if (child.type === PopoverBox && isOpened) {
          return React.cloneElement(child, {
            width,
            leftOrRight,
            topOrBottom,
          });
        }

        return null;
      })}
    </div>
  );
});

Popover.Toggle = PopoverToggle;
Popover.Box = PopoverBox;
Popover.Box.Content = PopoverBoxContent;
Popover.Box.Actions = PopoverBoxActions;

Popover.propTypes = {
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
  width: PropTypes.oneOf(['auto', 'sm', 'md', '80']),
  leftOrRight: PropTypes.oneOf(['left', 'right']),
  topOrBottom: PropTypes.oneOf(['top', 'bottom']),
  leftMargin: PropTypes.bool,
  topMargin: PropTypes.bool,
  children: PropTypes.node,
  opened: PropTypes.bool,
  hasActions: PropTypes.bool,
  inline: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  preventCloseOnClickOutside: PropTypes.bool,
};

Popover.defaultProps = {
  size: 'md',
  width: 'sm',
  leftOrRight: null,
  topOrBottom: null,
  leftMargin: null,
  topMargin: null,
  children: null,
  opened: false,
  hasActions: false,
  inline: false,
  onOpen: noop,
  onClose: noop,
  preventCloseOnClickOutside: false,
};

export default Popover;
