import React from 'react';
import PropTypes from 'prop-types';

function PopoverBoxActions({ children }) {
  return <div className="Popover-box-actions">{children}</div>;
}

PopoverBoxActions.propTypes = {
  children: PropTypes.node,
};

PopoverBoxActions.defaultProps = {
  children: null,
};

export default PopoverBoxActions;
