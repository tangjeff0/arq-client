import {
  ApolloClient,
  HttpLink,
  ApolloLink,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import fetch from 'unfetch';
import logger from 'utils/logger';
import { ENV_LOCAL } from 'constants/environments';
import { cache } from 'apollo/cache';

const httpLink = new HttpLink({
  uri: `${API_HOST}${API_VERSION}/graphql`,
  fetch,
});

const authLink = setContext((_, { headers }) => {
  // set headers for local dev
  if (ENV === ENV_LOCAL) {
    // get the remote-user from local storage if it exists
    const remoteUser = localStorage.getItem('X-Hasura-Remote-User');
    const remoteRole = localStorage.getItem('X-Hasura-Role');
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        'X-Hasura-Remote-User': remoteUser ? `${remoteUser}` : 'efranklin@example.com',
        'X-Hasura-Role': remoteRole ? `${remoteRole}` : 'api-user',
      },
    };
  }

  return { headers };
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, extensions }) => {
      logger(`[GraphQL error]: Message: ${message}, Location: ${extensions.code}`);
      // TODO: example code-based error behavior
      // switch (extensions.code) {
      //   case 'data-exception':
      //   case 'validation-failed':
      //     props.history.push('/something-went-wrong'); // can use withRouter and redirect
      //     break;
      //   default:
      //     break;
      // }
    });
  }

  if (networkError) {
    logger(`[Network error]: ${networkError}`);
    // props.history.push('/network-error') // can use withRouter and redirect
  }
});

// TODO: Reset cache on logout?
const client = new ApolloClient({
  cache,
  link: ApolloLink.from([errorLink, authLink, httpLink]),
  queryDuplication: false, // enable query cancellation
});

export default client;
