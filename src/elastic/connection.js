import elasticsearch from 'elasticsearch';

const client = new elasticsearch.Client({
  host: `${ES_HOST}`,
  // log: 'trace',
});

// Test es connection
// client.ping(
//   {
//     requestTimeout: 30000,
//   },
//   (error) => {
//     if (error) {
//       logger('Elasticsearch cluster is down!');
//     } else {
//       logger('Elasticsearch cluster is up!');
//     }
//   },
// );

export default client;
