import { useEffect, useState } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { get } from 'lodash';
import { v4 as uuid } from 'uuid';
import { INVITATION_NEW } from 'constants/invitationStates';
import { MY_DOCUMENTS_QUERY_ROOT, USERS_QUERY_ROOT } from 'constants/gql';
import { GET_DOC_COLLABORATORS, INVITE_COLLABORATORS } from 'queries/collaborators';
import useUser from 'hooks/useUser.jsx';

const useDocCollaborators = ({ documentQID }) => {
  const [allCollaborators, setAllCollaborators] = useState([]);
  const [selectedCollaborators, setSelectedCollaborators] = useState([]);

  const { user } = useUser();

  const { loading, error, data } = useQuery(GET_DOC_COLLABORATORS, {
    variables: { id: documentQID },
    fetchPolicy: 'cache-and-network',
  });

  // initialize with possible collaborators and existing invitations
  useEffect(() => {
    const users = get(data, USERS_QUERY_ROOT, []);
    const docInvitations = get(data, [MY_DOCUMENTS_QUERY_ROOT, 0, 'invitations'], []);

    const collaborators = users
      .filter(u => u.email !== user.email)
      .map((u) => {
        const invitationStatus = get(
          docInvitations.find(i => i.invitee.email === u.email),
          'status',
          INVITATION_NEW,
        );

        return {
          ...u,
          status: invitationStatus,
          qid: uuid(),
        };
      });

    setAllCollaborators(collaborators);

    const selected = collaborators.filter(collaborator => collaborator.status !== INVITATION_NEW);

    setSelectedCollaborators(selected);
  }, [data, user]);

  const [inviteCollaboratorsMutation] = useMutation(INVITE_COLLABORATORS);
  const inviteCollaborators = () => {
    // prevent duplicate invites
    const invitationObjects = selectedCollaborators
      .filter(c => c.status === INVITATION_NEW)
      .map(c => ({
        documentId: documentQID,
        collaboratorEmail: c.email,
      }));

    inviteCollaboratorsMutation({
      variables: {
        objects: invitationObjects,
      },
    });
  };

  const selectCollaborator = (email) => {
    const collaborator = allCollaborators.find(c => c.email === email);
    const newCollaborators = [
      ...selectedCollaborators,
      collaborator,
    ];
    setSelectedCollaborators(newCollaborators);
  };

  const deselectCollaborator = (email) => {
    const newCollaborators = selectedCollaborators
      .filter(c => c.email !== email);
    setSelectedCollaborators(newCollaborators);
  };

  return {
    loading,
    error,
    inviteCollaborators,
    selectCollaborator,
    deselectCollaborator,
    allCollaborators,
    selectedCollaborators,
  };
};

export default useDocCollaborators;
