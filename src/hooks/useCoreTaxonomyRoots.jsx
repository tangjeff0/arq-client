import { useQuery } from '@apollo/client';
import { GET_SOURCE_CHILDREN } from 'queries/sources';
import { SOURCES_QUERY_ROOT } from 'constants/gql';
import { SOURCE_ARQ } from 'constants/sources';
import { get } from 'lodash';

const useCoreTaxonomyRoots = () => {
  const { loading, error, data } = useQuery(GET_SOURCE_CHILDREN, {
    variables: { sourceName: SOURCE_ARQ },
  });

  const taxonomies = get(data, [SOURCES_QUERY_ROOT, 0, 'roots'], []);

  return { loading, error, taxonomies };
};

export default useCoreTaxonomyRoots;
