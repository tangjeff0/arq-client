import { useState } from 'react';
import { useQuery } from '@apollo/client';
import { GET_COMPANY_SEARCH_SETTINGS } from 'queries/searchkit';
import { GET_COMPANIES_TAXONOMIES } from 'queries/companies';
import { COMPANIES_QUERY_ROOT } from 'constants/gql';
import { companySearchVar } from 'apollo/cache';
import queryString from 'query-string';

const useCompanySearch = () => {
  const { data: { companySearch } } = useQuery(GET_COMPANY_SEARCH_SETTINGS);

  const {
    loading: tagsLoading,
    error: tagsError,
    data: tagsData,
  } = useQuery(GET_COMPANIES_TAXONOMIES);

  const [filter, setFilter] = useState('');

  const [filterOpen, setFilterOpen] = useState(false);

  const { filterSort: taxonomiesSort } = companySearch;

  const setTaxonomiesSort = (value) => {
    companySearchVar({
      ...companySearch,
      filterSort: value,
    });
  };

  const { filterOperator } = companySearch;

  const taxonomiesOperator = filterOperator
    || queryString.parse(window.location.search).taxonomiesOperator || 'AND';

  const setTaxonomiesOperator = (value) => {
    companySearchVar({
      ...companySearch,
      filterOperator: value,
    });
  };

  let include = '.*.*';

  if (tagsData && filter.length) {
    const tags = {};
    const problemSets = tagsData[COMPANIES_QUERY_ROOT];
    problemSets.forEach((p) => {
      p.capabilities.forEach((c) => { tags[c.id] = c; });
    });

    include = Object
      .values(tags)
      .filter(tag => tag.term.toLowerCase().indexOf(filter.toLowerCase()) > -1)
      .map(tag => tag.term);
  }

  return {
    error: tagsError,
    loading: tagsLoading,
    setTaxonomiesOperator,
    taxonomiesOperator,
    setTaxonomiesSort,
    taxonomiesSort,
    filter,
    setFilter,
    filterOpen,
    setFilterOpen,
    include,
  };
};

export default useCompanySearch;
