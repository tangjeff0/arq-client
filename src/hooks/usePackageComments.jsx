import { useMutation } from '@apollo/client';
import { useCallback } from 'react';
import { createComment } from 'utils/comments';
import { now } from 'utils/time';
import {
  ADD_PACKAGE_COMMENT,
  SOFT_DELETE_PACKAGE_COMMENT,
  GET_PACKAGE_COMMENTS,
} from 'queries/comments';
import { PACKAGE_QUERY_ROOT, PACKAGE_COMMENT_TYPENAME } from 'constants/gql';

const usePackageComments = () => {
  const [addPackageCommentMutation] = useMutation(ADD_PACKAGE_COMMENT);
  const addPackageComment = useCallback(
    (
      user,
      text,
      packageQID,
    ) => {
      const comment = createComment(user, text);

      const variables = {
        object: {
          packageId: packageQID,
          id: comment.id,
          text,
        },
      };

      const update = (cache) => {
        const { [PACKAGE_QUERY_ROOT]: pkg } = cache.readQuery({
          query: GET_PACKAGE_COMMENTS,
          variables: { id: packageQID },
        });

        cache.writeQuery({
          query: GET_PACKAGE_COMMENTS,
          variables: { id: packageQID },
          data: {
            [PACKAGE_QUERY_ROOT]: {
              id: packageQID,
              comments: pkg ? pkg.comments.concat(comment) : [comment],
              __typename: PACKAGE_COMMENT_TYPENAME,
            },
          },
        });
      };

      addPackageCommentMutation({ variables, update });
    },
    [addPackageCommentMutation],
  );

  const [deletePackageCommentMutation] = useMutation(SOFT_DELETE_PACKAGE_COMMENT);
  const deletePackageComment = useCallback(
    (packageQID, comment) => {
      const deletedAt = now();

      const variables = {
        id: comment.id,
        deletedAt,
      };

      const update = (cache) => {
        const { [PACKAGE_QUERY_ROOT]: pkg } = cache.readQuery({
          query: GET_PACKAGE_COMMENTS,
          variables: { id: packageQID },
        });

        const data = {
          [PACKAGE_QUERY_ROOT]: {
            ...pkg,
            comments: pkg.comments.map(c => ({
              ...c,
              deletedAt: comment.id === c.id ? deletedAt : c.deletedAt,
            })),
          },
        };

        cache.writeQuery({ query: GET_PACKAGE_COMMENTS, data });
      };

      deletePackageCommentMutation({ variables, update });
    },
    [deletePackageCommentMutation],
  );

  return {
    addPackageComment,
    deletePackageComment,
  };
};

export default usePackageComments;
