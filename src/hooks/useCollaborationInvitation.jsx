import { useCallback } from 'react';
import { useMutation } from '@apollo/client';
import { now } from 'utils/time';
import { UPDATE_COLLABORATION_INVITATION } from 'queries/collaborators';

const useCollaborationInvitation = () => {
  const [updateCollaborationInvitationMutation] = useMutation(UPDATE_COLLABORATION_INVITATION);
  const updateCollaborationInvitation = useCallback((qid) => {
    const seenAt = now();

    updateCollaborationInvitationMutation({
      variables: { id: qid, seenAt },
    });
  }, [updateCollaborationInvitationMutation]);

  return {
    updateCollaborationInvitation,
  };
};

export default useCollaborationInvitation;
