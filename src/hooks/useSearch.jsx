import client from 'elastic/connection';
import { useState } from 'react';

const useSearch = (index = 'taxonomies', field = 'term') => {
  const [searching, setSearching] = useState(false);
  const [hits, setHits] = useState([]);

  const onSearch = async (term) => {
    setSearching(true);

    const result = await client.search({
      index,
      body: {
        query: {
          match: {
            [field]: {
              query: term,
              operator: 'or',
              fuzziness: 'auto',
            },
          },
        },
      },
    });

    setSearching(false);
    setHits(result.hits.hits);
  };

  return {
    onSearch,
    hits,
    searching,
  };
};

export default useSearch;
