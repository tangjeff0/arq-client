import { useQuery } from '@apollo/client';
import { useState } from 'react';
import { GET_PROBLEM_SEARCH_SETTINGS } from 'queries/searchkit';
import { GET_PROBLEMS_TAXONOMIES } from 'queries/problems';
import { PROBLEMS_QUERY_ROOT } from 'constants/gql';
import { problemSearchVar } from 'apollo/cache';
import queryString from 'query-string';

const useProblemSearch = () => {
  const { data: { problemSearch } } = useQuery(GET_PROBLEM_SEARCH_SETTINGS);

  const {
    loading: tagsLoading,
    error: tagsError,
    data: tagsData,
  } = useQuery(GET_PROBLEMS_TAXONOMIES);

  const [filter, setFilter] = useState('');

  const [filterOpen, setFilterOpen] = useState(false);

  const { filterSort: taxonomiesSort } = problemSearch;

  const setTaxonomiesSort = (value) => {
    problemSearchVar({
      ...problemSearch,
      filterSort: value,
    });
  };

  const { filterOperator } = problemSearch;

  const taxonomiesOperator = filterOperator || queryString.parse(window.location.search).taxonomiesOperator || 'AND';

  const setTaxonomiesOperator = (value) => {
    problemSearchVar({
      ...problemSearch,
      filterOperator: value,
    });
  };

  let include = '.*.*';

  if (tagsData && filter.length) {
    const tags = {};
    const problemSets = tagsData[PROBLEMS_QUERY_ROOT];
    problemSets.forEach((p) => {
      p.capabilities.forEach((c) => { tags[c.id] = c; });
    });

    include = Object
      .values(tags)
      .filter(tag => tag.term.toLowerCase().indexOf(filter.toLowerCase()) > -1)
      .map(tag => tag.term);
  }

  return {
    error: tagsError,
    loading: tagsLoading,
    setTaxonomiesOperator,
    taxonomiesOperator,
    setTaxonomiesSort,
    taxonomiesSort,
    filter,
    setFilter,
    include,
    filterOpen,
    setFilterOpen,
  };
};

export default useProblemSearch;
