import { useHistory } from 'react-router-dom';
import useUser from 'hooks/useUser.jsx';

const useAdminCheck = () => {
  const { user } = useUser();
  const history = useHistory();

  const isAdmin = !!user.admin;

  if (!isAdmin) {
    history.push('/');
  }

  return { isAdmin };
};

export default useAdminCheck;
