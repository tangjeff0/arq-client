import { useQuery } from '@apollo/client';
import { GET_CUSTOMER } from 'queries/customers';
import { CUSTOMER_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useCustomer = ({ id }) => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_CUSTOMER, {
    variables: { id },
    fetchPolicy: 'cache-and-network',
  });

  const customer = get(data, CUSTOMER_QUERY_ROOT, {});

  return {
    customer,
    error,
    loading,
  };
};

export default useCustomer;
