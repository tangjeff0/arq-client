import { useQuery } from '@apollo/client';
import { GET_SOURCE_TREE } from 'queries/sources';
import { SOURCES_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useSourceTree = (sourceName) => {
  const { loading, error, data } = useQuery(
    GET_SOURCE_TREE, { variables: { sourceName } },
  );

  const roots = get(data, [SOURCES_QUERY_ROOT, 0, 'roots'], []);

  return { loading, error, roots };
};

export default useSourceTree;
