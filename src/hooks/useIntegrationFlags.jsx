const useIntegrationFlags = () => ({ salesforceEnabled: SALESFORCE_ENABLED });

export default useIntegrationFlags;
