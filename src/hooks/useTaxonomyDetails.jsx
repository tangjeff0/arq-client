import { useQuery } from '@apollo/client';
import { GET_TAXONOMY_DETAILS } from 'queries/taxonomies';
import { TAXONOMY_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useTaxonomyDetails = ({ id, fetchPolicy }) => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_TAXONOMY_DETAILS, {
    variables: { id },
    fetchPolicy: fetchPolicy || 'cache-and-network',
  });

  const taxonomy = get(data, [TAXONOMY_QUERY_ROOT]);

  return {
    error,
    loading,
    taxonomy,
  };
};

export default useTaxonomyDetails;
