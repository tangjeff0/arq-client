import { get } from 'lodash';
import { useQuery } from '@apollo/client';
import { PACKAGE_QUERY_ROOT } from 'constants/gql';
import { GET_PACKAGE_COMMENTS } from 'queries/comments';

const usePackageCommentCount = (pkg) => {
  const {
    loading: commentCountLoading,
    error: commentCountError,
    data,
  } = useQuery(GET_PACKAGE_COMMENTS, {
    variables: { id: pkg.id },
  });

  const commentCount = get(data, [PACKAGE_QUERY_ROOT, 'comments'], []).length;

  return {
    commentCountLoading,
    commentCountError,
    commentCount,
  };
};

export default usePackageCommentCount;
