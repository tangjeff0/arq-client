'use strict';

const cp = require('child_process');
const merge = require('webpack-merge');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const VERSION = cp.execSync('git rev-parse HEAD').toString().replace(/(\r\n|\n|\r)/gm, '');
const BRANCH = cp.execSync('git rev-parse --abbrev-ref HEAD').toString().replace(/(\r\n|\n|\r)/gm, '');
const REPO_URL = cp.execSync('git config --get remote.origin.url').toString().replace(/(\r\n|\n|\r|.git)/gm, '');
const BRANCH_URL = `${REPO_URL}/tree/${BRANCH}`;
const COMMIT_URL = `${REPO_URL}/commit/${VERSION}`;

const testMode = process.env.NODE_ENV === 'test';

const commonConfig = {
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: 'babel-loader',
    },
    {
      test: /\.scss$/,
      use: testMode ? 'ignore-loader' : [
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader',
          options: {
            url: false,
          },
        },
        {
          loader: 'postcss-loader',
        },
        {
          loader: 'sass-loader',
          options: {
            outputStyle: 'expanded',
            includePaths: [
              path.resolve(__dirname, 'src/styles'),
            ],
          },
        },
      ],
    },
    {
      test: /\.(jpg|png|gif|svg|woff|woff2|ttf|otf|eot)$/,
      loader: 'file-loader',
    }],
  },
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles.css',
    }),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src/public'),
        to: path.resolve(__dirname, 'dist'),
      },
      {
        from: path.resolve(__dirname, 'node_modules/pptxgenjs/dist/pptxgen.bundle.js'),
        to: path.resolve(__dirname, 'src/public/vendor/pptxgenjs/pptxgen.bundle.js'),
      },
      {
        from: path.resolve(__dirname, 'node_modules/pptxgenjs/dist/pptxgen.bundle.js'),
        to: path.resolve(__dirname, 'dist/vendor/pptxgenjs/pptxgen.bundle.js'),
      },
    ]),
    new HtmlWebpackPlugin({
      filename: '_service/status/index.html',
      template: 'webpack/_status.html',
      content: createStatusFile(),
    }),
  ],
};

function createStatusFile() {
  return JSON.stringify({
    maintainer: process.env.MAINTAINER_EMAIL,
    name: process.env.MAINTAINER_NAME,
    status: 'GREEN',
    version: VERSION,
    branch: BRANCH,
    branchURL: BRANCH_URL,
    commitURL: COMMIT_URL,
    distelliBuildNum: process.env.DISTELLI_BUILDNUM,
    distelliRelBranch: process.env.DISTELLI_RELBRANCH,
    distelliRelRevision: process.env.DISTELLI_RELREVISION,
  }, undefined, 2);
}

const environmentConfig = (() => {
  switch (process.env.NODE_ENV) {
    case 'production':
    case 'staging':
      return require('./webpack/production.js');
    default:
      return require('./webpack/local-dev.js');
  }
})();

module.exports = merge(commonConfig, environmentConfig);
