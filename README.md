# ArQ Client

[![pipeline status](https://gitlab.com/inqtel/arq-client/badges/production/pipeline.svg)](https://gitlab.com/inqtel/arq-client/-/commits/production)
[![coverage report](https://gitlab.com/inqtel/arq-client/badges/production/coverage.svg)](https://gitlab.com/inqtel/arq-client/-/commits/production)




`arq-client` is the `React` frontend for the [ArQ](https://gitlab.com/inqtel/arq) project. It uses the following libraries to interact with the [arq-api](https://gitlab.com/inqtel/arq-api#developer-quickstart-mac-osx):
* `React-apollo` to send query and mutation requests to Hasura generated GraphQL API
* `graphql-tag` a template literal tag that parses GraphQL queries
* `ElasticSearch.JS` client to query ElasticSearch



<img src="https://gitlab.com/inqtel/arq-client/uploads/46bec214d136c8ca400846565a83b9b6/arq-client.png" width="500" alt="Project diagram">



## Developer Quickstart (Mac OSX)

### Requirements
- [Install Docker](https://docs.docker.com/docker-for-mac/install/)
- [Install Node 11 or above](https://nodejs.org/en/)
- [Add SSH Key to Github](https://help.github.com/en/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account)
- [Set up `arq-api` to establish the GraphQL and Search APIs](https://gitlab.com/inqtel/arq-api#developer-quickstart-mac-osx)

### Clone
Clone this project.


```console
[development root]#> cd arq-client
```

### Environment Variables
Copy and paste the following into .env file and save.
```console
API_HOST='http://localhost:8081'
API_URL='/api'
API_VERSION='/v1'
CLIENT_BASE_URL='http://localhost:8005'
COLOPHON='Copyright © 2020 · All Rights Reserved'
ES_HOST='http://localhost:9200'
ENV='local-dev'
NODE_ENV='development'
PITCHBOOK_URL='https://my.pitchbook.com/profile'
SALESFORCE_ENABLED=false
SOURCE_ARQ_CODE='Core'
SOURCE_ARQ_NAME='IQT'
ZENDESK_ENABLED=false
ADMIN_EMAILS='mcurie@example.com,efranklin@example.com'
```

For additional ENV values listed in .env.example, request from another ArQ developer.


### Install dependencies

```console
[project root]#> yarn install
```


### Run Client

Start a local development server in watch mode:

```console
[project root]#> yarn run dev:local
```

This compiles webpack and starts the lighttpd server. View on [http://localhost:8005](http://localhost:8005)


## Development Tools

### Storybook

Gitlab CI deploys a static-storybook in [arq-client's Gitlab Pages](https://inqtel.gitlab.io/arq-client/) on merge to production.


```console
[project root]#> yarn run storybook
```

View on [http://localhost:9001](http://localhost:9001)



### Tests

[jest](https://jestjs.io/docs/en/api) component tests are run in the Gitlab CI testing pipeline.
All tests must pass and coverage should stay above <strong>35%</strong> before code can be merged in.

```console
#> yarn test
```


### Code Linting

Code linting is handled with `eslint`, using the Airbnb JS/JSX style. Rules modifications are in `.eslintrc`. Gitlab CI runs a lint-js job on every push to an unprotected branch. There must be `0` lint errors to merge code in.

```console
#> yarn lint
```


## Alternate Developer Quickstart with Docker

An alternate way to run the client is with Docker using `docker-compose`.

Ensure the following variables are defined in your .env file

```console
INTERNAL_CA_SCRIPT_URL=
MAINTAINER_NAME=
MAINTAINER_EMAIL=
```

### Build the container
```console
#> docker-compose -f docker-compose.yml -f docker-compose.development.yml build
```

### Start the container

```console
#> docker-compose -f docker-compose.yml -f docker-compose.development.yml up
```

View on [http://localhost:8005](http://localhost:8005).

### Interact with Client

```console
# Run bash inside container
># docker exec -it arq-client_web_1 /bin/bash
```



## Deployment

#### Deploying Staging / Production with Docker

When deploying to a server, use the commands below with the appropriate environment variables.

```console
#> docker build --build-arg NODE_ENV=staging -t arq_client_image .
#> docker run -id -p 8005:8005 --name=arq_client -e NODE_ENV="staging" arq_client_image
```

The site will run on port 8005.

For local development omit "NODE_ENV" settings.

For hosted servers choose NODE_ENV="development|staging|production".



## Environment variables

Environment variables are defined in GitLab Settings > CI/CD > Variables. New environment variables should be added to the repo in `generate-env.sh`, the local and production webpack configs, and in `.eslintrc`.

## Prod and Stage Notes

API and Client run on same domain, but different servers managed by our SSO proxy.

## Health Checks

Health checks are located at `/_service/status` and displays the commit hash fo the release. Be sure to compare the hash against the expected branch hash when deploying.

## Contributing

Thanks for your interest in contributing to ArQ! Get started [here](https://gitlab.com/inqtel/arq-client/-/blob/production/CONTRIBUTING.md).

## Bidirectional Syncing

Contributing your code to ArQ's open source `production` branches will make its way to our internal `production` branches, and vice versa, for those contributing from the inside. Learn more about our bidirectional syncing process [here](https://gitlab.com/inqtel/arq-client/-/blob/production/BIDIRECTIONAL-SYNCING.md).
