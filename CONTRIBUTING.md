# Welcome

Thank you for considering contributing to ArQ.

We created these guidelines to make contributing as easy as possible.

There are many ways to contribute, whether it's shooting us a note to tell us how you are using the ArQ tool, linking to our repository in your tutorials and blog posts, submitting bugs or feature requests, improving documentation, or adding your code to the ArQ codebase.

Any of these will give us some warm fuzzies.


# Ground Rules
* Be respectful. See [Contributor Covenant Code of Conduct](https://www.contributor-covenant.org/version/2/0/code_of_conduct/).
* Ensure that new code meets these requirements:
  * Follows the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
  * Client successfully starts via [`docker`](https://gitlab.com/inqtel/arq-client#with-docker-using-docker-compose) and [`yarn`](https://gitlab.com/inqtel/arq-client#run-client)
  * Jobs Pipeline successful
  * No console errors introduced
  * New components accompanied by stories in Storybook
  * Merge request [links](https://about.gitlab.com/blog/2016/03/08/gitlab-tutorial-its-all-connected/) to issue it's resolving.


# Getting started

To begin, [set up your SSH keys](https://www.youtube.com/watch?v=0z28J0RfaJM) and [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) the ArQ [client](https://gitlab.com/inqtel/arq-client) and [API](https://gitlab.com/inqtel/arq-client) repositories.

Follow the Developer Quickstart to start both [client](https://gitlab.com/inqtel/arq-client#developer-quickstart-mac-osx) and [API](https://gitlab.com/inqtel/arq-api#developer-quickstart-mac-osx).

Check out the [issues](https://gitlab.com/inqtel/arq-client/-/issues) we've listed for you and choose one to solve. [Create a new branch off of the issue](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch-from-an-issue) and check it out locally.

Write code.

After you ensure your new code meets our requirements above, create a [merge request](https://www.youtube.com/watch?v=Ddd3dbl4-2w) and assign to a maintainer. A maintainer will get back to you within a day or two, unless it's a holiday or weekend.

# Frameworks, patterns, and code organization

Read below for notes on the frameworks used, the patterns and code organization practices we'd like to follow, and some ongoing projects.

## Redux vs. Apollo cache

This client was first built to interact with a REST API, and local data and state was managed exclusively through Redux. Once the API supported GraphQL, we began using the [Apollo GraphQL Client](https://www.apollographql.com/docs/react/), and letting the Apollo cache handle local data and state in new pages and components. Advantages of the Apollo cache compared to Redux include faster development thanks to reduced boilerplate and automatic data normalization.

Over time, we have migrated several old pages and components from Redux to the Apollo cache. At this point, the repo still uses both Redux and Apollo for local data and state management. Going forward, however, work should be done using the Apollo cache. Over time we would like to migrate away from Redux entirely.

## ID vs. QID

As part of the database migration from Neo4J to Postgres, the primary key for database entities changed from `qid` to `id`. This change is complete on the client side from a functional perspective, but the nomenclature has not be updated everywhere. In many places variables still refer to, for example, `packageQID`. Going forward, `qid` should be considered deprecated, in favor of `id`, to reduce confusion and make object destructuring more straightforward.

`const { id } = architecture`

instead of

`const { qid: id } = architecture`

Over time we'd like to eliminate `qid` in variable names as a reference to a database key.

## Pages vs. components

In general, the `src/pages` directory should be used for distinct pages, and the `src/components` directory should be used for _reusable_ components.

## Hooks and Interfaces

We are using the hook, interface, component pattern outlined [here](https://medium.com/reactbrasil/pure-components-interfaces-and-logic-hooks-84c9ae4d8805) to keep our components modular and reusable. In general, we're striving to minimize use of full `React.Component` components and lifecycle methods, and maximize the use of function components and hooks. Having `<Interface />` components between the two keeps things tidy.

## Where do my hooks go?

Hooks that are shared between components, or very likely to be reused, should be placed in the `src/hooks` directory. Custom hooks that are built for a specific page or component can go directly in that page or component's directory.
