const webpack = require('webpack');
const dotenv = require('dotenv').config();

module.exports = {
  mode: 'production',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(dotenv.parsed.NODE_ENV),
      },
      API_HOST: JSON.stringify(dotenv.parsed.API_HOST),
      API_URL: JSON.stringify(dotenv.parsed.API_URL),
      API_VERSION: JSON.stringify(dotenv.parsed.API_VERSION),
      CLIENT_BASE_URL: JSON.stringify(dotenv.parsed.CLIENT_BASE_URL),
      ES_HOST: JSON.stringify(dotenv.parsed.ES_HOST),
      ENV: JSON.stringify(dotenv.parsed.ENV),
      COLOPHON: JSON.stringify(dotenv.parsed.COLOPHON),
      PITCHBOOK_URL: JSON.stringify(dotenv.parsed.PITCHBOOK_URL),
      SALESFORCE_ENABLED: JSON.stringify(dotenv.parsed.SALESFORCE_ENABLED),
      SALESFORCE_URL: JSON.stringify(dotenv.parsed.SALESFORCE_URL),
      SOURCE_ARQ_CODE: JSON.stringify(dotenv.parsed.SOURCE_ARQ_CODE),
      SOURCE_ARQ_NAME: JSON.stringify(dotenv.parsed.SOURCE_ARQ_NAME),
      ZENDESK_ARQ_SECTION: JSON.stringify(dotenv.parsed.ZENDESK_ARQ_SECTION),
      ZENDESK_ENABLED: dotenv.parsed.ZENDESK_ENABLED === 'true',
      ZENDESK_HOST: JSON.stringify(dotenv.parsed.ZENDESK_HOST),
      ZENDESK_SEARCH_TERMS_ARTICLE: JSON.stringify(dotenv.parsed.ZENDESK_SEARCH_TERMS_ARTICLE),
      TECH_TAGS_ARCHITECTURE_ID: JSON.stringify(dotenv.parsed.TECH_TAGS_ARCHITECTURE_ID),
    }),
  ],
};
